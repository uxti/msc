/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CotacaoEJB;
import com.bcr.controller.FornecedorEJB;
import com.bcr.controller.ProdutoEJB;
import com.bcr.controller.RequisicaoMaterialItemEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Cotacao;
import com.bcr.model.CotacaoItem;
import com.bcr.model.CotacaoItemFornecedor;
import com.bcr.model.Fornecedor;
import com.bcr.model.Grupo;
import com.bcr.model.Produto;
import com.bcr.model.RequisicaoMaterialItem;
import com.bcr.pojo.CotacaoImpressao;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import com.bcr.util.RelatorioFactory;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.component.tabview.Tab;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class CotacaoMB {

    @EJB
    CotacaoEJB cEJB;
    private Cotacao cotacao;
    private CotacaoItem cotacaoItem;
    private List<CotacaoItem> listaTemporariaCotacao;
    private List<Cotacao> cotacoesTemporarias;
    private List<RequisicaoMaterialItem> listaProdutoTempoSelecionar;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    FornecedorEJB fEJB;
    private Fornecedor fornecedor;
    @EJB
    ProdutoEJB pEJB;
    @EJB
    RequisicaoMaterialItemEJB rmiEJB;
    private Fornecedor fornecedorToSelect;

    public CotacaoMB() {
        novo();
    }

    public void novo() {
        cotacao = new Cotacao();
        listaTemporariaCotacao = new ArrayList<CotacaoItem>();
        cotacao.setStatus("Andamento");
        cotacao.setDtCadastro(new Date());
        cotacao.setValortotal(new BigDecimal(BigInteger.ZERO));
        cotacaoItem = new CotacaoItem();
        listaProdutoTempoSelecionar = new ArrayList<RequisicaoMaterialItem>();
        cotacoesTemporarias = new ArrayList<Cotacao>();
        fornecedorToSelect = new Fornecedor();
    }

    @PostConstruct
    public void preCarregar() {
        cotacao.setIdUsuario(uEJB.pegaUsuarioLogadoNaSessao());
    }

    public List<Cotacao> listaDeCotacoesAbertas() {
        return cEJB.listaCotacoesNaoDeLote();
    }

    public List<Cotacao> listaDeCotacoesEmLote() {
        return cEJB.listaCotacoesDeLote();
    }

    public void adicionarProdutoCotacao() {
        listaTemporariaCotacao.add(cotacaoItem);
        cotacaoItem = new CotacaoItem();
        cotacaoItem.setIdEmpresa(uEJB.pegaEmpresaLogadoNaSessao());
        cotacao.setCotacaoItemList(listaTemporariaCotacao);
    }

    public String statusCotacaoLote(String s) {
        try {
            if (s.equals("Cotando")) {
                return "label label-default";
            } else if (s.equals("Aguardando Pedido")) {
                return "label label-warning";
            } else if (s.equals("Aguardando Chegada")) {
                return "label label-success";
            } else {
                return "label label-primary";
            }
        } catch (Exception e) {
            return "";
        }
    }

    public Boolean verificarSeLoteEstaDividido(String lote) {
        Boolean situacao = false;
        int repeticoes = 0;
        for (Cotacao c : listaDeCotacoesEmLote()) {
            if (c.getLote().equals(lote)) {
                repeticoes++;
            }
        }
        if (repeticoes > 1) {
            situacao = true;
        } else {
            situacao = false;
        }
        return situacao;
    }

    public void calcularTotalDoItem(CotacaoItemFornecedor cif) {
        cif.setValorTotal(cif.getQuantidade().multiply(cif.getValorCotado()));
    }

    public void excluirProdutoCotacao(CotacaoItem item, int index, List<CotacaoItem> itens) {
        if (item.getIdCotacaoItem() != null) {
            cEJB.excluirCotacaoItem(item);
        }
        itens.remove(index);
        if (listaCotacaoGerar.indexOf(itens) > 0) {
            if (itens.size() < 1) {
                listaCotacaoGerar.remove(itens);
            }
        }
        cotacao.setValortotal(new BigDecimal(BigInteger.ZERO));
        BigDecimal d = new BigDecimal(BigInteger.ZERO);
        for (CotacaoItem aaa : cotacao.getCotacaoItemList()) {
            if (aaa.getValorTotal() != null) {
                d = d.add(aaa.getValorTotal().add(cotacao.getValortotal()));
            }
        }
        cotacao.setValortotal(d);
//        int ii = 0;
//        int yy = 0;
//        if (listaCotacaoGerar.size() > 0) {
//            for (List<CotacaoItem> cis : listaCotacaoGerar) {
//                for (CotacaoItem ci : cis) {
//                    if (ci == item) {
//                        break;
//                    } else {
//                        ii++;
//                    }
//                }
//                yy++;
//            }
//            listaCotacaoGerar.get(yy).remove(ii);
//        }


    }

    public void excluirProdutoCotacaoEdicao(CotacaoItem item, int index, List<CotacaoItem> list, Cotacao c) {
        if (item.getIdCotacaoItem() != null) {
            cEJB.excluirCotacaoItem(item);
        }
        list.remove(index);
        c.setValortotal(new BigDecimal(BigInteger.ZERO));
        BigDecimal d = new BigDecimal(BigInteger.ZERO);
        for (CotacaoItem aaa : c.getCotacaoItemList()) {
            if (aaa.getValorTotal() != null) {
                d = d.add(aaa.getValorTotal().add(c.getValortotal()));
            }
        }
        c.setValortotal(d);
    }

    public void excluirCotacaoFornecedor(CotacaoItem ci, CotacaoItemFornecedor cif, int index) {
        if (cif.getIDCotacaoItemFornecedor() != null) {
            cEJB.excluirCotacaoFornecedor(cif);
        }
        ci.getCotacaoItemFornecedorList().remove(index);
        try {
            if (!cotacao.getCotacaoItemList().isEmpty()) {
                cotacao.setValortotal(new BigDecimal(BigInteger.ZERO));
                BigDecimal d = new BigDecimal(BigInteger.ZERO);
                for (CotacaoItem aaa : cotacao.getCotacaoItemList()) {
                    if (aaa.getValorTotal() != null) {
                        d = d.add(aaa.getValorTotal().add(cotacao.getValortotal()));
                    }
                }
                cotacao.setValortotal(d);
            }
        } catch (Exception e) {
        }

    }

    public void selecionarFornecedorCotado(CotacaoItemFornecedor cif, CotacaoItem ci, int index) {
        for (CotacaoItemFornecedor aaa : ci.getCotacaoItemFornecedorList()) {
            aaa.setStatus("N");
        }
        ci.setIdFornecedorSelecionado(cif.getIdFornecedor());
        ci.setQuantidade(cif.getQuantidade());
        ci.setValorCotado(cif.getValorCotado());
        ci.setValorTotal(cif.getValorTotal());
        cif.setStatus("S");
        try {
            if (!cotacao.getCotacaoItemList().isEmpty()) {
                cotacao.setValortotal(new BigDecimal(BigInteger.ZERO));
                BigDecimal d = new BigDecimal(BigInteger.ZERO);
                for (CotacaoItem aaa : cotacao.getCotacaoItemList()) {
                    if (aaa.getValorTotal() != null) {
                        d = d.add(aaa.getValorTotal().add(cotacao.getValortotal()));
                    }
                }
                cotacao.setValortotal(d);
            }
        } catch (Exception e) {
        }


        RequestContext rc = RequestContext.getCurrentInstance();
        rc.update("tabs:frmCotas:rpt:" + index + ":tblCotacaoProduto");
        rc.update("tabs:frmCotas2:rpt:" + index + ":tblCotacaoProduto");
    }

    public void adicionarCotacaoFornecedor(CotacaoItem cotacaoItem, List<CotacaoItemFornecedor> cifs) {
        CotacaoItemFornecedor cif = new CotacaoItemFornecedor();
        cif.setQuantidade(cotacaoItem.getQuantidade());
        cif.setStatus("N");
        if (cifs == null || cifs.size() == 0) {
            if (cotacaoItem.getIdProduto().getIdFornecedorUltimo() != null) {
                cif.setIdFornecedor(cotacaoItem.getIdProduto().getIdFornecedorUltimo());
            } else {
                cif.setIdFornecedor(null);
            }
            if (cotacaoItem.getIdProduto().getPrecoCusto() != null) {
                cif.setValorCotado(cotacaoItem.getIdProduto().getPrecoCusto());
                cif.setValorTotal(cif.getQuantidade().multiply(cif.getValorCotado()));
            } else {
                cif.setValorCotado(BigDecimal.ZERO);
                cif.setValorTotal(BigDecimal.ZERO);
            }
        } else {
            cif.setValorCotado(BigDecimal.ZERO);
            cif.setIdFornecedor(null);
            cif.setValorTotal(BigDecimal.ZERO);
        }
        try {
            cotacaoItem.getCotacaoItemFornecedorList().add(cif);
        } catch (Exception e) {
            cotacaoItem.setCotacaoItemFornecedorList(new ArrayList<CotacaoItemFornecedor>());
            cotacaoItem.getCotacaoItemFornecedorList().add(cif);
        }
    }

    public void setarFornecedorPorGrupo(Fornecedor f, List<CotacaoItem> itns) {
        for (CotacaoItem ci : itns) {
            ci.setIdFornecedorSelecionado(f);
        }
        System.out.println("chamou a lista com " + itns.size() + " de registros");
    }

    public void calcularTotalCotacaoItem(CotacaoItem ci) {
        if (ci.getQuantidade() == null) {
            ci.setQuantidade(BigDecimal.ZERO);
        }
        if (ci.getValorCotado() == null) {
            ci.setValorCotado(BigDecimal.ZERO);
        }
        ci.setValorTotal(ci.getQuantidade().multiply(ci.getValorCotado()));
    }

    public void imprimirCotacao(List<CotacaoItem> list) {
        List<CotacaoImpressao> listaCotaImp = new ArrayList<CotacaoImpressao>();
        for (CotacaoItem ci : list) {
            CotacaoImpressao cotaImp = new CotacaoImpressao();
            cotaImp.setProduto(ci.getIdProduto().getDescricao());
            try {
                cotaImp.setUnidade(ci.getIdProduto().getIdUnidadeSaida().getDescricaoResumida());
            } catch (Exception e) {
                cotaImp.setUnidade(ci.getIdProduto().getIdUnidadeEntrada().getDescricaoResumida());
            }

            cotaImp.setQuantidade(ci.getQuantidade());
            if (ci.getValorCotado() != null) {
                cotaImp.setValor_unitario(ci.getValorCotado());
            } else {
                cotaImp.setValor_unitario(null);
            }
            cotaImp.setValor_total(ci.getValorTotal());
            listaCotaImp.add(cotaImp);
        }
        RelatorioFactory.relatorioListNoParametro("/WEB-INF/Relatorios/Cotacao/Relatorio_Cotacao_Produtos.jasper", listaCotaImp);
    }

    public List<RequisicaoMaterialItem> retornaItensPendentes() {
        return rmiEJB.listaProdutosPendentesCotacao();
    }
    private List<List<CotacaoItem>> listaCotacaoGerar = new ArrayList<List<CotacaoItem>>();

    class OrdernadorDeProdutos implements Comparator<RequisicaoMaterialItem> {

        @Override
        public int compare(RequisicaoMaterialItem e1, RequisicaoMaterialItem e2) {
            if (e1.getIdProduto().getIdProduto() == e2.getIdProduto().getIdProduto()) {
                return 0;
            } else if (e1.getIdProduto().getIdProduto() > e2.getIdProduto().getIdProduto()) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    public String verificarStringNula(String s) {
        if (s == null || s.equals("")) {
            return "";
        } else {
            return s + ",";
        }
    }

    public void importarProdutosPendentesParaCotacao() {
        try {

            int i = 0;
            cotacao = new Cotacao();
            cotacao.setCotacaoItemList(new ArrayList<CotacaoItem>());
            List<CotacaoItem> itens = new ArrayList<CotacaoItem>();
            List<CotacaoItem> itensTemp = new ArrayList<CotacaoItem>();
            listaCotacaoGerar = new ArrayList<List<CotacaoItem>>();
            Collections.sort(listaProdutoTempoSelecionar, new OrdernadorDeProdutos());
            List<Produto> produtosUsados = new ArrayList<Produto>();
            for (RequisicaoMaterialItem rmi : listaProdutoTempoSelecionar) {
                CotacaoItem ci = new CotacaoItem();
                ci.setIdProduto(rmi.getIdProduto());
                ci.setIdEmpresa(rmi.getIdRequisicaoMaterial().getIdEmpresa());
                if (ci.getIdProduto().getPrecoCustoAnterior() == null) {
                    ci.getIdProduto().setPrecoUltimaCompra(BigDecimal.ZERO);
                    pEJB.Atualizar(ci.getIdProduto());
                    ci.setValorCotado(BigDecimal.ZERO);
                } else {
                    ci.setValorCotado(ci.getIdProduto().getPrecoCustoAnterior());
                }
                if (rmi.getIdProduto().getQuantidadeReservada() == null) {
                    rmi.getIdProduto().setQuantidadeReservada(BigDecimal.ZERO);
                }
                if (rmi.getIdProduto().getQuantidade() == null) {
                    rmi.getIdProduto().setQuantidade(BigDecimal.ZERO);
                }
                if (rmi.getQuantidadeAprovada() == null) {
                    rmi.setQuantidadeAprovada(BigDecimal.ZERO);
                }
                ci.setQuantidade(rmi.getQuantidadeCotada());
                if (ci.getQuantidade().longValue() < 0) {
                    ci.setQuantidade(new BigDecimal(Math.abs(ci.getQuantidade().longValue())));
                }
                ci.setRequisicoesReferentes(verificarStringNula(ci.getRequisicoesReferentes()) + rmi.getIdRequisicaoMaterialItem());
                if (itens.isEmpty()) {
                    itens.add(ci);
                    produtosUsados.add(ci.getIdProduto());
                } else {
                    if (produtosUsados.indexOf(ci.getIdProduto()) >= 0) {
                        for (CotacaoItem cii : itens) {
                            if (cii.getIdProduto().equals(ci.getIdProduto())) {
                                cii.setRequisicoesReferentes(cii.getRequisicoesReferentes() + " , " + rmi.getIdRequisicaoMaterialItem());
                                cii.setQuantidade(cii.getQuantidade().add(ci.getQuantidade()));
                            }
                        }
                    } else {
                        itens.add(ci);
                        produtosUsados.add(ci.getIdProduto());
                    }
                }
            }
            cotacao.setCotacaoItemList(itens);


            List<CotacaoItem> lista = new ArrayList<CotacaoItem>();
            List<Grupo> ls = new ArrayList<Grupo>();

            for (CotacaoItem cotaItem : cotacao.getCotacaoItemList()) {
                lista = new ArrayList<CotacaoItem>();
                Grupo p = cotaItem.getIdProduto().getIdGrupo(); //Sabao

                if (ls.indexOf(p) <= -1) {
                    for (CotacaoItem cotai : cotacao.getCotacaoItemList()) {
                        if (cotai.getIdProduto().getIdGrupo().equals(p)) {
                            System.out.println("Produto " + cotai.getIdProduto().getDescricao());
                            lista.add(cotai);
                            ls.add(p);
                        }
                    }
                    listaCotacaoGerar.add(lista);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public BigDecimal retornaNumeroNegativoParaPositivo(BigDecimal qnt) {
        long l;
        if (qnt.longValue() < 0) {
            l = Math.abs((qnt).longValue());
        } else {
            l = qnt.longValue();
        }
        return new BigDecimal(l);
    }

    public String gerarLote(String num) {
        DateFormat df = new SimpleDateFormat("ddMMyy");
        Date d = new Date();
        return "LOT_" + df.format(d) + "_" + num;
    }

    public void salvarCotacoesEmLote() throws SQLException {
        String ultimoLote = cEJB.retornaUltimoLote(gerarLote(""));
        String lote = gerarLote(ultimoLote);
        Long lotNumero = new Date().getTime();
        Date d = new Date();
        try {
            for (List<CotacaoItem> list : listaCotacaoGerar) {
                Cotacao c = new Cotacao();
                c.setDtCadastro(new Date());
                c.setIdUsuario(uEJB.pegaUsuarioLogadoNaSessao());
                c.setLote(lote);
                c.setDtCadastro(d);
                c.setCotacaoItemList(list);
                for (CotacaoItem ci : c.getCotacaoItemList()) {
                    if (ci.getIdFornecedorSelecionado() == null) {
                        c.setStatus("Cotando");
                        break;
                    } else {
                        c.setStatus("Aguardando Pedido");
                    }
                }
                if (!list.isEmpty()) {
                    cEJB.finalizarCotacao(c);
                }

            }
            MensageFactory.info("Cotação salva com sucesso!!", "");
            novo();
        } catch (Exception e) {
            MensageFactory.error("Não foi possivel salvar a cotação!", "");
            Bcrutils.escreveLogErro(e);
        }

    }

    public void salvarCotacoesEmLoteEdicao() {
        try {
            for (Cotacao c : cotacoesTemporarias) {
                for (CotacaoItem ci : c.getCotacaoItemList()) {
                    for (CotacaoItemFornecedor cif : ci.getCotacaoItemFornecedorList()) {
                        if (cif.getStatus().equals("S")) {
                            ci.setIdFornecedorSelecionado(cif.getIdFornecedor());
                        }
                    }
                }
                cEJB.finalizarCotacaoNormal(c);
            }
            MensageFactory.info("Lote de cotação salvo com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possivel salvar o lote!", null);
            Bcrutils.escreveLogErro(e);
        }

    }

    public boolean verificaExisteGrupo(List<CotacaoItem> lista, Produto p) {
        boolean retorno = false;
        for (CotacaoItem ci : lista) {
            if (ci.getIdProduto().equals(p)) {
                retorno = true;
            }
        }
        return retorno;
    }

    public List<Cotacao> listarCotacoesPorLote(String lote, String status) {
        return cEJB.listarCotacoesPorLote(lote, status);
    }

    public void carregarCotacoesPorLote(String lote, String status) {
        cotacoesTemporarias = cEJB.listarCotacoesPorLote(lote, status);
        List<CotacaoItem> cois = new ArrayList<CotacaoItem>();
        List<List<CotacaoItem>> coiis = new ArrayList<List<CotacaoItem>>();
        for (Cotacao c : cotacoesTemporarias) {
            for (CotacaoItem ci : c.getCotacaoItemList()) {
                cois.add(ci);
            }
            coiis.add(cois);
        }
        listaCotacaoGerar = coiis;
    }

    public void salvarCotacao() {
        cEJB.finalizarCotacaoNormal(cotacao);
        MensageFactory.info("Informação! ", "Cotação salva com sucesso!");
    }

    public Cotacao getCotacao() {
        return cotacao;
    }

    public void setCotacao(Cotacao cotacao) {
        this.cotacao = cotacao;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public CotacaoItem getCotacaoItem() {
        return cotacaoItem;
    }

    public void setCotacaoItem(CotacaoItem cotacaoItem) {
        this.cotacaoItem = cotacaoItem;
    }

    public List<CotacaoItem> getListaTemporariaCotacao() {
        return listaTemporariaCotacao;
    }

    public void setListaTemporariaCotacao(List<CotacaoItem> listaTemporariaCotacao) {
        this.listaTemporariaCotacao = listaTemporariaCotacao;
    }

    public List<RequisicaoMaterialItem> getListaProdutoTempoSelecionar() {
        return listaProdutoTempoSelecionar;
    }

    public void setListaProdutoTempoSelecionar(List<RequisicaoMaterialItem> listaProdutoTempoSelecionar) {
        this.listaProdutoTempoSelecionar = listaProdutoTempoSelecionar;
    }

    public List<List<CotacaoItem>> getListaCotacaoGerar() {
        return listaCotacaoGerar;
    }

    public void setListaCotacaoGerar(List<List<CotacaoItem>> listaCotacaoGerar) {
        this.listaCotacaoGerar = listaCotacaoGerar;
    }

    public List<Cotacao> getCotacoesTemporarias() {
        return cotacoesTemporarias;
    }

    public Fornecedor getFornecedorToSelect() {
        return fornecedorToSelect;
    }

    public void setFornecedorToSelect(Fornecedor fornecedorToSelect) {
        this.fornecedorToSelect = fornecedorToSelect;
    }

    public void setCotacoesTemporarias(List<Cotacao> cotacoesTemporarias) {
        this.cotacoesTemporarias = cotacoesTemporarias;
    }
}
