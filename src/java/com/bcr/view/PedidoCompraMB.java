/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CotacaoEJB;
import com.bcr.controller.PedidoCompraEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.ContasPagar;
import com.bcr.model.Cotacao;
import com.bcr.model.CotacaoItem;
import com.bcr.model.Fornecedor;
import com.bcr.model.PedidoCompra;
import com.bcr.model.PedidoCompraItem;
import com.bcr.pojo.PedidoCompraPojo;
import com.bcr.pojo.PedidoCompraPre;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import com.bcr.util.RelatorioFactory;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.Query;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class PedidoCompraMB {

    @EJB
    CotacaoEJB cEJB;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    PedidoCompraEJB pcEJB;
    private List<Cotacao> cotacoesImportadas;
    private List<PedidoCompra> pedidosCompra;
    private List<PedidoCompra> listaDePedidos = new ArrayList<PedidoCompra>();
    private PedidoCompra pedidoCompra;
    private PedidoCompraItem pedidoCompraItem;

    public PedidoCompraMB() {
        novo();
    }

    public void novo() {
        cotacoesImportadas = new ArrayList<Cotacao>();
        pedidosCompra = new ArrayList<PedidoCompra>();
        listaDePedidos = new ArrayList<PedidoCompra>();
        pedidoCompra = new PedidoCompra();
        pedidoCompra.setDtEmissao(new Date());
        pedidoCompraItem = new PedidoCompraItem();
        pedidoCompra.setMovimentaEstoque("N");
    }

    public boolean podeGerarPedidoCompra(List<Cotacao> cotacoes) {
        boolean t = false;
        for (Cotacao c : cotacoes) {
            if (c.getStatus().equals("Aguardando Pedido")) {
                t = true;
            }
        }
        return t;
    }

    public void gerarPedidoCompra2(Cotacao cotacao) {
        System.out.println("chama essa disgraça");
        novo();
        List<PedidoCompraPojo> itens = new ArrayList<PedidoCompraPojo>();
        for (CotacaoItem ci : cotacao.getCotacaoItemList()) {
            PedidoCompraPojo pcj = new PedidoCompraPojo();
            pcj.setFornecedor(ci.getIdFornecedorSelecionado());
            pcj.setProduto(ci.getIdProduto());
            pcj.setQuantidade(ci.getQuantidade());
            pcj.setVlrUnitario(ci.getValorCotado());
            pcj.setVlrTotal(ci.getValorTotal());
            pcj.setCotacoesReferentes(ci.getRequisicoesReferentes());
            pcj.setCotacaoItem(ci);
//                pcj.setLoteReferente(lote);
            itens.add(pcj);
        }
        List<Fornecedor> fornecedorerUsados = new ArrayList<Fornecedor>();
        for (PedidoCompraPojo ppcj : itens) {
            if (fornecedorerUsados.indexOf(ppcj.getFornecedor()) == -1) {
                fornecedorerUsados.add(ppcj.getFornecedor());
                Fornecedor f = ppcj.getFornecedor();
                PedidoCompra pc = new PedidoCompra();
                pc.setDtEmissao(new Date());
                pc.setIdEmpresa(uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa());
                pc.setIdFornecedor(f);
                pc.setStatus("Aguardando Chegada");
//                pc.setLoteReferente(lote);
                List<PedidoCompraItem> intesPedido = new ArrayList<PedidoCompraItem>();
                for (PedidoCompraPojo i : itens) {
                    if (i.getFornecedor().equals(f)) {
                        PedidoCompraItem pci = new PedidoCompraItem();
                        pci.setIdProduto(i.getProduto());
                        pci.setQtdSolicitada(i.getQuantidade());
                        pci.setValorUnitario(i.getVlrUnitario());
                        pci.setValorTotal(i.getVlrTotal());
                        if (pci.getDescricao() == null) {
                            pci.setDescricao(i.getCotacoesReferentes());
                        } else {
                            pci.setDescricao(pci.getDescricao() + "," + i.getCotacoesReferentes());
                        }
                        pci.setIdCotacaoItem(i.getCotacaoItem());
                        if (pc.getValorTotal() == null) {
                            pc.setValorTotal(BigDecimal.ZERO);
                        }
                        pc.setValorTotal(pc.getValorTotal().add(pci.getValorTotal()));
                        intesPedido.add(pci);
                    }
                }
                pc.setPedidoCompraItemList(intesPedido);
                listaDePedidos.add(pc);
            }
        }
        System.out.println(listaDePedidos.size() + "hue.com.br");
    }

    public void gerarPedidoCompra(String lote, String status) {
        novo();
        cotacoesImportadas = cEJB.listarCotacoesPorLote(lote, status);
        List<PedidoCompraPojo> itens = new ArrayList<PedidoCompraPojo>();
        for (Cotacao c : cotacoesImportadas) {
            for (CotacaoItem ci : c.getCotacaoItemList()) {
                PedidoCompraPojo pcj = new PedidoCompraPojo();
                pcj.setFornecedor(ci.getIdFornecedorSelecionado());
                pcj.setProduto(ci.getIdProduto());
                pcj.setQuantidade(ci.getQuantidade());
                pcj.setVlrUnitario(ci.getValorCotado());
                pcj.setVlrTotal(ci.getValorTotal());
                pcj.setCotacoesReferentes(ci.getRequisicoesReferentes());
                pcj.setCotacaoItem(ci);
                pcj.setLoteReferente(lote);
                itens.add(pcj);
            }
        }
        List<Fornecedor> fornecedorerUsados = new ArrayList<Fornecedor>();

        for (PedidoCompraPojo ppcj : itens) {
            if (fornecedorerUsados.indexOf(ppcj.getFornecedor()) == -1) {
                fornecedorerUsados.add(ppcj.getFornecedor());
                Fornecedor f = ppcj.getFornecedor();
                PedidoCompra pc = new PedidoCompra();
                pc.setDtEmissao(new Date());
                pc.setIdEmpresa(uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa());
                pc.setIdFornecedor(f);
                pc.setStatus("Aguardando Chegada");
                pc.setLoteReferente(lote);
                List<PedidoCompraItem> intesPedido = new ArrayList<PedidoCompraItem>();
                for (PedidoCompraPojo i : itens) {
                    if (i.getFornecedor().equals(f)) {
                        PedidoCompraItem pci = new PedidoCompraItem();
                        pci.setIdProduto(i.getProduto());
                        pci.setQtdSolicitada(i.getQuantidade());
                        pci.setValorUnitario(i.getVlrUnitario());
                        pci.setValorTotal(i.getVlrTotal());
                        if (pci.getDescricao() == null) {
                            pci.setDescricao(i.getCotacoesReferentes());
                        } else {
                            pci.setDescricao(pci.getDescricao() + "," + i.getCotacoesReferentes());
                        }
                        pci.setIdCotacaoItem(i.getCotacaoItem());
                        if (pc.getValorTotal() == null) {
                            pc.setValorTotal(BigDecimal.ZERO);
                        }
                        pc.setValorTotal(pc.getValorTotal().add(pci.getValorTotal()));
                        intesPedido.add(pci);
                    }
                }
                pc.setPedidoCompraItemList(intesPedido);
                listaDePedidos.add(pc);
            }
        }
    }

    public void salvarPedidoCompra() {
        try {
            for (PedidoCompra pc : listaDePedidos) {
                pcEJB.atualizarStatusCotacao(pc.getLoteReferente());
                try {
                    pcEJB.SalvarPedido(pc);
                } catch (EJBException e) {
                    Bcrutils.descobreErroDeEJBException(e);
                }
            }

            MensageFactory.info("Pedidos foram salvos com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possivel salvar os pedidos! Verifique o log!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void salvarPedidoAvulso() {
        try {
            pedidoCompra.setIdEmpresa(uEJB.pegaEmpresaLogadoNaSessao());
            pedidoCompra.setStatus("Aguardando Chegada");
            pcEJB.SalvarPedidoAvulso(pedidoCompra);
            MensageFactory.info("Pedido salvo com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possivel salvar o pedido! Verifique o log!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void salvarPedidoAnalise() {
        try {
            pedidoCompra.setIdEmpresa(uEJB.pegaEmpresaLogadoNaSessao());
            pedidoCompra.setStatus("Analisado");
            pcEJB.SalvarPedidoAvulso(pedidoCompra);

            MensageFactory.info("Pedido analisado com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possivel analisar o pedido! Verifique o log!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public BigDecimal totalChegada(PedidoCompra pd) {

        BigDecimal resutado = new BigDecimal(BigInteger.ZERO);
        for (PedidoCompraItem pci : pd.getPedidoCompraItemList()) {
            if (pci.getValorTotalChegada() != null) {
                resutado = resutado.add(pci.getValorTotalChegada());
            }

        }
        return resutado;
    }

    public String verificarStatusLabel(PedidoCompra compra) {
        if (compra.getStatus().equals("Analisado")) {
            return "label label-success";
        } else {
            return "label label-danger";
        }
    }

    public void excluirPedido() {
        try {
            pcEJB.excluirPedido(pedidoCompra);
            MensageFactory.info("Pedido de Compra Excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível excluir o pedido de compra. Verifique o log!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void imprimirPedidoCompraPre(PedidoCompra pc, List<PedidoCompraItem> itens) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("data", pc.getDtEmissao());
        System.out.println(pc.getDtEmissao());
        map.put("nome_fornecedor", pc.getIdFornecedor().getNome());
        System.out.println(pc.getIdFornecedor().getNome());
        map.put("total", pc.getValorTotal());
        System.out.println(pc.getValorTotal());
        List<PedidoCompraPre> psitens = new ArrayList<PedidoCompraPre>();
        for (PedidoCompraItem pci : itens) {
            PedidoCompraPre pcp = new PedidoCompraPre();
            pcp.setProduto(pci.getIdProduto().getDescricao());
            System.out.println(pci.getIdProduto().getDescricao());
            pcp.setQuantidade(pci.getQtdSolicitada());
            pcp.setUnitario(pci.getValorUnitario());
            pcp.setTotal(pci.getValorTotal());
            psitens.add(pcp);
        }
        try {
            RelatorioFactory.imprimirPedidoCompraPre("/WEB-INF/Relatorios/PedidoCompra/Pedido_Compra_Pre.jasper", psitens, pc, map);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void adicionarProduto() {
        try {
            calcularTotais();
            pedidoCompra.getPedidoCompraItemList().add(pedidoCompraItem);
            pedidoCompraItem = new PedidoCompraItem();
            calcularTotalPedido();
        } catch (Exception e) {
            pedidoCompra.setPedidoCompraItemList(new ArrayList<PedidoCompraItem>());
            calcularTotais();
            pedidoCompra.getPedidoCompraItemList().add(pedidoCompraItem);
            pedidoCompraItem = new PedidoCompraItem();
            calcularTotalPedido();
        }
    }

    public void excluirItem(int index) {
        try {
            pedidoCompra.getPedidoCompraItemList().remove(index);
            calcularTotais();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void calcularTotalPedido() {
        BigDecimal bd = BigDecimal.ZERO;
        for (PedidoCompraItem pci : pedidoCompra.getPedidoCompraItemList()) {
            bd = bd.add(pci.getValorTotal());
        }
        pedidoCompra.setValorTotal(bd);
    }

    public void pegarCustoProduto() {
        pedidoCompraItem.setValorUnitario(pedidoCompraItem.getIdProduto().getPrecoCusto());
    }

    public void calcularTotais() {
        if (pedidoCompraItem.getValorUnitario() == null) {
            pedidoCompraItem.setValorUnitario(BigDecimal.ZERO);
        }
        if (pedidoCompraItem.getQtdSolicitada() == null) {
            pedidoCompraItem.setQtdSolicitada(BigDecimal.ZERO);
        }
        pedidoCompraItem.setValorTotal(pedidoCompraItem.getValorUnitario().multiply(pedidoCompraItem.getQtdSolicitada()));
    }

    public void imprimirPedidoCompra(Integer id) {
        try {
            RelatorioFactory.imprimirPedidoCompra(id, "/WEB-INF/Relatorios/PedidoCompra/Pedido_Compra.jasper");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void carregarPedidoParaAnalise() {
        pedidoCompra.setValorFinal(pedidoCompra.getValorTotal());
        for (PedidoCompraItem pci : pedidoCompra.getPedidoCompraItemList()) {
            pci.setStatus("S");
            pci.setValorUnitarioChegada(pci.getValorUnitario());
            pci.setValorTotalChegada(pci.getValorTotal());
            pci.setQtdChegada(pci.getQtdSolicitada());
        }
    }

    public void calcularValoresFinaisAnalise(PedidoCompraItem pci) {
        pci.setValorTotalChegada(pci.getQtdChegada().multiply(pci.getValorUnitarioChegada()));
        BigDecimal valorFinal = new BigDecimal(BigInteger.ZERO);
        for (PedidoCompraItem pcii : pci.getIdPedidoCompra().getPedidoCompraItemList()) {
            valorFinal = valorFinal.add(pcii.getValorTotalChegada());
        }
        pci.getIdPedidoCompra().setValorFinal(valorFinal);
    }

    public List<PedidoCompra> listarPedidosCompraNaoImportados() {
        return pcEJB.listarPedidosNaoImportados();
    }

    public List<PedidoCompra> listarPedidosCompra() {
        return pcEJB.ListarTodos();
    }

    public List<Cotacao> getCotacoesImportadas() {
        return cotacoesImportadas;
    }

    public void setCotacoesImportadas(List<Cotacao> cotacoesImportadas) {
        this.cotacoesImportadas = cotacoesImportadas;
    }

    public List<PedidoCompra> getPedidosCompra() {
        return pedidosCompra;
    }

    public void setPedidosCompra(List<PedidoCompra> pedidosCompra) {
        this.pedidosCompra = pedidosCompra;
    }

    public List<PedidoCompra> getListaDePedidos() {
        return listaDePedidos;
    }

    public void setListaDePedidos(List<PedidoCompra> listaDePedidos) {
        this.listaDePedidos = listaDePedidos;
    }

    public PedidoCompra getPedidoCompra() {
        return pedidoCompra;
    }

    public void setPedidoCompra(PedidoCompra pedidoCompra) {
        this.pedidoCompra = pedidoCompra;
    }

    public PedidoCompraItem getPedidoCompraItem() {
        return pedidoCompraItem;
    }

    public void setPedidoCompraItem(PedidoCompraItem pedidoCompraItem) {
        this.pedidoCompraItem = pedidoCompraItem;
    }
}
