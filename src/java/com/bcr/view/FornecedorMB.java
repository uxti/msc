/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CidadeEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.EstadoEJB;
import com.bcr.controller.FornecedorEJB;
import com.bcr.controller.TelefoneEJB;
import com.bcr.model.Cidade;
import com.bcr.model.Cliente;
import com.bcr.model.Estado;
import com.bcr.model.Fornecedor;
import com.bcr.model.Telefone;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class FornecedorMB implements Serializable {

    @EJB
    FornecedorEJB fEJB;
    private Fornecedor fornecedor;
    private Cidade cidade;
    @EJB
    CidadeEJB ciEJB;
    @EJB
    EstadoEJB ufEJB;
    private List<Cidade> cidades;
    private LazyDataModel<Cidade> cidadesLazy;
    private Telefone telefone;
    private List<Telefone> listaDeTelefones;
    @EJB
    TelefoneEJB tEJB;
    @EJB
    EmpresaEJB empEJB;

    /**
     * Creates a new instance of FornecedorMB
     */
    public FornecedorMB() {
        novo();
    }

    public void novo() {
        fornecedor = new Fornecedor();
        fornecedor.setDtCadastro(new Date());
        cidade = new Cidade();
        telefone = new Telefone();
        listaDeTelefones = new ArrayList<Telefone>();
    }

    public Fornecedor getFornecedor() {
        if (fornecedor != null) {
            if (fornecedor.getIdCidade() != null) {
                cidade = fornecedor.getIdCidade();
            }
            if (fornecedor.getTelefoneList() != null) {
                listaDeTelefones = fornecedor.getTelefoneList();
            }
            return fornecedor;
        } else {
            return new Fornecedor();
        }

    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public LazyDataModel<Cidade> getCidadesLazy() {
        return cidadesLazy;
    }

    public void setCidadesLazy(LazyDataModel<Cidade> cidadesLazy) {
        this.cidadesLazy = cidadesLazy;
    }

    public Telefone getTelefone() {
        return telefone;
    }

    public void setTelefone(Telefone telefone) {
        this.telefone = telefone;
    }

    public List<Telefone> getListaDeTelefones() {
        return listaDeTelefones;
    }

    public void setListaDeTelefones(List<Telefone> listaDeTelefones) {
        this.listaDeTelefones = listaDeTelefones;
    }

    public void pesquisarCEP(Integer enderecoTipo) throws MalformedURLException, DocumentException {
        String cep = fornecedor.getCep();
        if (cep != null) {
            String cep2 = cep.replace("-", "");
            URL url = new URL("http://viacep.com.br/ws/" + cep2 + "/xml/");
            Document document = getDocumento(url);
            Element root = document.getRootElement();
            cidade = new Cidade();
            String ibge = "";
            String uf = "";
            String nomeCidade = "";
            for (Iterator i = root.elementIterator(); i.hasNext();) {
                Element element = (Element) i.next();
                if (element.getQualifiedName().equals("logradouro")) {
                    fornecedor.setLogradouro(element.getText());
                    System.out.println(element.getText());
                }
                if (element.getQualifiedName().equals("bairro")) {
                    fornecedor.setBairro(element.getText());
                }
                if (element.getQualifiedName().equals("ibge")) {
                    ibge = element.getText();
                }
                if (element.getQualifiedName().equals("uf")) {
                    uf = element.getText();
                }
                if (element.getQualifiedName().equals("localidade")) {
                    nomeCidade = element.getText();
                    cidade = ciEJB.pesquisarCidadePorNome(nomeCidade);
                }
            }
            if (cidade.getIdCidade() == null) {
                Cidade c = new Cidade();
                c.setNome(nomeCidade);
                c.setCep(cep);
                c.setCodigoIBGE(ibge);
                Estado ee = new Estado();
                System.out.println("sigla pesquisada é: " + uf);
                ee = ufEJB.pesquisarPorSigla(uf);
                c.setIdEstado(ee);
                ciEJB.Salvar(c);
                cidade = c;
            }
        } else {
            System.out.println("cep normal é nulo");
        }
    }

    public static Document getDocumento(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    }

    public void carregarDatatableCidade() {

        cidadesLazy = new LazyDataModel<Cidade>() {
            private static final long serialVersionUID = 1L;

            public List<Cidade> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where c." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }
                setRowCount(Integer.parseInt(ciEJB.contarRegistros(Clausula).toString()));
                cidades = ciEJB.listarClientesLazy(first, pageSize, Clausula);
                return cidades;

            }
        };
    }

    public void salvar() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        fornecedor.setIdEmpresa(empEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa"))));
        if (fornecedor.getIdFornecedor() == null) {
            try {
                if (cidade.getIdCidade() != null) {
                    fornecedor.setIdCidade(cidade);
                }
                fEJB.salvar(fornecedor, listaDeTelefones);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possivel salvar!", null);
                System.out.println("Erro ocorrido foi é: " + e);
            }
        } else {
            try {
                if (cidade.getIdCidade() != null) {
                    fornecedor.setIdCidade(cidade);
                }
                fEJB.salvar(fornecedor, listaDeTelefones);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possivel atualizar!", null);
                System.out.println("Erro ocorrido foi é: " + e);
            }
        }
    }

    public void adicionarTelefone() {
        System.out.println(telefone.getNumero());
        System.out.println(telefone.getOperadora());
        listaDeTelefones.add(telefone);
        telefone = new Telefone();
    }

    public void importarDadosCliente() {
        if (fornecedor.getIdCliente() != null) {
            Cliente c = new Cliente();
            c = fornecedor.getIdCliente();
            fornecedor.setNome(c.getNome());
            fornecedor.setTipoFornecedor(c.getTipo());
            fornecedor.setCgccpf(c.getCgccpf());
            fornecedor.setEmail(c.getEmail());
            fornecedor.setHomePage(c.getHomePage());
            fornecedor.setCep(c.getCep());
            fornecedor.setTipoLogradouro(c.getTipoLogradouro());
            fornecedor.setLogradouro(c.getLogradouro());
            fornecedor.setNumero(c.getNumero());
            fornecedor.setComplemento(c.getComplemento());
            fornecedor.setBairro(c.getBairro());
            fornecedor.setIdCidade(c.getIdCidade());
            MensageFactory.info("Dados importados com sucesso!", null);
        } else {
            MensageFactory.warn("Selecione um cliente primeiramente!", null);
        }


    }

    public void excluirTelefone(Telefone t, int index) {
        try {
            if (t.getIdTelefone() != null) {
                tEJB.Excluir(t, t.getIdTelefone());
            }
            listaDeTelefones.remove(index);
            MensageFactory.info("Telefone removido com sucesso!", null);
        } catch (Exception e) {
        }
    }

    public List<Fornecedor> listaFornecedores() {
        return fEJB.ListarTodos();
    }

    public void excluir() {
        try {

            fEJB.Excluir(fornecedor, fornecedor.getIdFornecedor());
            MensageFactory.info("Fornecedor excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível excluir o fornecedor!", null);
            System.out.println("O erro ocorrido é: " + e);
        }
    }

    public List<Fornecedor> completeMethodFornecedor(String nome) {
        System.out.println(nome);
        System.out.println(fEJB.completeListaFornecedor(nome).size());
        return fEJB.completeListaFornecedor(nome);
    }
}
