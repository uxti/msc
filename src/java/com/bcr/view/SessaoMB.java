/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Acesso;
import com.bcr.model.Empresa;
import com.bcr.model.Parametrizacao;
import com.bcr.model.Usuario;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.servlet.http.HttpSession;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Renato
 */
@ManagedBean
@SessionScoped
public class SessaoMB {

    private Usuario usuario;
    @EJB
    UsuarioEJB uEJB;
    Acesso a = new Acesso();

    public SessaoMB() {
    }

    @PostConstruct
    public void init() {
        a = getUsuario().getIdPerfil().getIdAcesso();
    }

    public String retornaUsuario() {
        String usuarioLog = "";
        Object usuarioLogado = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (usuarioLogado instanceof UserDetails) {
            return usuarioLog = ((UserDetails) usuarioLogado).getUsername();
        } else {
            return usuarioLog = usuarioLogado.toString().toUpperCase();
        }
    }

    public Usuario getUsuario() {
        try {
            if (usuario.getIdUsuario() == null) {
                usuario.setDtUltimoAcesso(new Date());
                uEJB.Atualizar(usuario);
                usuario = (Usuario) uEJB.pegaUsuarioLogado();
                return usuario;
            } else {
                return usuario;
            }
        } catch (Exception e) {
//            usuario.setDtUltimoAcesso(new Date());
//            uEJB.Atualizar(usuario);
            usuario = (Usuario) uEJB.pegaUsuarioLogado();
            if (usuario.getIdUsuario() != null) {
                return usuario;
            } else {
                return new Usuario();
            }
        }
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String verificaSeSistemaGerencialOuFiscal() {
        Empresa e = uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa();
        Parametrizacao p = new Parametrizacao();
        p = e.getIdParametrizacao();
        if (p.getTipoSistema().equals("G")) {
            return "display:none;";
        } else {
            return "";
        }
    }

    public void atualizarUsuario() {
        try {
            uEJB.Atualizar(usuario);
            MensageFactory.info("Seus dados foram atualizados com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível atualizar os dados do seu perfil!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    @RequestMapping("logout")
    public String logout(HttpSession session) {
        session.invalidate();
        return "redirect:login";
    }

    public void teste() {
        UserDetails ud = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        ud.getUsername();
    }

    public boolean verificaMenuCliente() {

        boolean fim = true;
        if (a.getCadastroCliente() == false || a.getCadastroLocalTrabalho() == false) {
            fim = false;
        }
        return fim;
    }

    public boolean verificaMenuEmpresa() {
        boolean fim = true;
        if (a.getCadastroEmpresa() == false || a.getCadastroColaborador() == false
                || a.getCadastroUsuario() == false || a.getCadastroPerfil() == false
                || a.getCadastroSetor() == false || a.getCadastroCargo() == false) {
            fim = false;
        }
        return fim;
    }

    public boolean verificaMenuProduto() {
        boolean fim = true;
        if (a.getCadastroProduto() == false || a.getCadastroGrupo() == false
                || a.getCadastroSubgrupo() == false || a.getCadastroUnidade() == false
                || a.getCadastroNcm() == false || a.getCadastroLocalizacao() == false) {
            fim = false;
        }
        return fim;
    }

    public boolean verificaMenuAtivo() {
        boolean fim = true;
        if (a.getCadastroAtivo() == false || a.getCadastroGrupoativo() == false
                || a.getCadastroTipoativo() || a.getCadastroModeloativo() == false) {
            fim = false;
        }
        return fim;
    }

    public boolean verificaMenuLogistica() {
        boolean fim = true;
        if (a.getCadastroTransportadora() == false || verificaMenuProduto() == false
                || verificaMenuAtivo() == false) {
            fim = false;
        }
        return fim;
    }

    public boolean verificaMenuPlanoContas() {
        boolean fim = true;
        if (a.getCadastroPlanoContas() == false || a.getCadastroAgrupamento() == false
                || a.getCadastroCentrocusto() == false) {
            fim = false;
        }
        return fim;
    }

    public boolean verificaMenuBancos() {
        boolean fim = true;
        if (a.getCadastroBanco() == false || a.getCadastroContacorrente() == false
                || a.getCadastroCentrocusto() == false) {
            fim = false;
        }
        return fim;
    }

    public boolean verificaMenuFinanceiro() {
        boolean fim = true;
        if (a.getCadastroFormapagamento() == false || verificaMenuPlanoContas() == false
                || verificaMenuBancos() == false) {
            fim = false;
        }
        return fim;
    }

    public boolean verificaMenuOutros() {
        boolean fim = true;
        if (a.getCadastroEstado() == false || a.getCadastroCidade() == false) {
            fim = false;
        }
        return fim;
    }

    public String formatarMoedaView(BigDecimal valor) {
        return Bcrutils.formatarMoeda(valor);
    }
}
