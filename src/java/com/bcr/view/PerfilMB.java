/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.PerfilEJB;
import com.bcr.model.Acesso;
import com.bcr.model.Perfil;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class PerfilMB {

    @EJB
    PerfilEJB pEJB;
    private Perfil perfil;
    private Acesso acesso;

    /**
     * Creates a new instance of PerfilMB
     */
    public PerfilMB() {
        novo();
    }

    public void novo() {

        perfil = new Perfil();
        acesso = new Acesso();
        acesso.setFinanceiroAcessoTotal(false);
    }

    public Perfil getPerfil() {
        if (perfil.getIdAcesso() != null) {
            acesso = perfil.getIdAcesso();
        }
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Acesso getAcesso() {
        return acesso;
    }

    public void setAcesso(Acesso acesso) {
        this.acesso = acesso;
    }

    public void salvar() {
        if (perfil.getIdPerfil() == null) {
            try {
                perfil.setRole("ROLE_ROOT");
                pEJB.salvar(perfil, acesso);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível salvar!", null);
                Bcrutils.escreveLogErro(e);
            }
        } else {
            try {
                perfil.setRole("ROLE_ROOT");
                pEJB.salvar(perfil, acesso);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível atualizar!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public List<Perfil> listaPerfis() {
        return pEJB.ListarTodos();
    }

    public void excluir() {
        try {
            pEJB.Excluir(perfil, perfil.getIdPerfil());
            MensageFactory.info("Excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível excluir!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void mudaAcessoTotalFinanceiro() {
        if (acesso.getFinanceiroAcessoTotal() == null) {
            acesso.setFinanceiroAcessoTotal(false);
        }

        if (acesso.getFinanceiroAcessoTotal() != true) {
            acesso.setContasPagarEstornar(false);
            acesso.setModuloFinanceiro(false);
            acesso.setFinanceiroContasPagar(false);
            acesso.setFinanceiroContasReceber(false);
            acesso.setFinanceiroCaixa(false);
            acesso.setFinanceiroBanco(false);
            acesso.setContasPagarPesquisar(false);
            acesso.setContasPagarInserir(false);
            acesso.setContasPagarPagar(false);
            acesso.setContasPagarExcluir(false);
            acesso.setContasReceberEstornar(false);
            acesso.setContasPagarVer(false);
            acesso.setContasReceberPesquisar(false);
            acesso.setContasReceberInserir(false);
            acesso.setContasReceberReceber(false);
            acesso.setContasReceberExcluir(false);
            acesso.setContasReceberEstornar(false);
            acesso.setContasReceberVer(false);
            acesso.setCaixaPesquisar(false);
            acesso.setCaixaSangria(false);
            acesso.setCaixaSuprimento(false);
            acesso.setCaixaTransferencia(false);
            acesso.setBancoPesquisar(false);
            acesso.setBancoSaque(false);
            acesso.setBancoDeposito(false);
            acesso.setBancoTransferencia(false);
            acesso.setGestaoResultado(false);
            acesso.setFinanceiroCheque(false);
            acesso.setChequeAlterar(false);
            acesso.setChequeCompensar(false);
            acesso.setChequeExcluir(false);
            acesso.setChequeInserir(false);
            acesso.setChequePesquisar(false);
        } else {
            acesso.setContasPagarEstornar(true);
            acesso.setModuloFinanceiro(true);
            acesso.setFinanceiroContasPagar(true);
            acesso.setFinanceiroContasReceber(true);
            acesso.setFinanceiroCaixa(true);
            acesso.setFinanceiroBanco(true);
            acesso.setContasPagarPesquisar(true);
            acesso.setContasPagarInserir(true);
            acesso.setContasPagarPagar(true);
            acesso.setContasPagarExcluir(true);
            acesso.setContasReceberEstornar(true);
            acesso.setContasPagarVer(true);
            acesso.setContasReceberPesquisar(true);
            acesso.setContasReceberInserir(true);
            acesso.setContasReceberReceber(true);
            acesso.setContasReceberExcluir(true);
            acesso.setContasReceberEstornar(true);
            acesso.setContasReceberVer(true);
            acesso.setCaixaPesquisar(true);
            acesso.setCaixaSangria(true);
            acesso.setCaixaSuprimento(true);
            acesso.setCaixaTransferencia(true);
            acesso.setBancoPesquisar(true);
            acesso.setBancoSaque(true);
            acesso.setBancoDeposito(true);
            acesso.setBancoTransferencia(true);
            acesso.setGestaoResultado(true);
            acesso.setFinanceiroCheque(true);
            acesso.setChequeAlterar(true);
            acesso.setChequeCompensar(true);
            acesso.setChequeExcluir(true);
            acesso.setChequeInserir(true);
            acesso.setChequePesquisar(true);
        }
    }
    
    public void mudaAcessoTotalLogistica(){
        if (acesso.getLogisticaAcessoTotal() == null) {
            acesso.setLogisticaAcessoTotal(false);
        }
        
        if (acesso.getLogisticaAcessoTotal() != true) {
            acesso.setModuloLogistica(false);
            acesso.setLogisticaCompras(false);
            acesso.setLogisticaDepreciacao(false);
            acesso.setLogisticaEstoque(false);
            acesso.setLogisticaLancamentos(false);
            acesso.setLogisticaRequisicao(false);
        }else{
            acesso.setModuloLogistica(true);
            acesso.setLogisticaCompras(true);
            acesso.setLogisticaDepreciacao(true);
            acesso.setLogisticaEstoque(true);
            acesso.setLogisticaLancamentos(true);
            acesso.setLogisticaRequisicao(true);
        }
    }
}
