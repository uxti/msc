/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.NotaEntradaEJB;
import com.bcr.controller.NotaEntradaItemEJB;
import com.bcr.controller.PedidoCompraEJB;
import com.bcr.controller.ProdutoEJB;
import com.bcr.controller.RequisicaoMaterialEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Centrocustoplanocontas;
import com.bcr.model.Cfop;
import com.bcr.model.ContasPagar;
import com.bcr.model.Empresa;
import com.bcr.model.Fornecedor;
import com.bcr.model.LocalTrabalho;
import com.bcr.model.ModeloNfe;
import com.bcr.model.NotaEntrada;
import com.bcr.model.NotaEntradaItem;
import com.bcr.model.PedidoCompra;
import com.bcr.model.PedidoCompraItem;
import com.bcr.model.Produto;
import com.bcr.model.RequisicaoMaterial;
import com.bcr.model.Usuario;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class NotaEntradaMB {

    /* EJB's */
    @EJB
    NotaEntradaEJB notaEntradaEJB;
    @EJB
    NotaEntradaItemEJB NotaEntradaItemEJB;
    @EJB
    UsuarioEJB usuarioEJB;
    @EJB
    PedidoCompraEJB pcEJB;
    @EJB
    ProdutoEJB pEJB;
    @EJB
    RequisicaoMaterialEJB rmEJB;
    /* Objetos */
    private NotaEntrada notaEntrada;
    private NotaEntradaItem notaEntradaItem;
    private Produto produto;
    private Usuario usuario;
    private LocalTrabalho localTrabalho;
    private Fornecedor fornecedor;
    private Empresa empresa;
    private ModeloNfe modeloNfe;
    private Cfop cfop;

    /* Listas */
    private List<NotaEntrada> listaNotaEntradas;
    private List<NotaEntrada> listaNotaEntradaSelecionada;
    private List<NotaEntradaItem> listaNotaEntradaItens;
    private List<NotaEntrada> lista;
    private LazyDataModel<NotaEntrada> model;
    private ContasPagar contasPagar;
    private List<ContasPagar> listaContasPagar;
    private PedidoCompra pedidoCompra;
    private Centrocustoplanocontas centrocustoplanocontas;
    /* Variaveis */

    public NotaEntradaMB() {
        novo();
    }

    /* Método - Nova NF */
    public void novo() {
        notaEntrada = new NotaEntrada();
        novaItemNF();
        produto = new Produto();
        usuario = new Usuario();
        localTrabalho = new LocalTrabalho();
        fornecedor = new Fornecedor();
        empresa = new Empresa();
        modeloNfe = new ModeloNfe();
        contasPagar = new ContasPagar();
        listaContasPagar = new ArrayList<ContasPagar>();
        listaNotaEntradas = new ArrayList<NotaEntrada>();
        listaNotaEntradaItens = new ArrayList<NotaEntradaItem>();
        lista = new ArrayList<NotaEntrada>();
        listaNotaEntradaSelecionada = new ArrayList<NotaEntrada>();
        pedidoCompra = new PedidoCompra();
        centrocustoplanocontas = new Centrocustoplanocontas();
        notaEntrada.setValorDesconto(BigDecimal.ZERO);
        notaEntrada.setValorFrete(BigDecimal.ZERO);
        notaEntrada.setValorSeguro(BigDecimal.ZERO);
        notaEntrada.setOutrasDespesas(BigDecimal.ZERO);
        contasPagar.setCentrocustoplanocontasList(new ArrayList<Centrocustoplanocontas>());
        carregarNotaEntrada();
    }

    /* Método - Nova NF */
    public void novaNF() {
        novo();
        notaEntrada.setDtEmissao(new Date());
        notaEntrada.setStatus("Lançada");
        notaEntrada.setTipoEmissao("N");
        notaEntrada.setValortotalNF(BigDecimal.ZERO);
    }

    /* Método - Novo Item NF */
    public void novaItemNF() {
        notaEntradaItem = new NotaEntradaItem();
        notaEntradaItem = new NotaEntradaItem();
        notaEntradaItem.setValorDesconto(new BigDecimal(0));
        notaEntradaItem.setAliquotaICMS(new BigDecimal(0));
        notaEntradaItem.setAliquotaIPI(new BigDecimal(0));
        notaEntradaItem.setValorICMS(new BigDecimal(0));
        notaEntradaItem.setValorIPI(new BigDecimal(0));
        notaEntradaItem.setBasecalculoICMS(new BigDecimal(0));
    }

    public void adicionarItem() {
        if (notaEntradaItem.getIdProduto() == null || notaEntradaItem.getQuantidade() == null || notaEntradaItem.getValorUnitario() == null) {
            MensageFactory.warn("Preencha os campos 'Produto, Quantidade, Valor Unitário' pois são todos obrigatórios!", null);
        } else {
            notaEntradaItem.setSequencia(listaNotaEntradaItens.size() + 1);
            listaNotaEntradaItens.add(notaEntradaItem);
            notaEntrada.setNotaEntradaItemList(listaNotaEntradaItens);
            novaItemNF();
        }

    }

    public void calcularTotalItem() {
        if (notaEntradaItem.getValorDesconto() == null) {
            notaEntradaItem.setValorDesconto(BigDecimal.ZERO);
        }
        if (notaEntradaItem.getValorUnitario() == null) {
            notaEntradaItem.setValorUnitario(BigDecimal.ZERO);
        }
        if (notaEntradaItem.getQuantidade() == null) {
            notaEntradaItem.setQuantidade(BigDecimal.ZERO);
        }
        notaEntradaItem.setValorTotal(notaEntradaItem.getValorUnitario().multiply(notaEntradaItem.getQuantidade()).subtract(notaEntradaItem.getValorDesconto()));
    }

    public void gerarContasPagar() {
        if (notaEntrada.getIdFornecedor() == null) {
            MensageFactory.warn("O Fornecedor está vazio! Selecione um fornecedor primeiramente.", null);
        }
        if (contasPagar.getTipodocumento() == null || contasPagar.getTipodocumento().equals("") || contasPagar.getIdPlanoContas() == null || notaEntrada.getIdFornecedor() == null || notaEntrada.getNumero() == null) {
            MensageFactory.warn("Os campos 'Tipo de Documento, Plano de Contas, Fornecedor, Número de Documento' são obrigatórios!", null);
        } else {
            listaContasPagar = new ArrayList<ContasPagar>();
            int quantidadeParcelas = contasPagar.getNumParcela();
            BigDecimal valorParcela = notaEntrada.getValortotalNF().divide(new BigDecimal(quantidadeParcelas), MathContext.DECIMAL128);
//            List<Centrocustoplanocontas> centrosDivididos = new ArrayList<Centrocustoplanocontas>();
//            for(Centrocustoplanocontas cp : contasPagar.getCentrocustoplanocontasList()){
//                Centrocustoplanocontas cpp = new Centrocustoplanocontas();
//                cpp = cp;
//                cpp.getValor().divide(new BigDecimal(contasPagar.getNumParcela()));
//                centrosDivididos.add(cpp);
//            }

            for (int i = 0; i < quantidadeParcelas; i++) {
                ContasPagar cp = new ContasPagar();
                cp.setNumDocto(notaEntrada.getNumero());
                cp.setTotalParcelas(quantidadeParcelas);
                cp.setDtLancamento(new Date());
                cp.setDtVencimento(Bcrutils.addMes(notaEntrada.getDtEmissao(), i));
                cp.setNumParcela(i + 1);
                cp.setValorDuplicata(valorParcela);
                cp.setStatus(false);
                cp.setTipodocumento(contasPagar.getTipodocumento());
                cp.setIdEmpresa(usuarioEJB.pegaEmpresaLogadoNaSessao());
                cp.setIdFornecedor(notaEntrada.getIdFornecedor());
                cp.setIdPlanoContas(contasPagar.getIdPlanoContas());
                cp.setCentrocustoplanocontasList(new ArrayList<Centrocustoplanocontas>());
//                for (Centrocustoplanocontas cpc : contasPagar.getCentrocustoplanocontasList()) {
//                    cpc.setDataMovimento(cp.getDtVencimento());
//                    cpc.setIdContasPagar(cp);
//                }
//                if (notaEntrada.getPedidoCompraList().size() > 0) {
////                    cp.setCentrocustoplanocontasList(retornarCentrosCustosPlanosPorPedido(notaEntrada.getPedidoCompraList()));
//                }
                listaContasPagar.add(cp);
            }
        }

    }

    public BigDecimal totalDosItens() {
        BigDecimal valorFinal = new BigDecimal(BigInteger.ZERO);
//        try {
//            for (NotaEntradaItem nfi : notaEntrada.getNotaEntradaItemList()) {
//                valorFinal = valorFinal.add(nfi.getValorTotal());
//            }
//        } catch (Exception e) {
//            for (NotaEntradaItem nfi : notaEntrada.getNotaEntradaItemList()) {
//                valorFinal = valorFinal.add(nfi.getValorTotal());
//            }
//        }
        return valorFinal;
    }

    public List<Centrocustoplanocontas> retornarCentrosCustosPlanosPorPedido(List<PedidoCompra> pedidos) {
        List<String> requisicoes = new ArrayList<String>();
        BigDecimal valorTotalPedidos = new BigDecimal(BigInteger.ZERO);
        for (PedidoCompra pc : pedidos) {
            valorTotalPedidos = valorTotalPedidos.add(pc.getValorTotal());
            for (PedidoCompraItem pci : pc.getPedidoCompraItemList()) {
                Pattern pattern = Pattern.compile("([^,])+");
                Matcher matcher = pattern.matcher(pci.getDescricao());
                while (matcher.find()) {
                    requisicoes.add(pci.getIdCotacaoItem().getRequisicoesReferentes().substring(matcher.start(), matcher.end()));
                }
            }
        }
        List<RequisicaoMaterial> reqs = new ArrayList<RequisicaoMaterial>();
        for (String s : requisicoes) {
            Integer idRequisicao = Integer.parseInt(s.trim());
            RequisicaoMaterial rm = new RequisicaoMaterial();
            try {
                rm = rmEJB.pesquisarRequisicaoPorID(idRequisicao);
            } catch (Exception e) {
                System.out.println(e);
            }
            reqs.add(rm);
        }

        List<Centrocustoplanocontas> centros = new ArrayList<Centrocustoplanocontas>();

        for (RequisicaoMaterial rm : reqs) {
            Centrocustoplanocontas cpc = new Centrocustoplanocontas();
            if (rm.getDtSaida() == null) {
                cpc.setDataMovimento(new Date());
            } else {
                cpc.setDataMovimento(rm.getDtCadastro());
            }
            cpc.setIdCentroCusto(rm.getIdLocalTrabalho().getIdCentroCusto());
            cpc.setIdCliente(rm.getIdCliente());
            cpc.setIdLocalTrabalho(rm.getIdLocalTrabalho());
            cpc.setIdRequisicaoMaterial(rm);
            cpc.setTipo("D");
            cpc.setValor(rm.getValor());
            cpc.setPercentual(new BigDecimal((rm.getValor().longValue() * 100L) / valorTotalPedidos.longValue()));
            centros.add(cpc);
        }
        return centros;
    }

    public void verquantidade() {
        System.out.println(notaEntradaItem.getQuantidade());
    }

    /* Método - Verifica se o usuário pode editar ou excluir o registro */
    public boolean verificaSePodeEditar() {
        if (notaEntrada.getIdNotaEntrada() != null) {
            if (!notaEntrada.getContasPagarList().isEmpty()) {
                int situacao = 0;
                for (ContasPagar cp : notaEntrada.getContasPagarList()) {
                    if (cp.getStatus() == true) {
                        situacao = 1;
                    }
                }
                if (situacao == 1) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /* Método - Carrega todas as NF com Lazy */
    public void carregarNotaEntrada() {
        model = new LazyDataModel<NotaEntrada>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<NotaEntrada> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and n." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where n." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and n." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by n." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by n." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }

                setRowCount(Integer.parseInt(notaEntradaEJB.contarRegistro(Clausula).toString()));
                lista = notaEntradaEJB.listarRequisicaoMaterialLazyModeWhere(first, pageSize, Clausula);
                return lista;
            }
        };
    }

    public static String datePattern() {
        return "dd/MM/yyyy";
    }

    public String dtEmissao(Date date) {
        DateFormat format = new SimpleDateFormat(datePattern());
        return format.format(date);
    }

    /* Método - Salvar NF e seus itens */
    public void Salvar() {
        notaEntrada.setIdEmpresa(usuarioEJB.pegaEmpresaLogadoNaSessao());
        if (notaEntrada.getIdNotaEntrada() == null) {
            try {
                notaEntrada.setStatus("Lançada");
                if (listaContasPagar.size() == 0) {
//                    contasPagar = new ContasPagar();
                    System.out.println("vai lançar contas a pagar");
                    contasPagar.setNumDocto(notaEntrada.getNumero());
                    contasPagar.setTotalParcelas(1);
                    contasPagar.setDtLancamento(new Date());
                    //contasPagar.setDtVencimento(notaEntrada.getDtEmissao());
                    contasPagar.setNumParcela(1);
                    contasPagar.setValorDuplicata(notaEntrada.getValortotalNF());
                    contasPagar.setStatus(false);
                    contasPagar.setIdEmpresa(usuarioEJB.pegaEmpresaLogadoNaSessao());
                    contasPagar.setIdFornecedor(notaEntrada.getIdFornecedor());
                    contasPagar.setIdPlanoContas(contasPagar.getIdPlanoContas());
                }
                notaEntradaEJB.SalvarNotaEntrada(notaEntrada, listaContasPagar, contasPagar);
//                if (notaEntrada.getIdPedidoCompra() != null) {
//                    notaEntrada.getIdPedidoCompra().setStatus("Finalizado");
//                    pcEJB.AtualizarPedidoAposNota(notaEntrada.getIdPedidoCompra());
//
//                }
                MensageFactory.addMensagemPadraoSucesso("salvar");
            } catch (EJBException e) {
                MensageFactory.addMensagemPadraoErro("salvar");
                Bcrutils.descobreErroDeEJBException(e);
            }
        } else {
            try {
                notaEntradaEJB.SalvarNotaEntrada(notaEntrada, listaContasPagar, contasPagar);
                MensageFactory.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                MensageFactory.addMensagemPadraoErro("editar");
                System.out.println(e);
            }
        }

    }

    public void escrevePedido() {
        System.out.println(notaEntrada.getIdPedidoCompra().getIdPedidoCompra());
    }

    public void adicionarCentroCusto(List<Centrocustoplanocontas> lista) {
        lista.add(new Centrocustoplanocontas());
    }

    public void selecionarLocalDeTrabalho(Centrocustoplanocontas cc, int index) {
        cc.setIdCliente(cc.getIdLocalTrabalho().getIdCentroCusto().getIdCliente());
        cc.setIdCentroCusto(cc.getIdLocalTrabalho().getIdCentroCusto());
    }

    public void importarPedidoCompra() {
        Boolean situacaoMovimentaEstoque = false; // false = nao movimentou / true = movimentou
        try {
            if (notaEntrada.getPedidoCompraList().size() > 0) {
                notaEntrada.setDtEntrada(new Date());
                List<NotaEntradaItem> neil = new ArrayList<NotaEntradaItem>();
                BigDecimal valorTotal = new BigDecimal(BigInteger.ZERO);
                for (PedidoCompra pc : notaEntrada.getPedidoCompraList()) {
                    for (PedidoCompraItem pci : pc.getPedidoCompraItemList()) {
                        if (pci.getStatus().equals("S")) {
                            NotaEntradaItem nei = new NotaEntradaItem();
                            nei.setIdProduto(pci.getIdProduto());
                            nei.setQuantidade(pci.getQtdChegada());
                            nei.setValorUnitario(pci.getValorUnitarioChegada());
                            nei.setValorTotal(pci.getValorTotalChegada());
                            valorTotal = valorTotal.add(nei.getValorTotal());
                            neil.add(nei);
                        }
                    }
                    if (pc.getMovimentaEstoque().equals("S")) {
                        situacaoMovimentaEstoque = true;
                    }
                }
                notaEntrada.setValorTotalProdutos(valorTotal);
                notaEntrada.setValortotalNF(valorTotal);
                notaEntrada.setValorDesconto(BigDecimal.ZERO);
                notaEntrada.setValorFrete(BigDecimal.ZERO);
                notaEntrada.setValorSeguro(BigDecimal.ZERO);
                notaEntrada.setOutrasDespesas(BigDecimal.ZERO);
                notaEntrada.setNotaEntradaItemList(neil);
                carregarNotaEntrada();

                if (situacaoMovimentaEstoque) {
                    notaEntrada.setTipoProcesso("N");
                }
                System.out.println(notaEntrada.getTipoProcesso());
            }
            MensageFactory.info("Os pedidos foram importados com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível importar os pedidos!", null);
            System.out.println(e);
        }
    }

    /* Método - Exclui a NF */
    public void Excluir() {
        if (notaEntrada.getIdNotaEntrada() != null) {
            if (verificaSePodeEditar()) {
                MensageFactory.warn("Atenção! Esta nota tem contas a pagar quitadas! Para excluir estorne as contas a pagar!", null);
            } else {
                try {
                    notaEntradaEJB.excluirTudo(notaEntrada);
                    MensageFactory.addMensagemPadraoSucesso("excluir");
                } catch (Exception e) {
                    MensageFactory.error("Erro ao tentar excluir. Verifique o log!", null);
                    Bcrutils.escreveLogErro(e);
                }
            }

        }
    }

    /* Método - Exclui o item referente a NF */
    public void excluirItem(int index, NotaEntradaItem nei) {
        try {
//            listaNotaEntradaItens.remove(index);
            notaEntrada.getNotaEntradaItemList().remove(index);
            if (nei.getIdNotaEntradaItem() != null) {
                pEJB.SaidaEstoque(nei.getIdProduto(), nei.getQuantidade());
                NotaEntradaItemEJB.Excluir(nei, nei.getIdNotaEntradaItem());
            }
            MensageFactory.info("Item Removido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.info("Não foi possível excluir. Tente novamente.", null);
            System.out.println(e);
        }
    }

    /* Método - Calcula o total dos itens da NF de Entrada */
    /* Método - Seleciona o NF referenciada para objeto principal */
    public void selecionarNotaEntrada(NotaEntrada nf) {
        notaEntrada = nf;
    }

    /* Método - Seleciona o NFItem referenciada para objeto principal */
    public void selecionarNotaEntradaItem(NotaEntradaItem nfi) {
        notaEntradaItem = nfi;
    }

    public boolean semSelecionar() {
        if (listaNotaEntradaSelecionada.isEmpty()) {
            if (listaNotaEntradaSelecionada.size() == 1) {
                notaEntrada = listaNotaEntradaSelecionada.get(0);
            }
            return true;
        } else {
            if (listaNotaEntradaSelecionada.size() == 1) {
                notaEntrada = listaNotaEntradaSelecionada.get(0);
            }
            return false;
        }
    }

    public void novoCentroDeCusto() {
        centrocustoplanocontas = new Centrocustoplanocontas();
    }

    /* Método - Pesquisar NF de Entrada com paramêtros */
    /* Método - Lista todas as NF sem ordenação e sem Lazy */
    public List<NotaEntrada> listarNotasEntrada() {
        return notaEntradaEJB.ListarTodos();
    }

    /* Inicio GET's e SET's*/
    public NotaEntrada getNotaEntrada() {
        return notaEntrada;
    }

    public void setNotaEntrada(NotaEntrada notaEntrada) {
        this.notaEntrada = notaEntrada;
    }

    public NotaEntradaItem getNotaEntradaItem() {
        return notaEntradaItem;
    }

    public void setNotaEntradaItem(NotaEntradaItem notaEntradaItem) {
        this.notaEntradaItem = notaEntradaItem;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalTrabalho getLocalTrabalho() {
        return localTrabalho;
    }

    public void setLocalTrabalho(LocalTrabalho localTrabalho) {
        this.localTrabalho = localTrabalho;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<NotaEntrada> getListaNotaEntradas() {
        return listaNotaEntradas;
    }

    public void setListaNotaEntradas(List<NotaEntrada> listaNotaEntradas) {
        this.listaNotaEntradas = listaNotaEntradas;
    }

    public List<NotaEntradaItem> getListaNotaEntradaItens() {
        return listaNotaEntradaItens;
    }

    public void setListaNotaEntradaItens(List<NotaEntradaItem> listaNotaEntradaItens) {
        this.listaNotaEntradaItens = listaNotaEntradaItens;
    }

    public List<NotaEntrada> getLista() {
        return lista;
    }

    public void setLista(List<NotaEntrada> lista) {
        this.lista = lista;
    }

    public LazyDataModel<NotaEntrada> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<NotaEntrada> model) {
        this.model = model;
    }

    public List<NotaEntrada> getListaNotaEntradaSelecionada() {
        if (!listaNotaEntradaSelecionada.isEmpty()) {
            if (listaNotaEntradaSelecionada.size() == 1) {
                notaEntrada = listaNotaEntradaSelecionada.get(0);
                listaContasPagar = notaEntrada.getContasPagarList();
            } else {
                System.out.println("tamanho da lista é de " + listaNotaEntradaSelecionada.size());
            }
        }
        return listaNotaEntradaSelecionada;
    }

    public List<PedidoCompra> completeMethod(String id) {
        if (notaEntrada.getIdFornecedor() != null) {
            return pcEJB.completeMethod(id, notaEntrada.getIdFornecedor().getIdFornecedor());
        } else {
            MensageFactory.warn("Informe o fornecedor primeiramente!", null);
            return new ArrayList<PedidoCompra>();
        }
    }

    public void itemSelect() {
        for (PedidoCompra pc : notaEntrada.getPedidoCompraList()) {
            System.out.println(pc.getIdPedidoCompra());
        }
    }

    public void onItemSelect(SelectEvent event) {
        Produto o = (Produto) event.getObject();
        for (Produto p : pEJB.ListarTodos()) {
            if (o.getIdProduto().equals(p.getIdProduto())) {
                System.out.println("Estoque atual do produto " + p.getDescricao() + " quantidade " + p.getQuantidade());
                notaEntradaItem.setIdProduto(p);
            }
        }
    }

    public void setListaNotaEntradaSelecionada(List<NotaEntrada> listaNotaEntradaSelecionada) {
        this.listaNotaEntradaSelecionada = listaNotaEntradaSelecionada;
    }

    public ModeloNfe getModeloNfe() {
        return modeloNfe;
    }

    public void setModeloNfe(ModeloNfe modeloNfe) {
        this.modeloNfe = modeloNfe;
    }

    public Cfop getCfop() {
        return cfop;
    }

    public void setCfop(Cfop cfop) {
        this.cfop = cfop;
    }

    public ContasPagar getContasPagar() {
        return contasPagar;
    }

    public void setContasPagar(ContasPagar contasPagar) {
        this.contasPagar = contasPagar;
    }

    public List<ContasPagar> getListaContasPagar() {
        return listaContasPagar;
    }

    public void setListaContasPagar(List<ContasPagar> listaContasPagar) {
        this.listaContasPagar = listaContasPagar;
    }

    public PedidoCompra getPedidoCompra() {
        return pedidoCompra;
    }

    public void setPedidoCompra(PedidoCompra pedidoCompra) {
        this.pedidoCompra = pedidoCompra;
    }

    public Centrocustoplanocontas getCentrocustoplanocontas() {
        return centrocustoplanocontas;
    }

    public void setCentrocustoplanocontas(Centrocustoplanocontas centrocustoplanocontas) {
        this.centrocustoplanocontas = centrocustoplanocontas;
    }
}
