/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CaixaEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.MovimentoBancarioEJB;
import com.bcr.controller.PagamentoEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Banco;
import com.bcr.model.Centrocustoplanocontas;
import com.bcr.model.Cheque;
import com.bcr.model.ContaCorrente;
import com.bcr.model.ContasPagar;
import com.bcr.model.Fornecedor;
import com.bcr.pojo.PagamentoMultiplo;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import com.bcr.util.RelatorioFactory;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class PagamentoMB implements Serializable {

    @EJB
    PagamentoEJB pEJB;
    private ContasPagar contasPagar;
    private List<ContasPagar> contasPagarSelecionadas;
    private List<ContasPagar> listarContasPagar;
    @EJB
    EmpresaEJB eEJB;
    @EJB
    CaixaEJB cEJB;
    @EJB
    UsuarioEJB uEJB;
    private Centrocustoplanocontas centrocustoplanocontas;
    private List<Centrocustoplanocontas> centrosCusto;
    private Banco banco;
    private ContaCorrente contaCorrente;
    @EJB
    MovimentoBancarioEJB mbEJB;
    private String Ocorrencia;
    private String por, status;
    private Date dtIni, dtFim;
    private FileWriter writer;
    private PagamentoMultiplo pagamentoMultiplo;
    private List<PagamentoMultiplo> listaPagamentosMultiplos;
    private BigDecimal totalPagoMultiplo;
    private BigDecimal diferencaMultiplo;
    private BigDecimal totalCaixaMultiplo;
    private BigDecimal totalBancoMultiplo;
    private BigDecimal totalChequeMultiplo;

    public PagamentoMB() {
        novo();
    }

    public void novo() {
        contasPagar = new ContasPagar();
        contasPagar.setNumParcela(0);
        contasPagar.setStatus(false);
        centrosCusto = new ArrayList<Centrocustoplanocontas>();
        centrocustoplanocontas = new Centrocustoplanocontas();
        banco = new Banco();
        contaCorrente = new ContaCorrente();
        contasPagarSelecionadas = new ArrayList<ContasPagar>();
        listarContasPagar = new ArrayList<ContasPagar>();
        por = "";
        dtIni = new Date();
        dtFim = new Date();
        status = "";
        pagamentoMultiplo = new PagamentoMultiplo();
        pagamentoMultiplo.setDtPagamento(new Date());
        listaPagamentosMultiplos = new ArrayList<PagamentoMultiplo>();
        totalPagoMultiplo = new BigDecimal(BigInteger.ZERO);
        diferencaMultiplo = new BigDecimal(BigInteger.ZERO);
        totalCaixaMultiplo = new BigDecimal(BigInteger.ZERO);
        totalBancoMultiplo = new BigDecimal(BigInteger.ZERO);
        totalChequeMultiplo = new BigDecimal(BigInteger.ZERO);
    }

    public void lancarContasPagar() {
        contasPagar.setIdEmpresa(uEJB.pegaEmpresaLogadoNaSessao());
        System.out.println(contasPagar.getIdEmpresa().getIdEmpresa());
        if (contasPagar.getIdContasPagar() == null) {
            contasPagar.setDtLancamento(new Date());
        }
        //aqui verifica se somente lançou a conta normalmente em 1 parcela!
        if (contasPagar.getNumParcela().equals(0)) {
            if (contasPagar.getStatus() == true) {
                //Paga
                if (contasPagar.getPagoCom() != null) {

                    if (contasPagar.getPagoCom().equals("C")) {
                        try {
//                        pEJB.salvarLancandoNoCaixa(contasPagar);
                            pEJB.pagar(contasPagar);
                            MensageFactory.info("Pagamento efetuado com sucesso! Saída com caixa!", null);
//                        preCarregar();
                        } catch (Exception e) {
                            MensageFactory.error("Não foi possível pagar com o caixa! Verifique os logs!", null);
                            Bcrutils.escreveLogErro(e);
                        }
                    } else {
                        try {
                            pEJB.salvarLancandoNoBanco(contasPagar, contaCorrente, banco);
                            MensageFactory.info("Pagamento efetuado com sucesso! Saída com banco!", null);
//                        preCarregar();
                        } catch (Exception e) {
                            MensageFactory.error("Não foi possível pagar com o banco! Verifique os logs!", null);
                            Bcrutils.escreveLogErro(e);
                        }
                    }
                }

            } else {
                //Não paga
                try {
                    pEJB.salvarAtualizarNormal(contasPagar);
                    if (contasPagar.getIdContasPagar() == null) {
                        MensageFactory.info("Conta lançada com sucesso!", null);
                    } else {
                        MensageFactory.info("Conta atualizado com sucesso!", null);
                    }
//                    preCarregar();
                } catch (Exception e) {
                    if (contasPagar.getIdContasPagar() == null) {
                        MensageFactory.error("Nao foi possivel lançar a conta! Verifique o log!", null);
                        System.out.println("Nao foi possivel lancar a conta com uma parcela");
                        Bcrutils.escreveLogErro(e);
                    } else {
                        MensageFactory.error("Nao foi possivel atualizar a conta! Verifique o log!", null);
                        System.out.println("Nao foi possivel lancar a conta com uma parcela");
                        Bcrutils.escreveLogErro(e);
                    }
                }
            }
        } else {
            try {
                int quantidadeMeses = contasPagar.getNumParcela();
                Date dtPrimeiroVencimento = contasPagar.getDtVencimento();
                Date ultimaData = new Date();
                Long numLote = new Date().getTime();
                for (int i = 0; i < quantidadeMeses; i++) {
                    contasPagar.setNumParcela(i + 1);
                    contasPagar.setParcelaSistema(contasPagar.getNumParcela().toString());
                    contasPagar.setNumLote(numLote.toString());
                    Date novaData = new Date();
                    if (i == 0) {
                        novaData = dtPrimeiroVencimento;
                    } else {
                        if (getOcorrencia().equals("Diariamente")) {
                            novaData = Bcrutils.addDia(ultimaData, 1);
                        } else if (getOcorrencia().equals("Semanalmente")) {
                            novaData = Bcrutils.addDia(ultimaData, 7);
                        } else if (getOcorrencia().equals("Bimestralmente")) {
                            novaData = Bcrutils.addMes(ultimaData, 2);
                        } else if (getOcorrencia().equals("Trimestralmente")) {
                            novaData = Bcrutils.addMes(ultimaData, 3);
                        } else if (getOcorrencia().equals("Semestralmente")) {
                            novaData = Bcrutils.addMes(ultimaData, 4);
                        } else if (getOcorrencia().equals("Anualmente")) {
                            novaData = Bcrutils.addMes(ultimaData, 12);
                        } else {
                            novaData = Bcrutils.addMes(ultimaData, 1);
                        }
                    }
                    Calendar c = Calendar.getInstance();
                    c.setTime(novaData);
//                    if (c.get(Calendar.DAY_OF_WEEK) == 1) { // verifica se é domingo se for adiciona um dia
//                        novaData = Bcrutils.addDia(novaData, 1);
//                    }
//                    if (c.get(Calendar.DAY_OF_WEEK) == 7) { // verifica se é sabado se for adiciona dois dias
//                        novaData = Bcrutils.addDia(novaData, 2);
//                    }
                    contasPagar.setDtVencimento(novaData);
                    ultimaData = novaData;
                    pEJB.salvarAtualizarNormal(contasPagar);
                }
//                preCarregar();
                MensageFactory.info("Contas lançadas com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível lançar as contas!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
        pesquisar();
        novo();
        novoLancamento();
    }

    public String verificarStatusLabel(ContasPagar contasPagar) {
        if (contasPagar.getStatus() == true) {
            return "label label-success";
        } else if (contasPagar.getStatus() == false && contasPagar.getDtVencimento().before(new Date())) {
            return "label label-danger";
        } else if (contasPagar.getStatus() == false && contasPagar.getDtVencimento().after(new Date())) {
            return "label label-primary";
        } else {
            return "";
        }
    }

    public void preencherNominal() {
        if (pagamentoMultiplo.getColaborador() != null) {
            pagamentoMultiplo.setNominal(pagamentoMultiplo.getColaborador().getNome());
        } else {
            pagamentoMultiplo.setNominal(null);
        }
    }

    public void selecionarLocalDeTrabalho() {
        centrocustoplanocontas.setIdCliente(centrocustoplanocontas.getIdLocalTrabalho().getIdCentroCusto().getIdCliente());
        centrocustoplanocontas.setIdCentroCusto(centrocustoplanocontas.getIdLocalTrabalho().getIdCentroCusto());
    }

    public void excluirCentroCusto(int index, Centrocustoplanocontas cpc) {
        contasPagar.getCentrocustoplanocontasList().remove(index);
        if (cpc.getIdCentrocustoplanocontas() != null) {
            pEJB.excluirCentroCustoPlano(cpc);
        }
        System.out.println(contasPagar.getCentrocustoplanocontasList().size());
    }

    public String verificarStatusValue(ContasPagar contasPagar) {
        if (contasPagar.getStatus() == true) {
            return "Pago";
        } else if (contasPagar.getStatus() == false && contasPagar.getDtVencimento().before(new Date())) {
            return "Vencido";
        } else if (contasPagar.getStatus() == false && contasPagar.getDtVencimento().after(new Date())) {
            return "Aberto";
        } else {
            return "";
        }
    }

    public void atualizarConta() {
        try {
            pEJB.Atualizar(contasPagar);
            MensageFactory.info("Conta atualizada com sucesso!", null);
//            preCarregar();
        } catch (Exception e) {
            MensageFactory.error("Não foi possivel atualizar a conta!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void adicionarCentroCusto() {
        centrosCusto.add(centrocustoplanocontas);
        centrocustoplanocontas = new Centrocustoplanocontas();
        contasPagar.setCentrocustoplanocontasList(centrosCusto);
    }

    public BigDecimal valorRestanteCentroCusto() {
        BigDecimal val = new BigDecimal(BigInteger.ZERO);
        try {
            if (!contasPagar.getCentrocustoplanocontasList().isEmpty()) {
                for (Centrocustoplanocontas cpc : contasPagar.getCentrocustoplanocontasList()) {
                    val = val.add(cpc.getValor());
                }
            }
            return contasPagar.getValorDuplicata().subtract(val);
        } catch (Exception e) {
            return contasPagar.getValorDuplicata();
        }
    }

    public void pesquisar() {
        try {
            listarContasPagar = pEJB.pesquisar(contasPagar, por, status, dtIni, dtFim);
        } catch (Exception e) {
            Bcrutils.escreveLogErro(e);
        }
    }

    public void estornar() {
        if (contasPagar.getIdContasPagar() != null) {
            try {
                contasPagar.setStatus(false);
                contasPagar.setValorPago(null);
                contasPagar.setDtPagamento(null);
                contasPagar.setPagoCom(null);
                contasPagar.setIdBanco(null);
                contasPagar.setIdContaCorrente(null);
                pEJB.estornar(contasPagar);
                MensageFactory.info("Conta estornada com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível estornar a conta!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public void processarListaPagamento() {
        for (ContasPagar cp : contasPagarSelecionadas) {
            cp.setDtPagamento(new Date());
            cp.setValorPago(cp.getValorDuplicata());
        }
        List<ContasPagar> contasTemp = new ArrayList<ContasPagar>();
        contasTemp.addAll(contasPagarSelecionadas);
        int index = 0;
        for (ContasPagar cp : contasTemp) {
            if (cp.getStatus().equals(true)) {
                contasPagarSelecionadas.remove(index);
                index = index + 1;
            } else {
                index = index + 1;
            }
        }
    }

    public void pagarConta() {
        try {
            for (ContasPagar cp : contasPagarSelecionadas) {
                cp.setStatus(Boolean.TRUE);
                pEJB.pagar(cp);
            }
            MensageFactory.info("Pagamento efetuados com sucesso!", null);
            pesquisar();
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar efetuar o pagamento. Verifique o log!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void excluir() {
        if (contasPagar.getIdContasPagar() != null) {
            try {
                pEJB.Excluir(contasPagar, contasPagar.getIdContasPagar());
                MensageFactory.info("Excluído com sucesso!", null);
                listarContasPagar.remove(contasPagar);
                contasPagar = new ContasPagar();
                contasPagarSelecionadas = new ArrayList<ContasPagar>();
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar excluir. Verifique o log!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public void excluirLote() {

        List<ContasPagar> temp = new ArrayList<ContasPagar>();

        if (contasPagar.getIdContasPagar() != null) {
            try {
                pEJB.excluirLote(contasPagar.getNumLote());
                MensageFactory.info("Excluído com sucesso!", null);
                for (ContasPagar cp : listarContasPagar) {
                    if (cp.getNumLote().equals(contasPagar.getNumLote()));
                    temp.add(cp);
                }
                for (ContasPagar cp1 : temp) {
                    listarContasPagar.remove(cp1);
                }
                contasPagar = new ContasPagar();
                contasPagarSelecionadas = new ArrayList<ContasPagar>();
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar excluir. Verifique o log!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public boolean disable() {
        try {
            if (contasPagarSelecionadas.isEmpty() || contasPagar.getStatus().equals(true)) {
                if (contasPagarSelecionadas.size() == 1) {
                    contasPagar = contasPagarSelecionadas.get(0);
                }
                return true;
            } else {
                if (contasPagarSelecionadas.size() == 1) {
                    contasPagar = contasPagarSelecionadas.get(0);
                }
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    public boolean verificaSePodeExcluirEeditar() {
        try {
            if (contasPagarSelecionadas.size() == 1 && contasPagar.getStatus().equals(false)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return true;
        }

    }

    public boolean verificaSePodeVer() {
        try {
            if (contasPagarSelecionadas.size() == 1) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return true;
        }

    }

    public boolean verificaSePodeEstornar() {
        try {
            if (contasPagarSelecionadas.size() == 1 && contasPagar.getStatus().equals(true)) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            return true;
        }

    }

    public void novoLancamento() {
        contasPagar = new ContasPagar();
        contasPagar.setNumParcela(0);
        contasPagar.setStatus(Boolean.FALSE);
    }

    @PostConstruct
    public void preCarregar() {
        novo();
        listarContasPagar = listarContasVencendoHoje();
        calcularTotalPagar();
        calcularTotalPago();
        dtIni = new Date();
        dtFim = new Date();
    }

    public List<ContasPagar> listarContasVencendoHoje() {
        return pEJB.listarContasVencendoHoje();
    }

    public List<ContasPagar> listarPagamentos() {
        return pEJB.ListarTodos();
    }

    public void pagar(ContasPagar cp) {
        try {
            cp.setDtPagamento(new Date());
            cp.setValorPago(cp.getValorDuplicata());
            cp.setPagoCom("C");
            cp.setStatus(Boolean.TRUE);
            pEJB.pagar(cp);
            MensageFactory.info("Conta paga com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível pagar a conta. Verifique no log!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void pagarMultiplos() {
        try {

            contasPagar.setValorPago(contasPagar.getValorDuplicata());
            contasPagar.setPagoCom("M");
            contasPagar.setStatus(Boolean.TRUE);
            pEJB.Atualizar(contasPagar);
            for (PagamentoMultiplo pm : listaPagamentosMultiplos) {
                if (pm.getDestino().equals("Caixa")) {
                    pEJB.lancarMovimentoCaixa(pm.getValor(), pm.getContasPagar());
                } else if (pm.getDestino().equals("Banco")) {
                    pEJB.lancarMovimentoBancario(pm.getValor(), null, pm.getContaCorrente(), pm.getContasPagar(), pm.getBanco());
                } else if (pm.getDestino().equals("Cheque")) {
                    Cheque c = new Cheque();
                    c.setNumeroCheque(Integer.parseInt(pm.getNumCheque()));
                    c.setDtCompensacaoVencimento(pm.getDtCompensar());
                    c.setDtRecebimentoEmissao(new Date());
                    c.setValor(pm.getValor());
                    c.setIdColaborador(pm.getColaborador());
                    c.setIdEmpresa(uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa());
                    c.setIdFornecedor(contasPagar.getIdFornecedor());
                    c.setIdContaCorrente(pm.getContaCorrente());
                    c.setIdContasPagar(contasPagar);
                    c.setNominal(pm.getNominal());
                    c.setTipo("Normal");
                    c.setConta(getContaCorrente().getNumConta());
                    pEJB.lancarMovimentoBancario(pm.getValor(), c, pm.getContaCorrente(), pm.getContasPagar(), pm.getBanco());
                }
            }
            MensageFactory.info("Pagamento multiplo efetuado com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possivel efetuar o pagamento multiplo! Verifique nos logs!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void verificarSeDataVencimentoMenorQueHoje() {
        System.out.println(contasPagar.getDtVencimento());
        if (contasPagar.getDtVencimento() != null) {
            if (contasPagar.getDtVencimento().before(new Date())) {
                MensageFactory.warn("Atenção! Você esta lançando a data de vencimento com data anterior a hoje!", null);
            }
        }
    }

    public void adiar(ContasPagar cp) {
        try {
            cp.setDtVencimento(Bcrutils.addDia(cp.getDtVencimento(), 1));
            pEJB.Atualizar(cp);
            MensageFactory.info("Conta adiada com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível adiar a conta. Verifique no log!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void verificarStatus() {
        if (contasPagar.getStatus().equals(false)) {
            contasPagar.setPagoCom("");
        }
    }

    public void verificarQuantidadeDeLancamentos() {
        if (contasPagar.getNumParcela() > 1) {
            contasPagar.setStatus(false);
        }
    }

    public ContasPagar getContasPagar() {
        return contasPagar;
    }

    public void setContasPagar(ContasPagar contasPagar) {
        this.contasPagar = contasPagar;
    }

    public Centrocustoplanocontas getCentrocustoplanocontas() {
        return centrocustoplanocontas;
    }

    public void setCentrocustoplanocontas(Centrocustoplanocontas centrocustoplanocontas) {
        this.centrocustoplanocontas = centrocustoplanocontas;
    }

    public List<Centrocustoplanocontas> getCentrosCusto() {
        return centrosCusto;
    }

    public void setCentrosCusto(List<Centrocustoplanocontas> centrosCusto) {
        this.centrosCusto = centrosCusto;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public List<ContasPagar> getContasPagarSelecionadas() {
        if (!contasPagarSelecionadas.isEmpty()) {
            if (contasPagarSelecionadas.size() == 1) {
                contasPagar = contasPagarSelecionadas.get(0);
            }
        }
        return contasPagarSelecionadas;
    }

    public String calcularTotalPagar() {
        BigDecimal totalPagar = new BigDecimal(BigInteger.ZERO);
        if (listarContasPagar.isEmpty()) {
            return Bcrutils.formatarMoeda(new BigDecimal(BigInteger.ZERO));
        } else {
            for (ContasPagar cp : listarContasPagar) {
                if (cp.getStatus() == false) {
                    totalPagar = totalPagar.add(cp.getValorDuplicata());
                }
            }
            return Bcrutils.formatarMoeda(totalPagar);
        }

    }

    public String calcularTotalPago() {
        BigDecimal totalPago = new BigDecimal(BigInteger.ZERO);
        if (listarContasPagar.isEmpty()) {
            return Bcrutils.formatarMoeda(new BigDecimal(BigInteger.ZERO));
        } else {
            for (ContasPagar cp : listarContasPagar) {
                if (cp.getStatus() == true) {
                    totalPago = totalPago.add(cp.getValorPago());
                }
            }
            return Bcrutils.formatarMoeda(totalPago);
        }
    }

    public String calcularTotalVencido() {
        BigDecimal totalVencido = new BigDecimal(BigInteger.ZERO);
        if (listarContasPagar.isEmpty()) {
            return Bcrutils.formatarMoeda(new BigDecimal(BigInteger.ZERO));
        } else {
            for (ContasPagar cp : listarContasPagar) {
                if (cp.getStatus() == false && cp.getDtVencimento().before(new Date())) {
                    totalVencido = totalVencido.add(cp.getValorDuplicata());
                }
            }
            return Bcrutils.formatarMoeda(totalVencido);
        }
    }

    /*  Início método para gerar exportação para SoftCheque  */
    /*  Charles - 20/04/2015  */
    public void gerarExportacao() {
        List<Cheque> listaExportacao = new ArrayList<Cheque>();
        listaExportacao = contasPagar.getChequeList();
        if (!listaExportacao.isEmpty()) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMYYYYHHmmss");
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("ddMMYYYY");
            String dataFormatada = dateFormat.format(new Date());
            // Extensão exclusiva - .scq
            // Leiaute do nome do arquivo - Ex: PastaDefinidaPadraoDeImportacoes\exp_180420151551_IDContasPagar.scq
            String nomeArquivo = "D:\\MSC\\EXPORTACAO\\EXP_" + dataFormatada + ".scq";
            try {
                FileWriter writer = new FileWriter(nomeArquivo);
                for (Cheque chq : listaExportacao) {
                    // Numero_Cheque                   - Com zeros            - Comp: 6   - Formato: 999999
                    writer.append(StringUtils.leftPad(chq.getNumeroCheque().toString(), 6, "0").substring(0, 6));
                    // Data_Emissão                    - Não completar        - Comp: 8   - Formato:    ddmmaaaa
                    writer.append(dateFormat1.format(chq.getDtRecebimentoEmissao()));
                    // Data_Entrada (Data a Compensar) - Não completar        - Comp: 8   - Formato:    ddmmaaaa
                    writer.append(dateFormat1.format(chq.getDtCompensacaoVencimento()));
                    // Valor                           - Com "0" a esquerda   - Comp: 12  - Formato:    99999999
                    writer.append(StringUtils.leftPad(chq.getValor().toString().replace(",", "").replace(".", ""), 12, "0").substring(0, 12));
                    // Destinatário (Nonimal)          - Com " " a direita    - Comp: 60  - Formato:       X(60)
                    writer.append(StringUtils.rightPad(chq.getNominal().toString(), 60, " ").substring(0, 60));
                    // Referente a... (Observação)     - Com " " a direita    - Comp: 512 - Formato:      X(512)
                    writer.append(StringUtils.rightPad(chq.getObservacao().toString(), 512, " ").substring(0, 512));
                    // Código do banco                 - Com zeros            - Comp: 3   - Formato:        999
                    writer.append(StringUtils.leftPad(chq.getIdContaCorrente().getIdBanco().getNumeroBanco().toString(), 3, "0").substring(0, 3));
                    // Número da agência da conta      - Com zeros            - Comp: 4   - Formato:       9999
                    writer.append(StringUtils.leftPad(chq.getIdContaCorrente().getNumAgencia().toString(), 4, "0").substring(0, 4));
                    // Número da conta bancária        - Com zeros            - Comp: 15   - Formato: 999999999
                    writer.append(StringUtils.rightPad(chq.getIdContaCorrente().getNumConta().toString().replace("-", "").replace(" ", ""), 15, " ").substring(0, 15));
                    writer.append('\n');
                    writer.flush();
                }
                writer.close();
                MensageFactory.info("Exportação ok! Agora, acesse o SoftCheque, depois Ferramentas, Executar importação automática agora.", null);
            } catch (IOException e) {
                MensageFactory.error("Erro ao exportar. Entre em contato com o Suporte.", null);
                e.printStackTrace();
            }
        } else {
            listaExportacao = new ArrayList<Cheque>();
            MensageFactory.info("Exportação não realizada.", "");
        }
    }

    public void exportarCheque() {
        Bcrutils.gerarExportacao(contasPagar.getChequeList());
    }

    public void adicionarPagamentosMultiplos() {
        try {
            pagamentoMultiplo.setContasPagar(contasPagar);
            listaPagamentosMultiplos.add(pagamentoMultiplo);
            pagamentoMultiplo = new PagamentoMultiplo();
            calcularValoresMultiplos();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void excluirPagamentoMultiplo(int index) {
        listaPagamentosMultiplos.remove(index);
        calcularValoresMultiplos();
    }

    public void calcularValoresMultiplos() {
        totalPagoMultiplo = new BigDecimal(BigInteger.ZERO);
        diferencaMultiplo = new BigDecimal(BigInteger.ZERO);
        totalCaixaMultiplo = new BigDecimal(BigInteger.ZERO);
        totalBancoMultiplo = new BigDecimal(BigInteger.ZERO);
        totalChequeMultiplo = new BigDecimal(BigInteger.ZERO);


        for (PagamentoMultiplo pm : listaPagamentosMultiplos) {
            totalPagoMultiplo = totalPagoMultiplo.add(pm.getValor());
            diferencaMultiplo = contasPagar.getValorDuplicata().subtract(totalPagoMultiplo);
            if (pm.getDestino().equals("Caixa")) {
                totalCaixaMultiplo = totalCaixaMultiplo.add(pm.getValor());
            } else if (pm.getDestino().equals("Banco")) {
                totalBancoMultiplo = totalBancoMultiplo.add(pm.getValor());
            } else {
                totalChequeMultiplo = totalChequeMultiplo.add(pm.getValor());
            }
        }
    }

    public void imprimirRelatorioContasVencidasPeriodo() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("pIni", dtIni);
        map.put("pFim", dtFim);
        if (contasPagar.getIdFornecedor() == null) {
            map.put("pFornecedor", null);
        } else {
            map.put("pFornecedor", contasPagar.getIdFornecedor().getIdFornecedor());
        }
        RelatorioFactory.relatorioCustom("/WEB-INF/Relatorios/Pagamento/RelatorioContasPagarVencidasPeriodo.jasper", map);
    }

    public void imprimirRelatorioContasPrevistasAVencer() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("pIni", dtIni);
        map.put("pFim", dtFim);
        if (contasPagar.getIdFornecedor() == null) {
            map.put("pFornecedor", null);
        } else {
            map.put("pFornecedor", contasPagar.getIdFornecedor().getIdFornecedor());
        }
        RelatorioFactory.relatorioCustom("/WEB-INF/Relatorios/Pagamento/RelatorioContasPagarPrevistasParaVencer.jasper", map);
    }

    public void preencherDtCompensar() {
        pagamentoMultiplo.setDtCompensar(pagamentoMultiplo.getDtPagamento());
    }

    public void preencherValor() {
        contasPagar.setValorPago(contasPagar.getValorDuplicata());
    }

    public void preencherDtPagamento() {
        contasPagar.setDtPagamento(new Date());
    }

    /*  Fim método para gerar exportação para SoftCheque  */
    public Long totalParcela(Integer idFornecedor, String Dcto) {
        return pEJB.contaQuantidadeParcela(idFornecedor, Dcto);
    }

    public Long totalParcela(Integer idFornecedor, String Dcto, String nunLote) {
        return pEJB.contaQuantidadeTotalParcela(idFornecedor, Dcto, nunLote);
    }

    public Long numeroDocumentosLancados() {
        return pEJB.numeroDeLancamentos();
    }

    public BigDecimal valorTotalVencido() {
        return pEJB.valorTotalVencidos();
    }

    public void setContasPagarSelecionadas(List<ContasPagar> contasPagarSelecionadas) {
        this.contasPagarSelecionadas = contasPagarSelecionadas;
    }

    public List<ContasPagar> getListarContasPagar() {
        return listarContasPagar;
    }

    public void setListarContasPagar(List<ContasPagar> listarContasPagar) {
        this.listarContasPagar = listarContasPagar;
    }

    public String getOcorrencia() {
        return Ocorrencia;
    }

    public void setOcorrencia(String Ocorrencia) {
        this.Ocorrencia = Ocorrencia;
    }

    public String getPor() {
        return por;
    }

    public void setPor(String por) {
        this.por = por;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDtIni() {
        return dtIni;
    }

    public void setDtIni(Date dtIni) {
        this.dtIni = dtIni;
    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        this.dtFim = dtFim;
    }

    public FileWriter getWriter() {
        return writer;
    }

    public void setWriter(FileWriter writer) {
        this.writer = writer;
    }

    public PagamentoMultiplo getPagamentoMultiplo() {
        return pagamentoMultiplo;
    }

    public void setPagamentoMultiplo(PagamentoMultiplo pagamentoMultiplo) {
        this.pagamentoMultiplo = pagamentoMultiplo;
    }

    public List<PagamentoMultiplo> getListaPagamentosMultiplos() {
        return listaPagamentosMultiplos;
    }

    public void setListaPagamentosMultiplos(List<PagamentoMultiplo> listaPagamentosMultiplos) {
        this.listaPagamentosMultiplos = listaPagamentosMultiplos;
    }

    public BigDecimal getTotalPagoMultiplo() {
        return totalPagoMultiplo;
    }

    public void setTotalPagoMultiplo(BigDecimal totalPagoMultiplo) {
        this.totalPagoMultiplo = totalPagoMultiplo;
    }

    public BigDecimal getDiferencaMultiplo() {
        return diferencaMultiplo;
    }

    public void setDiferencaMultiplo(BigDecimal diferencaMultiplo) {
        this.diferencaMultiplo = diferencaMultiplo;
    }

    public BigDecimal getTotalCaixaMultiplo() {
        return totalCaixaMultiplo;
    }

    public void setTotalCaixaMultiplo(BigDecimal totalCaixaMultiplo) {
        this.totalCaixaMultiplo = totalCaixaMultiplo;
    }

    public BigDecimal getTotalBancoMultiplo() {
        return totalBancoMultiplo;
    }

    public void setTotalBancoMultiplo(BigDecimal totalBancoMultiplo) {
        this.totalBancoMultiplo = totalBancoMultiplo;
    }

    public BigDecimal getTotalChequeMultiplo() {
        return totalChequeMultiplo;
    }

    public void setTotalChequeMultiplo(BigDecimal totalChequeMultiplo) {
        this.totalChequeMultiplo = totalChequeMultiplo;
    }
}
