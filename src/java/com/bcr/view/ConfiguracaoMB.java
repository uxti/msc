/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.ConfiguracaoEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Empresa;
import com.bcr.model.Parametrizacao;
import com.bcr.model.Usuario;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import com.bcr.util.UsuarioLogado;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Schedule;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ViewScoped
@ManagedBean
public class ConfiguracaoMB {

    /* EJB's */
    @EJB
    ConfiguracaoEJB cEJB;
    @EJB
    UsuarioEJB usuarioEJB;
    @EJB
    EmpresaEJB empresaEJB;
    /* Objetos */
    private Usuario usuario;
    private Empresa empresa;
    private Parametrizacao parametrizacao;
    private UsuarioLogado ul;
    /* Listas */
    /* Variáveis */
    private String caminhoMySQL, destino, nomeArquivo, nomeArquivoCompactado, cmd;
    private String usuarioSchema = "msc";
    private String senha = "@uxti0823";
    private String BD = "msc";
    private static int TAMANHO_BUFFER = 4096;
    private String log;

    public ConfiguracaoMB() {
        parametrizacao = new Parametrizacao();
        ul = new UsuarioLogado();
        usuario = new Usuario();
        log = "";
    }

    public void novo() {
        parametrizacao = new Parametrizacao();
        ul = new UsuarioLogado();
        usuario = new Usuario();
    }

    public boolean verificarSePodeConfirar() {
        return cEJB.verificaExistenciaDeEmpresa();
    }

    public void executarBasic() {
        cEJB.executarConfiguracaoBasica();
    }

    /*  Início rotina de bkp  */
    /*  Charles - 23/04/2015  */
    public void GerarBackup() {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy_HHmmss");
        criarPastas();
        nomeArquivo = destino + "Backups\\" + getBD() + "_" + dateFormat.format(now);
        nomeArquivoCompactado = destino + "Backups\\" + getBD() + "_" + dateFormat.format(now) + ".zip";
        cmd = getCaminhoMySQL() + "mysqldump -u " + usuarioSchema + " -p" + senha + " -x " + getBD() + " -r " + nomeArquivo + ".sql";
        try {
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            compactar();
            MensageFactory.info("Backup concluído com sucesso. Arquivo salvo em " + destino + "Backups", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar fazer o backup!", null);
            System.out.println(e);
        }
    }

    public void criarPastas() {
        try {
            File diretorio = new File(getDestino());
            File subDiretorio, subDiretorio2;
            boolean success = diretorio.exists();
            if (!diretorio.exists()) {
                success = diretorio.mkdir();
                subDiretorio = new File(getDestino() + "\\Backups");
                if (!subDiretorio.exists()) {
                    success = subDiretorio.mkdir();
                }
                subDiretorio = new File(getDestino() + "\\report");
                if (!subDiretorio.exists()) {
                    success = subDiretorio.mkdir();
                }
                subDiretorio2 = new File(getDestino() + "\\Temp");
                if (!subDiretorio2.exists()) {
                    success = subDiretorio2.mkdir();
                }
            } else {
                subDiretorio = new File(getDestino() + "\\Backups");
                if (!subDiretorio.exists()) {
                    success = subDiretorio.mkdir();
                }
                subDiretorio = new File(getDestino() + "\\report");
                if (!subDiretorio.exists()) {
                    success = subDiretorio.mkdir();
                }
                subDiretorio2 = new File(getDestino() + "\\Temp");
                if (!subDiretorio2.exists()) {
                    success = subDiretorio2.mkdir();
                }
            }
        } catch (Exception e) {
            MensageFactory.error("Erro ao criar a pasta", null);
        }

    }

    public static void compactarParaZip(String arqSaida, String arqEntrada) throws IOException {
        int cont;
        byte[] dados = new byte[TAMANHO_BUFFER];
        BufferedInputStream origem = null;
        FileInputStream streamDeEntrada = null;
        FileOutputStream destino = null;
        ZipOutputStream saida = null;
        ZipEntry entry = null;
        try {
            destino = new FileOutputStream(new File(arqSaida));
            saida = new ZipOutputStream(new BufferedOutputStream(destino));
            File file = new File(arqEntrada);
            streamDeEntrada = new FileInputStream(file);
            origem = new BufferedInputStream(streamDeEntrada, TAMANHO_BUFFER);
            entry = new ZipEntry(file.getName());
            saida.putNextEntry(entry);
            while ((cont = origem.read(dados, 0, TAMANHO_BUFFER)) != -1) {
                saida.write(dados, 0, cont);
            }
            origem.close();
            saida.close();
        } catch (IOException e) {
            throw new IOException(e.getMessage());
        }
    }

    public void compactar() {
        try {
            compactarParaZip(nomeArquivoCompactado, nomeArquivo + ".sql");
            apagarArquivo(nomeArquivo + ".sql");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void apagarArquivo(String caminho) {
        try {
            File f = new File(caminho);
            f.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @PostConstruct
    public void carregarDados() {
        try {
            usuario = usuarioEJB.pegaUsuarioLogadoNaSessao();
        } catch (EJBException e) {
            Bcrutils.descobreErroDeEJBException(e);
            usuario = new Usuario();
        }

        try {
            parametrizacao = cEJB.selecionarParametrizacao(usuario.getIdEmpresa().getIdParametrizacao().getIdParametrizacao());
            destino = parametrizacao.getDestinoBkp();
            caminhoMySQL = parametrizacao.getCaminhoMysql();
        } catch (Exception e) {
            parametrizacao = new Parametrizacao();
            parametrizacao.setDestinoBkp("C:\\MSC\\");
            parametrizacao.setCaminhoMysql("C:\\Program Files (x86)\\MySQL\\MySQL Server 5.5\\bin\\");
        }

    }

    /* Fim rotina de bkp   */
    /* Início método para salvar parametrização  */
    /*  Charles - 23/04/2015  */
    public void salvarParametrizacao() {
        try {
            if (parametrizacao.getIdParametrizacao() == null) {
                cEJB.salvarParametrizacao(parametrizacao);
                empresa = usuario.getIdEmpresa();
                empresa.setIdParametrizacao(parametrizacao);
                empresaEJB.Salvar(empresa);
                MensageFactory.addMensagemPadraoSucesso("salvar");
            } else {
                cEJB.atualizarParametrizacao(parametrizacao);
                destino = parametrizacao.getDestinoBkp();
                caminhoMySQL = parametrizacao.getCaminhoMysql();
                MensageFactory.addMensagemPadraoSucesso("editar");
            }
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

   

    /* Fim método para salvar parametrização    */
    /* Ínicio GET's e SET's */
    public String getCaminhoMySQL() {
        return caminhoMySQL;
    }

    public void setCaminhoMySQL(String caminhoMySQL) {
        this.caminhoMySQL = caminhoMySQL;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getNomeArquivo() {
        return nomeArquivo;
    }

    public void setNomeArquivo(String nomeArquivo) {
        this.nomeArquivo = nomeArquivo;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getBD() {
        return BD;
    }

    public void setBD(String BD) {
        this.BD = BD;
    }

    public Parametrizacao getParametrizacao() {
        return parametrizacao;
    }

    public void setParametrizacao(Parametrizacao parametrizacao) {
        this.parametrizacao = parametrizacao;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public UsuarioLogado getUl() {
        return ul;
    }

    public void setUl(UsuarioLogado ul) {
        this.ul = ul;
    }

    public String getUsuarioSchema() {
        return usuarioSchema;
    }

    public void setUsuarioSchema(String usuarioSchema) {
        this.usuarioSchema = usuarioSchema;
    }

    public int getTAMANHO_BUFFER() {
        return TAMANHO_BUFFER;
    }

    public void setTAMANHO_BUFFER(int TAMANHO_BUFFER) {
        this.TAMANHO_BUFFER = TAMANHO_BUFFER;
    }

    public String getNomeArquivoCompactado() {
        return nomeArquivoCompactado;
    }

    public void setNomeArquivoCompactado(String nomeArquivoCompactado) {
        this.nomeArquivoCompactado = nomeArquivoCompactado;
    }

    public void verlog() {
        log = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(parametrizacao.getCaminhoLog()));
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            log = sb.toString();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void limparLog() {
        log = "";
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }
}