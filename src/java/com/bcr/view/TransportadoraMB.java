/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.CidadeEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.EstadoEJB;
import com.bcr.controller.TransportadoraEJB;
import com.bcr.model.Cidade;
import com.bcr.model.Empresa;
import com.bcr.model.Estado;
import com.bcr.model.Transportadora;
import com.bcr.util.MensageFactory;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class TransportadoraMB {

    @EJB
    TransportadoraEJB tEJB;
    private Transportadora transportadora;
    @EJB
    EmpresaEJB eEJB;
    private Empresa empresa;
    @EJB
    CidadeEJB cEJB;
    private Cidade cidade;
    private List<Cidade> cidades;
    private LazyDataModel<Cidade> cidadesLazy;
    @EJB
    EstadoEJB ufEJB;

    /**
     * Creates a new instance of TransportadoraMB
     */
    public TransportadoraMB() {
        novo();
    }

    public void novo() {
        transportadora = new Transportadora();
        cidade = new Cidade();
        cidades = new ArrayList<Cidade>();
    }

    public void carregarDatatableCidade() {
        cidadesLazy = new LazyDataModel<Cidade>() {
            private static final long serialVersionUID = 1L;

            public List<Cidade> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where c." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }
                setRowCount(Integer.parseInt(cEJB.contarRegistros(Clausula).toString()));
                cidades = cEJB.listarClientesLazy(first, pageSize, Clausula);
                return cidades;

            }
        };
    }

//    public static Document getDocumento(URL url) throws DocumentException {
//        SAXReader reader = new SAXReader();
//        Document document = reader.read(url);
//        return document;
//    }

//    public void pesquisarCEP() throws MalformedURLException, DocumentException {
//        String cep = empresa.getCep();
//        if (cep != null) {
//            String cep2 = cep.replace("-", "");
//            URL url = new URL("http://viacep.com.br/ws/" + cep2 + "/xml/");
//            Document document = getDocumento(url);
//            Element root = document.getRootElement();
//            cidade = new Cidade();
//            String ibge = "";
//            String uf = "";
//            String nomeCidade = "";
//            for (Iterator i = root.elementIterator(); i.hasNext();) {
//                Element element = (Element) i.next();
//                if (element.getQualifiedName().equals("logradouro")) {
//                    empresa.setLogradouro(element.getText());
//                    System.out.println(element.getText());
//                }
//                if (element.getQualifiedName().equals("bairro")) {
//                    empresa.setBairro(element.getText());
//                }
//                if (element.getQualifiedName().equals("ibge")) {
//                    ibge = element.getText();
//                }
//                if (element.getQualifiedName().equals("uf")) {
//                    uf = element.getText();
//                }
//                if (element.getQualifiedName().equals("localidade")) {
//                    nomeCidade = element.getText();
//                    cidade = cEJB.pesquisarCidadePorNome(nomeCidade);
//                }
//            }
//            if (cidade.getIdCidade() == null) {
//                Cidade c = new Cidade();
//                c.setNome(nomeCidade);
//                c.setCep(cep);
//                c.setCodigoIBGE(ibge);
//                Estado ee = new Estado();
//                ee = ufEJB.pesquisarPorSigla(uf);
//                c.setIdEstado(ee);
//                cEJB.Salvar(c);
//                cidade = c;
//            }
//        }
//    }

    public void salvar() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        transportadora.setIdEmpresa(eEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa"))));
        if (transportadora.getIdTransportadora() == null) {
            try {
                if (cidade.getIdCidade() != null) {
                    transportadora.setIdCidade(cidade);
                }
                tEJB.Salvar(transportadora);
                MensageFactory.info("Salvo com sucesso!", null);
                novo();
            } catch (Exception e) {
                MensageFactory.error("Não foi possivel salvar!", null);
                System.out.println("Erro ocorrido foi é: " + e);
            }
        } else {
            if (cidade.getIdCidade() != null) {
                transportadora.setIdCidade(cidade);
            }
            try {
                tEJB.Atualizar(transportadora);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possivel atualizar!", null);
                System.out.println("Erro ocorrido foi é: " + e);
            }
        }
    }

    public void excluir() {
        try {
            tEJB.Excluir(transportadora, transportadora.getIdTransportadora());
            MensageFactory.info("Transportadora excluída com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível excluir a transportadora!", null);
            System.out.println("O erro ocorrido é: " + e);
        }
    }

    public List<Transportadora> listaTransportadoras(){
        return tEJB.ListarTodos();
    }
    
    public List<Transportadora> autoCompleteTransportadora(String para) {
        return tEJB.autoCompleteTransportadora(para);
    }

    public Transportadora getTransportadora() {
        if (transportadora != null) {
            if (transportadora.getIdCidade() != null) {
                cidade = transportadora.getIdCidade();
            }
            return transportadora;
        }else{
            return new Transportadora();
        }       
    }

    public void setTransportadora(Transportadora transportadora) {
        this.transportadora = transportadora;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public LazyDataModel<Cidade> getCidadesLazy() {
        return cidadesLazy;
    }

    public void setCidadesLazy(LazyDataModel<Cidade> cidadesLazy) {
        this.cidadesLazy = cidadesLazy;
    }
}
