/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.TipoPropostaEJB;
import com.bcr.model.TipoProposta;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class TipoPropostaMB implements Serializable {

    @EJB
    TipoPropostaEJB tpEJB;
    private TipoProposta tipoProposta;

    public TipoPropostaMB() {
        novo();
    }

    public void novo() {
        tipoProposta = new TipoProposta();
    }

    public void Salvar() {
        try {
            if (tipoProposta.getIdTipoProposta() == null) {
                tpEJB.Salvar(tipoProposta);
                MensageFactory.addMensagemPadraoSucesso("salvar");
            } else {
                tpEJB.Atualizar(tipoProposta);
                MensageFactory.addMensagemPadraoSucesso("editar");
            }
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }

    }
    
    public void selecionarPorID(Integer id){
        tipoProposta = tpEJB.SelecionarPorID(id);
    }
    
    public void excluir(){
        try {
            tpEJB.Excluir(tipoProposta, tipoProposta.getIdTipoProposta());
            MensageFactory.addMensagemPadraoSucesso("excluir");
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("excluir");
            System.out.println("Erro ocorrido é: " + e);
        }
    }
    
    public List<TipoProposta> listarTodos(){
        return tpEJB.ListarTodos();
    }
    
    public TipoProposta getTipoProposta() {
        return tipoProposta;
    }

    public void setTipoProposta(TipoProposta tipoProposta) {
        this.tipoProposta = tipoProposta;
    }
}
