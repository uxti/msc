package com.bcr.view;

import com.bcr.controller.CargoEJB;
import com.bcr.controller.CidadeEJB;
import com.bcr.controller.ColaboradorEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.EstadoEJB;
import com.bcr.controller.LocalTrabalhoEJB;
import com.bcr.controller.SetorEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Cargo;
import com.bcr.model.Cidade;
import com.bcr.model.Colaborador;
import com.bcr.model.Empresa;
import com.bcr.model.Estado;
import com.bcr.model.LocalTrabalho;
import com.bcr.model.Setor;
import com.bcr.model.Usuario;
import com.bcr.util.MensageFactory;
import com.bcr.validator.ValidarCPF;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.http.HttpServletRequest;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class ColaboradorMB implements Serializable {

    /**
     * Creates a new instance of ColaboradorMB
     */
    @EJB
    ColaboradorEJB cEJB;
    private Colaborador colaborador;
    @EJB
    EmpresaEJB eEJB;
    private Empresa empresa;
    @EJB
    SetorEJB sEJB;
    private Setor setor;
    @EJB
    LocalTrabalhoEJB lEJB;
    private LocalTrabalho localTrabalho;
    @EJB
    CidadeEJB ciEJB;
    private Cidade cidade;
    private List<Cidade> cidades;
    private LazyDataModel<Cidade> cidadesLazy;
    @EJB
    EstadoEJB ufEJB;
    @EJB
    UsuarioEJB uEJB;
    private Usuario usuario;
    @EJB
    CargoEJB caEJB;
    private Cargo cargo;

    public ColaboradorMB() {
        novo();
    }

    public void novo() {
        colaborador = new Colaborador();
        empresa = new Empresa();
        setor = new Setor();
        localTrabalho = new LocalTrabalho();
        colaborador.setDtCadastro(new Date());
        colaborador.setBloqueado(false);
        usuario = new Usuario();
        cidade = new Cidade();
        cargo = new Cargo();
    }

    public Colaborador getColaborador() {
        if (colaborador != null) {
            if (colaborador.getIdSetor() != null) {
                setor = colaborador.getIdSetor();
            }
            if (colaborador.getIdLocalTrabalho() != null) {
                localTrabalho = colaborador.getIdLocalTrabalho();
            }
            if (colaborador.getIdUsuario() != null) {
                usuario = colaborador.getIdUsuario();
            }
            if (colaborador.getIdCidade() != null) {
                cidade = colaborador.getIdCidade();
            }
            if (colaborador.getIdCargo() != null) {
                cargo = colaborador.getIdCargo();
            }
            try {
                return colaborador;

            } catch (Exception e) {
                return new Colaborador();
            }
        }else{
            return new Colaborador();
        }
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Setor getSetor() {
        return setor;
    }

    public void setSetor(Setor setor) {
        this.setor = setor;
    }

    public LocalTrabalho getLocalTrabalho() {
        return localTrabalho;
    }

    public void setLocalTrabalho(LocalTrabalho localTrabalho) {
        this.localTrabalho = localTrabalho;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

    public LazyDataModel<Cidade> getCidadesLazy() {
        return cidadesLazy;
    }

    public void setCidadesLazy(LazyDataModel<Cidade> cidadesLazy) {
        this.cidadesLazy = cidadesLazy;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public void validarCPF() {
        ValidarCPF vCPF = new ValidarCPF();
        if (vCPF.isCPF(colaborador.getCpf())) {
            MensageFactory.info("CPF Válido", null);
        } else {
            MensageFactory.warn("CPF Inválido", null);
        }
    }

    public void pesquisarCEP(Integer enderecoTipo) throws MalformedURLException, DocumentException {
        String cep = colaborador.getCep();
        if (cep != null) {
            String cep2 = cep.replace("-", "");
            URL url = new URL("http://viacep.com.br/ws/" + cep2 + "/xml/");
            Document document = getDocumento(url);
            Element root = document.getRootElement();
            cidade = new Cidade();
            String ibge = "";
            String uf = "";
            String nomeCidade = "";
            for (Iterator i = root.elementIterator(); i.hasNext();) {
                Element element = (Element) i.next();
                if (element.getQualifiedName().equals("logradouro")) {
                    colaborador.setLogradouro(element.getText());
                    System.out.println(element.getText());
                }
                if (element.getQualifiedName().equals("bairro")) {
                    colaborador.setBairro(element.getText());
                }
                if (element.getQualifiedName().equals("ibge")) {
                    ibge = element.getText();
                }
                if (element.getQualifiedName().equals("uf")) {
                    uf = element.getText();
                }
                if (element.getQualifiedName().equals("localidade")) {
                    nomeCidade = element.getText();
                    cidade = ciEJB.pesquisarCidadePorNome(nomeCidade);
                }
            }
            if (cidade.getIdCidade() == null) {
                Cidade c = new Cidade();
                c.setNome(nomeCidade);
                c.setCep(cep);
                c.setCodigoIBGE(ibge);
                Estado ee = new Estado();
                System.out.println("sigla pesquisada é: " + uf);
                ee = ufEJB.pesquisarPorSigla(uf);
                c.setIdEstado(ee);
                ciEJB.Salvar(c);
                cidade = c;
            }
        } else {
            System.out.println("cep normal é nulo");
        }
    }

    public static Document getDocumento(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    }

    public void carregarDatatableCidade() {

        cidadesLazy = new LazyDataModel<Cidade>() {
            private static final long serialVersionUID = 1L;

            public List<Cidade> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where c." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }
                setRowCount(Integer.parseInt(ciEJB.contarRegistros(Clausula).toString()));
                cidades = ciEJB.listarClientesLazy(first, pageSize, Clausula);
                return cidades;

            }
        };
    }

    public void salvar() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        empresa = eEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
        colaborador.setIdEmpresa(empresa);
        if (setor != null) {
            colaborador.setIdSetor(setor);
        }
        if (localTrabalho != null) {
            colaborador.setIdLocalTrabalho(localTrabalho);
        }
        if (usuario != null) {
            colaborador.setIdUsuario(usuario);
        }
        if (cidade.getIdCidade() != null) {
            colaborador.setIdCidade(cidade);
        }
        if (cargo != null) {
            colaborador.setIdCargo(cargo);
        }
        if (colaborador.getIdColaborador() == null) {
            try {
                cEJB.Salvar(colaborador);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        } else {
            try {
                cEJB.Atualizar(colaborador);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }

    public void excluir() {
        try {
            cEJB.Excluir(colaborador, colaborador.getIdColaborador());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }

    public List<Colaborador> listarColaboradores() {
        return cEJB.ListarTodos();
    }
}
