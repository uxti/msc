/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.ProdutoEJB;
import com.bcr.model.Grupo;
import com.bcr.model.Produto;
import com.bcr.model.SubGrupo;
import com.bcr.pojo.ProdutoEstoque;
import com.bcr.util.MensageFactory;
import com.bcr.util.RelatorioFactory;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class EstoqueMB {

    @EJB
    ProdutoEJB pEJB;
    private Produto produto;
    private String tipoMovimento;
    private BigDecimal quantidade;
    private Grupo paramGrupo;
    private SubGrupo paramSubgrupo;
    private String paramEstoque;
    private String paramSituacao;

    public EstoqueMB() {
        novo();
    }

    public void novo() {
        produto = new Produto();
        paramGrupo = new Grupo();
        paramSubgrupo = new SubGrupo();
        quantidade = new BigDecimal(BigInteger.ZERO);
    }

    public void salvar() {
        try {
            if (tipoMovimento.equals("E")) {
                pEJB.EntradaEstoque(produto, quantidade);
            } else {
                pEJB.SaidaEstoque(produto, quantidade);
            }
            novo();
            MensageFactory.info("Estoque atualizado com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível atualizar o estoque!", null);
        }

    }

    public void processarEstoque() {
        List<Produto> listaProds = new ArrayList<Produto>();
        List<Produto> listaProdsProcessada = new ArrayList<Produto>();
        listaProds = pEJB.ListarTodos();

        if (paramGrupo != null) {
            for (Produto p : listaProds) {
                if (p.getIdGrupo().equals(paramGrupo)) {
                    listaProdsProcessada.add(p);
                }
            }
        } else {
            listaProdsProcessada = listaProds;
        }
        listaProds = listaProdsProcessada;
        listaProdsProcessada = new ArrayList<Produto>();



        if (paramSubgrupo != null) {
            for (Produto p : listaProds) {
                if (p.getIdSubgrupo().equals(paramSubgrupo)) {
                    listaProdsProcessada.add(p);
                }
            }
        } else {
            listaProdsProcessada = listaProds;
        }
        listaProds = listaProdsProcessada;
        listaProdsProcessada = new ArrayList<Produto>();


        System.out.println("A Sitaução é: " + paramSituacao);
        if (paramSituacao.equals("A")) {
            for (Produto p : listaProds) {
                if (p.getInativo().equals("0")) {
                    listaProdsProcessada.add(p);
                }
            }
        } else if (paramSituacao.equals("N")) {
            for (Produto p : listaProds) {
                if (p.getInativo().equals("1")) {
                    listaProdsProcessada.add(p);
                }
            }
        } else {
            for (Produto p : listaProds) {
                listaProdsProcessada.add(p);
            }
        }




        listaProds = listaProdsProcessada;
        listaProdsProcessada = new ArrayList<Produto>();


        System.out.println("O Estoque é: " + paramEstoque);
        if (paramEstoque.equals("N")) {
            for (Produto p : listaProds) {
                if (p.getQuantidade().longValue() < 0) {
                    listaProdsProcessada.add(p);
                }
            }
        } else if (paramEstoque.equals("P")) {
            for (Produto p : listaProds) {
                if (p.getQuantidade().longValue() > 0) {
                    listaProdsProcessada.add(p);
                }
            }
        } else if (paramEstoque.equals("Z")) {
            for (Produto p : listaProds) {
                if (p.getQuantidade().longValue() == 0) {
                    listaProdsProcessada.add(p);
                }
            }
        } else {
            for (Produto p : listaProds) {
                listaProdsProcessada.add(p);
            }
        }


        List<ProdutoEstoque> listaEstoque = new ArrayList<ProdutoEstoque>();
        for (Produto p : listaProdsProcessada) {
            ProdutoEstoque pe = new ProdutoEstoque();
            pe.setIdProduto(p.getIdProduto());
            pe.setDescricao(p.getDescricao());
            pe.setCusto(p.getPrecoCusto());
            pe.setEstoque(p.getQuantidade());

            if (p.getIdGrupo() == null) {
                pe.setGrupo("");
            } else {
                pe.setGrupo(p.getIdGrupo().getDescricao());
            }
            if (p.getIdSubgrupo() == null) {
                pe.setGrupo("");
            } else {
                pe.setSubgrupo(p.getIdSubgrupo().getDescricao());
            }
            pe.setUnidade(p.getIdUnidadeEntrada().getDescricaoResumida());
            pe.setTotal(pe.getCusto().multiply(pe.getEstoque()));
            listaEstoque.add(pe);
        }
        System.out.println(listaEstoque.size());
        RelatorioFactory.relatorioListNoParametro("/WEB-INF/Relatorios/Estoque/Relatorio_Estoque.jasper", listaEstoque);
    }

    public void onItemSelect(SelectEvent event) {
        Produto o = (Produto) event.getObject();
//        for (Produto p : pEJB.ListarTodos()) {
//            if (o.getIdProduto().equals(p.getIdProduto())) {
                produto = pEJB.SelecionarPorID(o.getIdProduto());
//            }
//        }
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public String getTipoMovimento() {
        return tipoMovimento;
    }

    public void setTipoMovimento(String tipoMovimento) {
        this.tipoMovimento = tipoMovimento;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public Grupo getParamGrupo() {
        return paramGrupo;
    }

    public void setParamGrupo(Grupo paramGrupo) {
        this.paramGrupo = paramGrupo;
    }

    public SubGrupo getParamSubgrupo() {
        return paramSubgrupo;
    }

    public void setParamSubgrupo(SubGrupo paramSubgrupo) {
        this.paramSubgrupo = paramSubgrupo;
    }

    public String getParamEstoque() {
        return paramEstoque;
    }

    public void setParamEstoque(String paramEstoque) {
        this.paramEstoque = paramEstoque;
    }

    public String getParamSituacao() {
        return paramSituacao;
    }

    public void setParamSituacao(String paramSituacao) {
        this.paramSituacao = paramSituacao;
    }
}
