/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.AtivoEJB;
import com.bcr.controller.ContratoEJB;
import com.bcr.controller.ProdutoEJB;
import com.bcr.controller.PropostaEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Ativo;
import com.bcr.model.Contrato;
import com.bcr.model.Grupo;
import com.bcr.model.Planilha;
import com.bcr.model.PlanilhaItem;
import com.bcr.model.PlanilhaMateriais;
import com.bcr.model.Produto;
import com.bcr.model.Proposta;
import com.bcr.model.PropostaFinanceira;
import com.bcr.model.PropostaItem;
import com.bcr.model.PropostaItemProduto;
import com.bcr.model.PropostaPessoal;
import com.bcr.pojo.GrupoValores;
import com.bcr.pojo.PropostaProduto;
import com.bcr.pojo.PropostaProdutoResumo;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.commons.beanutils.BeanComparator;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class PropostaMB {

    @EJB
    PropostaEJB pEJB;
    @EJB
    ContratoEJB cEJB;
    private PropostaFinanceira propostaFinanceira;
    private Proposta proposta;
    private List<PropostaItem> propostaItens;
    private List<PropostaItemProduto> propostaItensProduto;
    @EJB
    UsuarioEJB uEJB;
    private List<Integer> planilhasEmUso;
    private int posicaoUsada;
    private Contrato contrato;
    @EJB
    ProdutoEJB produtoEJB;
    @EJB
    AtivoEJB ativoEJB;
    private List<PropostaProduto> listaMateriais;
    private Grupo grupoFiltro;
    private List<PropostaProdutoResumo> listaProdutosResumo;
    BigDecimal valorTotalOrcamentoMensal = new BigDecimal(BigInteger.ZERO);
    BigDecimal valorTotalOrcamentoFuncionario = new BigDecimal(BigInteger.ZERO);
    private PropostaItemProduto propostaItemProduto = new PropostaItemProduto();

    public PropostaMB() {
        novo();
    }

    public void novo() {
        propostaFinanceira = new PropostaFinanceira();
        propostaFinanceira.setDtCadastro(new Date());
        proposta = new Proposta();
        propostaItens = new ArrayList<PropostaItem>();
        propostaItensProduto = new ArrayList<PropostaItemProduto>();
        planilhasEmUso = new ArrayList<Integer>();
        posicaoUsada = -1;
        contrato = new Contrato();
        listaMateriais = new ArrayList<PropostaProduto>();
        grupoFiltro = new Grupo();
        listaProdutosResumo = new ArrayList<PropostaProdutoResumo>();
        propostaItemProduto = new PropostaItemProduto();
    }

    public void salvar() {
        try {
            pEJB.Salvar(propostaFinanceira);
            MensageFactory.info("Proposta financeira salva com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível salvar a proposta financeira!", null);
        }
    }

    public void importarMateriais() {
        System.out.println(listaMateriais.size());
        try {
            if (listaMateriais.size() <= 0) {
                System.out.println("entrou no if");
                listaMateriais = new ArrayList<PropostaProduto>();
                for (Produto p : produtoEJB.ListarTodos()) {
                    PropostaProduto pp = new PropostaProduto();
                    pp.setProduto(p);
                    if (p.getMesesMaxDepreciacao() == null) {
                        p.setMesesMaxDepreciacao(1);
                        produtoEJB.Atualizar(p);
                    }
                    pp.setMesesDepreciacao(new BigDecimal(p.getMesesMaxDepreciacao()));
                    pp.setQuantidade(BigDecimal.ZERO);
                    if (p.getTipo().equals("Produto")) {
                        pp.setTipo("Produto");
                    } else if (p.getTipo().equals("Ativo")) {
                        pp.setTipo("Ativo");
                    }
                    listaMateriais.add(pp);
                }
                System.out.println(listaMateriais.size());
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public List<Planilha> listaPlanilhasUsadas() {
        List<Planilha> plUsadas = new ArrayList<Planilha>();
        if (proposta.getPropostaItemList() != null) {
            for (PropostaItemProduto pip : proposta.getPropostaItemProdutoList()) {
                if (plUsadas.indexOf(pip.getIdPlanilha()) == -1) {
                    plUsadas.add(pip.getIdPlanilha());
                }
            }
        }
        return plUsadas;
    }

    public void calcularValoresDosMateriais(PropostaProduto propostaProduto) {
//        System.out.println("Quantidade: " + propostaProduto.getQuantidade());
//        System.out.println("Custo: " + propostaProduto.getProduto().getPrecoCusto());
        try {
            if (propostaProduto.getProduto().getPrecoCusto() == null) {
                propostaProduto.getProduto().setPrecoCusto(new BigDecimal(BigInteger.ZERO));
            }
            if (propostaProduto.getQuantidade() == null) {
                propostaProduto.setQuantidade(new BigDecimal(BigInteger.ZERO));
            }
            propostaProduto.setValorTotal(propostaProduto.getQuantidade().multiply(propostaProduto.getProduto().getPrecoCusto()));
            propostaProduto.setValorTotalDepreciado(propostaProduto.getValorTotal().divide(propostaProduto.getMesesDepreciacao()));
        } catch (Exception e) {
            propostaProduto.setValorTotal(new BigDecimal(BigInteger.ZERO));
            propostaProduto.setValorTotalDepreciado(new BigDecimal(BigInteger.ZERO));
        }
    }

    public void importarMateriaisSelecionados() {
        List<PropostaItemProduto> lista = new ArrayList<PropostaItemProduto>();
        for (PropostaProduto pp : listaMateriais) {
            if (pp.getQuantidade().longValue() > 0) {
                PropostaItemProduto pip = new PropostaItemProduto();
                pip.setIdProduto(pp.getProduto());
                pip.setMesesDepreciacao(pp.getMesesDepreciacao());
                pip.setQuantidade(pp.getQuantidade());
                pip.setValorUnitario(pp.getProduto().getPrecoCusto());
                pip.setTotal(pp.getValorTotal());
                pip.setValorDepreciacao(pp.getValorTotalDepreciado());
                lista.add(pip);
            }
        }
        proposta.setPropostaItemProdutoList(lista);
        carregarResumoPropostaItem(proposta);
    }

    public void carregarResumoPropostaItem(Proposta p) {
        List<PropostaProdutoResumo> listaPPR = new ArrayList<PropostaProdutoResumo>();
        List<Grupo> gruposUsados = new ArrayList<Grupo>();
        if (proposta.getValorDesconto() == null) {
            proposta.setValorDesconto(new BigDecimal(BigInteger.ZERO));
        }
        if (proposta.getPercentualAplicado() == null) {
            proposta.setPercentualAplicado(new BigDecimal(BigInteger.ZERO));
        }
        //Verifica todos os grupos que tem sem repetir.
        if (p.getPropostaItemProdutoList() != null) {
            for (PropostaItemProduto pip : p.getPropostaItemProdutoList()) {
                if (gruposUsados.indexOf(pip.getIdProduto().getIdGrupo()) < 0) {
                    gruposUsados.add(pip.getIdProduto().getIdGrupo());
                }
            }
            for (Grupo g : gruposUsados) {
                PropostaProdutoResumo ppr = new PropostaProdutoResumo();
                BigDecimal valorTotal = new BigDecimal(BigInteger.ZERO);
                ppr.setGrupo(g);
                for (PropostaItemProduto pip : p.getPropostaItemProdutoList()) {
                    if (g.getIdGrupo() == pip.getIdProduto().getIdGrupo().getIdGrupo()) {
                        valorTotal = valorTotal.add(pip.getTotal());
                    }
                }
                ppr.setCusto(valorTotal);
                if (p.getNumeroFuncionario() == 0) {
                    p.setNumeroFuncionario(1);
                }
                ppr.setCustoPorFuncionario(ppr.getCusto().divide(new BigDecimal(p.getNumeroFuncionario())));
                listaPPR.add(ppr);
            }
        }

        listaProdutosResumo = listaPPR;

        valorTotalOrcamentoMensal = new BigDecimal(BigInteger.ZERO);
        valorTotalOrcamentoFuncionario = new BigDecimal(BigInteger.ZERO);

        for (PropostaProdutoResumo ppp : listaProdutosResumo) {
            valorTotalOrcamentoMensal = valorTotalOrcamentoMensal.add(ppp.getCusto());
            valorTotalOrcamentoFuncionario = valorTotalOrcamentoFuncionario.add(ppp.getCustoPorFuncionario());
        }
        if (proposta.getValorDesconto() == null) {
            proposta.setValorDesconto(new BigDecimal(BigInteger.ZERO));
        }
        if (proposta.getPercentualAplicado() == null) {
            proposta.setValorDesconto(new BigDecimal(BigInteger.ZERO));
        }

        BigDecimal difMensal = ((valorTotalOrcamentoMensal.multiply(proposta.getPercentualAplicado())).divide(new BigDecimal(100)));
        BigDecimal difFuncio = ((valorTotalOrcamentoFuncionario.multiply(proposta.getPercentualAplicado())).divide(new BigDecimal(100)));

        valorTotalOrcamentoMensal = valorTotalOrcamentoMensal.add(difMensal);
        valorTotalOrcamentoFuncionario = valorTotalOrcamentoFuncionario.add(difFuncio);

//        proposta.setValorTotal(valorTotalOrcamentoMensal);

    }

    public void salvarPropostaComplete() {
        proposta.setIdEmpresa(uEJB.pegaEmpresaLogadoNaSessao());
        proposta.setIdUsuario(uEJB.pegaUsuarioLogadoNaSessao());
        try {
            if (proposta.getDtEmissao() != null) {
                if (propostaFinanceira.getPropostaList() == null) {
                    List<Proposta> props = new ArrayList<Proposta>();
                    props.add(proposta);
                    propostaFinanceira.setPropostaList(props);
                } else {
                    if (posicaoUsada != -1) {
                        propostaFinanceira.getPropostaList().set(posicaoUsada, proposta);
                        posicaoUsada = -1;
                    } else {
                        propostaFinanceira.getPropostaList().add(proposta);
                    }
                }
            }
            propostaFinanceira.setValorTotal(new BigDecimal(BigInteger.ZERO));
            BigDecimal valorTotalPropostaFinanceira = new BigDecimal(BigInteger.ZERO);
            if (!propostaFinanceira.getPropostaList().isEmpty()) {
                for (Proposta p : propostaFinanceira.getPropostaList()) {
                    if (p.getPropostaItemList() != null) {
                        calcularValorTotalProposta(p);
                        System.out.println("valor da proposta " + p.getValorTotal());
                        valorTotalPropostaFinanceira = valorTotalPropostaFinanceira.add(p.getValorTotal());
                    } else {
                        valorTotalPropostaFinanceira = new BigDecimal(BigInteger.ZERO);
                    }
                }
                propostaFinanceira.setValorTotal(valorTotalPropostaFinanceira);
            }
            pEJB.SalvarPropostaCompleta(propostaFinanceira);
            novo();
            MensageFactory.info("Proposta salva com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Erro ao tentar salvar a proposta!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void calcularValorTotalProposta(Proposta proposta) {
        BigDecimal valorItem = new BigDecimal(BigInteger.ZERO);
        if (proposta.getPropostaItemList() != null) {
            for (PropostaItem pi : proposta.getPropostaItemList()) {
                if (pi.getRegistroTotalizador() != null) {
                    if (pi.getRegistroTotalizador().equals("S")) {
                        if (pi.getValor() == null) {
                            pi.setValor(new BigDecimal(BigInteger.ZERO));
                        }
                        valorItem = valorItem.add(pi.getValor());
                    }
                }
            }
        }
        proposta.setValorTotal(valorItem);
    }

    public void importarPlanilhas(Planilha planilha) {
        propostaItens = new ArrayList<PropostaItem>();
        for (PlanilhaItem pi : planilha.getPlanilhaItemList()) {
            PropostaItem poi = new PropostaItem();
            poi.setAcao(pi.getAcao());
            poi.setDescricao(pi.getDescricao());
            poi.setGrupo(pi.getGrupo());
            poi.setIdPlanilhaItem(pi);
            poi.setPercentual(pi.getPercentual());
            poi.setPercentualEditavel(pi.getPercentualEditavel());
            poi.setPosicao(pi.getPosicao());
            poi.setPosicaoFinal(pi.getPosicaoFinal());
            poi.setPosicaoInicial(pi.getPosicaoInicial());
            poi.setTipo(pi.getTipo());
            poi.setValor(pi.getValor());
            poi.setValorEditavel(pi.getValorEditavel());
            poi.setRegistroTotalizador(pi.getRegistroTotalizador());
            poi.setIdPlanilhaItem(pi);
            poi.setIdPlanilha(pi.getIdPlanilha());
            poi.setIdGrupo(pi.getIdGrupo());
            propostaItens.add(poi);
        }
        if (planilha.getTipo().equals("M")) {
            Comparator<PropostaItem> comp = new BeanComparator("posicao");
            Collections.sort(propostaItens, comp);
        }
        if (proposta.getPropostaItemList() == null) {
            proposta.setPropostaItemList(propostaItens);
        } else {
            proposta.getPropostaItemList().addAll(propostaItens);
        }

        if (planilha.getPlanilhaMateriaisList().size() > 0) {
            for (PlanilhaMateriais pm : planilha.getPlanilhaMateriaisList()) {
                PropostaItemProduto pip = new PropostaItemProduto();
                pip.setIdProduto(pm.getIdProduto());
                if (pm.getIdProduto().getPrecoCusto() == null) {
                    pm.getIdProduto().setPrecoCusto(new BigDecimal(BigInteger.ZERO));
                }
                pip.setValorUnitario(pm.getIdProduto().getPrecoCusto());
                pip.setValorDepreciacao(new BigDecimal(pip.getValorUnitario().longValue() / pm.getMesesDepreciado()));
                pip.setQuantidade(pm.getQuantidade());
                pip.setTotal(pip.getQuantidade().multiply(pip.getValorUnitario()));
                pip.setIdPlanilha(pm.getIdPlanilha());
                pip.setMesesDepreciacao(new BigDecimal(pm.getMesesDepreciado()));
                pip.setValorTotalDepreciacao(pip.getQuantidade().multiply(pip.getValorDepreciacao()));
                try {
                    proposta.getPropostaItemProdutoList().add(pip);
                    pip = new PropostaItemProduto();
                } catch (Exception e) {
                    proposta.setPropostaItemProdutoList(new ArrayList<PropostaItemProduto>());
                    proposta.getPropostaItemProdutoList().add(pip);
                    pip = new PropostaItemProduto();
                }
            }
        } else {
            System.out.println("foi no else");
            PropostaItemProduto pip = new PropostaItemProduto();
            pip.setIdPlanilha(planilha);
            List<PropostaItemProduto> pips = new ArrayList<PropostaItemProduto>();
            pips.add(pip);
            proposta.setPropostaItemProdutoList(pips);
        }

        processarPlanilhasEmUso();
    }

    public void calcularTotaisPropostaItemProduto(PropostaItemProduto pip, String tipo) {
        if (pip != null) {
            if (tipo.equals("1")) {
                pip.setTotal(pip.getQuantidade().multiply(pip.getValorUnitario()));
                pip.setValorTotalDepreciacao(pip.getValorDepreciacao().multiply(pip.getQuantidade()));
            } else {
                pip.setValorDepreciacao(new BigDecimal(pip.getValorUnitario().longValue() / pip.getMesesDepreciacao().longValue()));
                pip.setValorTotalDepreciacao(pip.getValorDepreciacao().multiply(pip.getQuantidade()));
            }
        }
    }

    public void adicionarItem(Planilha p) {
        PropostaItemProduto pip = new PropostaItemProduto();
        System.out.println(p.getIdPlanilha());
        try {
            pip.setIdPlanilha(p);
            try {
                proposta.getPropostaItemProdutoList().add(pip);
            } catch (Exception e) {
                proposta.setPropostaItemList(new ArrayList<PropostaItem>());
                List<PropostaItemProduto> pips = new ArrayList<PropostaItemProduto>();
                pips.add(pip);
                proposta.setPropostaItemProdutoList(pips);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void adicionarPessoal() {
        PropostaPessoal pp = new PropostaPessoal();
        pp.setQuantidade(1);
        try {
            proposta.getPropostaPessoalList().add(pp);
        } catch (Exception e) {
            proposta.setPropostaPessoalList(new ArrayList<PropostaPessoal>());
            proposta.getPropostaPessoalList().add(pp);
        }
        System.out.println(proposta.getPropostaPessoalList().size());
    }

    public void excluirPropostaProdutoItem(PropostaItemProduto pip, int index) {
        try {
            if (pip.getIDPropostaItemProduto() != null) {
                pEJB.excluirPropostaItemProduto(pip);
            }
            System.out.println("Essa lista tem antes " + proposta.getPropostaItemProdutoList().size());
            proposta.getPropostaItemProdutoList().remove(index);
            System.out.println("Essa lista tem " + proposta.getPropostaItemProdutoList().size());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void excluirPropostaPlanilha(int idPlanilha) {
        List<PropostaItem> itens = new ArrayList<PropostaItem>();
        List<PropostaItemProduto> materiais = new ArrayList<PropostaItemProduto>();


        for (PropostaItem pi : proposta.getPropostaItemList()) {
            if (pi.getIdPlanilha().getIdPlanilha().equals(idPlanilha)) {
                itens.add(pi);
            }
        }
        for (PropostaItemProduto pip : proposta.getPropostaItemProdutoList()) {
            if (pip.getIdPlanilha().getIdPlanilha().equals(idPlanilha)) {
                materiais.add(pip);
            }
        }

        for (PropostaItem pi : itens) {
            if (pi.getIdPlanilhaItem() != null) {
                pEJB.excluirPropostaItem(pi);
            }

        }
        for (PropostaItemProduto pip : materiais) {
            if (pip.getIDPropostaItemProduto() != null) {
                System.out.println(pip.getIDPropostaItemProduto());
                pEJB.excluirPropostaItemProduto(pip);
            }
        }

        proposta.getPropostaItemList().removeAll(itens);
        proposta.getPropostaItemProdutoList().removeAll(materiais);
    }

    public void selecionarProduto(PropostaItemProduto p) {
        if (p.getQuantidade() == null) {
            p.setQuantidade(new BigDecimal(BigInteger.ONE));
        }
        p.setValorUnitario(p.getIdProduto().getPrecoCusto());
        p.setTotal(p.getQuantidade().multiply(p.getValorUnitario()));
        if (p.getMesesDepreciacao() == null) {
            p.setMesesDepreciacao(new BigDecimal(BigInteger.ONE));
        }
        p.setValorDepreciacao(p.getTotal().divide(p.getMesesDepreciacao()));
    }

    public void importarPlanilhaProdutos(Planilha planilha) {
        propostaItensProduto = new ArrayList<PropostaItemProduto>();
        for (PlanilhaItem pi : planilha.getPlanilhaItemList()) {
            PropostaItemProduto pip = new PropostaItemProduto();
            pip.setIdAtivo(pi.getIdAtivo());
            pip.setIdPlanilhaItem(pi);
            pip.setIdProduto(pi.getIdProduto());
            pip.setIdProposta(pip.getIdProposta());
            propostaItensProduto.add(pip);
        }
        if (proposta.getPropostaItemProdutoList() == null) {
            proposta.setPropostaItemProdutoList(propostaItensProduto);
        } else {
            proposta.getPropostaItemProdutoList().addAll(propostaItensProduto);
        }
        processarPlanilhasEmUso();
    }

    public void calcularValoresProdutos(PropostaItemProduto pip) {
        if (pip.getIdPlanilhaItem().getIdAtivo() != null && pip.getQuantidade() != null) {
            pip.setTotal(pip.getQuantidade().multiply(pip.getIdPlanilhaItem().getIdAtivo().getValorCustoMensal()));
        }
        if (pip.getIdPlanilhaItem().getIdProduto() != null && pip.getQuantidade() != null) {
            pip.setTotal(pip.getQuantidade().multiply(pip.getIdProduto().getPrecoCusto()));
        }
    }

    public void calcularValores(Planilha planilha) {

        for (PropostaItem pi : proposta.getPropostaItemList()) {
            if (pi.getIdPlanilhaItem().getIdPlanilha() == planilha) {
                if (pi.getIdPlanilhaItem().getIdProduto() == null && pi.getIdPlanilhaItem().getIdAtivo() == null) {
                    if (pi.getValor() == null) {
                        pi.setValor(BigDecimal.ZERO);
                    }
                }
            }
        }

        for (PropostaItem pi : proposta.getPropostaItemList()) {
            if (pi.getIdPlanilhaItem().getIdPlanilha() == planilha) {
                if (pi.getIdPlanilhaItem().getIdProduto() == null && pi.getIdPlanilhaItem().getIdAtivo() == null) {
                    if (pi.getPosicaoInicial() != null) {
                        if (!pi.getPosicaoInicial().isEmpty()) {
                            pi.setValor(BigDecimal.ZERO);
                        }
                    }
                }
            }

        }

        for (PropostaItem pi : proposta.getPropostaItemList()) {
            if (pi.getIdPlanilhaItem().getIdPlanilha() == planilha) {
                if (pi.getIdPlanilhaItem().getIdProduto() == null && pi.getIdPlanilhaItem().getIdAtivo() == null) {
                    if (!pi.getPosicaoInicial().isEmpty()) {
                        String posInicial = pi.getPosicaoInicial();
                        String posFinal = pi.getPosicaoFinal();

                        List<BigDecimal> bd = new ArrayList<BigDecimal>();
                        System.out.println(pi.getDescricao());
                        if (!pi.getPosicaoFinal().contains("P")) {
                            if (pi.getAcao().equals("++")) {
                                BigDecimal valPosIni = new BigDecimal(BigInteger.ZERO);
                                BigDecimal valPosFim = new BigDecimal(BigInteger.ZERO);
                                for (PropostaItem pi2 : proposta.getPropostaItemList()) {
                                    if (pi.getPosicaoInicial().equals(pi2.getPosicao().toString())) {
                                        valPosIni = pi2.getValor();
                                    }
                                    if (pi.getPosicaoFinal().equals(pi2.getPosicao().toString())) {
                                        valPosFim = pi2.getValor();
                                    }
                                }
                                pi.setValor(retornaValor(pi.getAcao(), valPosIni, valPosFim));
                            } else if (pi.getAcao().equals("--")) {
                                BigDecimal valPosIni = new BigDecimal(BigInteger.ZERO);
                                BigDecimal valPosFim = new BigDecimal(BigInteger.ZERO);
                                for (PropostaItem pi2 : proposta.getPropostaItemList()) {
                                    if (pi.getPosicaoInicial().equals(pi2.getPosicao().toString())) {
                                        valPosIni = pi2.getValor();
                                    }
                                    if (pi.getPosicaoFinal().equals(pi2.getPosicao().toString())) {
                                        valPosFim = pi2.getValor();
                                    }
                                }
                                pi.setValor(retornaValor(pi.getAcao(), valPosIni, valPosFim));
                            } else if (pi.getAcao().equals("//")) {
                                BigDecimal valPosIni = new BigDecimal(BigInteger.ZERO);
                                BigDecimal valPosFim = new BigDecimal(BigInteger.ZERO);
                                for (PropostaItem pi2 : proposta.getPropostaItemList()) {
                                    if (pi.getPosicaoInicial().equals(pi2.getPosicao().toString())) {
                                        valPosIni = pi2.getValor();
                                    }
                                    if (pi.getPosicaoFinal().equals(pi2.getPosicao().toString())) {
                                        valPosFim = pi2.getValor();
                                    }
                                }
                                System.out.println("Ação " + pi.getAcao());
                                System.out.println("Inicial " + valPosIni);
                                System.out.println("Final " + valPosFim);
                                pi.setValor(retornaValor(pi.getAcao(), valPosIni, valPosFim));
                            } else if (pi.getAcao().equals("**")) {
                                BigDecimal valPosIni = new BigDecimal(BigInteger.ZERO);
                                BigDecimal valPosFim = new BigDecimal(BigInteger.ZERO);
                                for (PropostaItem pi2 : proposta.getPropostaItemList()) {
                                    if (pi.getPosicaoInicial().equals(pi2.getPosicao().toString())) {
                                        valPosIni = pi2.getValor();
                                    }
                                    if (pi.getPosicaoFinal().equals(pi2.getPosicao().toString())) {
                                        valPosFim = pi2.getValor();
                                    }
                                }
                                pi.setValor(retornaValor(pi.getAcao(), valPosIni, valPosFim));
                            } else {
                                for (int i = Integer.parseInt(posInicial); i < Integer.parseInt(posFinal.replace("P", "")) + 1; i++) {
                                    for (PropostaItem pi2 : proposta.getPropostaItemList()) {
                                        if (pi.getIdPlanilhaItem().getIdPlanilha() == planilha) {
                                            if (pi2.getPosicao().equals(i) && pi2.getIdPlanilhaItem().getIdPlanilha().equals(planilha)) {
                                                if (pi.getAcao().equals("*") || pi.getAcao().equals("/")) {
                                                    if (pi2.getValor() == null) {
                                                        pi2.setValor(new BigDecimal(BigInteger.ZERO));
                                                    }
                                                    bd.add(pi2.getValor());

                                                } else {
                                                    pi.setValor(retornaValor(pi.getAcao(), pi2.getValor(), pi.getValor()));
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            if (pi.getAcao().equals("*") || pi.getAcao().equals("/")) {
                                System.out.println(pi.getDescricao());
                                System.out.println("bd0 " + bd.get(0));
                                System.out.println("bd1 " + bd.get(1));
                                pi.setValor(retornaValor(pi.getAcao(), bd.get(0), bd.get(1)));
                            }
                        } else {
//                            System.out.println("vamos calcular o item da posição (%)" + pi.getPosicao());
                            String posInicialp = pi.getPosicaoInicial();
                            for (PropostaItem pi2 : proposta.getPropostaItemList()) {
                                if (pi.getIdPlanilhaItem().getIdPlanilha() == planilha) {
                                    if (pi2.getPosicao().toString().equals(posInicialp) && pi2.getIdPlanilhaItem().getIdPlanilha().equals(planilha)) {
//                                        System.out.println("achou a posição inicial do item " + pi2.getPosicao());
//                                        System.out.println("valor dela é  " + pi2.getValor());
                                        if (pi.getAcao().equals("+")) {
                                            pi.setValor(retornaValor(pi.getAcao(), pi.getValor(), pi2.getValor()));
                                        } else if (pi.getAcao().equals("%")) {
//                                            System.out.println("vai calcular o %");
//                                            System.out.println("valor do campo do percentual " + pi.getPercentual());
                                            pi.setValor(retornaValor(pi.getAcao(), pi2.getValor(), pi.getPercentual()));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }


        if (proposta.getPropostaItemProdutoList().size() > 0 && proposta.getPropostaItemProdutoList().get(0).getIdProduto() != null) {
            List<GrupoValores> gvs = new ArrayList<GrupoValores>();
            List<Grupo> gruposUsados = new ArrayList<Grupo>();
            for (PropostaItemProduto pip : proposta.getPropostaItemProdutoList()) {
                if (gruposUsados.indexOf(pip.getIdProduto().getIdGrupo()) < 0) {
                    gruposUsados.add(pip.getIdProduto().getIdGrupo());
                }
            }

            for (Grupo gp : gruposUsados) {
                GrupoValores gv = new GrupoValores();
                gv.setGrupo(gp);
                for (PropostaItemProduto pip : proposta.getPropostaItemProdutoList()) {
                    if (pip.getIdProduto().getIdGrupo().equals(gp)) {
                        if (gv.getValor() == null) {
                            gv.setValor(new BigDecimal(BigInteger.ZERO));
                        }
                        gv.setValor(gv.getValor().add(pip.getValorDepreciacao()));
                    }
                }
                gvs.add(gv);
            }

            for (PropostaItem pi : proposta.getPropostaItemList()) {
                if (pi.getIdGrupo() != null) {
                    for (GrupoValores gvss : gvs) {
                        if (pi.getIdGrupo().equals(gvss.getGrupo())) {
                            pi.setValor(gvss.getValor());
                        }
                    }
                }
            }


        }



        for (int x = 0; x < 20; x++) {
            for (int y = 0; y < 20; y++) {
                for (int i = 0; i < 20; i++) {
                    RequestContext rc = RequestContext.getCurrentInstance();
                    rc.update("frmNovaProposta:tabsOne:tabs:grp1:" + x + ":idMinPanel:tabtwo:agrp:" + y + ":grp2:" + i + ":tblMao");
                }
            }
        }





        BigDecimal valorDaProposta = new BigDecimal(BigInteger.ZERO);
        for (PropostaItem pi2 : proposta.getPropostaItemList()) {
            if (pi2.getRegistroTotalizador().equals("S")) {
                valorDaProposta = valorDaProposta.add(pi2.getValor());
            }
        }
        proposta.setValorTotal(valorDaProposta);
        RequestContext rr = RequestContext.getCurrentInstance();
        rr.update("frmNovaProposta:tabsOne:valorTotal");

    }

    public BigDecimal retornaValor(String acao, BigDecimal val1, BigDecimal val2) {
        BigDecimal result = new BigDecimal(BigInteger.ZERO);
        if (val1 == null) {
            val1 = new BigDecimal(BigInteger.ZERO);
        }
        if (val2 == null) {
            val2 = new BigDecimal(BigInteger.ZERO);
        }
        if (acao.equals("+")) {
            result = val1.add(val2);
        } else if (acao.equals("++")) {
            result = val1.add(val2);
        } else if (acao.equals("-")) {
            result = val1.subtract(val2);
        } else if (acao.equals("--")) {
            result = val1.subtract(val2);
        } else if (acao.equals("*")) {
            result = val1.multiply(val2);
        } else if (acao.equals("/")) {
            result = val1.divide(val2, BigDecimal.ROUND_UP);
        } else if (acao.equals("//")) {
            try {
                result = val1.divide(val2, BigDecimal.ROUND_UP);
            } catch (Exception e) {
                Float f = val1.floatValue() / val2.floatValue();
                result = new BigDecimal(f);
            }
        } else if (acao.equals("%")) {
            result = val1.multiply(val2).divide(new BigDecimal(100));
        } else if (acao.equals("**")) {
            result = val1.multiply(val2);
        }
        return result;
    }

    public void addProposta(PropostaFinanceira propostaFinanceira) {
        this.propostaFinanceira = propostaFinanceira;
        proposta = new Proposta();
        proposta.setDtEmissao(new Date());
        proposta.setStatus("A");
    }

    public void novaProposta(PropostaFinanceira propostaFinanceira, int index) {
        this.propostaFinanceira = propostaFinanceira;
        proposta = new Proposta();
        proposta.setDtEmissao(new Date());

        posicaoUsada = index;
        if (propostaFinanceira.getPropostaList().size() > 0) {
            proposta = propostaFinanceira.getPropostaList().get(index);
            if (proposta.getPropostaItemList() != null) {
                Comparator<PropostaItem> comp = new BeanComparator("posicao");
                Collections.sort(proposta.getPropostaItemList(), comp);
            }
            processarPlanilhasEmUso();
        }
        carregarResumoPropostaItem(proposta);
    }

    public void excluirProposta(Proposta proposta) {
        try {
            pEJB.excluir(proposta);
            MensageFactory.info("Proposta excluída com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.fatal("Não foi possível excluír a proposta!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void editarProposta(Proposta p, PropostaFinanceira pf) {
        proposta = p;
        propostaFinanceira = pf;
    }

    public boolean verificarPlanilhaProdutoEmUso(Planilha planilha) {
        Boolean situacao = false;
        if (proposta.getPropostaItemList() != null && proposta.getPropostaItemList().size() > 0) {
            if (planilhasEmUso.indexOf(planilha.getIdPlanilha()) > -1) {
                situacao = true;
            }
        }
        return situacao;
    }

    public boolean verificarPlanilhaMaoEmUso(Planilha planilha) {
        Boolean situacao = false;
        if (proposta.getPropostaItemProdutoList() != null && proposta.getPropostaItemProdutoList().size() > 0) {
            if (planilhasEmUso.indexOf(planilha.getIdPlanilha()) > -1) {
                situacao = true;
            }
        }
        return situacao;
    }

    //acesse o link http://3.bp.blogspot.com/-sOIOvRYlNuM/VHJPoywmOmI/AAAAAAAANJQ/n2WZaXevCss/s1600/thisis.png hue
    public void gerarContrato(Proposta p) {
        contrato = new Contrato();
        contrato.setIdCliente(p.getIdPropostaFinanceira().getIdCliente());
        contrato.setIdEmpresa(uEJB.pegaEmpresaLogadoNaSessao());
        contrato.setIdPropostaFinanceira(p.getIdPropostaFinanceira());
        contrato.setIdUsuario(uEJB.pegaUsuarioLogadoNaSessao());
        contrato.setDtInicio(new Date());
        contrato.setStatus("Aberto");
        contrato.setIdProposta(p);
        contrato.setIdPropostaFinanceira(p.getIdPropostaFinanceira());
        contrato.setValor(p.getValorTotal());

    }

    public Integer numeroDoContratoAtual() {
        return cEJB.ultimoContrato() + 1;
    }

    public void salvarContrato() {
        try {
            cEJB.Salvar(contrato);
            contrato = new Contrato();
            MensageFactory.info("Contrato salvo com sucesso!", null);
        } catch (EJBException e) {
            Bcrutils.descobreErroDeEJBException(e);
//            MensageFactory.error("Nao foi possível salvar o contrato!", null);
//            Bcrutils.escreveLogErro(e);
        }
    }

    public void excluirPessoal(PropostaPessoal pp, int index) {
        if (pp.getIdPropostaPessoal() != null) {
            pEJB.excluirPropostaPessoa(pp);
        }
        proposta.getPropostaPessoalList().remove(index);
    }

    public List<PropostaFinanceira> listarPropostasFinanceiras() {
        return pEJB.ListarTodos();
    }

    public PropostaFinanceira getPropostaFinanceira() {
        return propostaFinanceira;
    }

    public void setPropostaFinanceira(PropostaFinanceira propostaFinanceira) {
        this.propostaFinanceira = propostaFinanceira;
    }

    public Proposta getProposta() {
        return proposta;
    }

    public void setProposta(Proposta proposta) {
        this.proposta = proposta;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    private void processarPlanilhasEmUso() {
        if (proposta.getPropostaItemList() != null) {
            for (PropostaItem pi : proposta.getPropostaItemList()) {
                if (planilhasEmUso.indexOf(pi.getIdPlanilhaItem().getIdPlanilha().getIdPlanilha()) == -1) {
                    planilhasEmUso.add(pi.getIdPlanilhaItem().getIdPlanilha().getIdPlanilha());
                }
            }
        }

    }

    public List<PropostaProduto> getListaMateriais() {
        return listaMateriais;
    }

    public void setListaMateriais(List<PropostaProduto> listaMateriais) {
        this.listaMateriais = listaMateriais;
    }

    public Grupo getGrupoFiltro() {
        return grupoFiltro;
    }

    public void setGrupoFiltro(Grupo grupoFiltro) {
        this.grupoFiltro = grupoFiltro;
    }

    public List<PropostaItemProduto> getPropostaItensProduto() {
        return propostaItensProduto;
    }

    public void setPropostaItensProduto(List<PropostaItemProduto> propostaItensProduto) {
        this.propostaItensProduto = propostaItensProduto;
    }

    public List<PropostaProdutoResumo> getListaProdutosResumo() {
        return listaProdutosResumo;
    }

    public void setListaProdutosResumo(List<PropostaProdutoResumo> listaProdutosResumo) {
        this.listaProdutosResumo = listaProdutosResumo;
    }

    public BigDecimal getValorTotalOrcamentoMensal() {
        return valorTotalOrcamentoMensal;
    }

    public void setValorTotalOrcamentoMensal(BigDecimal valorTotalOrcamentoMensal) {
        this.valorTotalOrcamentoMensal = valorTotalOrcamentoMensal;
    }

    public BigDecimal getValorTotalOrcamentoFuncionario() {
        return valorTotalOrcamentoFuncionario;
    }

    public void setValorTotalOrcamentoFuncionario(BigDecimal valorTotalOrcamentoFuncionario) {
        this.valorTotalOrcamentoFuncionario = valorTotalOrcamentoFuncionario;
    }

    public PropostaItemProduto getPropostaItemProduto() {
        return propostaItemProduto;
    }

    public void setPropostaItemProduto(PropostaItemProduto propostaItemProduto) {
        this.propostaItemProduto = propostaItemProduto;
    }
}
