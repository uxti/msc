package com.bcr.view;

import com.bcr.controller.*;
import com.bcr.model.*;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import com.bcr.util.UsuarioLogado;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.StreamedContent;

@ManagedBean
@ViewScoped
public class ChequeMB {

    @EJB
    ChequeEJB chequeEJB;
    private Cheque cheque;
    private Cheque chequeExcluir;
    @EJB
    ContaCorrenteEJB contaCorrenteEJB;
    private ContaCorrente contaCorrente;
    @EJB
    FornecedorEJB fornecedorEJB;
    private Fornecedor fornecedor;
    @EJB
    PagamentoEJB pagamentoEJB;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    CaixaEJB caixaEJB;
    private ContasReceber contasReceber;
    private Banco banco;
    private Colaborador colaborador;
    private Usuario usuario;
    private UsuarioLogado usuarioLogado;
    private Empresa empresa;
    private ContasPagar contasPagar;
    private List<Cheque> listaCheque, listagemCheques, listaChequeSelecionado;
    private List<Colaborador> listaColaborador;
    private List<ContaCorrente> listaContaCorrente;
    private List<Fornecedor> listaFornecedor;
    private Double valorTotal;
    private boolean flag;
    private BigDecimal widgetValorPago, widgetValorRestante, widgetValorTotal, valorTotalSelecionado;
    private String status, por;
    private Date dtIni, dtFim;
    private StreamedContent file;

    public ChequeMB() {
        novo();
    }

    public void novo() {
        cheque = new Cheque();
        chequeExcluir = new Cheque();
        colaborador = new Colaborador();
        usuario = new Usuario();
        contaCorrente = new ContaCorrente();
        empresa = new Empresa();
        fornecedor = new Fornecedor();
        usuarioLogado = new UsuarioLogado();
        contasPagar = new ContasPagar();
        contasReceber = new ContasReceber();
        banco = new Banco();
        listaCheque = new ArrayList<Cheque>();
        listagemCheques = new ArrayList<Cheque>();
        listaChequeSelecionado = new ArrayList<Cheque>();
        listaContaCorrente = new ArrayList<ContaCorrente>();
        listaFornecedor = new ArrayList<Fornecedor>();
        valorTotal = 0D;
        valorTotalSelecionado = BigDecimal.ZERO;
        cheque.setDtRecebimentoEmissao(new Date());
        //cheque.setDtCompensacaoVencimento(Bcrutils.addDia(new Date(), uEJB.retornaParametrizacaoEmpresa().getDiasCompensacaoCheque()));
        flag = false;
    }

    @PostConstruct
    public void preCarregar() {
        listagemCheques = chequeEJB.listaChequesCompensarMensal();
    }

    public void pesquisar() {
        try {
            listagemCheques = chequeEJB.pesquisar(cheque, por, status, dtIni, dtFim);
        } catch (Exception e) {
            Bcrutils.escreveLogErro(e);
        }
    }

    public void Salvar() {
        empresa = uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa();
        if (cheque.getIdCheque() != null) {
            try {
                if (cheque.getTipo().equals("Saque")) {
                    cheque.setNominal(uEJB.pegaEmpresaLogadoNaSessao().getNomeFantasia());
                }
                chequeEJB.Atualizar(cheque);
                preCarregar();
                MensageFactory.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                MensageFactory.addMensagemPadraoErro("editar");
            }
        } else {
            if (listaCheque.isEmpty()) {
                if (cheque.getTipo().equals("Normal")) {
                    if (verificarRequiredFornecedor()) {
                        MensageFactory.error("Informe o fornecedor ou o campo Nominal.", null);
                    } else {
                        lancar();
                    }
                } else {
                    lancar();
                }
            }
            if (!listaCheque.isEmpty()) {
                for (Cheque chq : listaCheque) {
                    if (chq.getTipo().equals("Saque")) {
                        chq.setNominal(uEJB.pegaEmpresaLogadoNaSessao().getNomeFantasia());
                        chequeEJB.Salvar(chq);
                        try {
//                            pagamentoEJB.lancarDebitoMovimentoBancarioCheque(chq.getValor(), chq, chq.getIdContaCorrente(), chq.getIdContaCorrente().getIdBanco());
//                            pagamentoEJB.lancarCreditoMovimentoCaixaCheque(chq.getValor(), chq);
                        } catch (EJBException e) {
                            Bcrutils.descobreErroDeEJBException(e);
                            System.out.println(e);
                        }

                    } else {
                        chequeEJB.Salvar(chq);
                        // Lançar cheque no Movimento Bancário.
                        try {
//                            pagamentoEJB.lancarDebitoMovimentoBancarioCheque(chq.getValor(), chq, chq.getIdContaCorrente(), chq.getIdContaCorrente().getIdBanco());
                        } catch (EJBException e) {
                            Bcrutils.descobreErroDeEJBException(e);
                            System.out.println(e);
                        }

                    }
                }
                //14082015 - Charles
                // Método para exportar o cheque para SoftCheque, ficando na fila de impressão do mesmo.                
                if (pegarUsuarioLogado().getIdParametrizacao().getExportarChequeSoftcheque()) {
                    exportarCheque(listaCheque);
                }
                preCarregar();
                MensageFactory.addMensagemPadraoSucesso("salvar");

            }
        }
    }

    public void SalvarVarios() {
        try {
            for (Cheque chq : listaCheque) {
                chequeEJB.Salvar(chq);
                if (chq.getIdContasPagar() != null) {
                    chq.getIdContasPagar().setDtPagamento(new Date());
                    chq.getIdContasPagar().setIdContaCorrente(chq.getIdContaCorrente());
                    chq.getIdContasPagar().setIdBanco(chq.getIdContaCorrente().getIdBanco());
                    chq.getIdContasPagar().setStatus(Boolean.TRUE);
                    chq.getIdContasPagar().setPagoCom("B");
                    chq.getIdContasPagar().setValorPago(chq.getIdContasPagar().getValorDuplicata());
                }
                if (chq.getIdContasPagar() != null) {
                    pagamentoEJB.pagarCheque(chq.getIdContasPagar(), chq);
                    MensageFactory.info("Cheques lançados.", "Pagamentos efetuados com sucesso!");
                } else {
//                    pagamentoEJB.lancarMovimentoBancario(chq.getValor(), chq, contaCorrente, null, banco);
                    MensageFactory.info("OK.", "Cheque lançados.");
                }
            }
            // 14082015 - Charles
            // Método para exportar o cheque para SoftCheque, ficando na fila de impressão do mesmo.                
            if (pegarUsuarioLogado().getIdParametrizacao().getExportarChequeSoftcheque()) {
                exportarCheque(listaCheque);
            }
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    public void incluir() {
        try {
            empresa = uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String dataChequeFormatada = "";
            String dataAtualFormatada = "";

            if (colaborador.getIdColaborador() != null) {
                cheque.setIdColaborador(colaborador);
            }
            if (fornecedor.getIdFornecedor() != null) {
                cheque.setIdFornecedor(fornecedor);
            }
            if (contaCorrente.getIdContaCorrente() != null) {
                cheque.setConta(contaCorrente.getNumConta());
                cheque.setIdContaCorrente(contaCorrente);
            }
            if (cheque.getIdEmpresa() == null) {
                cheque.setIdEmpresa(empresa);
            }
            if (cheque.getTipo() == null) {
                cheque.setTipo("Normal");
            }
            dataChequeFormatada = dateFormat.format(cheque.getDtCompensacaoVencimento());
            dataAtualFormatada = dateFormat.format(new Date());

            cheque.setCompensado("N");
            listaCheque.add(cheque);
            carregarPagamentoAposIncluir();
            if (widgetValorRestante.compareTo(BigDecimal.ZERO) == 0) {
                MensageFactory.info("Saldo pago ok!", "Clique em Salvar e Sair para gravar os pagamentos.");
            } else {
                MensageFactory.info("Cheque adicionado.", "Adicione os demais cheques para completar o pagamento.");
            }
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    public boolean verificarRequiredFornecedor() {
        if (cheque.getIdFornecedor() == null) {
            if (cheque.getNominal() == null || cheque.getNominal().equals("")) {
                return true;
            } else {
                return false;
            }
        } else {
            if (!cheque.getTipo().equals("Saque")) {
                if (cheque.getNominal().isEmpty() || cheque.getNominal().equals("")) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public void lancar() {
        try {
            empresa = uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa();
            if (cheque.getIdEmpresa() == null) {
                cheque.setIdEmpresa(empresa);
            }
            cheque.setCompensado("N");
            if (cheque.getTipo().equals("Saque")) {
                cheque.setNominal(empresa.getNomeFantasia());
            }
            if (cheque.getIdContaCorrente() != null) {
                cheque.setConta(cheque.getIdContaCorrente().getNumConta());
            }
            if (cheque.getDtCompensacaoVencimento() == null) {
                cheque.setDtCompensacaoVencimento(new Date());
            }
            if (cheque.getIdEmpresa() == null) {
                cheque.setNominal(empresa.getNomeFantasia() == null ? empresa.getRazaoSocial() : empresa.getNomeFantasia());
            }
            listaCheque.add(cheque);
            cheque = new Cheque();
            MensageFactory.info("Cheque incluído!", "Clique em Salvar para sair.");
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("salvar");
            System.out.println(e);
        }
    }

    public void carregarPagamento(List listaContasPagar) {
        if (listaContasPagar.size() > 1 || listaContasPagar.isEmpty()) {
            MensageFactory.warn("Atenção!", "Para pagamento com vários cheques, selecione somente um Contas a Pagar.");
        } else {
            cheque = new Cheque();
            listaCheque = new ArrayList<Cheque>();
            cheque.setIdContasPagar((ContasPagar) listaContasPagar.get(0));
            if (cheque.getIdContasPagar().getIdFornecedor() != null) {
                cheque.setIdFornecedor(cheque.getIdContasPagar().getIdFornecedor());
                cheque.setNominal(cheque.getIdContasPagar().getIdFornecedor().getNome());
            }
            cheque.setDtCompensacaoVencimento(cheque.getIdContasPagar().getDtVencimento());
            cheque.setDtRecebimentoEmissao(new Date());
            cheque.setIdEmpresa(cheque.getIdContasPagar().getIdEmpresa());
            cheque.setTipo("Normal");
            cheque.setValor(cheque.getIdContasPagar().getValorDuplicata());
            widgetValorTotal = cheque.getIdContasPagar().getValorDuplicata();
            calcularValoresRestantes();
        }
    }

    public void carregarPagamentoAposIncluir() {
        contasPagar = new ContasPagar();
        contaCorrente = new ContaCorrente();
        contasPagar = cheque.getIdContasPagar();
        cheque = new Cheque();
        cheque.setIdContasPagar(contasPagar);
        if (cheque.getIdContasPagar().getIdFornecedor() != null) {
            cheque.setIdFornecedor(cheque.getIdContasPagar().getIdFornecedor());
        }
        cheque.setDtCompensacaoVencimento(cheque.getIdContasPagar().getDtVencimento());
        cheque.setDtRecebimentoEmissao(new Date());
        cheque.setIdEmpresa(cheque.getIdContasPagar().getIdEmpresa());
        cheque.setTipo("Normal");
        calcularValoresRestantes();
        cheque.setValor(widgetValorRestante);
    }

    public void calcularValoresRestantes() {
        BigDecimal valorPago = BigDecimal.ZERO;
        BigDecimal valorTotal = BigDecimal.ZERO;
        BigDecimal valorRestante = BigDecimal.ZERO;
        for (Cheque ch : listaCheque) {
            valorPago = valorPago.add(ch.getValor());
        }
        if (valorPago == null) {
            valorPago = new BigDecimal(BigInteger.ZERO);
        }
        if (widgetValorTotal == null) {
            widgetValorTotal = new BigDecimal(BigInteger.ZERO);
        }
        valorRestante = widgetValorTotal.subtract(valorPago);
        widgetValorPago = valorPago;
        widgetValorRestante = valorRestante;
    }

    public void excluir() {
        int indice = 0;
        List<Cheque> listaTemp = new ArrayList<Cheque>();
        listaTemp = listaCheque;
        for (Cheque ch : listaTemp) {
            System.out.println(indice);
            if (ch == chequeExcluir) {
                listaCheque.remove(indice);
                MensageFactory.info("Ok!", "Cheque excluído.");
                break;
            } else {
                indice++;
            }
        }
    }

    public void cancelar() {
        int indice = 0;
        List<Cheque> listaTemp = new ArrayList<Cheque>();
        listaTemp = listagemCheques;
        for (Cheque ch : listaTemp) {
            if (ch == chequeExcluir) {
                ch.setTipo("Cancelado");
                pagamentoEJB.cancelarCheque(ch);
                MensageFactory.info("Ok! Cheque cancelado.", "");
                break;
            } else {
                indice++;
            }
        }
    }

    public void excluirTemp() {
        int indice = 0;
        List<Cheque> listaTemp = new ArrayList<Cheque>();
        listaTemp = listaCheque;
        for (Cheque ch : listaTemp) {
            System.out.println(chequeExcluir.getValor().toString());
            System.out.println(ch.equals(chequeExcluir));
            if (ch.equals(chequeExcluir)) {
                listaCheque.remove(indice);
                MensageFactory.info("Ok!", "Cheque excluído.");
                break;
            } else {
                indice++;
            }
        }
        calcularValoresRestantes();
        cheque.setValor(widgetValorRestante);
    }

    public void selecionarChequeExcluir(Cheque ch) {
        chequeExcluir = ch;
        System.out.println("Valor: " + chequeExcluir.getValor());
    }

    public void selecionarCheque() {
        if (listaChequeSelecionado.size() > 1) {
            MensageFactory.error("Erro.", "Não é possivel excluir vários cheques.");
        } else {
            chequeExcluir = listaChequeSelecionado.get(0);
        }
    }

    public void compensarSelecionados() {
        try {
            for (Cheque ch : listaChequeSelecionado) {
//                ch.setDtCompensacaoVencimento(new Date());
                chequeEJB.compensar(ch, usuarioLogado.retornaUsuario());
                pagamentoEJB.lancarMovimentoBancarioCompensacao(ch.getValor(), ch, ch.getIdContaCorrente(), ch.getIdContasPagar(), ch.getIdContaCorrente().getIdBanco(), ch.getDtCompensacaoVencimento());
                if (ch.getTipo().equals("Saque")) {
                    if (caixaEJB.verificarExisteMovimentoCaixa(ch.getDtCompensacaoVencimento(), uEJB.pegaEmpresaLogadoNaSessao()) > 0) {
                        CaixaItem ci = new CaixaItem();
                        ci.setHistorico("Cheque descontado para gerar entrada no caixa para pagamentos!");
                        ci.setIdCheque(ch);
                        ci.setTipo("C");
                        ci.setTipomovimento("Cheque Descontado");
                        ci.setValor(ch.getValor());
                        ci.setIdCaixa(caixaEJB.retornaCaixaPorData(ch.getDtCompensacaoVencimento(), uEJB.pegaEmpresaLogadoNaSessao()));
                        caixaEJB.inserirMovimentacao(ci);
                    } else {
                        caixaEJB.criaNovoCaixa(ch.getDtCompensacaoVencimento(), uEJB.pegaEmpresaLogadoNaSessao());
                        CaixaItem ci = new CaixaItem();
                        ci.setHistorico("Cheque descontado para gerar entrada no caixa para pagamentos!");
                        ci.setIdCheque(ch);
                        ci.setTipo("C");
                        ci.setTipomovimento("Cheque Descontado");
                        ci.setValor(ch.getValor());
                        ci.setIdCaixa(caixaEJB.retornaCaixaPorData(ch.getDtCompensacaoVencimento(), uEJB.pegaEmpresaLogadoNaSessao()));
                        caixaEJB.inserirMovimentacao(ci);
                    }
                }
            }
            MensageFactory.info("Ok.", "Cheques compensados com sucesso!");
        } catch (Exception e) {
            MensageFactory.fatal("Erro.", "Ocorreu um erro ao tentar compensar os cheques.");
            System.out.println(e);
        }
    }

    public boolean verificarSePodeCompensar() {
        boolean retorno = true;
        valorTotalSelecionado = BigDecimal.ZERO;
        for (Cheque chq : listaChequeSelecionado) {
            valorTotalSelecionado = valorTotalSelecionado.add(chq.getValor());
            if (chq.getCompensado().equals("S")) {
                retorno = false;
            }
        }
        return retorno;
    }

    public String renderizar() {
        if (cheque.getTipo() != null) {
            if (cheque.getTipo().equals("Saque")) {
                return "display:none;";
            } else {
                return "";
            }
        } else {
            return "";
        }
    }

    public void exportarCheque(Cheque ch) {
        List<Cheque> lista = new ArrayList<Cheque>();
        if (ch == null) {
            // Se for varios cheques
            lista = listaChequeSelecionado;
        } else {
            // Cheque avulso
            lista.add(ch);
        }
        exportarCheque(lista);
    }

    public void exportarCheque(List<Cheque> listaCheque) {
        Bcrutils.gerarExportacao(listaCheque);
    }

    public StreamedContent getFile() {
        return file;
    }

    public void setFile(StreamedContent file) {
        this.file = file;
    }

    public void preencherNominal() {
        if (cheque.getIdFornecedor() != null) {
            cheque.setNominal(cheque.getIdFornecedor().getNome());
        } else {
            cheque.setNominal(null);
        }
    }

    public void zerarChequeAExcluir() {
        chequeExcluir = new Cheque();
    }

    public List carregarCheques() {
        return chequeEJB.ListarTodos();
    }

    public void excluir(Cheque c) {
        try {
            chequeEJB.Excluir(c, c.getIdCheque());
            MensageFactory.addMensagemPadraoSucesso("excluir");
            novo();
        } catch (Exception e) {
            MensageFactory.addMensagemPadraoErro("excluir");
        }
    }

    public Cheque selecionarPorID(Integer id) {
        cheque = (Cheque) chequeEJB.SelecionarPorID(id);
        if (cheque.getIdFornecedor() != null) {
            fornecedor = cheque.getIdFornecedor();
        }
        if (cheque.getIdColaborador() != null) {
            colaborador = cheque.getIdColaborador();
        }
        if (cheque.getIdContaCorrente() != null) {
            contaCorrente = cheque.getIdContaCorrente();
        }
        if (cheque.getIdContasPagar() != null) {
            contasPagar = cheque.getIdContasPagar();
        }
        if (cheque.getIdContasReceber() != null) {
            contasReceber = cheque.getIdContasReceber();
        }
        return cheque;
    }

    public boolean verificaCompensado(Cheque c) {
        String s = chequeEJB.verificarCompensado(c.getIdCheque());
        if (s == null) {
            return false;
        }
        return s.equals("S");
    }

    public void compensarCheque() {
        try {
            if (cheque != null) {
                chequeEJB.compensar(cheque, usuarioLogado.retornaUsuario());
                MensageFactory.info("Informação", "Cheque atualizado com sucesso!");
                novo();
            }
        } catch (Exception e) {
            MensageFactory.error("Erro", "Aconteceu algo inesperado ao tentar excluir!");
            System.out.println(e);
        }
    }

    public boolean verificaSePodeVer() {
        if (listaChequeSelecionado.size() == 1) {
            return false;
        } else {
            return true;
        }
    }

    public Empresa pegarUsuarioLogado() {
        empresa = uEJB.pegaEmpresaLogadoNaSessao();
        return empresa;
    }

    public boolean verificaSePodeExcluirEeditar() {
        if (listaChequeSelecionado.size() == 1) {
            return false;
        } else {
            return true;
        }
    }

    public boolean disable() {
        if (listaChequeSelecionado.isEmpty()) {
            if (listaChequeSelecionado.size() == 1) {
                cheque = listaChequeSelecionado.get(0);
            }
            return true;
        } else {
            if (listaChequeSelecionado.size() == 1) {
                cheque = listaChequeSelecionado.get(0);
            }
            return false;
        }
    }

    public void selecionarColaborador(Colaborador c) {
        colaborador = c;
    }

    public List listaFornecedores() {
        return fornecedorEJB.ListarTodos();
    }

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public UsuarioLogado getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(UsuarioLogado usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public ContasReceber getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(ContasReceber contasReceber) {
        this.contasReceber = contasReceber;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public ContasPagar getContasPagar() {
        return contasPagar;
    }

    public void setContasPagar(ContasPagar contasPagar) {
        this.contasPagar = contasPagar;
    }

    public List<Cheque> getListaCheque() {
        return listaCheque;
    }

    public void setListaCheque(List<Cheque> listaCheque) {
        this.listaCheque = listaCheque;
    }

    public List<Colaborador> getListaColaborador() {
        return listaColaborador;
    }

    public void setListaColaborador(List<Colaborador> listaColaborador) {
        this.listaColaborador = listaColaborador;
    }

    public List<ContaCorrente> getListaContaCorrente() {
        return listaContaCorrente;
    }

    public void setListaContaCorrente(List<ContaCorrente> listaContaCorrente) {
        this.listaContaCorrente = listaContaCorrente;
    }

    public List<Fornecedor> getListaFornecedor() {
        return listaFornecedor;
    }

    public void setListaFornecedor(List<Fornecedor> listaFornecedor) {
        this.listaFornecedor = listaFornecedor;
    }

    public Cheque getChequeExcluir() {
        return chequeExcluir;
    }

    public void setChequeExcluir(Cheque chequeExcluir) {
        this.chequeExcluir = chequeExcluir;
    }

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public BigDecimal getWidgetValorPago() {
        return widgetValorPago;
    }

    public void setWidgetValorPago(BigDecimal widgetValorPago) {
        this.widgetValorPago = widgetValorPago;
    }

    public BigDecimal getWidgetValorTotal() {
        return widgetValorTotal;
    }

    public void setWidgetValorTotal(BigDecimal widgetValorTotal) {
        this.widgetValorTotal = widgetValorTotal;
    }

    public BigDecimal getWidgetValorRestante() {
        return widgetValorRestante;
    }

    public void setWidgetValorRestante(BigDecimal widgetValorRestante) {
        this.widgetValorRestante = widgetValorRestante;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPor() {
        return por;
    }

    public void setPor(String por) {
        this.por = por;
    }

    public Date getDtIni() {
        return dtIni;
    }

    public void setDtIni(Date dtIni) {
        this.dtIni = dtIni;
    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        this.dtFim = dtFim;
    }

    public List<Cheque> getListagemCheques() {
        return listagemCheques;
    }

    public void setListagemCheques(List<Cheque> listagemCheques) {
        this.listagemCheques = listagemCheques;
    }

    public List<Cheque> getListaChequeSelecionado() {
        return listaChequeSelecionado;
    }

    public void setListaChequeSelecionado(List<Cheque> listaChequeSelecionado) {
        this.listaChequeSelecionado = listaChequeSelecionado;
    }

    public BigDecimal getValorTotalSelecionado() {
        return valorTotalSelecionado;
    }

    public void setValorTotalSelecionado(BigDecimal valorTotalSelecionado) {
        this.valorTotalSelecionado = valorTotalSelecionado;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
}
