/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.EstadoEJB;
import com.bcr.model.Estado;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class EstadoMB implements Serializable {

    @EJB
    EstadoEJB eEJB;
    private Estado estado;
    private Estado estadoSelecionado;

    /**
     * Creates a new instance of EstadoMB
     */
    public EstadoMB() {
        novo();
    }

    public void novo() {
        estado = new Estado();
        estadoSelecionado = new Estado();
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Estado getEstadoSelecionado() {
        return estadoSelecionado;
    }

    public void setEstadoSelecionado(Estado estadoSelecionado) {
        this.estadoSelecionado = estadoSelecionado;
    }
    
    

    public void salvar() {
        if (estado.getIdEstado() == null) {
            try {
                eEJB.Salvar(estado);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        } else {
            try {
                eEJB.Atualizar(estado);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }

    public List<Estado> listarEstados() {
        return eEJB.ListarTodos();
    }

    public void excluir() {
        try {
            eEJB.Excluir(estado, estado.getIdEstado());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
    
    

   
}
