/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.AtivoTipoEJB;
import com.bcr.model.TipoAtivo;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class AtivoTipoMB {

    @EJB
    AtivoTipoEJB atEJB;
    private TipoAtivo tipoAtivo;

    public AtivoTipoMB() {
        novo();
    }

    public void novo() {
        tipoAtivo = new TipoAtivo();
    }

    public void salvar() {
        if (tipoAtivo.getIdTipoAtivo() == null) {
            try {
                atEJB.Salvar(tipoAtivo);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Não foi possível salvar!", null);
                Bcrutils.escreveLogErro(e);
            }
        } else {
            try {
                atEJB.Atualizar(tipoAtivo);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Não foi possível atualizar!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public void excluir() {
        try {
            if (tipoAtivo != null) {
                atEJB.Excluir(tipoAtivo, tipoAtivo.getIdTipoAtivo());
                MensageFactory.info("Excluído com sucesso!", null);
            }
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível excluir!", null);
        }
    }

    public List<TipoAtivo> listarTodos() {
        return atEJB.ListarTodos();
    }

    public TipoAtivo getTipoAtivo() {
        return tipoAtivo;
    }

    public void setTipoAtivo(TipoAtivo tipoAtivo) {
        this.tipoAtivo = tipoAtivo;
    }
}
