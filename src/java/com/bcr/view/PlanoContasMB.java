/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.PlanoContasEJB;
import com.bcr.model.PlanoContas;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class PlanoContasMB implements Serializable{

    @EJB
    PlanoContasEJB pcEJB;
    private PlanoContas planoContas;
    private List<PlanoContas> listPlanoContas;
    private List<PlanoContas> listPlanoContasSinteticas;
    @EJB
    EmpresaEJB empEJB;
    

    /**
     * Creates a new instance of PlanoContasMB
     */
    public PlanoContasMB() {
        novo();
    }

    public void novo() {
        planoContas = new PlanoContas();
        listPlanoContas = new ArrayList<PlanoContas>();
        listPlanoContasSinteticas = new ArrayList<PlanoContas>();
    }

    public PlanoContas getPlanoContas() {
        return planoContas;
    }

    public void setPlanoContas(PlanoContas planoContas) {
        this.planoContas = planoContas;
    }

    public void salvar() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        planoContas.setIdEmpresa(empEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa"))));
        if (planoContas.getIdPlanoContas() == null) {
            try {
                pcEJB.Salvar(planoContas);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível salvar!", null);
                System.out.println("Erro ocorrido é: " + e);
            }
        } else {
            try {
                pcEJB.Atualizar(planoContas);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível atualizar!", null);
                System.out.println("Erro ocorrido é: " + e);
            }
        }
    }

    public List<PlanoContas> listarPlanosDeConta() {
        return pcEJB.listaPlanosContas();
    }
    public List<PlanoContas> listarPlanosDeContaCentrosCustos() {
        return pcEJB.listaPlanosContasCentroCustos();
    }

    public List<PlanoContas> listarPlanosDeContaSintetica() {
        return pcEJB.listaPlanosContasSintetica();
    }
    public List<PlanoContas> listarPlanosDeContaAnalitica() {
        return pcEJB.listaPlanosContasAnalitica();
    }

    public Integer retornaTamanhoString(String s) {
        return s.length();
    }

    public List<PlanoContas> getListPlanoContas() {
        return listPlanoContas;
    }

    public void setListPlanoContas(List<PlanoContas> listPlanoContas) {
        this.listPlanoContas = listPlanoContas;
    }

    public List<PlanoContas> getListPlanoContasSinteticas() {
        return listPlanoContasSinteticas;
    }

    public void setListPlanoContasSinteticas(List<PlanoContas> listPlanoContasSinteticas) {
        this.listPlanoContasSinteticas = listPlanoContasSinteticas;
    }
    
    

    public void excluir() {
        try {
            pcEJB.Excluir(planoContas, planoContas.getIdPlanoContas());
            MensageFactory.info("Excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("O erro ocorrido é:" + e);
        }
    }

    public void setaProximaCodigo() {
        String cod = planoContas.getIdPlanoContasReferente().getCodigo();
        System.out.println(cod);
        planoContas.setCodigo(cod);
    }
    
    public List<PlanoContas> listaPlanoContasCredito(){
        return pcEJB.listaPlanosContasCredito();
    }
    public List<PlanoContas> listaPlanoContasDebito(){
        return pcEJB.listaPlanosContasDebito();
    }
}
