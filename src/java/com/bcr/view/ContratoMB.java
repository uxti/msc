/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.ContratoEJB;
import com.bcr.model.ComunicadoImplantacao;
import com.bcr.model.Contrato;
import com.bcr.model.Grupo;
import com.bcr.model.PropostaItemProduto;
import com.bcr.pojo.GrupoValores;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class ContratoMB {

    @EJB
    ContratoEJB cEJB;
    private Contrato contrato;
    private ComunicadoImplantacao comunicadoImplantacao;

    public ContratoMB() {
        novo();
    }

    public void novo() {
        contrato = new Contrato();
        comunicadoImplantacao = new ComunicadoImplantacao();

    }

    public void gerarComunicadoDeImplantacao(Contrato contrato) {
        comunicadoImplantacao = new ComunicadoImplantacao();
        comunicadoImplantacao.setIdContrato(contrato);
        comunicadoImplantacao.setValorFaturar(contrato.getValor());
        comunicadoImplantacao.setDtComunicado(new Date());
    }
    
    public List<ComunicadoImplantacao> listarComunicadosAGerarMovimentaca(){
        return cEJB.listarComunicadosNaoGeradosDoMes();
    }

    public void salvarComunicado() {
        try {
            cEJB.salvarComunicado(comunicadoImplantacao);
            if (comunicadoImplantacao.getIdComunicado() == null) {
                MensageFactory.info("Comunicado salvo com sucesso!", null);
            } else {
                MensageFactory.info("Comunicado editado com sucesso!", null);
            }
            comunicadoImplantacao = new ComunicadoImplantacao();
        } catch (Exception e) {
            MensageFactory.warn("Não foi possivel persistir o comunicado! Verifique o log!", null);
            Bcrutils.escreveLogErro(e);
        }
    }
    
    

    public List<GrupoValores> listaGrupos() {
        List<Grupo> gruposUsados = new ArrayList<Grupo>();
        for (PropostaItemProduto pip : comunicadoImplantacao.getIdContrato().getIdProposta().getPropostaItemProdutoList()) {
            if (gruposUsados.indexOf(pip.getIdProduto().getIdGrupo()) == -1) {
                gruposUsados.add(pip.getIdProduto().getIdGrupo());
            }
        }
        List<GrupoValores> gvs = new ArrayList<GrupoValores>();
        for (Grupo g : gruposUsados) {
            GrupoValores gv = new GrupoValores();
            gv.setGrupo(g);
            BigDecimal valorGrupo = new BigDecimal(BigInteger.ZERO);
            for (PropostaItemProduto pip : comunicadoImplantacao.getIdContrato().getIdProposta().getPropostaItemProdutoList()) {
                if (g.getIdGrupo().equals(pip.getIdProduto().getIdGrupo().getIdGrupo())) {
                    valorGrupo = valorGrupo.add(pip.getValorTotalDepreciacao());
                }
            }
            gv.setValor(valorGrupo);
            gvs.add(gv);
        }
        return gvs;
    }

    public List<PropostaItemProduto> listaProdutosPorGrupo(GrupoValores gv) {
        List<PropostaItemProduto> pips = new ArrayList<PropostaItemProduto>();
        for (PropostaItemProduto pip : comunicadoImplantacao.getIdContrato().getIdProposta().getPropostaItemProdutoList()) {
            if (pip.getIdProduto().getIdGrupo().getIdGrupo().equals(gv.getGrupo().getIdGrupo())) {
                pips.add(pip);
            }
        }
        return pips;
    }

    public List<Contrato> listarContratos() {
        return cEJB.listarContratos();
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public ComunicadoImplantacao getComunicadoImplantacao() {
        return comunicadoImplantacao;
    }

    public void setComunicadoImplantacao(ComunicadoImplantacao comunicadoImplantacao) {
        this.comunicadoImplantacao = comunicadoImplantacao;
    }
}
