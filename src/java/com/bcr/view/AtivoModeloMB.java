/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.AtivoModeloEJB;
import com.bcr.model.ModeloAtivo;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class AtivoModeloMB {

    @EJB
    AtivoModeloEJB amEJB;
    private ModeloAtivo modeloAtivo;

    public AtivoModeloMB() {
        novo();
    }

    public void salvar() {
        if (modeloAtivo.getIdModeloVeiculo() == null) {
            try {
                amEJB.Salvar(modeloAtivo);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Erro ao tentar salvar!", null);
                Bcrutils.escreveLogErro(e);
            }
        } else {
            try {
                amEJB.Atualizar(modeloAtivo);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Erro ao tentar atualizar!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public void excluir() {
        try {
            if (modeloAtivo != null) {
                amEJB.Excluir(modeloAtivo, modeloAtivo.getIdModeloVeiculo());
                MensageFactory.info("Excluído com sucesso!", null);
            }
        } catch (Exception e) {
            MensageFactory.warn("Erro ao tentar excluir!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public List<ModeloAtivo> listarTodos() {
        return amEJB.ListarTodos();
    }

    public void novo() {
        modeloAtivo = new ModeloAtivo();
    }

    public ModeloAtivo getModeloAtivo() {
        return modeloAtivo;
    }

    public void setModeloAtivo(ModeloAtivo modeloAtivo) {
        this.modeloAtivo = modeloAtivo;
    }
}
