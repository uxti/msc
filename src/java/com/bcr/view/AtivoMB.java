/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.AtivoEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Ativo;
import com.bcr.pojo.Depreciacao;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class AtivoMB {

    @EJB
    AtivoEJB aEJB;
    private Ativo ativo;
    private LazyDataModel<Ativo> ativosLazy;
    @EJB
    UsuarioEJB uEJB;
    private List<Depreciacao> movimentacaoDepreciacoes;
    private List<Ativo> listaAtivos;

    public AtivoMB() {
        novo();
        carregarDatatableCliente();
    }

    public void salvar() {
        ativo.setIdEmpresa(uEJB.pegaEmpresaLogadoNaSessao());
        if (ativo.getIdAtivo() == null) {
            try {
                aEJB.Salvar(ativo);
                novo();
                MensageFactory.info("Ativo salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Não foi possível salvar o ativo!", null);
                Bcrutils.escreveLogErro(e);
            }
        } else {
            try {
                aEJB.Atualizar(ativo);
                novo();
                MensageFactory.info("Ativo atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Não foi possível atualizar o ativo!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public void excluir() {
        if (ativo != null) {
            try {
                aEJB.Excluir(ativo, ativo.getIdAtivo());
                MensageFactory.info("Ativo excluído com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Não foi possível excluir o ativo!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public List<Ativo> completeMethod(String par) {
        return aEJB.autoCompleteAtivo(par);
    }

    public void pesquisarDepreciacoesPorAtivo() {
        if (ativo.getIdAtivo() != null) {
            System.out.println("nao é nulo");
            movimentacaoDepreciacoes = listaMovimentoDepreciacao(ativo);
            System.out.println("tamanho " + movimentacaoDepreciacoes.size());
        } else {
            System.out.println("ta falando q ta nulo");
        }
    }
    
    public void pesquisarGruposAtivos(){
        if(ativo.getIdGrupoAtivo() != null){
            listaAtivos = aEJB.listarAtivosPorGrupo(ativo.getIdGrupoAtivo());
        }
    }

    public List<Depreciacao> listaMovimentoDepreciacao(Ativo ativo) {
        Date dtInicio = ativo.getDtAquisicao();
        int mesIncrement = ativo.getMesesDepreciado();
        List<Depreciacao> depreciacoes = new ArrayList<Depreciacao>();
        for (int i = 0; i < mesIncrement; i++) {
            Depreciacao d = new Depreciacao();
            d.setAtivo(ativo);
            d.setPeriodo(dtInicio);
            d.setValorRateio(ativo.getValorCustoMensal().multiply(new BigDecimal(i + 1)));
            d.setValorAtivo(ativo.getValorCompra().subtract(d.getValorRateio()));
            depreciacoes.add(d);
            dtInicio = Bcrutils.addMes(dtInicio, 1);
            System.out.println("primeira data " + dtInicio);
        }
        return depreciacoes;
    }

    public List<Ativo> listarTodos() {
        return aEJB.ListarTodos();
    }

    public void novo() {
        ativo = new Ativo();
        movimentacaoDepreciacoes = new ArrayList<Depreciacao>();
        listaAtivos = new ArrayList<Ativo>();
    }

    public void calcularDataTermino() throws ParseException {
        System.out.println(ativo.getDtAquisicao());
        System.out.println(ativo.getMesesMaxDepreciacao());
        if (ativo.getDtAquisicao() != null && ativo.getMesesMaxDepreciacao() != null) {
            System.out.println("falou q nao eh nulo nenhum dos campos");
            ativo.setDtTerminoUso(Bcrutils.addMes(ativo.getDtAquisicao(), ativo.getMesesMaxDepreciacao()));
            Calendar startCalendar = new GregorianCalendar();
            startCalendar.setTime(ativo.getDtAquisicao());
            Calendar endCalendar = new GregorianCalendar();
            endCalendar.setTime(new Date());
            int diffYear = endCalendar.get(Calendar.YEAR) - startCalendar.get(Calendar.YEAR);
            int diffMonth = diffYear * 12 + endCalendar.get(Calendar.MONTH) - startCalendar.get(Calendar.MONTH);
            ativo.setMesesDepreciado(diffMonth);
        }
    }

    public void calcularValores() {
        if (ativo.getValorCompra() != null) {
            System.out.println(ativo.getValorCompra().longValue() / ativo.getMesesMaxDepreciacao().longValue());
            ativo.setValorCustoMensal(new BigDecimal(ativo.getValorCompra().longValue() / ativo.getMesesMaxDepreciacao().longValue()));
            if (ativo.getMesesDepreciado() != 0) {
                ativo.setValorRateioDepreciacao(ativo.getValorCustoMensal().multiply(new BigDecimal(ativo.getMesesDepreciado())));
//                ativo.setValorResidual(ativo.getValorCompra().multiply(ativo.getValorRateioDepreciacao()));
            }
        }
    }

    public void carregarDatatableCliente() {
        ativosLazy = new LazyDataModel<Ativo>() {
            private static final long serialVersionUID = 1L;

            public List<Ativo> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where c." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and c." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by c." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }
                System.out.println(Clausula);
                setRowCount(Integer.parseInt(aEJB.contarRegistros(Clausula).toString()));
                List<Ativo> ativos = new ArrayList<Ativo>();
                ativos = aEJB.listarLazy(first, pageSize, Clausula);
                return ativos;

            }
        };
    }

    public Ativo getAtivo() {
        return ativo;
    }

    public void setAtivo(Ativo ativo) {
        this.ativo = ativo;
    }

    public LazyDataModel<Ativo> getAtivosLazy() {
        return ativosLazy;
    }

    public void setAtivosLazy(LazyDataModel<Ativo> ativosLazy) {
        this.ativosLazy = ativosLazy;
    }

    public List<Depreciacao> getMovimentacaoDepreciacoes() {
        return movimentacaoDepreciacoes;
    }

    public void setMovimentacaoDepreciacoes(List<Depreciacao> movimentacaoDepreciacoes) {
        this.movimentacaoDepreciacoes = movimentacaoDepreciacoes;
    }

    public List<Ativo> getListaAtivos() {
        return listaAtivos;
    }

    public void setListaAtivos(List<Ativo> listaAtivos) {
        this.listaAtivos = listaAtivos;
    }
    
}
