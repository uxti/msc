/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.pojo.Menu;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class menuMB {

    private Menu menu;
    private List<Menu> menus;
    private String[] menua = new String[1000];

    public menuMB() {
        carregar();
    }

    public void carregar() {
        menu = new Menu();
        menus = new ArrayList<Menu>();
        carregarMenus();
    }

    public void carregarMenus() {
        menus.add(new Menu("Cliente", "/MSC/Restrito/Cliente/"));
        menus.add(new Menu("Local de Trabalho", "/MSC/Restrito/LocalTrabalho/"));
        menus.add(new Menu("Fornecedor", "/MSC/Restrito/Fornecedor/"));
        menus.add(new Menu("Empresa", "/MSC/Restrito/Empresa/"));
        menus.add(new Menu("Colaborador", "/MSC/Restrito/Colaborador/"));
        menus.add(new Menu("Usuário", "/MSC/Restrito/Usuario/"));
        menus.add(new Menu("Perfil", "/MSC/Restrito/Perfil/"));
        menus.add(new Menu("Setor", "/MSC/Restrito/Setor/"));
        menus.add(new Menu("Cargo", "/MSC/Restrito/Cargo/"));
        menus.add(new Menu("Produto", "/MSC/Restrito/Produto/"));
        menus.add(new Menu("Grupo", "/MSC/Restrito/Grupo/"));
        menus.add(new Menu("Sub-Grupo", "/MSC/Restrito/Subgrupo/"));
        menus.add(new Menu("Ativo", "/MSC/Restrito/Ativo/"));
        menus.add(new Menu("Grupo Ativo", "/MSC/Restrito/AtivoGrupo/"));
        menus.add(new Menu("Tipo Ativo", "/MSC/Restrito/AtivoModelo/"));
        menus.add(new Menu("Unidade", "/MSC/Restrito/Unidade/"));
        menus.add(new Menu("NCM", "/MSC/Restrito/NCM/"));
        menus.add(new Menu("Localizações", "/MSC/Restrito/Localizacao/"));
        menus.add(new Menu("Transportadora", "/MSC/Restrito/Transportadora/"));
        menus.add(new Menu("Financeiro", "/MSC/Restrito/Financeiro/"));
        menus.add(new Menu("Plano de Contas", "/MSC/Restrito/PlanoConta/"));
        menus.add(new Menu("Agrupamento", "/MSC/Restrito/Agrupamento/"));
        menus.add(new Menu("Centro de Custo", "/MSC/Restrito/CentroCusto/"));
        menus.add(new Menu("Banco", "/MSC/Restrito/Banco/"));
        menus.add(new Menu("Conta Corrente", "/MSC/Restrito/ContaCorrente/"));
        menus.add(new Menu("Forma de pagamento", "/MSC/Restrito/FormaPagamento/"));
        menus.add(new Menu("Levant. Téc. de Mão-de-Obra", "/MSC/Restrito/PlanilhaMaoObra/"));
        menus.add(new Menu("Unidade", "/MSC/Restrito/Unidade/"));
    }

    public String selecionar(SelectEvent event) {
        Menu m = (Menu) event.getObject();
        return m.getHref();
    }

    public List<Menu> completeText(String query) {
        System.out.println(query);
        List<Menu> ls = new ArrayList<Menu>();
        for (Menu m : menus) {
            if (m.getLabel().contains(query)) {
                ls.add(m);
            }
        }
        return ls;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }
}
