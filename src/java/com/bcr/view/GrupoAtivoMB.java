/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.AtivoGrupoEJB;
import com.bcr.model.GrupoAtivo;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class GrupoAtivoMB {

    @EJB
    AtivoGrupoEJB agEJB;
    private GrupoAtivo ativoGrupo;

    /**
     * Creates a new instance of AtivoGrupo
     */
    public GrupoAtivoMB() {
        novo();
    }

    public void novo() {
        ativoGrupo = new GrupoAtivo();
    }

    public void salvar() {
        try {
            if (ativoGrupo.getIdGrupoAtivo() == null) {
                agEJB.Salvar(ativoGrupo);
                MensageFactory.info("Grupo de Ativo cadastrado com sucesso!", null);
            }else{
                agEJB.Atualizar(ativoGrupo);
                MensageFactory.info("Grupo de Ativo atualizado com sucesso!", null);
            }

            novo();
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível salvar o grupo de ativo!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public List<GrupoAtivo> listarGrupos() {
        return agEJB.ListarTodos();
    }

    public void excluir() {
        if (ativoGrupo != null) {
            agEJB.Excluir(ativoGrupo, ativoGrupo.getIdGrupoAtivo());
        }
    }

    public GrupoAtivo getAtivoGrupo() {
        return ativoGrupo;
    }

    public void setAtivoGrupo(GrupoAtivo ativoGrupo) {
        this.ativoGrupo = ativoGrupo;
    }
}
