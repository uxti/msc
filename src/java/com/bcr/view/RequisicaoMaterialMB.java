/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.ProdutoEJB;
import com.bcr.controller.RequisicaoMaterialEJB;
import com.bcr.controller.RequisicaoMaterialItemEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Cliente;
import com.bcr.model.Cotacao;
import com.bcr.model.CotacaoItem;
import com.bcr.model.Empresa;
import com.bcr.model.LocalTrabalho;
import com.bcr.model.Produto;
import com.bcr.model.PropostaItemProduto;
import com.bcr.util.MensageFactory;
import com.bcr.model.RequisicaoMaterial;
import com.bcr.model.RequisicaoMaterialItem;
import com.bcr.model.Usuario;
import com.bcr.util.Bcrutils;
import com.bcr.util.RelatorioFactory;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.Query;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Charles
 */
@ManagedBean
@ViewScoped
public class RequisicaoMaterialMB implements Serializable {

    /* EJB's */
    @EJB
    RequisicaoMaterialEJB requisicaoMaterialEJB;
    @EJB
    UsuarioEJB usuarioEJB;
    @EJB
    RequisicaoMaterialItemEJB requisicaoMaterialItemEJB;
    @EJB
    ProdutoEJB pEJB;
    /* Objetos */
    private RequisicaoMaterial requisicaoMaterial;
    private RequisicaoMaterial requisicaoMaterialReferente;
    private RequisicaoMaterialItem requisicaoMaterialItem;
    private Usuario usuario;
    private LocalTrabalho localTrabalho;
    private Empresa empresa;
    private Cliente cliente;
    /* Listas */
    private List<RequisicaoMaterial> listaRequisicaoMaterial;
    private List<RequisicaoMaterial> listaRequisicaoMaterialSelecionadas;
    private List<LocalTrabalho> listaLocalTrabalho;
    private List<RequisicaoMaterialItem> listaRequisicaoMaterialItems;
    private List<Cliente> listaClientes;
    private List<Produto> listaProdutos;
    /* Variáveis temporárias */
    private String lote;
    private BigDecimal quantidade, valorItem, valorTotalItem, valorTotalItensRequisicao;
    private Integer quantidadeTotalItensRequisicao;
    List<RequisicaoMaterial> lista = new ArrayList<RequisicaoMaterial>();
    private LazyDataModel<RequisicaoMaterial> model;
    private Cotacao cotacao;
    private RequisicaoMaterialItem requisicaoMaterialItemQuantidade;
    private Date dtIni, dtFim;
    private String status;
    private List<RequisicaoMaterialItem> materiaisEstoqueMinimoComprado;

    /* Início metodos padrões */
    public RequisicaoMaterialMB() {
        novo();
    }

    public void novo() {
        try {
            lista = new ArrayList<RequisicaoMaterial>();
            requisicaoMaterial = new RequisicaoMaterial();
            requisicaoMaterialReferente = new RequisicaoMaterial();
            requisicaoMaterialItem = new RequisicaoMaterialItem();
            usuario = new Usuario();
            localTrabalho = new LocalTrabalho();
            empresa = new Empresa();
            cliente = new Cliente();
            listaRequisicaoMaterial = new ArrayList<RequisicaoMaterial>();
            listaRequisicaoMaterialSelecionadas = new ArrayList<RequisicaoMaterial>();
            listaProdutos = new ArrayList<Produto>();
            listaLocalTrabalho = new ArrayList<LocalTrabalho>();
            listaRequisicaoMaterialItems = new ArrayList<RequisicaoMaterialItem>();
            listaClientes = new ArrayList<Cliente>();
            quantidade = new BigDecimal(BigInteger.ONE);
            valorItem = new BigDecimal(BigInteger.ZERO);
            valorTotalItem = new BigDecimal(BigInteger.ZERO);
            quantidadeTotalItensRequisicao = 0;
            requisicaoMaterial.setDtCadastro(new Date());
            cotacao = new Cotacao();
            requisicaoMaterialItemQuantidade = new RequisicaoMaterialItem();
//            requisicaoMaterialItemQuantidade.setQuantidadeAprovada(new BigDecimal(BigInteger.ZERO));
            carregarRequisicoes();
            materiaisEstoqueMinimoComprado = new ArrayList<RequisicaoMaterialItem>();
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void carregarRequisicoes() {
        model = new LazyDataModel<RequisicaoMaterial>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<RequisicaoMaterial> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where p." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }

                setRowCount(Integer.parseInt(requisicaoMaterialEJB.contarRegistro(Clausula).toString()));
                lista = requisicaoMaterialEJB.listarRequisicaoMaterialLazyModeWhere(first, pageSize, Clausula);
                return lista;
            }
        };
    }

    public void pesquisarRequisicoes() {
        listaRequisicaoMaterial = requisicaoMaterialEJB.listarRequisaoMateriais(requisicaoMaterial.getIdCliente(), dtIni, dtFim, status);
    }

    public void novaRequisicao() {
        novo();
        requisicaoMaterial.setDtCadastro(new Date());
        requisicaoMaterial.setStatus("Emitido");
        requisicaoMaterial.setTipo("E");
        requisicaoMaterial.setValor(BigDecimal.ZERO);
    }

    public void novaItemRequisicao() {
        requisicaoMaterialItem = new RequisicaoMaterialItem();
        valorTotalItem = new BigDecimal(BigInteger.ZERO);
    }

    public void onSelectCotrato() {
        List<PropostaItemProduto> pip = new ArrayList<PropostaItemProduto>();
        pip = requisicaoMaterial.getIdComunicado().getIdContrato().getIdProposta().getPropostaItemProdutoList();
        List<RequisicaoMaterialItem> itens = new ArrayList<RequisicaoMaterialItem>();
        for (PropostaItemProduto pi : pip) {
            RequisicaoMaterialItem rmi = new RequisicaoMaterialItem();
            rmi.setIdProduto(pi.getIdProduto());
            rmi.setQuantidade(pi.getQuantidade());
            rmi.setStatus("Aguardando");
            rmi.setVlrUnitario(pi.getValorUnitario());
            itens.add(rmi);
        }
        requisicaoMaterial.setRequisicaoMaterialItemList(itens);
    }

    public void Salvar() {
        usuario = usuarioEJB.pegaUsuarioLogadoNaSessao();
        requisicaoMaterial.setIdUsuario(usuario);
        requisicaoMaterial.setIdEmpresa(usuario.getIdEmpresa());
        requisicaoMaterial.setPrioridade(2);
        requisicaoMaterial.setValor(BigDecimal.ZERO);
        BigDecimal totalReq = new BigDecimal(BigInteger.ZERO);
        for (RequisicaoMaterialItem rmm : requisicaoMaterial.getRequisicaoMaterialItemList()) {
            totalReq = totalReq.add(rmm.getVlrUnitario().multiply(rmm.getQuantidade()));
        }
        requisicaoMaterial.setValor(totalReq);
        if (requisicaoMaterial.getIdRequisicaoMaterial() == null) {
            try {
                requisicaoMaterial.setStatus("Aguardando Análise");
//                requisicaoMaterial.setTipo("S");
                requisicaoMaterialEJB.SalvarRequisicao(requisicaoMaterial);
                MensageFactory.addMensagemPadraoSucesso("salvar");
            } catch (Exception e) {
                MensageFactory.addMensagemPadraoErro("salvar");
                System.out.println(e);
            }
        } else {
            try {
                requisicaoMaterialEJB.SalvarRequisicao(requisicaoMaterial);
                MensageFactory.addMensagemPadraoSucesso("editar");
            } catch (Exception e) {
                MensageFactory.addMensagemPadraoErro("editar");
                System.out.println(e);
            }
        }
        carregarRequisicoes();
    }

    public List<RequisicaoMaterial> listarRequisicoes() {
        return requisicaoMaterialEJB.listarRequisaoMateriais();
    }

    public void Excluir() {
        if (requisicaoMaterial.getIdRequisicaoMaterial() != null) {
            try {
                requisicaoMaterialEJB.Excluir(requisicaoMaterial, requisicaoMaterial.getIdRequisicaoMaterial());
                MensageFactory.addMensagemPadraoSucesso("excluir");
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar excluir. Verifique o log!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public void excluirItem() {
        try {
            int indice = 0;
            List<RequisicaoMaterialItem> listaTemporaria = new ArrayList<RequisicaoMaterialItem>();
            listaTemporaria = listaRequisicaoMaterialItems;
            for (RequisicaoMaterialItem rmi : listaTemporaria) {
                if (rmi == requisicaoMaterialItem) {
                    listaRequisicaoMaterialItems.remove(indice);
                    break;
                } else {
                    indice++;
                }
            }
            calcularValoresListaRequisicao();
            MensageFactory.info("Item Removido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.info("Não foi possível excluir. Tente novamente.", null);
            System.out.println(e);
        }
    }

    public void imprimirRequisicao(Integer id) {
        try {
            RelatorioFactory.RelatorioRequisicao("/WEB-INF/Relatorios/Requisicao/Impressao_Requisicao.jasper", id);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void adicionarItem() {
        int existeProduto = 0;
//        if (!requisicaoMaterial.getRequisicaoMaterialItemList().isEmpty()) {
//            listaRequisicaoMaterialItems = requisicaoMaterial.getRequisicaoMaterialItemList();
//        }
        if (requisicaoMaterial.getRequisicaoMaterialItemList() != null) {
            if (!requisicaoMaterial.getRequisicaoMaterialItemList().isEmpty()) {
                listaRequisicaoMaterialItems = new ArrayList<RequisicaoMaterialItem>();
                listaRequisicaoMaterialItems = requisicaoMaterial.getRequisicaoMaterialItemList();
            }
        }
        if (listaRequisicaoMaterialItems.size() > 0) {
            for (RequisicaoMaterialItem rmi : listaRequisicaoMaterialItems) {
                if (requisicaoMaterialItem.getIdProduto().getIdProduto().equals(rmi.getIdProduto().getIdProduto())) {
                    existeProduto = 1;
                }
            }
        }
        if (existeProduto == 1) {
            MensageFactory.warn("Atenção este produto já foi adicionado!", null);
        } else {
            try {
                requisicaoMaterialItem.setDescricao(requisicaoMaterialItem.getIdProduto().getDescricao());
                requisicaoMaterialItem.setStatus("Aguardando");
                listaRequisicaoMaterialItems.add(requisicaoMaterialItem);
                requisicaoMaterialItem = new RequisicaoMaterialItem();
                requisicaoMaterial.setRequisicaoMaterialItemList(listaRequisicaoMaterialItems);
                calcularValoresListaRequisicao();
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        calcularValoresListaRequisicao();
    }

    public void removerItemRequisicaoMaterial(int index, RequisicaoMaterialItem rmi) {
        try {
            requisicaoMaterial.getRequisicaoMaterialItemList().remove(index);
            MensageFactory.info("Item removido!", "Item removido!");
            if (rmi.getIdRequisicaoMaterialItem() != null) {
                requisicaoMaterialItemEJB.Excluir(rmi, rmi.getIdRequisicaoMaterialItem());
            }
            calcularValoresListaRequisicao();
        } catch (Exception e) {
            MensageFactory.info("Não foi possível remover!", "Não foi possível remover!");
            Bcrutils.escreveLogErro(e);
        }
    }

    public boolean desabilitarCotacao(RequisicaoMaterialItem materialItem) {
        boolean situacao = true;
        if (materialItem.getIdRequisicaoMaterial().getTipo().equals("M")) {
            if (materialItem.getIdProduto().getQuantidade().subtract(materialItem.getIdProduto().getEstoqueMin()).subtract(materialItem.getIdProduto().getQuantidadeReservada()).longValue() < materialItem.getQuantidadeAprovada().longValue()) {
                situacao = false;
            } else {
                situacao = true;
            }
        } else {
            if (materialItem.getIdProduto().getQuantidade().subtract(materialItem.getIdProduto().getQuantidadeReservada()).longValue() < materialItem.getQuantidadeAprovada().longValue()) {
                situacao = false;
            } else {
                situacao = true;
            }
        }
        return situacao;
    }

    public boolean verificaSePodeConfirmarSaida(RequisicaoMaterialItem materialItem) {
        boolean situacao = true;


        return situacao;
    }

    public boolean verificaSePodeCotar(RequisicaoMaterialItem materialItem) {
        boolean situacao = true;
//        System.out.println(materialItem.getIdProduto().getDescricao());
//        System.out.println(materialItem.getIdProduto().getQuantidade());
//        System.out.println(materialItem.getIdProduto().getQuantidadeReservada());
//        System.out.println(materialItem.getIdProduto().getEstoqueMin());
//        System.out.println("Estoque Disponivel " + materialItem.getIdProduto().getQuantidade().subtract(materialItem.getIdProduto().getQuantidadeReservada()).subtract(materialItem.getIdProduto().getEstoqueMin()));
//        System.out.println("Requisição Material " + materialItem.getIdRequisicaoMaterialItem());
//        System.out.println("Requisitada " + materialItem.getQuantidadeAprovada());

//        if (materialItem.getIdRequisicaoMaterial().getTipo().equals("M")) {
//            if (materialItem.getStatus().equals("Aguardando Saida")) {
//                if (materialItem.getIdProduto().getQuantidade().subtract(materialItem.getIdProduto().getEstoqueMin()).longValue() >= materialItem.getQuantidadeAprovada().longValue() && (materialItem.getStatus().equals("Aguardando Saida") || materialItem.getStatus().equals("Aguardando"))) {
//                    return false;
//                } else {
//                    return true;
//                }
//            } else {
//                try {
//                    System.out.println(materialItem.getIdProduto().getQuantidade());
//                    System.out.println(materialItem.getIdProduto().getQuantidadeReservada());
//                    System.out.println(materialItem.getIdProduto().getEstoqueMin());
//                    System.out.println(materialItem.getQuantidadeAprovada());
//                    System.out.println(materialItem.getStatus());
//                } catch (Exception e) {
//                    System.out.println(e);
//                }
//                if (materialItem.getIdProduto().getQuantidade().subtract(materialItem.getIdProduto().getQuantidadeReservada()).subtract(materialItem.getIdProduto().getEstoqueMin()).longValue() >= materialItem.getQuantidadeAprovada().longValue() && ((materialItem.getStatus().equals("Aguardando Saida") || materialItem.getStatus().equals("Aguardando")))) {
//                    return false;
//                } else {
//                    return true;
//                }
//            }
//
//        } else {
        System.out.println(materialItem.getIdProduto().getQuantidade());
        System.out.println(materialItem.getIdProduto().getQuantidadeReservada());
        System.out.println(materialItem.getQuantidadeAprovada());
        System.out.println(materialItem.getStatus());
        if (materialItem.getIdProduto().getQuantidade().subtract(materialItem.getIdProduto().getQuantidadeReservada()).longValue() >= materialItem.getQuantidadeAprovada().longValue() && (materialItem.getStatus().equals("Aguardando Saida") || materialItem.getStatus().equals("Aguardando"))) {
            situacao = false;
        } else if (materialItem.getStatus().equals("Aguardando")) {
            return true;
        }
        if (materialItem.getStatus().equals("Aguardando Saida")) {
            situacao = false;
        }
        return situacao;
    }

    public void ver(List<RequisicaoMaterialItem> lista, int index) {
        if (requisicaoMaterialItemQuantidade.getQuantidadeAprovada() != null) {
            RequisicaoMaterialItem rmi = lista.get(index);
            BigDecimal quantidadeAnterior = rmi.getQuantidadeAprovada();
            rmi.setQuantidadeAprovada(requisicaoMaterialItemQuantidade.getQuantidadeAprovada());
            BigDecimal quantidadeNova = rmi.getQuantidadeAprovada();
            requisicaoMaterialItemEJB.Atualizar(rmi);
            lista.set(index, rmi);

            BigDecimal diferenca;
            diferenca = quantidadeNova.subtract(quantidadeAnterior);
            System.out.println("Quantidade de Diferença Para Atualizar Estoque Reserva" + diferenca);
            pEJB.AtualizarQuantidadeReserva(rmi.getIdProduto(), diferenca);
        }
    }

    public String calcularValoresListaRequisicao() {
        valorTotalItensRequisicao = new BigDecimal(BigInteger.ZERO);
        quantidadeTotalItensRequisicao = 0;
        for (RequisicaoMaterialItem rmi : listaRequisicaoMaterialItems) {
            BigDecimal valorTotalRequisicao = new BigDecimal(BigInteger.ZERO);
            valorTotalRequisicao = rmi.getVlrUnitario().multiply(BigDecimal.valueOf(rmi.getQuantidade().longValue()));
            valorTotalItensRequisicao = valorTotalItensRequisicao.add(valorTotalRequisicao);
            quantidadeTotalItensRequisicao++;
        }
        requisicaoMaterial.setValor(valorTotalItensRequisicao);
        return Bcrutils.formatarMoeda(valorTotalItensRequisicao);
    }


    /* Início metodos de preenchimento */
    public void carregarDados() {
        try {
            usuario = usuarioEJB.pegaUsuarioLogadoNaSessao();
            empresa = usuario.getIdEmpresa();
        } catch (Exception e) {
            MensageFactory.info("Erro ao carregar os dados do usuário. Verifique seu cadastro.", null);
            System.out.println(e);
        }

    }

    public BigDecimal verificarEstoqueHue(RequisicaoMaterialItem requisicaoMaterialItem) {
        if (requisicaoMaterialItem.getIdProduto().getQuantidade().subtract(requisicaoMaterialItem.getIdProduto().getQuantidadeReservada()).longValue() < 0) {
            return new BigDecimal(BigInteger.ZERO);
        } else if (requisicaoMaterialItem.getStatus().equals("Aguardando Saida")) {
            return requisicaoMaterialItem.getIdProduto().getQuantidade();
        } else {
            return requisicaoMaterialItem.getIdProduto().getQuantidade().subtract(requisicaoMaterialItem.getIdProduto().getQuantidadeReservada());
        }
    }

    public BigDecimal verificarEstoqueBr(RequisicaoMaterialItem requisicaoMaterialItem) {
        if (requisicaoMaterialItem.getIdProduto().getQuantidade().subtract(requisicaoMaterialItem.getIdProduto().getQuantidadeReservada()).subtract(requisicaoMaterialItem.getIdProduto().getEstoqueMin()).longValue() < 0) {
            return new BigDecimal(BigInteger.ZERO);
        } else if (requisicaoMaterialItem.getStatus().equals("Aguardando Saida")) {
            return requisicaoMaterialItem.getIdProduto().getQuantidade().subtract(requisicaoMaterialItem.getIdProduto().getEstoqueMin());
        } else {
            return requisicaoMaterialItem.getIdProduto().getQuantidade().subtract(requisicaoMaterialItem.getIdProduto().getQuantidadeReservada()).subtract(requisicaoMaterialItem.getIdProduto().getEstoqueMin());
        }
    }

    public void selecionarItem(RequisicaoMaterialItem rmi) {
        requisicaoMaterialItem = rmi;
    }

    public void setarValores() {
        if (requisicaoMaterialItem.getIdProduto() != null) {
            requisicaoMaterialItem.setVlrUnitario(requisicaoMaterialItem.getIdProduto().getPrecoCusto());
//            requisicaoMaterialItem.setQuantidade(1);
        }
    }

    public void verificarOQueEditar() {
        System.out.println(requisicaoMaterial.getIdRequisicaoMaterial());
    }

    public boolean verificaSePodeEditar() {
        try {
            if (requisicaoMaterial.getIdRequisicaoMaterial() != null) {
                if (requisicaoMaterial.getStatus().equals("Aguardando Análise")) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            return true;
        }

    }

    public boolean verificaSePodeRecusar() {
        if (requisicaoMaterial.getIdRequisicaoMaterial() != null) {
            if (!requisicaoMaterial.getStatus().equals("Finalizado") || requisicaoMaterial.getStatus().equals("Recusado")) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public void setarCliente() {
        if (requisicaoMaterial.getIdLocalTrabalho() != null) {
            requisicaoMaterial.setIdCliente(requisicaoMaterial.getIdLocalTrabalho().getIdCentroCusto().getIdCliente());
        }
    }

    public void aprovar_item(RequisicaoMaterialItem item) {
        item.setQuantidadeAprovada(item.getQuantidade());
    }

    public void recusar_item(RequisicaoMaterialItem item) {
        item.setQuantidadeAprovada(new BigDecimal(BigInteger.ZERO));
    }

    public void recusar() {
        try {
            requisicaoMaterial.setStatus("Recusado");
            requisicaoMaterialEJB.Atualizar(requisicaoMaterial);
            requisicaoMaterial = new RequisicaoMaterial();
            listaRequisicaoMaterial = new ArrayList<RequisicaoMaterial>();
            MensageFactory.info("Requisição recusada!", "Requisição recusada!");
        } catch (Exception e) {
            MensageFactory.error("Não foi possível recusar!", "Não foi possível recusar!");
            Bcrutils.escreveLogErro(e);
        }


    }

    public boolean semSelecionar() {
        if (listaRequisicaoMaterialSelecionadas.isEmpty()) {
            if (listaRequisicaoMaterialSelecionadas.size() == 1) {
                requisicaoMaterial = listaRequisicaoMaterialSelecionadas.get(0);
                listaRequisicaoMaterialItems = requisicaoMaterial.getRequisicaoMaterialItemList();
            }
            return true;
        } else {
            if (listaRequisicaoMaterialSelecionadas.size() == 1) {
                requisicaoMaterial = listaRequisicaoMaterialSelecionadas.get(0);
                System.out.println(requisicaoMaterial.getIdRequisicaoMaterial());
                listaRequisicaoMaterialItems = requisicaoMaterial.getRequisicaoMaterialItemList();
            }
            return false;
        }
    }

    public boolean verificaSePodeAprovarRecusar() {
        if (listaRequisicaoMaterialSelecionadas.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public void aprovarRequisicoes() {
        if (!listaRequisicaoMaterialSelecionadas.isEmpty()) {
            try {
                for (RequisicaoMaterial ri : listaRequisicaoMaterialSelecionadas) {
                    ri.setStatus("Análise de Almoxarifado");
                    for (RequisicaoMaterialItem rmi : ri.getRequisicaoMaterialItemList()) {
                        rmi.setQuantidadeAprovada(rmi.getQuantidade());
                        if (rmi.getIdProduto().getQuantidade() == null) {
                            rmi.getIdProduto().setQuantidade(BigDecimal.ZERO);
                        }
                        if (rmi.getIdProduto().getQuantidadeReservada() == null) {
                            rmi.getIdProduto().setQuantidadeReservada(BigDecimal.ZERO);
                        }
                        if (rmi.getQuantidadeAprovada() == null) {
                            rmi.setQuantidadeAprovada(BigDecimal.ZERO);
                        }
//                        if ((rmi.getIdProduto().getQuantidade().subtract(rmi.getIdProduto().getQuantidadeReservada())).longValue() < rmi.getQuantidadeAprovada().longValue()) {
//                            rmi.getIdRequisicaoMaterial().setStatus("Em Cotação");
//                            rmi.setStatus("Cotação");
//                        }
                        requisicaoMaterialEJB.atualizarItem(rmi);
                    }
                    requisicaoMaterialEJB.Atualizar(ri);
                }
                MensageFactory.info("Requisições aprovadas!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível aprovar as requisições.", null);
            }

        }
        carregarRequisicoes();
    }

    public void aprovarRequisicao() {
        try {
            requisicaoMaterial.setStatus("Análise de Almoxarifado");
            requisicaoMaterialEJB.Atualizar(requisicaoMaterial);
            for (RequisicaoMaterialItem rmi : requisicaoMaterial.getRequisicaoMaterialItemList()) {
                if (rmi.getQuantidade() == null) {
                    rmi.setQuantidade(BigDecimal.ZERO);
                }
                if (rmi.getVlrUnitario() == null) {
                    rmi.setVlrUnitario(BigDecimal.ZERO);
                }
                if (rmi.getIdProduto().getQuantidadeReservada() == null) {
                    rmi.getIdProduto().setQuantidadeReservada(BigDecimal.ZERO);
                }
                rmi.setQuantidadeAprovada(rmi.getQuantidade());
                if (rmi.getIdProduto().getQuantidade() == null) {
                    rmi.getIdProduto().setQuantidade(BigDecimal.ZERO);
                }
                if (rmi.getIdProduto().getQuantidadeReservada() == null) {
                    rmi.getIdProduto().setQuantidadeReservada(BigDecimal.ZERO);
                }
                if (rmi.getQuantidadeAprovada() == null) {
                    rmi.setQuantidadeAprovada(BigDecimal.ZERO);
                }
//                if ((rmi.getIdProduto().getQuantidade().subtract(rmi.getIdProduto().getQuantidadeReservada())).longValue() < rmi.getQuantidadeAprovada().longValue()) {
//                    rmi.getIdRequisicaoMaterial().setStatus("Em Cotação");
//                    rmi.setStatus("Cotação");
//                }
                requisicaoMaterialEJB.atualizarItem(rmi);
                requisicaoMaterialEJB.Atualizar(rmi.getIdRequisicaoMaterial());
            }

            MensageFactory.info("Requisições aprovada!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível aprovar a requisição.", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public void recusarRequisicoes() {
        if (!listaRequisicaoMaterialSelecionadas.isEmpty()) {
            try {
                for (RequisicaoMaterial ri : listaRequisicaoMaterialSelecionadas) {
                    ri.setStatus("Recusado");
                    requisicaoMaterialEJB.Atualizar(ri);
                }
                MensageFactory.info("Requisições recusadas!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível recusar as requisições.", null);
            }
        }
        carregarRequisicoes();
    }

    public void recusarRequisicao() {
        try {
            requisicaoMaterial.setStatus("Recusado");
            requisicaoMaterialEJB.Atualizar(requisicaoMaterial);
            MensageFactory.info("Requisições recusada!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível recusar a requisição.", null);
        }
    }

    public void confirmarTodosItensComEstoque() {
        List<RequisicaoMaterial> requisicaoNovas = new ArrayList<RequisicaoMaterial>();
        try {
            for (RequisicaoMaterial rm : requisicaoMaterialEJB.listarRequisicoesParaAlmoxarifado()) {
                for (RequisicaoMaterialItem rmi : rm.getRequisicaoMaterialItemList()) {
                    if (rmi.getStatus().equals("Aguardando Saida")) {
                        if ((rmi.getIdProduto().getQuantidade().longValue()) >= rmi.getQuantidadeAprovada().longValue() && (rmi.getStatus().equals("Aguardando") || rmi.getStatus().equals("Aguardando Saida"))) {
                            rmi.setStatus("Aceito");
//                            rmi.getIdProduto().setQuantidadeReservada(rmi.getIdProduto().getQuantidadeReservada().add(rmi.getQuantidadeAprovada()));
                            pEJB.Atualizar(rmi.getIdProduto());
                        }
                    } else {
                        if ((rmi.getIdProduto().getQuantidade().subtract(rmi.getIdProduto().getQuantidadeReservada()).longValue()) >= rmi.getQuantidadeAprovada().longValue() && (rmi.getStatus().equals("Aguardando") || rmi.getStatus().equals("Aguardando Saida"))) {
//                        if (!rmi.getStatus().equals("Aguardando Saida")) {
                            rmi.setStatus("Aceito");
                            rmi.getIdProduto().setQuantidadeReservada(rmi.getIdProduto().getQuantidadeReservada().add(rmi.getQuantidadeAprovada()));
                            pEJB.Atualizar(rmi.getIdProduto());
                            requisicaoMaterialItemEJB.Atualizar(rmi);
//                        } else {
//                            rmi.setStatus("Aguardando Saida");
//                            requisicaoMaterialItemEJB.Atualizar(rmi);
//                        }
                        }
                    }


                    if ((rmi.getIdProduto().getQuantidade().longValue()) == rmi.getQuantidadeAprovada().longValue()) {
                        if (verificaSePodeComprarMinimo(rmi.getIdProduto(), materiaisEstoqueMinimoComprado)) {
                            if (rmi.getIdProduto().getEstoqueMin().longValue() > 0) {
                                RequisicaoMaterial r = new RequisicaoMaterial();
                                r.setIdUsuario(usuarioEJB.pegaUsuarioLogadoNaSessao());
                                r.setIdEmpresa(usuarioEJB.pegaEmpresaLogadoNaSessao());
                                r.setIdLocalTrabalho(usuarioEJB.pegaEmpresaLogadoNaSessao().getIdLocalTrabalho());
                                r.setIdCliente(usuarioEJB.pegaEmpresaLogadoNaSessao().getIdLocalTrabalho().getIdCentroCusto().getIdCliente());
                                r.setStatus("Em Cotação");
                                r.setDtCadastro(new Date());
                                r.setTipo("R");
                                r.setObservacao("Requisição gerada referente ao abastecimento do estoque minimo referente a requisição " + rmi.getIdRequisicaoMaterial().getIdRequisicaoMaterial() + " !");
                                List<RequisicaoMaterialItem> itens = new ArrayList<RequisicaoMaterialItem>();
                                RequisicaoMaterialItem item = new RequisicaoMaterialItem();
                                item.setIdProduto(rmi.getIdProduto());
                                item.setDescricao(rmi.getIdProduto().getDescricao());
                                item.setQuantidade(rmi.getIdProduto().getEstoqueMin());
                                item.setQuantidadeAprovada(rmi.getIdProduto().getEstoqueMin());
                                item.setVlrUnitario(rmi.getVlrUnitario());
                                item.setStatus("Cotação");
                                item.setQuantidadeCotada(rmi.getIdProduto().getEstoqueMin());
                                itens.add(item);
                                r.setRequisicaoMaterialItemList(itens);
                                requisicaoNovas.add(r);
//                            requisicaoMaterialEJB.SalvarRequisicao(r);
                            }
                        }
                    }
                }
                verificaStatusDaRequisicao(rm);
            }

            for (RequisicaoMaterial rm : requisicaoMaterialEJB.listarRequisicoesParaAlmoxarifado()) {
                System.out.println(rm.getIdRequisicaoMaterial());
                System.out.println(rm.getStatus());
                if (rm.getStatus().equals("Aguardando Pedido")) {
                    for (RequisicaoMaterialItem rmi : rm.getRequisicaoMaterialItemList()) {
                        if (rmi.getStatus().equals("Aguardando Saida")) {
                            rmi.setStatus("Aceito");
                            System.out.println("ta faendo a alteração aqui!");
//                            rmi.getIdProduto().setQuantidadeReservada(rmi.getIdProduto().getQuantidadeReservada().add(rmi.getQuantidadeAprovada()));
                            pEJB.Atualizar(rmi.getIdProduto());
                            requisicaoMaterialItemEJB.Atualizar(rmi);
                        }
                    }
                    verificaStatusDaRequisicao(rm);
                }
            }

            if (!requisicaoNovas.isEmpty()) {
                List<Produto> produtosUsados = new ArrayList<Produto>();
                for (RequisicaoMaterial rm : requisicaoNovas) {
                    for (RequisicaoMaterialItem rmi : rm.getRequisicaoMaterialItemList()) {
                        if (produtosUsados.indexOf(rmi.getIdProduto()) < 0) {
                            produtosUsados.add(rmi.getIdProduto());
                        }
                    }
                }

                RequisicaoMaterial r = new RequisicaoMaterial();
                r.setIdUsuario(usuarioEJB.pegaUsuarioLogadoNaSessao());
                r.setIdEmpresa(usuarioEJB.pegaEmpresaLogadoNaSessao());
                r.setIdLocalTrabalho(usuarioEJB.pegaEmpresaLogadoNaSessao().getIdLocalTrabalho());
                r.setIdCliente(usuarioEJB.pegaEmpresaLogadoNaSessao().getIdLocalTrabalho().getIdCentroCusto().getIdCliente());
                r.setStatus("Em Cotação");
                r.setDtCadastro(new Date());
                r.setTipo("R");
                r.setObservacao("Requisição gerada referente ao abastecimento do estoque minimo !");
                List<RequisicaoMaterialItem> itens = new ArrayList<RequisicaoMaterialItem>();
                for (Produto p : produtosUsados) {
                    RequisicaoMaterialItem rmi = new RequisicaoMaterialItem();
                    for (RequisicaoMaterial rm : requisicaoNovas) {
                        for (RequisicaoMaterialItem rmi2 : rm.getRequisicaoMaterialItemList()) {
                            if (rmi2.getIdProduto().getIdProduto() == p.getIdProduto()) {
                                rmi.setIdProduto(rmi2.getIdProduto());
                                rmi.setDescricao(rmi2.getIdProduto().getDescricao());
                                if (rmi.getQuantidadeAprovada() == null) {
                                    rmi.setQuantidadeAprovada(new BigDecimal(BigInteger.ZERO));
                                }
                                rmi.setQuantidadeAprovada(rmi.getQuantidadeAprovada().add(rmi2.getQuantidadeAprovada()));
                                rmi.setVlrUnitario(rmi.getVlrUnitario());
                                rmi.setStatus("Cotação");
                                rmi.setQuantidadeCotada(rmi.getQuantidadeAprovada());
                            }
                        }
                    }
                    itens.add(rmi);
                    materiaisEstoqueMinimoComprado.add(rmi);
                }
                r.setRequisicaoMaterialItemList(itens);
                System.out.println("Vai salvar as requisições geradas para cotação!");
                requisicaoMaterialEJB.SalvarRequisicao(r);
            }
        } catch (Exception e) {
            Bcrutils.escreveLogErro(e);
        }
    }

    public boolean verificaSePodeComprarMinimo(Produto p, List<RequisicaoMaterialItem> lista) {
        List<Produto> ps = new ArrayList<Produto>();
        for (RequisicaoMaterialItem rmi : lista) {
            ps.add(p);
        }
        if (ps.indexOf(p) == -1) {
            return true;
        } else {
            return false;
        }
    }

    public void cotarTodosItensSemEstoque() {
        List<RequisicaoMaterialItem> itensParaAbastecerMinimo = new ArrayList<RequisicaoMaterialItem>();
        try {
            for (RequisicaoMaterial rm : requisicaoMaterialEJB.listarRequisicoesParaAlmoxarifado()) {
                System.out.println("Vamos Verificar a Requisição " + rm.getIdRequisicaoMaterial() + " para cotar!");
                for (RequisicaoMaterialItem rmi : rm.getRequisicaoMaterialItemList()) {
                    System.out.println("\t Vamos verificar o item " + rmi.getIdProduto().getDescricao());
                    if (rmi.getStatus().equals("Aguardando")) {
                        System.out.println("\t\t Esta com status de aguardando");
                        if ((rmi.getQuantidadeAprovada().longValue() > (rmi.getIdProduto().getQuantidade().subtract(rmi.getIdProduto().getQuantidadeReservada()).longValue())) && (!rmi.getStatus().equals("Aceito")) && (rmi.getStatus().equals("Aguardando"))) {
                            System.out.println("\t\t\t Não possui estoque marca para cotar!");
                            rmi.setStatus("Cotação");
                            if (rmi.getIdProduto().getQuantidade().subtract(rmi.getIdProduto().getQuantidadeReservada()).longValue() == 0) {
                                rmi.setQuantidadeCotada(rmi.getQuantidadeAprovada());
                            } else if (rmi.getIdProduto().getQuantidade().subtract(rmi.getIdProduto().getQuantidadeReservada()).longValue() < rmi.getQuantidadeAprovada().longValue()) {
                                BigDecimal estoqueLivre = rmi.getIdProduto().getQuantidade().subtract(rmi.getIdProduto().getQuantidadeReservada());
                                System.out.println("Este produto tem "+estoqueLivre+ " de estoque livre!");
                                rmi.setQuantidadeCotada(rmi.getQuantidadeAprovada().subtract(estoqueLivre));
                            }
                            rmi.getIdProduto().setQuantidadeReservada(rmi.getIdProduto().getQuantidadeReservada().add(rmi.getQuantidadeAprovada()));
//                            if (rmi.getIdProduto().getQuantidade().subtract(rmi.getIdProduto().getQuantidadeReservada()).longValue() < 0) {
                            if (rmi.getIdProduto().getEstoqueMin().longValue() > 0) {
                                if (verificaSePodeComprarMinimo(rmi.getIdProduto(), itensParaAbastecerMinimo) && verificaSePodeComprarMinimo(rmi.getIdProduto(), materiaisEstoqueMinimoComprado)) {
                                    System.out.println("\t\t\t\t Este produto vai comprar o minimo");
                                    RequisicaoMaterialItem itemAbastecimentoMinimo = new RequisicaoMaterialItem();
                                    itemAbastecimentoMinimo.setDescricao(rmi.getIdProduto().getDescricao());
                                    itemAbastecimentoMinimo.setIdProduto(rmi.getIdProduto());
                                    itemAbastecimentoMinimo.setQuantidade(rmi.getIdProduto().getEstoqueMin());
                                    itemAbastecimentoMinimo.setQuantidadeAprovada(rmi.getIdProduto().getEstoqueMin());
                                    itemAbastecimentoMinimo.setQuantidadeCotada(rmi.getIdProduto().getEstoqueMin());
                                    itemAbastecimentoMinimo.setStatus("Cotação");
                                    itemAbastecimentoMinimo.setVlrUnitario(rmi.getIdProduto().getPrecoCusto());
                                    itensParaAbastecerMinimo.add(itemAbastecimentoMinimo);
                                    materiaisEstoqueMinimoComprado.add(itemAbastecimentoMinimo);
                                } else {
                                    System.out.println("\t\t\t\t Este produto ja foi comprado o minimo");
                                }
                            }
//                            }
//                            else {
////                                rmi.setQuantidadeCotada(rmi.getQuantidadeAprovada().subtract(rmi.getIdProduto().getQuantidade().subtract(rmi.getIdProduto().getQuantidadeReservada())).add(rmi.getIdProduto().getEstoqueMin()));
//                                rmi.setQuantidadeCotada(rmi.getQuantidadeAprovada());
//                                if (rmi.getIdProduto().getQuantidade().subtract(rmi.getIdProduto().getQuantidadeReservada()).longValue() == 0 || rmi.getIdProduto().getQuantidade().subtract(rmi.getIdProduto().getQuantidadeReservada()).longValue() < rmi.getIdProduto().getEstoqueMin().longValue()) {
//                                    if (verificaSePodeComprarMinimo(rmi.getIdProduto(), itensParaAbastecerMinimo)) {
//                                        if (rmi.getIdProduto().getEstoqueMin() != null) {
//                                            if (rmi.getIdProduto().getEstoqueMin().longValue() > 0) {
//                                                RequisicaoMaterialItem itemAbastecimentoMinimo = new RequisicaoMaterialItem();
//                                                itemAbastecimentoMinimo.setDescricao(rmi.getIdProduto().getDescricao());
//                                                itemAbastecimentoMinimo.setIdProduto(rmi.getIdProduto());
//                                                itemAbastecimentoMinimo.setQuantidade(rmi.getIdProduto().getEstoqueMin());
//                                                itemAbastecimentoMinimo.setQuantidadeAprovada(rmi.getIdProduto().getEstoqueMin());
//                                                itemAbastecimentoMinimo.setQuantidadeCotada(rmi.getIdProduto().getEstoqueMin());
//                                                itemAbastecimentoMinimo.setStatus("Cotação");
//                                                itemAbastecimentoMinimo.setVlrUnitario(rmi.getIdProduto().getPrecoCusto());
//                                                itensParaAbastecerMinimo.add(itemAbastecimentoMinimo);
//                                            }
//                                        }
//                                    }
//                                }
//                            }

                            pEJB.Atualizar(rmi.getIdProduto());
                            requisicaoMaterialItemEJB.Atualizar(rmi);
                            rm.setStatus("Em Cotação");
                            requisicaoMaterialEJB.Atualizar(rm);
                        }
                    }
                }
                verificaStatusDaRequisicao(rm);
            }
            if (itensParaAbastecerMinimo.size() > 0) {
                List<Produto> produtosUsados = new ArrayList<Produto>();
                for (RequisicaoMaterialItem rmi : itensParaAbastecerMinimo) {
                    if (produtosUsados.indexOf(rmi.getIdProduto()) < 0) {
                        produtosUsados.add(rmi.getIdProduto());
                    }
                }

                RequisicaoMaterial r = new RequisicaoMaterial();
                r.setIdUsuario(usuarioEJB.pegaUsuarioLogadoNaSessao());
                r.setIdEmpresa(usuarioEJB.pegaEmpresaLogadoNaSessao());
                r.setIdLocalTrabalho(usuarioEJB.pegaEmpresaLogadoNaSessao().getIdLocalTrabalho());
                r.setIdCliente(usuarioEJB.pegaEmpresaLogadoNaSessao().getIdLocalTrabalho().getIdCentroCusto().getIdCliente());
                r.setStatus("Em Cotação");
                r.setDtCadastro(new Date());
                r.setTipo("R");
                r.setObservacao("Requisição gerada referente ao abastecimento do estoque minimo !");
                List<RequisicaoMaterialItem> itens = new ArrayList<RequisicaoMaterialItem>();
                for (Produto p : produtosUsados) {
                    RequisicaoMaterialItem rmi = new RequisicaoMaterialItem();
                    for (RequisicaoMaterialItem rmi2 : itensParaAbastecerMinimo) {
                        if (rmi2.getIdProduto().getIdProduto() == p.getIdProduto()) {
                            rmi.setIdProduto(rmi2.getIdProduto());
                            rmi.setDescricao(rmi2.getIdProduto().getDescricao());
                            if (rmi.getQuantidadeAprovada() == null) {
                                rmi.setQuantidadeAprovada(new BigDecimal(BigInteger.ZERO));
                            }
                            rmi.setQuantidadeAprovada(rmi.getQuantidadeAprovada().add(rmi2.getQuantidadeAprovada()));
                            rmi.setVlrUnitario(rmi.getVlrUnitario());
                            rmi.setStatus("Cotação");
                            rmi.setQuantidadeCotada(rmi.getQuantidadeAprovada());
                        }
                    }
                    itens.add(rmi);
                }
                r.setRequisicaoMaterialItemList(itens);
                System.out.println("Vai salvar as requisições geradas para cotação!");
                requisicaoMaterialEJB.SalvarRequisicao(r);

            }
        } catch (Exception e) {
            Bcrutils.escreveLogErro(e);
        }
    }

    public boolean verificarSeProdutoJaComprouEstoqueMinimo(Produto p) {
        boolean fim = false;
        for (RequisicaoMaterial rm : requisicaoMaterialEJB.listarRequisicoesParaAlmoxarifado()) {
            for (RequisicaoMaterialItem rmi : rm.getRequisicaoMaterialItemList()) {
                if (rmi.getIdProduto().getIdProduto().equals(p.getIdProduto()) && rmi.getStatus().equals("Cotação")) {
                    System.out.println("O produto existe na cotação " + rmi.getIdRequisicaoMaterial().getIdRequisicaoMaterial());
                    fim = true;
                    break;
                }
            }
        }
        return fim;
    }

    /* Fim metodos de preenchimento */
//    Inicio dos metodos para análise
    public List<RequisicaoMaterial> listaRequisicoesParaAnalise() {
        return requisicaoMaterialEJB.listarRequisicoesParaAnalise();
    }

    public List<RequisicaoMaterial> listaRequisicoesParaAlmoxarifado() {
        return requisicaoMaterialEJB.listarRequisicoesParaAlmoxarifado();
    }

    public boolean verificaParaAnalise() {
        if (listaRequisicaoMaterialSelecionadas.isEmpty() || listaRequisicaoMaterialSelecionadas.size() > 1) {
            return true;
        } else {
            requisicaoMaterial = listaRequisicaoMaterialSelecionadas.get(0);
            if (requisicaoMaterial.getIdRequisicaoMaterial() == null && requisicaoMaterial.getStatus() != "Emitido") {
                return true;
            } else {
                return false;
            }
        }

    }

    public void processarValores() {
        for (RequisicaoMaterialItem ri : requisicaoMaterial.getRequisicaoMaterialItemList()) {
            ri.setQuantidadeAprovada(ri.getQuantidade());
        }
    }

    //Verificações para validações de almoxarifadas e para ordem de compras
    public boolean verificarSeTemEstoqueNaRequisicao(RequisicaoMaterial requisicaoMaterial) {
        boolean retorno = false;
        if (!requisicaoMaterial.getRequisicaoMaterialItemList().isEmpty()) {
            for (RequisicaoMaterialItem ri : requisicaoMaterial.getRequisicaoMaterialItemList()) {
                if (ri.getIdProduto().getQuantidade().longValue() < ri.getQuantidadeAprovada().longValue()) {
                    retorno = true;
                }
            }
        }
        return retorno;
    }

    public String verificarQuantidadeItemEstoque(RequisicaoMaterialItem requisicaoMaterialItem) {
        if (requisicaoMaterialItem.getQuantidadeAprovada().longValue() > requisicaoMaterialItem.getIdProduto().getQuantidade().longValue()) {
            return "icons/Alert.png";
        } else {
            return "icons/confirmar.png";
        }
    }

    public boolean verificarQuantidadeItemEstoqueBol(RequisicaoMaterialItem requisicaoMaterialItem) {
        if (requisicaoMaterialItem.getQuantidade().longValue() > requisicaoMaterialItem.getIdProduto().getQuantidade().longValue()) {
            return true;
        } else {
            return false;
        }
    }

    public void finalizarSaidaRequisicaoMaterial(RequisicaoMaterial requisicaoMaterial) {
        requisicaoMaterial.setStatus("Finalizado");
        requisicaoMaterialEJB.Atualizar(requisicaoMaterial);
        for (RequisicaoMaterialItem rmi : requisicaoMaterial.getRequisicaoMaterialItemList()) {
            pEJB.SaidaEstoque(rmi.getIdProduto(), rmi.getQuantidadeAprovada());
            rmi.getIdProduto().setQuantidadeReservada(rmi.getIdProduto().getQuantidadeReservada().subtract(rmi.getQuantidadeAprovada()));
            pEJB.Atualizar(rmi.getIdProduto());
        }
        MensageFactory.info("Requisição finalizada com sucesso!", null);
    }

    public void finalizarSaidaRequisicaoMaterialSemMovimentarEstoque(RequisicaoMaterial requisicaoMaterial) {
        requisicaoMaterial.setStatus("Finalizado");
        requisicaoMaterialEJB.Atualizar(requisicaoMaterial);
//        for (RequisicaoMaterialItem rmi : requisicaoMaterial.getRequisicaoMaterialItemList()) {
//            pEJB.SaidaEstoque(rmi.getIdProduto(), rmi.getQuantidadeAprovada());
//            rmi.getIdProduto().setQuantidadeReservada(rmi.getIdProduto().getQuantidadeReservada().subtract(rmi.getQuantidadeAprovada()));
//            pEJB.Atualizar(rmi.getIdProduto());
//        }
        MensageFactory.info("Requisição finalizada com sucesso!", null);
    }

    public void confirmarAnaliseDeItem(RequisicaoMaterialItem ri, List<RequisicaoMaterialItem> list, int index) {

        if (!ri.getStatus().equals("Aguardando Saida")) {
            ri.getIdProduto().setQuantidadeReservada(ri.getIdProduto().getQuantidadeReservada().add(ri.getQuantidadeAprovada()));
            pEJB.Atualizar(ri.getIdProduto());

            if ((ri.getIdProduto().getQuantidade().longValue()) == ri.getQuantidadeAprovada().longValue()) {
                if (ri.getIdProduto().getEstoqueMin().longValue() > 0) {
                    RequisicaoMaterial r = new RequisicaoMaterial();
                    r.setIdUsuario(usuarioEJB.pegaUsuarioLogadoNaSessao());
                    r.setIdEmpresa(usuarioEJB.pegaEmpresaLogadoNaSessao());
                    r.setIdLocalTrabalho(usuarioEJB.pegaEmpresaLogadoNaSessao().getIdLocalTrabalho());
                    r.setIdCliente(usuarioEJB.pegaEmpresaLogadoNaSessao().getIdLocalTrabalho().getIdCentroCusto().getIdCliente());
                    r.setStatus("Em Cotação");
                    r.setDtCadastro(new Date());
                    r.setTipo("R");
                    r.setObservacao("Requisição gerada referente ao abastecimento do estoque minimo!");
                    List<RequisicaoMaterialItem> itens = new ArrayList<RequisicaoMaterialItem>();
                    RequisicaoMaterialItem item = new RequisicaoMaterialItem();
                    item.setIdProduto(ri.getIdProduto());
                    item.setDescricao(ri.getIdProduto().getDescricao());
                    item.setQuantidade(ri.getIdProduto().getEstoqueMin());
                    item.setQuantidadeAprovada(ri.getIdProduto().getEstoqueMin());
                    item.setVlrUnitario(ri.getVlrUnitario());
                    item.setStatus("Cotação");
                    item.setQuantidadeCotada(ri.getIdProduto().getEstoqueMin());
                    itens.add(item);
                    r.setRequisicaoMaterialItemList(itens);
                    requisicaoMaterialEJB.SalvarRequisicao(r);
                }
            }
        }
        ri.setStatus("Aceito");
        requisicaoMaterialItemEJB.Atualizar(ri);
        verificaStatusDaRequisicao(ri.getIdRequisicaoMaterial());
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.update("tabs:frmAnaliseAlmox:tblItens:" + index + ":tblSubItens");
        rc.update("tabs:frmAnaliseAlmox:tblItens");
    }

    public void cotarItem(RequisicaoMaterialItem item) {
        item.setStatus("Cotação");

        if (item.getIdProduto().getQuantidadeReservada() == null) {
            item.getIdProduto().setQuantidadeReservada(new BigDecimal(BigInteger.ZERO));
        }
        if (item.getIdProduto().getQuantidade() == null) {
            item.getIdProduto().setQuantidade(new BigDecimal(BigInteger.ZERO));
        }
        if (item.getIdProduto().getQuantidadeReservada() == null) {
            item.getIdProduto().setQuantidadeReservada(new BigDecimal(BigInteger.ZERO));
        }


        if (item.getIdProduto().getQuantidade().subtract(item.getIdProduto().getQuantidadeReservada()).longValue() < 0) {
            item.setQuantidadeCotada(item.getQuantidadeAprovada().add(item.getIdProduto().getEstoqueMin()));
        } else {
            item.setQuantidadeCotada(item.getQuantidadeAprovada().subtract(item.getIdProduto().getQuantidade().subtract(item.getIdProduto().getQuantidadeReservada())).add(item.getIdProduto().getEstoqueMin()));
        }
        item.getIdProduto().setQuantidadeReservada(item.getIdProduto().getQuantidadeReservada().add(item.getQuantidadeAprovada()));

        pEJB.Atualizar(item.getIdProduto());
        Math.abs(item.getQuantidadeCotada().longValue());
        requisicaoMaterialItemEJB.Atualizar(item);
        requisicaoMaterialEJB.Atualizar(item.getIdRequisicaoMaterial());
        verificaStatusDaRequisicao(item.getIdRequisicaoMaterial());
        MensageFactory.info("Item marcado para cotar!", null);

    }

    public void rollbackSituacao(RequisicaoMaterialItem item) {
        item.setStatus("Aguardando");
        Long l = Math.abs((item.getIdProduto().getQuantidadeReservada().subtract(item.getQuantidadeAprovada())).longValue());
        item.getIdProduto().setQuantidadeReservada(new BigDecimal(l));
        item.setQuantidadeCotada(new BigDecimal(BigInteger.ZERO));
        pEJB.Atualizar(item.getIdProduto());
        requisicaoMaterialItemEJB.Atualizar(item);
        verificaStatusDaRequisicao(item.getIdRequisicaoMaterial());
    }

    public void verificaStatusDaRequisicao(RequisicaoMaterial r) {
        boolean bol = false;
        if (!r.getStatus().equals("Aguardando Pedido")) {
            for (RequisicaoMaterialItem rmi : r.getRequisicaoMaterialItemList()) {
                if (rmi.getStatus().equals("Aguardando")) {
                    bol = true;
                    break;
                }
            }
            if (bol) {
                if (!r.getStatus().equals("Análise de Almoxarifado")) {
                    r.setStatus("Análise de Almoxarifado");
                    requisicaoMaterialEJB.Atualizar(r);
                }
            } else {
                boolean bol2 = false;
                for (RequisicaoMaterialItem rmi : r.getRequisicaoMaterialItemList()) {
                    if (rmi.getStatus().equals("Aceito")) {
                        bol2 = true;
                    } else {
                        bol2 = false;
                        break;
                    }
                }
                if (bol2) {
                    r.setStatus("Aguardando Saída");
                    requisicaoMaterialEJB.Atualizar(r);
                } else {
                    r.setStatus("Em Cotação");
                    requisicaoMaterialEJB.Atualizar(r);
                }
            }
        }

        if (r.getStatus().equals("Aguardando Pedido")) {
            int i = 0;
            for (RequisicaoMaterialItem rmi : r.getRequisicaoMaterialItemList()) {
                if (!"Aceito".equals(rmi.getStatus())) {
                    i = 1;
                }
            }
            if (i == 0) {
                r.setStatus("Aguardando Saída");
                requisicaoMaterialEJB.Atualizar(r);
            }
        }
    }

    public String statusRequisicao(String s) {
        try {
            if (s.equals("Aprovado")) {
                return "label label-success";
            } else if (s.equals("Recusado")) {
                return "label label-danger";
            } else if (s.equals("Finalizado")) {
                return "label label-info";
            } else if (s.equals("Aguardando Análise")) {
                return "label label-warning";
            } else if (s.equals("Análise de Almoxarifado")) {
                return "label label-warning";
            } else if (s.equals("Aguardando Saída")) {
                return "label label-info";
            } else if (s.equals("Em Cotação")) {
                return "label label-danger";
            } else {
                return "label label-primary";
            }
        } catch (Exception e) {
            return "";
        }

    }

    /*Inicio das pesquisas*/
    public List<RequisicaoMaterial> carregaRequisicaoDoMesAtual() {
        try {
            return requisicaoMaterialEJB.carregaRequisicaoDoMesAtual(Bcrutils.primeiroDiaDoMes(), Bcrutils.ultimoDiaDoMes());
        } catch (Exception e) {
            return requisicaoMaterialEJB.listarRequisaoMateriais();
        }
    }

    /* Início GET's e SET's */
    public RequisicaoMaterial getRequisicaoMaterial() {
        return requisicaoMaterial;
    }

    public void setRequisicaoMaterial(RequisicaoMaterial requisicaoMaterial) {
        this.requisicaoMaterial = requisicaoMaterial;
    }

    public RequisicaoMaterialItem getRequisicaoMaterialItem() {
        return requisicaoMaterialItem;
    }

    public void setRequisicaoMaterialItem(RequisicaoMaterialItem requisicaoMaterialItem) {
        this.requisicaoMaterialItem = requisicaoMaterialItem;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalTrabalho getLocalTrabalho() {
        return localTrabalho;
    }

    public void setLocalTrabalho(LocalTrabalho localTrabalho) {
        this.localTrabalho = localTrabalho;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<RequisicaoMaterial> getListaRequisicaoMaterial() {
        return listaRequisicaoMaterial;
    }

    public void setListaRequisicaoMaterial(List<RequisicaoMaterial> listaRequisicaoMaterial) {
        this.listaRequisicaoMaterial = listaRequisicaoMaterial;
    }

    public List<LocalTrabalho> getListaLocalTrabalho() {
        return listaLocalTrabalho;
    }

    public void setListaLocalTrabalho(List<LocalTrabalho> listaLocalTrabalho) {
        this.listaLocalTrabalho = listaLocalTrabalho;
    }

    public List<RequisicaoMaterialItem> getListaRequisicaoMaterialItems() {
        return listaRequisicaoMaterialItems;
    }

    public void setListaRequisicaoMaterialItems(List<RequisicaoMaterialItem> listaRequisicaoMaterialItems) {
        this.listaRequisicaoMaterialItems = listaRequisicaoMaterialItems;
    }

    public List<Cliente> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public RequisicaoMaterial getRequisicaoMaterialReferente() {
        return requisicaoMaterialReferente;
    }

    public void setRequisicaoMaterialReferente(RequisicaoMaterial requisicaoMaterialReferente) {
        this.requisicaoMaterialReferente = requisicaoMaterialReferente;
    }

    public List<RequisicaoMaterial> getListaRequisicaoMaterialSelecionadas() {
        return listaRequisicaoMaterialSelecionadas;
    }

    public void setListaRequisicaoMaterialSelecionadas(List<RequisicaoMaterial> listaRequisicaoMaterialSelecionadas) {
        this.listaRequisicaoMaterialSelecionadas = listaRequisicaoMaterialSelecionadas;
    }

    public List<Produto> getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(List<Produto> listaProdutos) {
        this.listaProdutos = listaProdutos;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValorItem() {
        return valorItem;
    }

    public void setValorItem(BigDecimal valorItem) {
        this.valorItem = valorItem;
    }

    public BigDecimal getValorTotalItem() {
        return valorTotalItem;
    }

    public void setValorTotalItem(BigDecimal valorTotalItem) {
        this.valorTotalItem = valorTotalItem;
    }

    public BigDecimal getValorTotalItensRequisicao() {
        return valorTotalItensRequisicao;
    }

    public void setValorTotalItensRequisicao(BigDecimal valorTotalItensRequisicao) {
        this.valorTotalItensRequisicao = valorTotalItensRequisicao;
    }

    public Integer getQuantidadeTotalItensRequisicao() {
        return quantidadeTotalItensRequisicao;
    }

    public void setQuantidadeTotalItensRequisicao(Integer quantidadeTotalItensRequisicao) {
        this.quantidadeTotalItensRequisicao = quantidadeTotalItensRequisicao;
    }

    public List<RequisicaoMaterial> getLista() {
        return lista;
    }

    public void setLista(List<RequisicaoMaterial> lista) {
        this.lista = lista;
    }

    public LazyDataModel<RequisicaoMaterial> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<RequisicaoMaterial> model) {
        this.model = model;
    }

    public Cotacao getCotacao() {
        return cotacao;
    }

    public void setCotacao(Cotacao cotacao) {
        this.cotacao = cotacao;
    }

    public RequisicaoMaterialItem getRequisicaoMaterialItemQuantidade() {
        return requisicaoMaterialItemQuantidade;
    }

    public void setRequisicaoMaterialItemQuantidade(RequisicaoMaterialItem requisicaoMaterialItemQuantidade) {
        this.requisicaoMaterialItemQuantidade = requisicaoMaterialItemQuantidade;
    }

    public Date getDtIni() {
        return dtIni;
    }

    public void setDtIni(Date dtIni) {
        this.dtIni = dtIni;
    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        this.dtFim = dtFim;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
