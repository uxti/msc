
package com.bcr.view;

import com.bcr.controller.CargoEJB;
import com.bcr.controller.EmpresaEJB;
import com.bcr.model.Cargo;
import com.bcr.model.Empresa;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class CargoMB implements Serializable{

    /**
     * Creates a new instance of CargoMB
     */
    @EJB
    CargoEJB caEJB;
    private Cargo cargo;
    @EJB
    EmpresaEJB eEJB;
    private Empresa empresa;

    public CargoMB() {
        novo();
    }

    public void novo() {
        cargo = new Cargo();
        empresa = new Empresa();
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public void salvar() {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        empresa = eEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
        cargo.setIdEmpresa(empresa);
        if (cargo.getIdCargo() == null) {
            try {
                caEJB.Salvar(cargo);
                novo();
                MensageFactory.info("Salvo efetuado com sucesso", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        } else {
            try {
                caEJB.Atualizar(cargo);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }

    public void excluir() {
        try {
            caEJB.Excluir(cargo, cargo.getIdCargo());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
    public List<Cargo> listarCargos(){
        return caEJB.ListarTodos();
    }
}
