/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.PlanilhaProdutoEJB;
import com.bcr.model.Planilha;
import com.bcr.model.PlanilhaItem;
import com.bcr.model.PlanilhaMateriais;
import com.bcr.model.Produto;
import com.bcr.model.PropostaItem;
import com.bcr.pojo.GrupoPlanilha;
import com.bcr.pojo.TipoPlanilha;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.commons.beanutils.BeanComparator;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class PlanilhaMaoObraMB {

    @EJB
    PlanilhaProdutoEJB pmoEJB;
    private Planilha planilha;
    private PlanilhaItem planilhaItem;
    private List<PlanilhaItem> itensTemporarios;
    private List<TipoPlanilha> tipos;
    private List<GrupoPlanilha> grupos;
    private PlanilhaMateriais materiais;

    public PlanilhaMaoObraMB() {
        novo();
    }

    public void novo() {
        planilha = new Planilha();
        planilhaItem = new PlanilhaItem();
        itensTemporarios = new ArrayList<PlanilhaItem>();
        tipos = new ArrayList<TipoPlanilha>();
        iniciarTipoGrupos();
        materiais = new PlanilhaMateriais();
    }

    public void salvar() {
        if (planilha.getDescricao() == null) {
            MensageFactory.info("Informe a descrição da planilha!", null);
        } else {
            planilha.setTipo("M");
            if (planilha.getIdPlanilha() == null) {
                try {
                    pmoEJB.salvarCompleto(planilha);
                    novo();
                    MensageFactory.info("Planilha salvar com sucesso!", null);
                } catch (Exception e) {
                    MensageFactory.warn("Erro ao tentar salvar a planilha!", null);
                }
            } else {
                try {
                    pmoEJB.salvarCompleto(planilha);
                    novo();
                    MensageFactory.info("Planilha atualizada com sucesso!", null);
                } catch (Exception e) {
                    MensageFactory.warn("Erro ao tentar atualizar a planilha!", null);
                }
            }
        }
    }

    public void carregarGrupos(String tipo) {
        for (TipoPlanilha tp : tipos) {
            if (tp.getDescricao().equals(tipo)) {
                grupos = tp.getGrupos();
            }
        }
    }

    public void copiarPlanilha() {
        planilha.setIdPlanilha(null);
        for (PlanilhaItem pi : planilha.getPlanilhaItemList()) {
            pi.setIdPlanilhaItem(null);
            pi.setIdPlanilha(null);
        }
        itensTemporarios.addAll(planilha.getPlanilhaItemList());
        for (PlanilhaMateriais pm : planilha.getPlanilhaMateriaisList()) {
            pm.setIdPlanilhaMateriais(null);
            pm.setIdPlanilha(null);
        }
    }

    public int retornaUltimaPosicaoNaPlanilha(){
        int i = 0;
        for(PlanilhaItem pi : itensTemporarios){
            if(pi.getPosicao() > i){
                i = pi.getPosicao();
            }
        }
        return i;
    }
    
    public void adicionar() {
        planilhaItem.setPosicao(retornaUltimaPosicaoNaPlanilha() + 1);
        itensTemporarios.add(planilhaItem);
        String tipoTemp = planilhaItem.getTipo();
        String grupoTemp = planilhaItem.getGrupo();
        planilhaItem = new PlanilhaItem();
        planilhaItem.setTipo(tipoTemp);
        planilhaItem.setGrupo(grupoTemp);
        planilha.setPlanilhaItemList(itensTemporarios);

//        RequestContext rc = RequestContext.getCurrentInstance();

//        for (int y = 0; y < planilha.getPlanilhaItemList().size(); y++) {
//            for (int i = 0; i < planilha.getPlanilhaItemList().size(); i++) {
//                rc.update("frmCadastro:tabss:agrp:" + y + ":pan:agrp2:" + i + ":tblItens");
//            }
//        }
    }

    public void adicionarMaterial() {
        System.out.println("chamou isso");
        try {
            planilha.getPlanilhaMateriaisList().add(materiais);
            materiais = new PlanilhaMateriais();
        } catch (Exception e) {
            System.out.println(e);
            planilha.setPlanilhaMateriaisList(new ArrayList<PlanilhaMateriais>());
            planilha.getPlanilhaMateriaisList().add(materiais);
            materiais = new PlanilhaMateriais();
        }
    }

    public void excluirMaterial(PlanilhaMateriais material, int index) {
        if (material.getIdPlanilhaMateriais() != null) {
            pmoEJB.excluirPlanilhaMaterial(material);
        }
        planilha.getPlanilhaMateriaisList().remove(index);
    }

    public List<Planilha> listaPlanilhas() {
        return pmoEJB.listaPlanilhasMaoObra();
    }

    public void excluir() {
        if (planilha != null) {
            try {
                pmoEJB.excluirCompleto(planilha);
                MensageFactory.info("Planilha excluída com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Não foi possível excluir a planilha!", null);
                Bcrutils.escreveLogErro(e);
            }
        }

    }

    public Planilha getPlanilha() {
        if (planilha.getIdPlanilha() != null) {
            if (!planilha.getPlanilhaItemList().isEmpty()) {
                Comparator<PlanilhaItem> comp = new BeanComparator("posicao");
                Collections.sort(planilha.getPlanilhaItemList(), comp);
                itensTemporarios = planilha.getPlanilhaItemList();


            }
        }
        return planilha;
    }

    public void excluirItemPlanilha(int index, PlanilhaItem pi) {
        if (pi.getIdPlanilhaItem() != null) {
            pmoEJB.excluirItemPlanilha(pi);
        }
        itensTemporarios.remove(pi);
        planilha.getPlanilhaItemList().remove(pi);
    }

    public String escreveCor(String param) {
        if (param.equals("1 - Mão de Obra")) {
            return "background: #ADD8E6";
        } else if (param.equals("2 - Encargos Sociais")) {
            return "background: #E6E6FA";
        } else if (param.equals("3 - Insumos")) {
            return "background: #5F9EA0";
        } else if (param.equals("4 - Demais Componentes")) {
            return "background: #F5DEB3";
        } else if (param.equals("5 - Tributos")) {
            return "background: #E0FFFF";
        } else {
            return "";
        }
    }

    public void setPlanilha(Planilha planilha) {
        this.planilha = planilha;
    }

    public PlanilhaItem getPlanilhaItem() {
        return planilhaItem;
    }

    public void setPlanilhaItem(PlanilhaItem planilhaItem) {
        this.planilhaItem = planilhaItem;
    }

    public List<PlanilhaItem> getItensTemporarios() {
        return itensTemporarios;
    }

    public void setItensTemporarios(List<PlanilhaItem> itensTemporarios) {
        this.itensTemporarios = itensTemporarios;
    }

    public List<TipoPlanilha> getTipos() {
        return tipos;
    }

    public void setTipos(List<TipoPlanilha> tipos) {
        this.tipos = tipos;
    }

    public List<GrupoPlanilha> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<GrupoPlanilha> grupos) {
        this.grupos = grupos;
    }

    public PlanilhaMateriais getMateriais() {
        return materiais;
    }

    public void setMateriais(PlanilhaMateriais materiais) {
        this.materiais = materiais;
    }

    private void iniciarTipoGrupos() {
        TipoPlanilha tp1 = new TipoPlanilha("1 - Mão de Obra");
        GrupoPlanilha g1 = new GrupoPlanilha("Remuneração", tp1);
        GrupoPlanilha g2 = new GrupoPlanilha("Reserva Técnica", tp1);
        GrupoPlanilha g02 = new GrupoPlanilha("Outros", tp1);
        List<GrupoPlanilha> gp1 = new ArrayList<GrupoPlanilha>();
        gp1.add(g1);
        gp1.add(g2);
        gp1.add(g02);
        tp1.setGrupos(gp1);
        System.out.println("tem " + tp1.getGrupos().size() + " grupos!");
        tipos.add(tp1);

        TipoPlanilha tp2 = new TipoPlanilha("2 - Encargos Sociais");
        GrupoPlanilha g3 = new GrupoPlanilha("Grupo A", tp2);
        GrupoPlanilha g4 = new GrupoPlanilha("Grupo B", tp2);
        GrupoPlanilha g5 = new GrupoPlanilha("Grupo C", tp2);
        GrupoPlanilha g6 = new GrupoPlanilha("Grupo D", tp2);
        GrupoPlanilha g06 = new GrupoPlanilha("Outros", tp2);
        List<GrupoPlanilha> gp2 = new ArrayList<GrupoPlanilha>();
        gp2.add(g3);
        gp2.add(g4);
        gp2.add(g5);
        gp2.add(g6);
        gp2.add(g06);
        tp2.setGrupos(gp2);
        tipos.add(tp2);

        TipoPlanilha tp3 = new TipoPlanilha("3 - Insumos");
        GrupoPlanilha g7 = new GrupoPlanilha("Outros", tp3);
        List<GrupoPlanilha> gp3 = new ArrayList<GrupoPlanilha>();
        gp3.add(g7);
        tp3.setGrupos(gp3);
        tipos.add(tp3);

        TipoPlanilha tp4 = new TipoPlanilha("4 - Demais Componentes");
        GrupoPlanilha g8 = new GrupoPlanilha("Outros", tp4);
        List<GrupoPlanilha> gp4 = new ArrayList<GrupoPlanilha>();
        gp4.add(g8);
        tp4.setGrupos(gp4);
        tipos.add(tp4);

        TipoPlanilha tp5 = new TipoPlanilha("5 - Tributos");
        GrupoPlanilha g9 = new GrupoPlanilha("Custo Mensal Por Empregado", tp5);
        GrupoPlanilha g09 = new GrupoPlanilha("Outros", tp5);
        List<GrupoPlanilha> gp5 = new ArrayList<GrupoPlanilha>();
        gp5.add(g9);
        gp5.add(g09);
        tp5.setGrupos(gp5);
        tipos.add(tp5);


        System.out.println("tipos carregados" + tipos.size());
    }
}
