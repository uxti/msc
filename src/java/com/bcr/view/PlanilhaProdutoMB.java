/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.PlanilhaProdutoEJB;
import com.bcr.model.Planilha;
import com.bcr.model.PlanilhaItem;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class PlanilhaProdutoMB {

    @EJB
    PlanilhaProdutoEJB ppEJB;
    private Planilha planilha;
    private PlanilhaItem planilhaItem;
    private List<PlanilhaItem> listaItensTemporaria;

    public PlanilhaProdutoMB() {
        novo();
    }

    public void novo() {
        planilha = new Planilha();
        planilhaItem = new PlanilhaItem();
        listaItensTemporaria = new ArrayList<PlanilhaItem>();
        planilhaItem.setTipo("P");
    }

    public void salvar() {
        planilha.setTipo("P");
        if (planilha.getIdPlanilha() == null) {
            try {
                ppEJB.salvarCompleto(planilha);
                novo();
                MensageFactory.info("Planilha de produtos salva com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Ocorreu algum erro ao tentar salvar a plnilha! Verifique no log.", null);
                Bcrutils.escreveLogErro(e);
            }
        } else {
            try {
                ppEJB.salvarCompleto(planilha);
                novo();
                MensageFactory.info("Planilha de produtos atualizada com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Ocorreu algum erro ao tentar atualizar a plnilha! Verifique no log.", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public void adiciounou() {
        try {
            listaItensTemporaria.add(planilhaItem);
            planilhaItem = new PlanilhaItem();
            planilhaItem.setTipo("P");
            planilha.setPlanilhaItemList(listaItensTemporaria);
        } catch (Exception e) {
            Bcrutils.escreveLogErro(e);
        }
    }

    public void excluir() {
        if (planilha != null) {
            try {
                ppEJB.excluirCompleto(planilha);
                MensageFactory.info("Planilha excluida com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Erro ao tentar excluir a planilha!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public List<Planilha> listarPlanilhas() {
        return ppEJB.ListarTodos();
    }
    public List<Planilha> listarPlanilhasMaoDeObra() {
        return ppEJB.listaPlanilhasMaoObra();
    }
    public List<Planilha> listarPlanilhasProdutos() {
        return ppEJB.listaPlanilhasProduto();
    }

    public void verTipo() {
        System.out.println(planilhaItem.getTipo());
    }

    public Planilha getPlanilha() {
        if (planilha.getIdPlanilha() != null) {
            if (!planilha.getPlanilhaItemList().isEmpty()) {
                listaItensTemporaria = planilha.getPlanilhaItemList();
            }
        }
        return planilha;
    }

    public void setPlanilha(Planilha planilha) {
        this.planilha = planilha;
    }

    public PlanilhaItem getPlanilhaItem() {
        return planilhaItem;
    }

    public void setPlanilhaItem(PlanilhaItem planilhaItem) {
        this.planilhaItem = planilhaItem;
    }
}
