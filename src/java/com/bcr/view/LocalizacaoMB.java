/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.LocalizacaoEJB;
import com.bcr.model.Localizacao;
import com.bcr.util.BcrLog;
import com.bcr.util.MensageFactory;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class LocalizacaoMB {

    @EJB
    LocalizacaoEJB lEJB;
    private Localizacao localizacao;

    public LocalizacaoMB() {
        novo();
    }

    public void novo() {
        localizacao = new Localizacao();
    }

    public void salvar() {
        if (localizacao.getIdLocalizacao() == null) {
            try {
                lEJB.Salvar(localizacao);
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println(e);
//                BcrLog.criarLog("O erro ocorrido é: ", new Date(), e, LocalizacaoMB.class);
            }
        } else {
            try {
                lEJB.Atualizar(localizacao);
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println(e);
//                BcrLog.criarLog("O erro ocorrido é: ", new Date(), e, LocalizacaoMB.class);
            }
        }
    }

    public List<Localizacao> listaTodos() {
        return lEJB.ListarTodos();
    }

    public void excluir() {
        try {
            lEJB.Excluir(localizacao, localizacao.getIdLocalizacao());
            MensageFactory.info("Excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
//            BcrLog.criarLog("O erro ocorrido é: ", new Date(), e, LocalizacaoMB.class);
        }
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }
}
