/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Perfil;
import com.bcr.model.Usuario;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class UsuarioMB {

    @EJB
    UsuarioEJB uEJB;
    private Usuario usuario;

    public UsuarioMB() {
        usuario = new Usuario();
        usuario.setDtCadastro(new Date());
        usuario.setDtUltimaAtualizacao(new Date());
    }

    public void novo() {
        usuario = new Usuario();
    }

    public void salvar() {
        if (usuario.getIdUsuario() == null) {
            try {
                uEJB.Salvar(usuario);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                Bcrutils.escreveLogErro(e);
            }
        } else {
            try {
                uEJB.Atualizar(usuario);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    
  
    
    public List<Perfil> listaPerfis() {
        return uEJB.listaPerfis();
    }

    public List<Usuario> listaUsuarios() {
        return uEJB.ListarTodos();
    }

    public void excluir() {
        try {
            uEJB.Excluir(usuario, usuario.getIdUsuario());
            MensageFactory.info("Excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            Bcrutils.escreveLogErro(e);
        }
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
