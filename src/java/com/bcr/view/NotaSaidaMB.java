/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.NotaSaidaEJB;
import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Centrocustoplanocontas;
import com.bcr.model.ContasReceber;
import com.bcr.model.NotaSaida;
import com.bcr.model.NotaSaidaItem;
import com.bcr.util.Bcrutils;
import com.bcr.util.MensageFactory;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class NotaSaidaMB {

    @EJB
    NotaSaidaEJB nsEJB;
    @EJB
    UsuarioEJB uEJB;
    private NotaSaida notaSaida;
    private NotaSaidaItem notaSaidaItem;
    private List<NotaSaidaItem> itensTemporarios;
    private LazyDataModel<NotaSaida> model;
    private ContasReceber contasReceber;
    private List<ContasReceber> listaContasReceber;

    public NotaSaidaMB() {
        novo();
    }

    public void novo() {
        notaSaida = new NotaSaida();
        notaSaidaItem = new NotaSaidaItem();
        itensTemporarios = new ArrayList<NotaSaidaItem>();
        listaContasReceber = new ArrayList<ContasReceber>();
        contasReceber = new ContasReceber();
        contasReceber.setCentrocustoplanocontasList(new ArrayList<Centrocustoplanocontas>());
        notaSaida.setContasReceberList(new ArrayList<ContasReceber>());
        carregarNotaEntrada();
    }

    public void adicionarItem() {
        itensTemporarios.add(notaSaidaItem);
        notaSaida.setNotaSaidaItemList(itensTemporarios);
        notaSaidaItem = new NotaSaidaItem();
        try {
            calcularValoresNota();
        } catch (Exception e) {
        }
    }

    public void excluirItem(NotaSaidaItem nsi) {
        try {
            if (nsi.getIdNotaSaidaItem() != null) {
                nsEJB.excluirItem(nsi);
            }
            novo();
            calcularValoresNota();
            MensageFactory.info("Item excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível excluir o item!", null);
        }

    }

    public void calcularTotalItem() {
        if (notaSaidaItem.getValorDesconto() == null) {
            notaSaidaItem.setValorDesconto(BigDecimal.ZERO);
        }
        if (notaSaidaItem.getQuantidade() == null) {
            notaSaidaItem.setQuantidade(BigDecimal.ZERO);
        }
        if (notaSaidaItem.getValorUnitario() == null) {
            notaSaidaItem.setValorUnitario(BigDecimal.ZERO);
        }
        notaSaidaItem.setValorTotal(notaSaidaItem.getQuantidade().multiply(notaSaidaItem.getValorUnitario()).subtract(notaSaidaItem.getValorDesconto()));
    }

    public void calcularTotalItem(NotaSaidaItem nsi) {
        if (nsi.getValorDesconto() == null) {
            nsi.setValorDesconto(BigDecimal.ZERO);
        }
        if (nsi.getQuantidade() == null) {
            nsi.setQuantidade(BigDecimal.ZERO);
        }
        if (nsi.getValorUnitario() == null) {
            nsi.setValorUnitario(BigDecimal.ZERO);
        }
        nsi.setValorTotal(nsi.getQuantidade().multiply(nsi.getValorUnitario()).subtract(nsi.getValorDesconto()));
    }

    public void verquantidade() {
        System.out.println(notaSaidaItem.getQuantidade());
    }

    public void salvar() {
        notaSaida.setIdEmpresa(uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa());
        calcularValoresNota();
        if (notaSaida.getIdNotaSaida() == null) {
            try {
                if (notaSaida.getIdFormaPagamento() != null) {
                    if (notaSaida.getIdFormaPagamento().getTipo().equals("V")) {
                        listaContasReceber = new ArrayList<ContasReceber>();
                        ContasReceber cr = new ContasReceber();
                        cr.setIdCliente(notaSaida.getIdCliente());
                        cr.setDtLancamento(new Date());
                        cr.setNumParcela(1);
                        cr.setTotalParcelas(1);
                        cr.setDtVencimento(contasReceber.getDtVencimento());
                        cr.setTotalParcelas(contasReceber.getNumParcela());
                        cr.setStatus(false);
                        cr.setNumDocto(notaSaida.getNumero());
                        cr.setIdEmpresa(uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa());
                        cr.setTipodocumento(contasReceber.getTipodocumento());
                        cr.setIdPlanoContas(contasReceber.getIdPlanoContas());
                        cr.setValorDuplicata(notaSaida.getValortotalNF());
                        cr.setCentrocustoplanocontasList(contasReceber.getCentrocustoplanocontasList());
                        System.out.println("Esse contas a receber é a vista e tem "+cr.getCentrocustoplanocontasList().size()+" centros de custo!");
                        notaSaida.getContasReceberList().add(cr);
                    }
                }
                nsEJB.SalvarNovo(notaSaida);
                novo();
                MensageFactory.info("Nota salva com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Não foi possível salvar a nota!", null);
                Bcrutils.escreveLogErro(e);
            }
        } else {
            try {
                if (notaSaida.getIdFormaPagamento() != null) {
                    if (notaSaida.getIdFormaPagamento().getTipo().equals("V")) {
                        listaContasReceber = new ArrayList<ContasReceber>();
                        ContasReceber cr = new ContasReceber();
                        cr.setIdCliente(notaSaida.getIdCliente());
                        cr.setDtLancamento(new Date());
                        cr.setNumParcela(1);
                        cr.setTotalParcelas(1);
                        cr.setDtVencimento(contasReceber.getDtVencimento());
                        cr.setTotalParcelas(contasReceber.getNumParcela());
                        cr.setStatus(false);
                        cr.setNumDocto(notaSaida.getNumero());
                        cr.setIdEmpresa(uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa());
                        cr.setTipodocumento(contasReceber.getTipodocumento());
                        cr.setIdPlanoContas(contasReceber.getIdPlanoContas());
                        cr.setValorDuplicata(notaSaida.getValortotalNF());
                        listaContasReceber.add(cr);
                        notaSaida.setContasReceberList(listaContasReceber);
                    }
                }
                nsEJB.SalvarNovo(notaSaida);
                novo();
                MensageFactory.info("Nota atualizada com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.warn("Não foi possível atualizar a nota!", null);
                Bcrutils.escreveLogErro(e);
            }

        }
    }

    public void carregarNotaEntrada() {
        model = new LazyDataModel<NotaSaida>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<NotaSaida> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
                        if (sf.toString().contains("where")) {
                            sf.append(" and n." + filterProperty + " like'%" + filterValue + "%'");
                        } else {
                            sf.append(" where n." + filterProperty + " like'%" + filterValue + "%'");
                        }
                    } else {
                        sf.append(" and n." + filterProperty + " like'%" + filterValue + "%'");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by n." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by n." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }

                System.out.println(Clausula);
                setRowCount(Integer.parseInt(nsEJB.contarRegistro(Clausula).toString()));
                List<NotaSaida> lista = new ArrayList<NotaSaida>();
                lista = nsEJB.listarNotaSaidaLazyModeWhere(first, pageSize, Clausula);
                return lista;
            }
        };
    }

    public void excluir() {
        if (notaSaida.getIdNotaSaida() != null) {
            try {
                nsEJB.excluirNovo(notaSaida);
                MensageFactory.info("Nota excluída com sucesso!", null);
                novo();
            } catch (Exception e) {
                MensageFactory.warn("Não foi possivel excluir a nota!", null);
                Bcrutils.escreveLogErro(e);
            }
        }
    }

    public void calcularValoresNota() {
        BigDecimal valorTotal = new BigDecimal(BigInteger.ZERO);
        BigDecimal valorDesconto = new BigDecimal(BigInteger.ZERO);
        if (notaSaida.getNotaSaidaItemList().isEmpty()) {
            valorTotal = new BigDecimal(BigInteger.ZERO);
            valorDesconto = new BigDecimal(BigInteger.ZERO);
        } else {
            for (NotaSaidaItem nsi : notaSaida.getNotaSaidaItemList()) {
                if (nsi.getValorDesconto() == null) {
                    nsi.setValorDesconto(new BigDecimal(BigInteger.ZERO));
                }
                valorTotal = valorTotal.add(nsi.getValorTotal());
                valorDesconto = valorDesconto.add(nsi.getValorDesconto());
            }
        }

        notaSaida.setValorDesconto(valorDesconto);
        notaSaida.setValortotalNF(valorTotal);
        notaSaida.setValorTotalProdutos(valorTotal);
    }

    public void gerarParcelas() {
        int totalParcelas = contasReceber.getNumParcela();
        BigDecimal valorParcelas = new BigDecimal(notaSaida.getValortotalNF().longValue() / totalParcelas);
        for (int i = 0; i < totalParcelas; i++) {
            ContasReceber cr = new ContasReceber();
            cr.setIdCliente(notaSaida.getIdCliente());
            cr.setDtLancamento(new Date());
            cr.setNumParcela(i + 1);
            cr.setDtVencimento(Bcrutils.addMes(notaSaida.getDtEmissao(), i + 1));
            cr.setTotalParcelas(contasReceber.getNumParcela());
            cr.setStatus(false);
            cr.setNumDocto(notaSaida.getNumero());
            cr.setIdEmpresa(uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa());
            cr.setTipodocumento(contasReceber.getTipodocumento());
            cr.setIdPlanoContas(contasReceber.getIdPlanoContas());
            cr.setValorDuplicata(valorParcelas);
            cr.setCentrocustoplanocontasList(new ArrayList<Centrocustoplanocontas>());
            listaContasReceber.add(cr);
        }
        notaSaida.setContasReceberList(listaContasReceber);
    }

    public void selecionarLocalDeTrabalho(Centrocustoplanocontas cc, int index) {
        cc.setIdCliente(cc.getIdLocalTrabalho().getIdCentroCusto().getIdCliente());
        cc.setIdCentroCusto(cc.getIdLocalTrabalho().getIdCentroCusto());
    }

    public void adicionarCentroCusto(List<Centrocustoplanocontas> lista) {
        lista.add(new Centrocustoplanocontas());
    }

    public NotaSaida getNotaSaida() {
        return notaSaida;
    }

    public void setNotaSaida(NotaSaida notaSaida) {
        this.notaSaida = notaSaida;
    }

    public NotaSaidaItem getNotaSaidaItem() {
        return notaSaidaItem;
    }

    public void setNotaSaidaItem(NotaSaidaItem notaSaidaItem) {
        this.notaSaidaItem = notaSaidaItem;
    }

    public List<NotaSaidaItem> getItensTemporarios() {
        return itensTemporarios;
    }

    public void setItensTemporarios(List<NotaSaidaItem> itensTemporarios) {
        this.itensTemporarios = itensTemporarios;
    }

    public LazyDataModel<NotaSaida> getModel() {
        return model;
    }

    public void setModel(LazyDataModel<NotaSaida> model) {
        this.model = model;
    }

    public ContasReceber getContasReceber() {
        return contasReceber;
    }

    public void setContasReceber(ContasReceber contasReceber) {
        this.contasReceber = contasReceber;
    }

    public List<ContasReceber> getListaContasReceber() {
        return listaContasReceber;
    }

    public void setListaContasReceber(List<ContasReceber> listaContasReceber) {
        this.listaContasReceber = listaContasReceber;
    }
}
