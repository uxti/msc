/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.EmpresaEJB;
import com.bcr.controller.LocalTrabalhoEJB;
import com.bcr.model.Empresa;
import com.bcr.model.LocalTrabalho;
import com.bcr.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class LocalTrabalhoMB implements Serializable{

    /**
     * Creates a new instance of LocalTrabalhoMB
     */
    @EJB
    LocalTrabalhoEJB ltEJB;
    private LocalTrabalho localTrabalho;
    @EJB
    EmpresaEJB eEJB;
    private Empresa empresa;
    
    public LocalTrabalhoMB() {
        novo();
    }
    public void novo(){
        localTrabalho = new LocalTrabalho();
        empresa = new Empresa();
    }

    public LocalTrabalho getLocalTrabalho() {
        return localTrabalho;
    }

    public void setLocalTrabalho(LocalTrabalho localTrabalho) {
        this.localTrabalho = localTrabalho;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public void salvar(){
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        empresa = eEJB.SelecionarPorID(Integer.parseInt(request.getParameter("id_empresa")));
        localTrabalho.setIdEmpresa(empresa);
        if(localTrabalho.getIdLocalTrabalho() == null){
            try {
                ltEJB.Salvar(localTrabalho);
                novo();
                MensageFactory.info("Salvo efetuado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }else{
            try {
                ltEJB.Atualizar(localTrabalho);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }
    public void excluir(){
        try {
            ltEJB.Excluir(localTrabalho, localTrabalho.getIdLocalTrabalho());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
    public List<LocalTrabalho> listarLocaisDeTrabalho(){
        return ltEJB.ListarTodos();
    }
    
    public List<LocalTrabalho> listarLocaisDeTrabalhoPorCentroCusto(Integer id){
        return ltEJB.ListarLocaisdeTrabalhoPorCentroCusto(id);
    }
    
    public List<LocalTrabalho> autoComplete(String nome){
        return ltEJB.autoCompleteLocalTrabalho(nome);
    }
}
