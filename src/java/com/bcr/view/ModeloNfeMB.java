/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.view;

import com.bcr.controller.ModeloNfeEJB;
import com.bcr.model.ModeloNfe;
import com.bcr.util.MensageFactory;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class ModeloNfeMB {

    /**
     * Creates a new instance of ModeloNfeMB
     */
    @EJB
    ModeloNfeEJB mEJB;
    ModeloNfe modeloNfe;
    public ModeloNfeMB() {
        novo();
    }
    public void novo(){
        modeloNfe = new ModeloNfe();
    }

    public ModeloNfe getModeloNfe() {
        return modeloNfe;
    }

    public void setModeloNfe(ModeloNfe modeloNfe) {
        this.modeloNfe = modeloNfe;
    }
    public void salvar(){
        if(modeloNfe.getIdModelo() == null){
            try {
                mEJB.Salvar(modeloNfe);
                novo();
                MensageFactory.info("Salvo efetuado com sucesso! ", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }else{
            try {
                mEJB.Atualizar(modeloNfe);
                novo();
                MensageFactory.info("Atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("Erro ocorrido: " + e);
            }
        }
    }
    public void excluir(){
        try {
            mEJB.Excluir(modeloNfe, modeloNfe.getIdModelo());
            MensageFactory.info("Excluido com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
    public List<ModeloNfe> listarModelos(){
        return mEJB.ListarTodos();
    }
}
