/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.security;

import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Perfil;
import com.bcr.model.Usuario;
import com.bcr.util.Conexao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Renato
 */
public class AppUserDetailsService implements UserDetailsService {

    @EJB
    UsuarioEJB uEJB;
    Conexao con = new Conexao();

    public Usuario getUserByLogin(String login) throws SQLException {
        Statement stmt = con.conexaoJDBC().createStatement();
        String sql = "Select * from Usuario where login = '" + login+"'";
        ResultSet rs = stmt.executeQuery(sql);
        Usuario u = new Usuario();
        while (rs.next()) {
            u.setIdUsuario(rs.getInt(0));
            String sqlPerfil = "Select * from Perfil where id_perfil = " + rs.getInt(1);
            ResultSet rs2 = stmt.executeQuery(sqlPerfil);
            Perfil p = new Perfil();
            p.setIdPerfil(rs2.getInt(0));
            p.setRole(rs2.getString(1));
            p.setDescricao(rs2.getString(2));
            u.setIdPerfil(p);
            u.setLogin(rs.getString(5));
            u.setSenha(rs.getString(6));
        }
        stmt.close();
        rs.close();
        return u;



    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        Usuario u = new Usuario();
        try {
            u = getUserByLogin(login);
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println(u.getNome());
        UsuarioSistema ud = null;
        if (u != null) {
            ud = new UsuarioSistema(u, getPerfis(u));
            System.out.println(ud.getUsuario().getLogin());
        }
        return ud;
    }

    public Collection<? extends GrantedAuthority> getPerfis(Usuario uu) {
        List<SimpleGrantedAuthority> perfis = new ArrayList();
        for (Perfil p : uEJB.retornaPerfil(uu)) {
            perfis.add(new SimpleGrantedAuthority(p.getRole()));
            System.out.println(p.getRole());
        }
        return perfis;
    }
}
