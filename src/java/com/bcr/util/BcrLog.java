/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author Renato
 */
public class BcrLog {

    public static void criarLog(String mensagem, Date dt, Exception e, Class c) {
        Logger logger = Logger.getLogger(c.getName());
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        try {
            FileHandler simpleHandler = new FileHandler(""+df.format(dt) +"_log.txt", true);
            simpleHandler.setFormatter(new SimpleFormatter());
            logger.addHandler(simpleHandler);
            logger.setUseParentHandlers(false);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Falha ao criar log", ex);
        }
        logger.info(mensagem);
        logger.warning(e.getMessage());

    }
}
