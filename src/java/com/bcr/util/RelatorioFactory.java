/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.util;

import com.bcr.model.PedidoCompra;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

/**
 *
 * @author Renato
 */
public class RelatorioFactory extends Conexao {

    private static Connection getConexao() {
        return Conexao.conexaoJDBC();
    }

    public static void gestaoResultado(String relatorio, List list, BigDecimal r1, BigDecimal r2, BigDecimal r3, BigDecimal p1, BigDecimal p2, BigDecimal p3, Date d1, Date d2) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            System.out.println(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("rRecebidas", r1);
            map.put("rVencidas", r2);
            map.put("rAbertas", r3);
            map.put("pPagas", p1);
            map.put("pVencidas", p2);
            map.put("pAbertas", p3);
            map.put("periodoIni", d1);
            map.put("periodoFim", d2);
            JRBeanCollectionDataSource fonteDados = new JRBeanCollectionDataSource(list);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, fonteDados);
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void extratoBancario(String relatorio, List list, String nome, String conta, String banco, Date mesReferente) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("nome", nome);
            map.put("conta_corrente", conta);
            map.put("banco", banco);
            map.put("mes_referente", mesReferente);
            JRBeanCollectionDataSource fonteDados = new JRBeanCollectionDataSource(list);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, fonteDados);
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void relatorioListNoParametro(String relatorio, List list) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("USUARIO", usuario);
            JRBeanCollectionDataSource fonteDados = new JRBeanCollectionDataSource(list);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, fonteDados);
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void RelatorioRequisicao(String relatorio, Integer id) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("id_requisicao", id);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void imprimirPedidoCompra(Integer id, String relatorio) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            map.put("ID_PEDIDO", id);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void relatorioCustom(String relatorio, Map<String, Object> map) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("SUBREPORT_DIR", context.getExternalContext().getRealPath("/WEB-INF/Relatorios/") + System.getProperty("file.separator"));
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, getConexao());
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }

    public static void imprimirPedidoCompraPre(String relatorio, List list, PedidoCompra pc, Map<String, Object> map) {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            HttpServletResponse response = (HttpServletResponse) context
                    .getExternalContext().getResponse();
            InputStream reportStream = context.getExternalContext().getResourceAsStream(relatorio);
            response.setContentType("application/pdf");
            ServletOutputStream servletOutputStream = response.getOutputStream();
            JRBeanCollectionDataSource fonteDados = new JRBeanCollectionDataSource(list);
            JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream, map, fonteDados);
            servletOutputStream.flush();
            servletOutputStream.close();
        } catch (JRException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            context.responseComplete();
        }
    }
    
    
   
}
