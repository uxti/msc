/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.util;

import com.bcr.controller.EmpresaEJB;
import com.bcr.model.Cheque;
import com.bcr.model.Empresa;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Renato
 */
public class Bcrutils {

    @EJB
    private static EmpresaEJB empEJB;

    public static Date ultimoDiaDoMes() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMaximum(Calendar.DAY_OF_MONTH);
        data.set(Calendar.DAY_OF_MONTH, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date primeiroDiaDoMes() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMinimum(Calendar.DAY_OF_MONTH);
        data.set(Calendar.DAY_OF_MONTH, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date primeiroDiaDaSemana() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMinimum(Calendar.DAY_OF_WEEK);
        data.set(Calendar.DAY_OF_WEEK, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date ultimoDiaDaSemana() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMaximum(Calendar.DAY_OF_WEEK);
        data.set(Calendar.DAY_OF_WEEK, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date primeiroDiaDoAno() {
        Calendar data = new GregorianCalendar();
        int primeiro_dia_ano = data.getActualMinimum(Calendar.DAY_OF_YEAR);
        data.set(Calendar.DAY_OF_YEAR, primeiro_dia_ano);
        return data.getTime();
    }

    public static Date ultimoDiaDoAno() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_ano = data.getActualMaximum(Calendar.DAY_OF_YEAR);
        data.set(Calendar.DAY_OF_YEAR, ultimo_dia_ano);
        return data.getTime();
    }

    public static Date addMes(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.MONTH, qtd);
        return cal.getTime();
    }

    public static Date addDia(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.DAY_OF_MONTH, qtd);
        return cal.getTime();
    }

    public static Date addAno(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.YEAR, qtd);
        return cal.getTime();
    }

    public static String formatarMoeda(Double val) {
        return NumberFormat.getCurrencyInstance().format(val);
    }

    public static String formatarMoeda(String val) {
        return NumberFormat.getCurrencyInstance().format(val);
    }

    public static String formatarMoeda(BigDecimal val) {
        return NumberFormat.getCurrencyInstance().format(val);
    }

    public static String formataData(Date d) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt", "BR"));
        if (d != null) {
            return df.format(d);
        } else {
            return "";
        }
    }

    public static Empresa retornaEmpresaLogada(String paramName) {
        HttpServletRequest request = (HttpServletRequest) FacesContext
                .getCurrentInstance().getExternalContext().getRequest();
        Empresa e = empEJB.SelecionarPorID(Integer.parseInt(request.getParameter(paramName)));
        return e;
    }

    public static Empresa retornaEmpresaLogadaPorID(Integer id) {
        Empresa e = empEJB.SelecionarPorID(id);
        return e;
    }

    public static Integer retornaTamanhoString(String s) {
        return s.length();
    }

    public static String formataDataDDMMYYYY(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        if (date != null) {
            return df.format(date);
        }
        return "";
    }

    public static Date removeHoraMinutoSegundoDeData(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        return calendar.getTime();
    
    }

    public static boolean betweenDates(Date date, Date dateStart, Date dateEnd) {
        if (date != null && dateStart != null && dateEnd != null) {
            if ((date.after(dateStart) || date.equals(dateStart)) && (date.before(dateEnd) || date.equals(dateEnd))) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static void escreveLogErro(Exception e) {
        System.out.println("O erro ocorrido é:" + e);
        System.out.println(e);
    }

    public static Date retornaPrimeiroData(int mes, int ano) {
        Calendar c = new GregorianCalendar(ano, mes, 1);
        int dia = c.getMinimum(Calendar.DAY_OF_MONTH);
        Calendar ca = new GregorianCalendar(ano, mes, dia);
        return ca.getTime();
    }

    public static Date retornaUltimoData(int mes, int ano) {
        Calendar c = new GregorianCalendar(ano, mes, 1);
        int dia = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        Calendar ca = new GregorianCalendar(ano, mes, dia);
        return ca.getTime();
    }

    public static String retornaMes(int i) {
        if (i == 0) {
            return "Jan";
        } else if (i == 1) {
            return "Fev";
        } else if (i == 2) {
            return "Mar";
        } else if (i == 3) {
            return "Abr";
        } else if (i == 4) {
            return "Mai";
        } else if (i == 5) {
            return "Jun";
        } else if (i == 6) {
            return "Jul";
        } else if (i == 7) {
            return "Ago";
        } else if (i == 8) {
            return "Set";
        } else if (i == 9) {
            return "Out";
        } else if (i == 10) {
            return "Nov";
        } else if (i == 11) {
            return "Dez";
        } else {
            return "INVÁLIDO";
        }
    }

    public static void descobreErroDeEJBException(EJBException e) {
        @SuppressWarnings("ThrowableResultIgnored")
        Exception cause = e.getCausedByException();
        if (cause instanceof ConstraintViolationException) {
            @SuppressWarnings("ThrowableResultIgnored")
            ConstraintViolationException cve = (ConstraintViolationException) e.getCausedByException();
            for (Iterator<ConstraintViolation<?>> it = cve.getConstraintViolations().iterator(); it.hasNext();) {
                ConstraintViolation<? extends Object> v = it.next();
                System.err.println(v);
                System.err.println("==>>" + v.getMessage());
            }
        }
    }

    public static void gerarExportacao(List<Cheque> listaChequeExportar) {
        if (!listaChequeExportar.isEmpty()) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMYYYYHHmmss");
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("ddMMYYYY");
            java.text.DecimalFormat df = new java.text.DecimalFormat("#,##0.00");
            String dataFormatada = dateFormat.format(new Date());
            // Extensão exclusiva - .scq
            // Leiaute do nome do arquivo - Ex: PastaDefinidaPadraoDeImportacoes\exp_180420151551_IDContasPagar.scq
            String nomeArquivo = "D:\\MSC\\EXPORTACAO\\EXP_" + dataFormatada + ".scq";
            try {
                FileWriter writer = new FileWriter(nomeArquivo);
                String valor = "";
                for (Cheque chq : listaChequeExportar) {
                    valor = "";
                    // Numero_Cheque                   - Com zeros            - Comp: 6   - Formato: 999999
                    writer.append(StringUtils.leftPad(chq.getNumeroCheque().toString(), 6, "0").substring(0, 6));
                    // Data_Emissão                    - Não completar        - Comp: 8   - Formato:    ddmmaaaa
                    writer.append(dateFormat1.format(chq.getDtRecebimentoEmissao()));
                    // Data_Entrada (Data a Compensar) - Não completar        - Comp: 8   - Formato:    ddmmaaaa
                    writer.append(dateFormat1.format(chq.getDtCompensacaoVencimento()));
                    valor = df.format(chq.getValor());
                    // Valor                           - Com "0" a esquerda   - Comp: 12  - Formato:    99999999
                    writer.append(StringUtils.leftPad(valor.replace(",", "").replace(".", ""), 12, "0").substring(0, 12));
                    System.out.println(valor);
                    System.out.println(StringUtils.leftPad(valor.replace(",", "").replace(".", ""), 12, "0").substring(0, 12));
                    // Destinatário (Nonimal)          - Com " " a direita    - Comp: 60  - Formato:       X(60)
                    writer.append(StringUtils.rightPad(chq.getNominal().toString(), 60, " ").substring(0, 60));
                    // Referente a... (Observação)     - Com " " a direita    - Comp: 512 - Formato:      X(512)
                    writer.append(StringUtils.rightPad(chq.getObservacao().toString(), 512, " ").substring(0, 512));
                    // Código do banco                 - Com zeros            - Comp: 3   - Formato:        999
                    writer.append(StringUtils.leftPad(chq.getIdContaCorrente().getIdBanco().getNumeroBanco().toString(), 3, "0").substring(0, 3));
                    // Número da agência da conta      - Com zeros            - Comp: 4   - Formato:       9999
                    writer.append(StringUtils.leftPad(chq.getIdContaCorrente().getNumAgencia().toString(), 4, "0").substring(0, 4));
                    // Número da conta bancária        - Com zeros            - Comp: 15   - Formato: 999999999
                    writer.append(StringUtils.rightPad(chq.getIdContaCorrente().getNumConta().toString().replace("-", "").replace(" ", ""), 15, " ").substring(0, 15));
                    writer.append('\n');
                    writer.flush();
                }
                writer.close();
                MensageFactory.info("Exportação ok! Agora, acesse o SoftCheque, depois Ferramentas, Executar importação automática agora.", null);
            } catch (IOException e) {
                MensageFactory.error("Erro ao exportar. Entre em contato com o Suporte.", null);
                e.printStackTrace();
            }
        } else {
            MensageFactory.info("Exportação não realizada.", "");
        }
    }
}
