/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.util;

import com.bcr.controller.UsuarioEJB;
import com.bcr.model.Usuario;
import javax.ejb.EJB;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author Renato
 */
public class UsuarioLogado {

    private Usuario usuario;
    private Usuario user;
    @EJB
    UsuarioEJB uEJB;

    public String retornaUsuario() {
        String usuarioLog = "";
        Object usuarioLogado = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (usuarioLogado instanceof UserDetails) {
            return usuarioLog = ((UserDetails) usuarioLogado).getUsername();
        } else {
            return usuarioLog = usuarioLogado.toString().toUpperCase();
        }
    }

    public Usuario getUsuario() {
        try {
            if (usuario.getIdUsuario() == null) {
                return usuario = (Usuario) uEJB.pegaUsuarioLogado();
            } else {
                return usuario;
            }
        } catch (Exception e) {
            return usuario = (Usuario) uEJB.pegaUsuarioLogado();
        }

    }
}
