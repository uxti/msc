/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "contas_pagar")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContasPagar.findAll", query = "SELECT c FROM ContasPagar c"),
    @NamedQuery(name = "ContasPagar.findByIdContasPagar", query = "SELECT c FROM ContasPagar c WHERE c.idContasPagar = :idContasPagar"),
    @NamedQuery(name = "ContasPagar.findByDtLancamento", query = "SELECT c FROM ContasPagar c WHERE c.dtLancamento = :dtLancamento"),
    @NamedQuery(name = "ContasPagar.findByDtDuplicata", query = "SELECT c FROM ContasPagar c WHERE c.dtDuplicata = :dtDuplicata"),
    @NamedQuery(name = "ContasPagar.findByDtPagamento", query = "SELECT c FROM ContasPagar c WHERE c.dtPagamento = :dtPagamento"),
    @NamedQuery(name = "ContasPagar.findByDtVencimento", query = "SELECT c FROM ContasPagar c WHERE c.dtVencimento = :dtVencimento"),
    @NamedQuery(name = "ContasPagar.findByDtEstorno", query = "SELECT c FROM ContasPagar c WHERE c.dtEstorno = :dtEstorno"),
    @NamedQuery(name = "ContasPagar.findByNumParcela", query = "SELECT c FROM ContasPagar c WHERE c.numParcela = :numParcela"),
    @NamedQuery(name = "ContasPagar.findByNumDocto", query = "SELECT c FROM ContasPagar c WHERE c.numDocto = :numDocto"),
    @NamedQuery(name = "ContasPagar.findByValorPago", query = "SELECT c FROM ContasPagar c WHERE c.valorPago = :valorPago"),
    @NamedQuery(name = "ContasPagar.findByValorDuplicata", query = "SELECT c FROM ContasPagar c WHERE c.valorDuplicata = :valorDuplicata"),
    @NamedQuery(name = "ContasPagar.findByStatus", query = "SELECT c FROM ContasPagar c WHERE c.status = :status"),
    @NamedQuery(name = "ContasPagar.findByObservacao", query = "SELECT c FROM ContasPagar c WHERE c.observacao = :observacao"),
    @NamedQuery(name = "ContasPagar.findByNumDuplicata", query = "SELECT c FROM ContasPagar c WHERE c.numDuplicata = :numDuplicata"),
    @NamedQuery(name = "ContasPagar.findByPagoCom", query = "SELECT c FROM ContasPagar c WHERE c.pagoCom = :pagoCom"),
    @NamedQuery(name = "ContasPagar.findByTipodocumento", query = "SELECT c FROM ContasPagar c WHERE c.tipodocumento = :tipodocumento"),
    @NamedQuery(name = "ContasPagar.findByNumLote", query = "SELECT c FROM ContasPagar c WHERE c.numLote = :numLote"),
    @NamedQuery(name = "ContasPagar.findByParcelaSistema", query = "SELECT c FROM ContasPagar c WHERE c.parcelaSistema = :parcelaSistema"),
    @NamedQuery(name = "ContasPagar.findByLote", query = "SELECT c FROM ContasPagar c WHERE c.lote = :lote"),
    @NamedQuery(name = "ContasPagar.findByTotalParcelas", query = "SELECT c FROM ContasPagar c WHERE c.totalParcelas = :totalParcelas")})
public class ContasPagar implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_contas_pagar")
    private Integer idContasPagar;
    @Column(name = "dt_lancamento")
    @Temporal(TemporalType.DATE)
    private Date dtLancamento;
    @Column(name = "dt_duplicata")
    @Temporal(TemporalType.DATE)
    private Date dtDuplicata;
    @Column(name = "dt_pagamento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtPagamento;
    @Column(name = "dt_vencimento")
    @Temporal(TemporalType.DATE)
    private Date dtVencimento;
    @Column(name = "dt_estorno")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEstorno;
    @Column(name = "num_parcela")
    private Integer numParcela;
    @Size(max = 60)
    @Column(name = "num_docto")
    private String numDocto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_pago")
    private BigDecimal valorPago;
    @Column(name = "valor_duplicata")
    private BigDecimal valorDuplicata;
    @Column(name = "status")
    private Boolean status;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @Size(max = 60)
    @Column(name = "num_duplicata")
    private String numDuplicata;
    @Size(max = 10)
    @Column(name = "pago_com")
    private String pagoCom;
    @Size(max = 100)
    @Column(name = "Tipo_documento")
    private String tipodocumento;
    @Size(max = 50)
    @Column(name = "num_lote")
    private String numLote;
    @Size(max = 50)
    @Column(name = "parcela_sistema")
    private String parcelaSistema;
    @Size(max = 100)
    @Column(name = "lote")
    private String lote;
    @Column(name = "total_parcelas")
    private Integer totalParcelas;
    @JoinColumn(name = "id_plano_contas", referencedColumnName = "id_plano_contas")
    @ManyToOne
    private PlanoContas idPlanoContas;
    @JoinColumn(name = "id_nota_entrada", referencedColumnName = "id_nota_entrada")
    @ManyToOne
    private NotaEntrada idNotaEntrada;
    @JoinColumn(name = "id_conta_corrente", referencedColumnName = "id_conta_corrente")
    @ManyToOne
    private ContaCorrente idContaCorrente;
    @JoinColumn(name = "id_forma_pagamento", referencedColumnName = "id_forma_pagamento")
    @ManyToOne
    private FormaPagamento idFormaPagamento;
    @JoinColumn(name = "id_fornecedor", referencedColumnName = "id_fornecedor")
    @ManyToOne
    private Fornecedor idFornecedor;
    @JoinColumn(name = "id_cheque", referencedColumnName = "id_cheque")
    @ManyToOne
    private Cheque idCheque;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_banco", referencedColumnName = "id_banco")
    @ManyToOne
    private Banco idBanco;
    @OneToMany(mappedBy = "idContasPagar")
    private List<MovimentacaoBancaria> movimentacaoBancariaList;
    @OneToMany(mappedBy = "idContasPagar")
    private List<Cheque> chequeList;
    @OneToMany(mappedBy = "idContasPagar")
    private List<CaixaItem> caixaItemList;
    @OneToMany(mappedBy = "idContasPagar")
    private List<Centrocustoplanocontas> centrocustoplanocontasList;

    public ContasPagar() {
    }

    public ContasPagar(Integer idContasPagar) {
        this.idContasPagar = idContasPagar;
    }

    public Integer getIdContasPagar() {
        return idContasPagar;
    }

    public void setIdContasPagar(Integer idContasPagar) {
        this.idContasPagar = idContasPagar;
    }

    public Date getDtLancamento() {
        return dtLancamento;
    }

    public void setDtLancamento(Date dtLancamento) {
        this.dtLancamento = dtLancamento;
    }

    public Date getDtDuplicata() {
        return dtDuplicata;
    }

    public void setDtDuplicata(Date dtDuplicata) {
        this.dtDuplicata = dtDuplicata;
    }

    public Date getDtPagamento() {
        return dtPagamento;
    }

    public void setDtPagamento(Date dtPagamento) {
        this.dtPagamento = dtPagamento;
    }

    public Date getDtVencimento() {
        return dtVencimento;
    }

    public void setDtVencimento(Date dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    public Date getDtEstorno() {
        return dtEstorno;
    }

    public void setDtEstorno(Date dtEstorno) {
        this.dtEstorno = dtEstorno;
    }

    public Integer getNumParcela() {
        return numParcela;
    }

    public void setNumParcela(Integer numParcela) {
        this.numParcela = numParcela;
    }

    public String getNumDocto() {
        return numDocto;
    }

    public void setNumDocto(String numDocto) {
        this.numDocto = numDocto;
    }

    public BigDecimal getValorPago() {
        return valorPago;
    }

    public void setValorPago(BigDecimal valorPago) {
        this.valorPago = valorPago;
    }

    public BigDecimal getValorDuplicata() {
        return valorDuplicata;
    }

    public void setValorDuplicata(BigDecimal valorDuplicata) {
        this.valorDuplicata = valorDuplicata;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getNumDuplicata() {
        return numDuplicata;
    }

    public void setNumDuplicata(String numDuplicata) {
        this.numDuplicata = numDuplicata;
    }

    public String getPagoCom() {
        return pagoCom;
    }

    public void setPagoCom(String pagoCom) {
        this.pagoCom = pagoCom;
    }

    public String getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(String tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public String getNumLote() {
        return numLote;
    }

    public void setNumLote(String numLote) {
        this.numLote = numLote;
    }

    public String getParcelaSistema() {
        return parcelaSistema;
    }

    public void setParcelaSistema(String parcelaSistema) {
        this.parcelaSistema = parcelaSistema;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public Integer getTotalParcelas() {
        return totalParcelas;
    }

    public void setTotalParcelas(Integer totalParcelas) {
        this.totalParcelas = totalParcelas;
    }

    public PlanoContas getIdPlanoContas() {
        return idPlanoContas;
    }

    public void setIdPlanoContas(PlanoContas idPlanoContas) {
        this.idPlanoContas = idPlanoContas;
    }

    public NotaEntrada getIdNotaEntrada() {
        return idNotaEntrada;
    }

    public void setIdNotaEntrada(NotaEntrada idNotaEntrada) {
        this.idNotaEntrada = idNotaEntrada;
    }

    public ContaCorrente getIdContaCorrente() {
        return idContaCorrente;
    }

    public void setIdContaCorrente(ContaCorrente idContaCorrente) {
        this.idContaCorrente = idContaCorrente;
    }

    public FormaPagamento getIdFormaPagamento() {
        return idFormaPagamento;
    }

    public void setIdFormaPagamento(FormaPagamento idFormaPagamento) {
        this.idFormaPagamento = idFormaPagamento;
    }

    public Fornecedor getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(Fornecedor idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public Cheque getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Cheque idCheque) {
        this.idCheque = idCheque;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Banco getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Banco idBanco) {
        this.idBanco = idBanco;
    }

    @XmlTransient
    public List<MovimentacaoBancaria> getMovimentacaoBancariaList() {
        return movimentacaoBancariaList;
    }

    public void setMovimentacaoBancariaList(List<MovimentacaoBancaria> movimentacaoBancariaList) {
        this.movimentacaoBancariaList = movimentacaoBancariaList;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    @XmlTransient
    public List<CaixaItem> getCaixaItemList() {
        return caixaItemList;
    }

    public void setCaixaItemList(List<CaixaItem> caixaItemList) {
        this.caixaItemList = caixaItemList;
    }

    @XmlTransient
    public List<Centrocustoplanocontas> getCentrocustoplanocontasList() {
        return centrocustoplanocontasList;
    }

    public void setCentrocustoplanocontasList(List<Centrocustoplanocontas> centrocustoplanocontasList) {
        this.centrocustoplanocontasList = centrocustoplanocontasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContasPagar != null ? idContasPagar.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContasPagar)) {
            return false;
        }
        ContasPagar other = (ContasPagar) object;
        if ((this.idContasPagar == null && other.idContasPagar != null) || (this.idContasPagar != null && !this.idContasPagar.equals(other.idContasPagar))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ContasPagar[ idContasPagar=" + idContasPagar + " ]";
    }
    
}
