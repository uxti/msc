/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "cliente_responsavel")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ClienteResponsavel.findAll", query = "SELECT c FROM ClienteResponsavel c"),
    @NamedQuery(name = "ClienteResponsavel.findByIdClienteResponsavel", query = "SELECT c FROM ClienteResponsavel c WHERE c.idClienteResponsavel = :idClienteResponsavel"),
    @NamedQuery(name = "ClienteResponsavel.findByTipo", query = "SELECT c FROM ClienteResponsavel c WHERE c.tipo = :tipo"),
    @NamedQuery(name = "ClienteResponsavel.findByNome", query = "SELECT c FROM ClienteResponsavel c WHERE c.nome = :nome"),
    @NamedQuery(name = "ClienteResponsavel.findByCpf", query = "SELECT c FROM ClienteResponsavel c WHERE c.cpf = :cpf"),
    @NamedQuery(name = "ClienteResponsavel.findByEmail", query = "SELECT c FROM ClienteResponsavel c WHERE c.email = :email"),
    @NamedQuery(name = "ClienteResponsavel.findByCep", query = "SELECT c FROM ClienteResponsavel c WHERE c.cep = :cep"),
    @NamedQuery(name = "ClienteResponsavel.findByTipoLogradouro", query = "SELECT c FROM ClienteResponsavel c WHERE c.tipoLogradouro = :tipoLogradouro"),
    @NamedQuery(name = "ClienteResponsavel.findByLogradouro", query = "SELECT c FROM ClienteResponsavel c WHERE c.logradouro = :logradouro"),
    @NamedQuery(name = "ClienteResponsavel.findByNumero", query = "SELECT c FROM ClienteResponsavel c WHERE c.numero = :numero"),
    @NamedQuery(name = "ClienteResponsavel.findByComplemento", query = "SELECT c FROM ClienteResponsavel c WHERE c.complemento = :complemento"),
    @NamedQuery(name = "ClienteResponsavel.findByEstadoCivil", query = "SELECT c FROM ClienteResponsavel c WHERE c.estadoCivil = :estadoCivil"),
    @NamedQuery(name = "ClienteResponsavel.findByObservacoes", query = "SELECT c FROM ClienteResponsavel c WHERE c.observacoes = :observacoes")})
public class ClienteResponsavel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cliente_responsavel")
    private Integer idClienteResponsavel;
    @Size(max = 20)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 255)
    @Column(name = "nome")
    private String nome;
    @Size(max = 14)
    @Column(name = "CPF")
    private String cpf;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="E-mail inválido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "email")
    private String email;
    @Size(max = 9)
    @Column(name = "CEP")
    private String cep;
    @Size(max = 20)
    @Column(name = "tipo_logradouro")
    private String tipoLogradouro;
    @Size(max = 255)
    @Column(name = "logradouro")
    private String logradouro;
    @Size(max = 5)
    @Column(name = "numero")
    private String numero;
    @Size(max = 255)
    @Column(name = "complemento")
    private String complemento;
    @Size(max = 15)
    @Column(name = "estado_civil")
    private String estadoCivil;
    @Size(max = 255)
    @Column(name = "observacoes")
    private String observacoes;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;

    public ClienteResponsavel() {
    }

    public ClienteResponsavel(Integer idClienteResponsavel) {
        this.idClienteResponsavel = idClienteResponsavel;
    }

    public Integer getIdClienteResponsavel() {
        return idClienteResponsavel;
    }

    public void setIdClienteResponsavel(Integer idClienteResponsavel) {
        this.idClienteResponsavel = idClienteResponsavel;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idClienteResponsavel != null ? idClienteResponsavel.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClienteResponsavel)) {
            return false;
        }
        ClienteResponsavel other = (ClienteResponsavel) object;
        if ((this.idClienteResponsavel == null && other.idClienteResponsavel != null) || (this.idClienteResponsavel != null && !this.idClienteResponsavel.equals(other.idClienteResponsavel))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ClienteResponsavel[ idClienteResponsavel=" + idClienteResponsavel + " ]";
    }
    
}
