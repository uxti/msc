/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "nota_saida_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotaSaidaItem.findAll", query = "SELECT n FROM NotaSaidaItem n"),
    @NamedQuery(name = "NotaSaidaItem.findByIdNotaSaidaItem", query = "SELECT n FROM NotaSaidaItem n WHERE n.idNotaSaidaItem = :idNotaSaidaItem"),
    @NamedQuery(name = "NotaSaidaItem.findByCst", query = "SELECT n FROM NotaSaidaItem n WHERE n.cst = :cst"),
    @NamedQuery(name = "NotaSaidaItem.findByQuantidade", query = "SELECT n FROM NotaSaidaItem n WHERE n.quantidade = :quantidade"),
    @NamedQuery(name = "NotaSaidaItem.findByValorUnitario", query = "SELECT n FROM NotaSaidaItem n WHERE n.valorUnitario = :valorUnitario"),
    @NamedQuery(name = "NotaSaidaItem.findByValorTotal", query = "SELECT n FROM NotaSaidaItem n WHERE n.valorTotal = :valorTotal"),
    @NamedQuery(name = "NotaSaidaItem.findByBasecalculoICMS", query = "SELECT n FROM NotaSaidaItem n WHERE n.basecalculoICMS = :basecalculoICMS"),
    @NamedQuery(name = "NotaSaidaItem.findByValorICMS", query = "SELECT n FROM NotaSaidaItem n WHERE n.valorICMS = :valorICMS"),
    @NamedQuery(name = "NotaSaidaItem.findByValorIPI", query = "SELECT n FROM NotaSaidaItem n WHERE n.valorIPI = :valorIPI"),
    @NamedQuery(name = "NotaSaidaItem.findByAliquotaICMS", query = "SELECT n FROM NotaSaidaItem n WHERE n.aliquotaICMS = :aliquotaICMS"),
    @NamedQuery(name = "NotaSaidaItem.findByAliquotaIPI", query = "SELECT n FROM NotaSaidaItem n WHERE n.aliquotaIPI = :aliquotaIPI"),
    @NamedQuery(name = "NotaSaidaItem.findByNumeracao", query = "SELECT n FROM NotaSaidaItem n WHERE n.numeracao = :numeracao"),
    @NamedQuery(name = "NotaSaidaItem.findByValorFrete", query = "SELECT n FROM NotaSaidaItem n WHERE n.valorFrete = :valorFrete"),
    @NamedQuery(name = "NotaSaidaItem.findByValorSeguro", query = "SELECT n FROM NotaSaidaItem n WHERE n.valorSeguro = :valorSeguro"),
    @NamedQuery(name = "NotaSaidaItem.findByValorDesconto", query = "SELECT n FROM NotaSaidaItem n WHERE n.valorDesconto = :valorDesconto")})
public class NotaSaidaItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_nota_saida_item")
    private Integer idNotaSaidaItem;
    @Size(max = 4)
    @Column(name = "CST")
    private String cst;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "quantidade")
    private BigDecimal quantidade;
    @Column(name = "valor_unitario")
    private BigDecimal valorUnitario;
    @Column(name = "valor_total")
    private BigDecimal valorTotal;
    @Column(name = "base_calculo_ICMS")
    private BigDecimal basecalculoICMS;
    @Column(name = "valor_ICMS")
    private BigDecimal valorICMS;
    @Column(name = "valor_IPI")
    private BigDecimal valorIPI;
    @Column(name = "aliquota_ICMS")
    private BigDecimal aliquotaICMS;
    @Column(name = "aliquota_IPI")
    private BigDecimal aliquotaIPI;
    @Column(name = "numeracao")
    private Integer numeracao;
    @Column(name = "valor_frete")
    private BigDecimal valorFrete;
    @Column(name = "valor_seguro")
    private BigDecimal valorSeguro;
    @Column(name = "valor_desconto")
    private BigDecimal valorDesconto;
    @JoinColumn(name = "id_CFOP", referencedColumnName = "ID_CFOP")
    @ManyToOne
    private Cfop idCFOP;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne(optional = false)
    private Produto idProduto;
    @JoinColumn(name = "id_nota_saida", referencedColumnName = "id_nota_saida")
    @ManyToOne(optional = false)
    private NotaSaida idNotaSaida;

    public NotaSaidaItem() {
    }

    public NotaSaidaItem(Integer idNotaSaidaItem) {
        this.idNotaSaidaItem = idNotaSaidaItem;
    }

    public Integer getIdNotaSaidaItem() {
        return idNotaSaidaItem;
    }

    public void setIdNotaSaidaItem(Integer idNotaSaidaItem) {
        this.idNotaSaidaItem = idNotaSaidaItem;
    }

    public String getCst() {
        return cst;
    }

    public void setCst(String cst) {
        this.cst = cst;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public BigDecimal getBasecalculoICMS() {
        return basecalculoICMS;
    }

    public void setBasecalculoICMS(BigDecimal basecalculoICMS) {
        this.basecalculoICMS = basecalculoICMS;
    }

    public BigDecimal getValorICMS() {
        return valorICMS;
    }

    public void setValorICMS(BigDecimal valorICMS) {
        this.valorICMS = valorICMS;
    }

    public BigDecimal getValorIPI() {
        return valorIPI;
    }

    public void setValorIPI(BigDecimal valorIPI) {
        this.valorIPI = valorIPI;
    }

    public BigDecimal getAliquotaICMS() {
        return aliquotaICMS;
    }

    public void setAliquotaICMS(BigDecimal aliquotaICMS) {
        this.aliquotaICMS = aliquotaICMS;
    }

    public BigDecimal getAliquotaIPI() {
        return aliquotaIPI;
    }

    public void setAliquotaIPI(BigDecimal aliquotaIPI) {
        this.aliquotaIPI = aliquotaIPI;
    }

    public Integer getNumeracao() {
        return numeracao;
    }

    public void setNumeracao(Integer numeracao) {
        this.numeracao = numeracao;
    }

    public BigDecimal getValorFrete() {
        return valorFrete;
    }

    public void setValorFrete(BigDecimal valorFrete) {
        this.valorFrete = valorFrete;
    }

    public BigDecimal getValorSeguro() {
        return valorSeguro;
    }

    public void setValorSeguro(BigDecimal valorSeguro) {
        this.valorSeguro = valorSeguro;
    }

    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public Cfop getIdCFOP() {
        return idCFOP;
    }

    public void setIdCFOP(Cfop idCFOP) {
        this.idCFOP = idCFOP;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    public NotaSaida getIdNotaSaida() {
        return idNotaSaida;
    }

    public void setIdNotaSaida(NotaSaida idNotaSaida) {
        this.idNotaSaida = idNotaSaida;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotaSaidaItem != null ? idNotaSaidaItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaSaidaItem)) {
            return false;
        }
        NotaSaidaItem other = (NotaSaidaItem) object;
        if ((this.idNotaSaidaItem == null && other.idNotaSaidaItem != null) || (this.idNotaSaidaItem != null && !this.idNotaSaidaItem.equals(other.idNotaSaidaItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.NotaSaidaItem[ idNotaSaidaItem=" + idNotaSaidaItem + " ]";
    }
    
}
