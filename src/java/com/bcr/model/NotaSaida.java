/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "nota_saida")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotaSaida.findAll", query = "SELECT n FROM NotaSaida n"),
    @NamedQuery(name = "NotaSaida.findByIdNotaSaida", query = "SELECT n FROM NotaSaida n WHERE n.idNotaSaida = :idNotaSaida"),
    @NamedQuery(name = "NotaSaida.findByNumero", query = "SELECT n FROM NotaSaida n WHERE n.numero = :numero"),
    @NamedQuery(name = "NotaSaida.findBySerie", query = "SELECT n FROM NotaSaida n WHERE n.serie = :serie"),
    @NamedQuery(name = "NotaSaida.findByStatus", query = "SELECT n FROM NotaSaida n WHERE n.status = :status"),
    @NamedQuery(name = "NotaSaida.findByTipoSaida", query = "SELECT n FROM NotaSaida n WHERE n.tipoSaida = :tipoSaida"),
    @NamedQuery(name = "NotaSaida.findByChaveAcesso", query = "SELECT n FROM NotaSaida n WHERE n.chaveAcesso = :chaveAcesso"),
    @NamedQuery(name = "NotaSaida.findByProtocoloAutorizacao", query = "SELECT n FROM NotaSaida n WHERE n.protocoloAutorizacao = :protocoloAutorizacao"),
    @NamedQuery(name = "NotaSaida.findByProtocoloRecebimento", query = "SELECT n FROM NotaSaida n WHERE n.protocoloRecebimento = :protocoloRecebimento"),
    @NamedQuery(name = "NotaSaida.findByDtEmissao", query = "SELECT n FROM NotaSaida n WHERE n.dtEmissao = :dtEmissao"),
    @NamedQuery(name = "NotaSaida.findByDtSaida", query = "SELECT n FROM NotaSaida n WHERE n.dtSaida = :dtSaida"),
    @NamedQuery(name = "NotaSaida.findByHoraSaida", query = "SELECT n FROM NotaSaida n WHERE n.horaSaida = :horaSaida"),
    @NamedQuery(name = "NotaSaida.findByBasecalculoICMS", query = "SELECT n FROM NotaSaida n WHERE n.basecalculoICMS = :basecalculoICMS"),
    @NamedQuery(name = "NotaSaida.findByValorICMS", query = "SELECT n FROM NotaSaida n WHERE n.valorICMS = :valorICMS"),
    @NamedQuery(name = "NotaSaida.findByBasecalculoICMSST", query = "SELECT n FROM NotaSaida n WHERE n.basecalculoICMSST = :basecalculoICMSST"),
    @NamedQuery(name = "NotaSaida.findByValorICMSST", query = "SELECT n FROM NotaSaida n WHERE n.valorICMSST = :valorICMSST"),
    @NamedQuery(name = "NotaSaida.findByValorFrete", query = "SELECT n FROM NotaSaida n WHERE n.valorFrete = :valorFrete"),
    @NamedQuery(name = "NotaSaida.findByValorSeguro", query = "SELECT n FROM NotaSaida n WHERE n.valorSeguro = :valorSeguro"),
    @NamedQuery(name = "NotaSaida.findByValorDesconto", query = "SELECT n FROM NotaSaida n WHERE n.valorDesconto = :valorDesconto"),
    @NamedQuery(name = "NotaSaida.findByOutrasDespesas", query = "SELECT n FROM NotaSaida n WHERE n.outrasDespesas = :outrasDespesas"),
    @NamedQuery(name = "NotaSaida.findByValorIPI", query = "SELECT n FROM NotaSaida n WHERE n.valorIPI = :valorIPI"),
    @NamedQuery(name = "NotaSaida.findByValortotalNF", query = "SELECT n FROM NotaSaida n WHERE n.valortotalNF = :valortotalNF"),
    @NamedQuery(name = "NotaSaida.findByValorTotalProdutos", query = "SELECT n FROM NotaSaida n WHERE n.valorTotalProdutos = :valorTotalProdutos"),
    @NamedQuery(name = "NotaSaida.findByValorTotalServicos", query = "SELECT n FROM NotaSaida n WHERE n.valorTotalServicos = :valorTotalServicos"),
    @NamedQuery(name = "NotaSaida.findByBasecalculoISSQN", query = "SELECT n FROM NotaSaida n WHERE n.basecalculoISSQN = :basecalculoISSQN"),
    @NamedQuery(name = "NotaSaida.findByValorISSQN", query = "SELECT n FROM NotaSaida n WHERE n.valorISSQN = :valorISSQN"),
    @NamedQuery(name = "NotaSaida.findByTipoFrete", query = "SELECT n FROM NotaSaida n WHERE n.tipoFrete = :tipoFrete"),
    @NamedQuery(name = "NotaSaida.findByPlacaFrete", query = "SELECT n FROM NotaSaida n WHERE n.placaFrete = :placaFrete"),
    @NamedQuery(name = "NotaSaida.findByPlacaUFfrete", query = "SELECT n FROM NotaSaida n WHERE n.placaUFfrete = :placaUFfrete"),
    @NamedQuery(name = "NotaSaida.findByQuantidadeFrete", query = "SELECT n FROM NotaSaida n WHERE n.quantidadeFrete = :quantidadeFrete"),
    @NamedQuery(name = "NotaSaida.findByModalidadeFrete", query = "SELECT n FROM NotaSaida n WHERE n.modalidadeFrete = :modalidadeFrete"),
    @NamedQuery(name = "NotaSaida.findByMarcaFrete", query = "SELECT n FROM NotaSaida n WHERE n.marcaFrete = :marcaFrete"),
    @NamedQuery(name = "NotaSaida.findByNumeroFrete", query = "SELECT n FROM NotaSaida n WHERE n.numeroFrete = :numeroFrete"),
    @NamedQuery(name = "NotaSaida.findByPesoBruto", query = "SELECT n FROM NotaSaida n WHERE n.pesoBruto = :pesoBruto"),
    @NamedQuery(name = "NotaSaida.findByPesoLiquido", query = "SELECT n FROM NotaSaida n WHERE n.pesoLiquido = :pesoLiquido"),
    @NamedQuery(name = "NotaSaida.findByInformacoesComplementares", query = "SELECT n FROM NotaSaida n WHERE n.informacoesComplementares = :informacoesComplementares"),
    @NamedQuery(name = "NotaSaida.findByObservacoes", query = "SELECT n FROM NotaSaida n WHERE n.observacoes = :observacoes"),
    @NamedQuery(name = "NotaSaida.findByCodigoNumerico", query = "SELECT n FROM NotaSaida n WHERE n.codigoNumerico = :codigoNumerico"),
    @NamedQuery(name = "NotaSaida.findByCodigoMunicipioGerador", query = "SELECT n FROM NotaSaida n WHERE n.codigoMunicipioGerador = :codigoMunicipioGerador"),
    @NamedQuery(name = "NotaSaida.findByImpressaoDanf", query = "SELECT n FROM NotaSaida n WHERE n.impressaoDanf = :impressaoDanf")})
public class NotaSaida implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_nota_saida")
    private Integer idNotaSaida;
    @Size(max = 20)
    @Column(name = "numero")
    private String numero;
    @Size(max = 3)
    @Column(name = "serie")
    private String serie;
    @Size(max = 50)
    @Column(name = "status")
    private String status;
    @Size(max = 100)
    @Column(name = "tipo_saida")
    private String tipoSaida;
    @Column(name = "chave_acesso")
    private Integer chaveAcesso;
    @Size(max = 99)
    @Column(name = "protocolo_autorizacao")
    private String protocoloAutorizacao;
    @Size(max = 99)
    @Column(name = "protocolo_recebimento")
    private String protocoloRecebimento;
    @Basic(optional = false)
    @Column(name = "dt_emissao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEmissao;
    @Basic(optional = false)
    @Column(name = "dt_saida")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtSaida;
    @Basic(optional = false)
    @Column(name = "hora_saida")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horaSaida;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "base_calculo_ICMS")
    private BigDecimal basecalculoICMS;
    @Column(name = "valor_ICMS")
    private BigDecimal valorICMS;
    @Column(name = "base_calculo_ICMS_ST")
    private BigDecimal basecalculoICMSST;
    @Column(name = "valor_ICMS_ST")
    private BigDecimal valorICMSST;
    @Column(name = "valor_frete")
    private BigDecimal valorFrete;
    @Column(name = "valor_seguro")
    private BigDecimal valorSeguro;
    @Column(name = "valor_desconto")
    private BigDecimal valorDesconto;
    @Column(name = "outras_despesas")
    private BigDecimal outrasDespesas;
    @Column(name = "valor_IPI")
    private BigDecimal valorIPI;
    @Column(name = "valor_total_NF")
    private BigDecimal valortotalNF;
    @Column(name = "valor_total_produtos")
    private BigDecimal valorTotalProdutos;
    @Column(name = "valor_total_servicos")
    private BigDecimal valorTotalServicos;
    @Column(name = "base_calculo_ISSQN")
    private BigDecimal basecalculoISSQN;
    @Column(name = "valor_ISSQN")
    private BigDecimal valorISSQN;
    @Size(max = 100)
    @Column(name = "tipo_frete")
    private String tipoFrete;
    @Size(max = 8)
    @Column(name = "placa_frete")
    private String placaFrete;
    @Size(max = 2)
    @Column(name = "placa_UF_frete")
    private String placaUFfrete;
    @Column(name = "quantidade_frete")
    private Short quantidadeFrete;
    @Size(max = 1)
    @Column(name = "modalidade_frete")
    private String modalidadeFrete;
    @Size(max = 60)
    @Column(name = "marca_frete")
    private String marcaFrete;
    @Column(name = "numero_frete")
    private Integer numeroFrete;
    @Column(name = "peso_bruto")
    private BigDecimal pesoBruto;
    @Column(name = "peso_liquido")
    private BigDecimal pesoLiquido;
    @Size(max = 255)
    @Column(name = "informacoes_complementares")
    private String informacoesComplementares;
    @Size(max = 255)
    @Column(name = "observacoes")
    private String observacoes;
    @Column(name = "codigo_numerico")
    private Integer codigoNumerico;
    @Size(max = 7)
    @Column(name = "codigo_municipio_gerador")
    private String codigoMunicipioGerador;
    @Size(max = 1)
    @Column(name = "impressao_danf")
    private String impressaoDanf;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNotaSaida")
    private List<NotaSaidaItem> notaSaidaItemList;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_nota_observacao", referencedColumnName = "id_nota_observacao")
    @ManyToOne
    private NotaObservacao idNotaObservacao;
    @JoinColumn(name = "id_forma_pagamento", referencedColumnName = "id_forma_pagamento")
    @ManyToOne
    private FormaPagamento idFormaPagamento;
    @JoinColumn(name = "id_servico", referencedColumnName = "id_servico")
    @ManyToOne
    private Servico idServico;
    @JoinColumn(name = "id_local_trabalho", referencedColumnName = "id_local_trabalho")
    @ManyToOne
    private LocalTrabalho idLocalTrabalho;
    @JoinColumn(name = "id_CFOP", referencedColumnName = "ID_CFOP")
    @ManyToOne
    private Cfop idCFOP;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @JoinColumn(name = "id_requisicao_material", referencedColumnName = "id_requisicao_material")
    @ManyToOne
    private RequisicaoMaterial idRequisicaoMaterial;
    @JoinColumn(name = "id_fornecedor", referencedColumnName = "id_fornecedor")
    @ManyToOne
    private Fornecedor idFornecedor;
    @JoinColumn(name = "id_plano_contas", referencedColumnName = "id_plano_contas")
    @ManyToOne
    private PlanoContas idPlanoContas;
    @JoinColumn(name = "id_comunicado", referencedColumnName = "id_comunicado")
    @ManyToOne
    private ComunicadoImplantacao idComunicado;
    @JoinColumn(name = "id_modelo", referencedColumnName = "id_modelo")
    @ManyToOne
    private ModeloNfe idModelo;
    @OneToMany(mappedBy = "idNotaSaida")
    private List<ContasReceber> contasReceberList;

    public NotaSaida() {
    }

    public NotaSaida(Integer idNotaSaida) {
        this.idNotaSaida = idNotaSaida;
    }

    public NotaSaida(Integer idNotaSaida, Date dtEmissao, Date dtSaida, Date horaSaida) {
        this.idNotaSaida = idNotaSaida;
        this.dtEmissao = dtEmissao;
        this.dtSaida = dtSaida;
        this.horaSaida = horaSaida;
    }

    public Integer getIdNotaSaida() {
        return idNotaSaida;
    }

    public void setIdNotaSaida(Integer idNotaSaida) {
        this.idNotaSaida = idNotaSaida;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTipoSaida() {
        return tipoSaida;
    }

    public void setTipoSaida(String tipoSaida) {
        this.tipoSaida = tipoSaida;
    }

    public Integer getChaveAcesso() {
        return chaveAcesso;
    }

    public void setChaveAcesso(Integer chaveAcesso) {
        this.chaveAcesso = chaveAcesso;
    }

    public String getProtocoloAutorizacao() {
        return protocoloAutorizacao;
    }

    public void setProtocoloAutorizacao(String protocoloAutorizacao) {
        this.protocoloAutorizacao = protocoloAutorizacao;
    }

    public String getProtocoloRecebimento() {
        return protocoloRecebimento;
    }

    public void setProtocoloRecebimento(String protocoloRecebimento) {
        this.protocoloRecebimento = protocoloRecebimento;
    }

    public Date getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Date dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public Date getDtSaida() {
        return dtSaida;
    }

    public void setDtSaida(Date dtSaida) {
        this.dtSaida = dtSaida;
    }

    public Date getHoraSaida() {
        return horaSaida;
    }

    public void setHoraSaida(Date horaSaida) {
        this.horaSaida = horaSaida;
    }

    public BigDecimal getBasecalculoICMS() {
        return basecalculoICMS;
    }

    public void setBasecalculoICMS(BigDecimal basecalculoICMS) {
        this.basecalculoICMS = basecalculoICMS;
    }

    public BigDecimal getValorICMS() {
        return valorICMS;
    }

    public void setValorICMS(BigDecimal valorICMS) {
        this.valorICMS = valorICMS;
    }

    public BigDecimal getBasecalculoICMSST() {
        return basecalculoICMSST;
    }

    public void setBasecalculoICMSST(BigDecimal basecalculoICMSST) {
        this.basecalculoICMSST = basecalculoICMSST;
    }

    public BigDecimal getValorICMSST() {
        return valorICMSST;
    }

    public void setValorICMSST(BigDecimal valorICMSST) {
        this.valorICMSST = valorICMSST;
    }

    public BigDecimal getValorFrete() {
        return valorFrete;
    }

    public void setValorFrete(BigDecimal valorFrete) {
        this.valorFrete = valorFrete;
    }

    public BigDecimal getValorSeguro() {
        return valorSeguro;
    }

    public void setValorSeguro(BigDecimal valorSeguro) {
        this.valorSeguro = valorSeguro;
    }

    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public BigDecimal getOutrasDespesas() {
        return outrasDespesas;
    }

    public void setOutrasDespesas(BigDecimal outrasDespesas) {
        this.outrasDespesas = outrasDespesas;
    }

    public BigDecimal getValorIPI() {
        return valorIPI;
    }

    public void setValorIPI(BigDecimal valorIPI) {
        this.valorIPI = valorIPI;
    }

    public BigDecimal getValortotalNF() {
        return valortotalNF;
    }

    public void setValortotalNF(BigDecimal valortotalNF) {
        this.valortotalNF = valortotalNF;
    }

    public BigDecimal getValorTotalProdutos() {
        return valorTotalProdutos;
    }

    public void setValorTotalProdutos(BigDecimal valorTotalProdutos) {
        this.valorTotalProdutos = valorTotalProdutos;
    }

    public BigDecimal getValorTotalServicos() {
        return valorTotalServicos;
    }

    public void setValorTotalServicos(BigDecimal valorTotalServicos) {
        this.valorTotalServicos = valorTotalServicos;
    }

    public BigDecimal getBasecalculoISSQN() {
        return basecalculoISSQN;
    }

    public void setBasecalculoISSQN(BigDecimal basecalculoISSQN) {
        this.basecalculoISSQN = basecalculoISSQN;
    }

    public BigDecimal getValorISSQN() {
        return valorISSQN;
    }

    public void setValorISSQN(BigDecimal valorISSQN) {
        this.valorISSQN = valorISSQN;
    }

    public String getTipoFrete() {
        return tipoFrete;
    }

    public void setTipoFrete(String tipoFrete) {
        this.tipoFrete = tipoFrete;
    }

    public String getPlacaFrete() {
        return placaFrete;
    }

    public void setPlacaFrete(String placaFrete) {
        this.placaFrete = placaFrete;
    }

    public String getPlacaUFfrete() {
        return placaUFfrete;
    }

    public void setPlacaUFfrete(String placaUFfrete) {
        this.placaUFfrete = placaUFfrete;
    }

    public Short getQuantidadeFrete() {
        return quantidadeFrete;
    }

    public void setQuantidadeFrete(Short quantidadeFrete) {
        this.quantidadeFrete = quantidadeFrete;
    }

    public String getModalidadeFrete() {
        return modalidadeFrete;
    }

    public void setModalidadeFrete(String modalidadeFrete) {
        this.modalidadeFrete = modalidadeFrete;
    }

    public String getMarcaFrete() {
        return marcaFrete;
    }

    public void setMarcaFrete(String marcaFrete) {
        this.marcaFrete = marcaFrete;
    }

    public Integer getNumeroFrete() {
        return numeroFrete;
    }

    public void setNumeroFrete(Integer numeroFrete) {
        this.numeroFrete = numeroFrete;
    }

    public BigDecimal getPesoBruto() {
        return pesoBruto;
    }

    public void setPesoBruto(BigDecimal pesoBruto) {
        this.pesoBruto = pesoBruto;
    }

    public BigDecimal getPesoLiquido() {
        return pesoLiquido;
    }

    public void setPesoLiquido(BigDecimal pesoLiquido) {
        this.pesoLiquido = pesoLiquido;
    }

    public String getInformacoesComplementares() {
        return informacoesComplementares;
    }

    public void setInformacoesComplementares(String informacoesComplementares) {
        this.informacoesComplementares = informacoesComplementares;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Integer getCodigoNumerico() {
        return codigoNumerico;
    }

    public void setCodigoNumerico(Integer codigoNumerico) {
        this.codigoNumerico = codigoNumerico;
    }

    public String getCodigoMunicipioGerador() {
        return codigoMunicipioGerador;
    }

    public void setCodigoMunicipioGerador(String codigoMunicipioGerador) {
        this.codigoMunicipioGerador = codigoMunicipioGerador;
    }

    public String getImpressaoDanf() {
        return impressaoDanf;
    }

    public void setImpressaoDanf(String impressaoDanf) {
        this.impressaoDanf = impressaoDanf;
    }

    @XmlTransient
    public List<NotaSaidaItem> getNotaSaidaItemList() {
        return notaSaidaItemList;
    }

    public void setNotaSaidaItemList(List<NotaSaidaItem> notaSaidaItemList) {
        this.notaSaidaItemList = notaSaidaItemList;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public NotaObservacao getIdNotaObservacao() {
        return idNotaObservacao;
    }

    public void setIdNotaObservacao(NotaObservacao idNotaObservacao) {
        this.idNotaObservacao = idNotaObservacao;
    }

    public FormaPagamento getIdFormaPagamento() {
        return idFormaPagamento;
    }

    public void setIdFormaPagamento(FormaPagamento idFormaPagamento) {
        this.idFormaPagamento = idFormaPagamento;
    }

    public Servico getIdServico() {
        return idServico;
    }

    public void setIdServico(Servico idServico) {
        this.idServico = idServico;
    }

    public LocalTrabalho getIdLocalTrabalho() {
        return idLocalTrabalho;
    }

    public void setIdLocalTrabalho(LocalTrabalho idLocalTrabalho) {
        this.idLocalTrabalho = idLocalTrabalho;
    }

    public Cfop getIdCFOP() {
        return idCFOP;
    }

    public void setIdCFOP(Cfop idCFOP) {
        this.idCFOP = idCFOP;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public RequisicaoMaterial getIdRequisicaoMaterial() {
        return idRequisicaoMaterial;
    }

    public void setIdRequisicaoMaterial(RequisicaoMaterial idRequisicaoMaterial) {
        this.idRequisicaoMaterial = idRequisicaoMaterial;
    }

    public Fornecedor getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(Fornecedor idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public PlanoContas getIdPlanoContas() {
        return idPlanoContas;
    }

    public void setIdPlanoContas(PlanoContas idPlanoContas) {
        this.idPlanoContas = idPlanoContas;
    }

    public ModeloNfe getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(ModeloNfe idModelo) {
        this.idModelo = idModelo;
    }

    @XmlTransient
    public List<ContasReceber> getContasReceberList() {
        return contasReceberList;
    }

    public void setContasReceberList(List<ContasReceber> contasReceberList) {
        this.contasReceberList = contasReceberList;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotaSaida != null ? idNotaSaida.hashCode() : 0);
        return hash;
    }

    public ComunicadoImplantacao getIdComunicado() {
        return idComunicado;
    }

    public void setIdComunicado(ComunicadoImplantacao idComunicado) {
        this.idComunicado = idComunicado;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaSaida)) {
            return false;
        }
        NotaSaida other = (NotaSaida) object;
        if ((this.idNotaSaida == null && other.idNotaSaida != null) || (this.idNotaSaida != null && !this.idNotaSaida.equals(other.idNotaSaida))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.NotaSaida[ idNotaSaida=" + idNotaSaida + " ]";
    }
    
}
