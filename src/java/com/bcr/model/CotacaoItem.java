/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "cotacao_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CotacaoItem.findAll", query = "SELECT c FROM CotacaoItem c"),
    @NamedQuery(name = "CotacaoItem.findByIdCotacaoItem", query = "SELECT c FROM CotacaoItem c WHERE c.idCotacaoItem = :idCotacaoItem"),
    @NamedQuery(name = "CotacaoItem.findByValorCotado", query = "SELECT c FROM CotacaoItem c WHERE c.valorCotado = :valorCotado"),
    @NamedQuery(name = "CotacaoItem.findByQuantidade", query = "SELECT c FROM CotacaoItem c WHERE c.quantidade = :quantidade"),
    @NamedQuery(name = "CotacaoItem.findByValorTotal", query = "SELECT c FROM CotacaoItem c WHERE c.valorTotal = :valorTotal")})
public class CotacaoItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cotacao_item")
    private Integer idCotacaoItem;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_cotado")
    private BigDecimal valorCotado;
    @Column(name = "quantidade")
    private BigDecimal quantidade;
    @Column(name = "Requisicoes_Referentes")
    private String requisicoesReferentes;
    @Column(name = "Valor_Total")
    private BigDecimal valorTotal;
    @JoinColumn(name = "id_cotacao", referencedColumnName = "id_cotacao")
    @ManyToOne
    private Cotacao idCotacao;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne(optional = false)
    private Produto idProduto;
    @JoinColumn(name = "id_fornecedor_selecionado", referencedColumnName = "id_fornecedor")
    @ManyToOne
    private Fornecedor idFornecedorSelecionado;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @OneToMany(mappedBy = "idCotacaoItem")
    private List<CotacaoItemFornecedor> cotacaoItemFornecedorList;
    @OneToMany(mappedBy = "idCotacaoItem")
    private List<PedidoCompra> pedidoCompraList;
    @OneToMany(mappedBy = "idCotacaoItem")
    private List<PedidoCompraItem> pedidoCompraItemList;

    public CotacaoItem() {
    }

    public CotacaoItem(Integer idCotacaoItem) {
        this.idCotacaoItem = idCotacaoItem;
    }

    public Integer getIdCotacaoItem() {
        return idCotacaoItem;
    }

    public void setIdCotacaoItem(Integer idCotacaoItem) {
        this.idCotacaoItem = idCotacaoItem;
    }

    public BigDecimal getValorCotado() {
        return valorCotado;
    }

    public void setValorCotado(BigDecimal valorCotado) {
        this.valorCotado = valorCotado;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public String getRequisicoesReferentes() {
        return requisicoesReferentes;
    }

    public void setRequisicoesReferentes(String requisicoesReferentes) {
        this.requisicoesReferentes = requisicoesReferentes;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public Cotacao getIdCotacao() {
        return idCotacao;
    }

    public void setIdCotacao(Cotacao idCotacao) {
        this.idCotacao = idCotacao;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    public Fornecedor getIdFornecedorSelecionado() {
        return idFornecedorSelecionado;
    }

    public void setIdFornecedorSelecionado(Fornecedor idFornecedorSelecionado) {
        this.idFornecedorSelecionado = idFornecedorSelecionado;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @XmlTransient
    public List<CotacaoItemFornecedor> getCotacaoItemFornecedorList() {
        return cotacaoItemFornecedorList;
    }

    public void setCotacaoItemFornecedorList(List<CotacaoItemFornecedor> cotacaoItemFornecedorList) {
        this.cotacaoItemFornecedorList = cotacaoItemFornecedorList;
    }

    @XmlTransient
    public List<PedidoCompra> getPedidoCompraList() {
        return pedidoCompraList;
    }

    public void setPedidoCompraList(List<PedidoCompra> pedidoCompraList) {
        this.pedidoCompraList = pedidoCompraList;
    }

    @XmlTransient
    public List<PedidoCompraItem> getPedidoCompraItemList() {
        return pedidoCompraItemList;
    }

    public void setPedidoCompraItemList(List<PedidoCompraItem> pedidoCompraItemList) {
        this.pedidoCompraItemList = pedidoCompraItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCotacaoItem != null ? idCotacaoItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CotacaoItem)) {
            return false;
        }
        CotacaoItem other = (CotacaoItem) object;
        if ((this.idCotacaoItem == null && other.idCotacaoItem != null) || (this.idCotacaoItem != null && !this.idCotacaoItem.equals(other.idCotacaoItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.CotacaoItem[ idCotacaoItem=" + idCotacaoItem + " ]";
    }
    
}
