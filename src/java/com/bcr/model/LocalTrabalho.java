/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "local_trabalho")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LocalTrabalho.findAll", query = "SELECT l FROM LocalTrabalho l"),
    @NamedQuery(name = "LocalTrabalho.findByIdLocalTrabalho", query = "SELECT l FROM LocalTrabalho l WHERE l.idLocalTrabalho = :idLocalTrabalho"),
    @NamedQuery(name = "LocalTrabalho.findByDescricao", query = "SELECT l FROM LocalTrabalho l WHERE l.descricao = :descricao")})
public class LocalTrabalho implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_local_trabalho")
    private Integer idLocalTrabalho;
    @Size(max = 100)
    @Column(name = "descricao")
    private String descricao;
    @JoinColumn(name = "id_centro_custo", referencedColumnName = "id_centro_custo")
    @ManyToOne
    private CentroCusto idCentroCusto;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @OneToMany(mappedBy = "idLocalTrabalho")
    private List<NotaEntrada> notaEntradaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLocalTrabalho")
    private List<Colaborador> colaboradorList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLocalTrabalho")
    private List<RequisicaoMaterial> requisicaoMaterialList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLocalTrabalho")
    private List<Ativo> ativoList;
    @OneToMany(mappedBy = "idLocalTrabalho")
    private List<Proposta> propostaList;
    @OneToMany(mappedBy = "idLocalTrabalho")
    private List<NotaSaida> notaSaidaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLocalTrabalho")
    private List<Centrocustoplanocontas> centrocustoplanocontasList;
    @OneToMany(mappedBy = "idLocalTrabalho")
    private List<Empresa> empresaList;

    public LocalTrabalho() {
    }

    public LocalTrabalho(Integer idLocalTrabalho) {
        this.idLocalTrabalho = idLocalTrabalho;
    }

    public Integer getIdLocalTrabalho() {
        return idLocalTrabalho;
    }

    public void setIdLocalTrabalho(Integer idLocalTrabalho) {
        this.idLocalTrabalho = idLocalTrabalho;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public CentroCusto getIdCentroCusto() {
        return idCentroCusto;
    }

    public void setIdCentroCusto(CentroCusto idCentroCusto) {
        this.idCentroCusto = idCentroCusto;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @XmlTransient
    public List<NotaEntrada> getNotaEntradaList() {
        return notaEntradaList;
    }

    public void setNotaEntradaList(List<NotaEntrada> notaEntradaList) {
        this.notaEntradaList = notaEntradaList;
    }

    @XmlTransient
    public List<Colaborador> getColaboradorList() {
        return colaboradorList;
    }

    public void setColaboradorList(List<Colaborador> colaboradorList) {
        this.colaboradorList = colaboradorList;
    }

    @XmlTransient
    public List<RequisicaoMaterial> getRequisicaoMaterialList() {
        return requisicaoMaterialList;
    }

    public void setRequisicaoMaterialList(List<RequisicaoMaterial> requisicaoMaterialList) {
        this.requisicaoMaterialList = requisicaoMaterialList;
    }

    @XmlTransient
    public List<Ativo> getAtivoList() {
        return ativoList;
    }

    public void setAtivoList(List<Ativo> ativoList) {
        this.ativoList = ativoList;
    }

    @XmlTransient
    public List<Proposta> getPropostaList() {
        return propostaList;
    }

    public void setPropostaList(List<Proposta> propostaList) {
        this.propostaList = propostaList;
    }

    @XmlTransient
    public List<NotaSaida> getNotaSaidaList() {
        return notaSaidaList;
    }

    public void setNotaSaidaList(List<NotaSaida> notaSaidaList) {
        this.notaSaidaList = notaSaidaList;
    }

    @XmlTransient
    public List<Centrocustoplanocontas> getCentrocustoplanocontasList() {
        return centrocustoplanocontasList;
    }

    public void setCentrocustoplanocontasList(List<Centrocustoplanocontas> centrocustoplanocontasList) {
        this.centrocustoplanocontasList = centrocustoplanocontasList;
    }

    @XmlTransient
    public List<Empresa> getEmpresaList() {
        return empresaList;
    }

    public void setEmpresaList(List<Empresa> empresaList) {
        this.empresaList = empresaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLocalTrabalho != null ? idLocalTrabalho.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LocalTrabalho)) {
            return false;
        }
        LocalTrabalho other = (LocalTrabalho) object;
        if ((this.idLocalTrabalho == null && other.idLocalTrabalho != null) || (this.idLocalTrabalho != null && !this.idLocalTrabalho.equals(other.idLocalTrabalho))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.LocalTrabalho[ idLocalTrabalho=" + idLocalTrabalho + " ]";
    }
    
}
