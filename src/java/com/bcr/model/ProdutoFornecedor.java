/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "produto_fornecedor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProdutoFornecedor.findAll", query = "SELECT p FROM ProdutoFornecedor p"),
    @NamedQuery(name = "ProdutoFornecedor.findByIdProdutoFornecedor", query = "SELECT p FROM ProdutoFornecedor p WHERE p.idProdutoFornecedor = :idProdutoFornecedor")})
public class ProdutoFornecedor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_produto_fornecedor")
    private Integer idProdutoFornecedor;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne
    private Produto idProduto;
    @JoinColumn(name = "id_fornecedor", referencedColumnName = "id_fornecedor")
    @ManyToOne(optional = false)
    private Fornecedor idFornecedor;

    public ProdutoFornecedor() {
    }

    public ProdutoFornecedor(Integer idProdutoFornecedor) {
        this.idProdutoFornecedor = idProdutoFornecedor;
    }

    public Integer getIdProdutoFornecedor() {
        return idProdutoFornecedor;
    }

    public void setIdProdutoFornecedor(Integer idProdutoFornecedor) {
        this.idProdutoFornecedor = idProdutoFornecedor;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    public Fornecedor getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(Fornecedor idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProdutoFornecedor != null ? idProdutoFornecedor.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProdutoFornecedor)) {
            return false;
        }
        ProdutoFornecedor other = (ProdutoFornecedor) object;
        if ((this.idProdutoFornecedor == null && other.idProdutoFornecedor != null) || (this.idProdutoFornecedor != null && !this.idProdutoFornecedor.equals(other.idProdutoFornecedor))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ProdutoFornecedor[ idProdutoFornecedor=" + idProdutoFornecedor + " ]";
    }
    
}
