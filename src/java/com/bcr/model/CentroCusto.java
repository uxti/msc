/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "centro_custo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CentroCusto.findAll", query = "SELECT c FROM CentroCusto c"),
    @NamedQuery(name = "CentroCusto.findByIdCentroCusto", query = "SELECT c FROM CentroCusto c WHERE c.idCentroCusto = :idCentroCusto"),
    @NamedQuery(name = "CentroCusto.findByDescricao", query = "SELECT c FROM CentroCusto c WHERE c.descricao = :descricao"),
    @NamedQuery(name = "CentroCusto.findByImplantacao", query = "SELECT c FROM CentroCusto c WHERE c.implantacao = :implantacao"),
    @NamedQuery(name = "CentroCusto.findByTipo", query = "SELECT c FROM CentroCusto c WHERE c.tipo = :tipo"),
    @NamedQuery(name = "CentroCusto.findByComplemento", query = "SELECT c FROM CentroCusto c WHERE c.complemento = :complemento"),
    @NamedQuery(name = "CentroCusto.findByAtivo", query = "SELECT c FROM CentroCusto c WHERE c.ativo = :ativo")})
public class CentroCusto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_centro_custo")
    private Integer idCentroCusto;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "implantacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date implantacao;
    @Size(max = 30)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 255)
    @Column(name = "complemento")
    private String complemento;
    @Size(max = 1)
    @Column(name = "ativo")
    private String ativo;
    @OneToMany(mappedBy = "idCentroCusto")
    private List<LocalTrabalho> localTrabalhoList;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id_contrato")
    @ManyToOne
    private Contrato idContrato;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCentroCusto")
    private List<CentroCustoAgrupamento> centroCustoAgrupamentoList;
    @OneToMany(mappedBy = "idCentroCusto")
    private List<Centrocustoplanocontas> centrocustoplanocontasList;

    public CentroCusto() {
    }

    public CentroCusto(Integer idCentroCusto) {
        this.idCentroCusto = idCentroCusto;
    }

    public Integer getIdCentroCusto() {
        return idCentroCusto;
    }

    public void setIdCentroCusto(Integer idCentroCusto) {
        this.idCentroCusto = idCentroCusto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getImplantacao() {
        return implantacao;
    }

    public void setImplantacao(Date implantacao) {
        this.implantacao = implantacao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getAtivo() {
        return ativo;
    }

    public void setAtivo(String ativo) {
        this.ativo = ativo;
    }

    @XmlTransient
    public List<LocalTrabalho> getLocalTrabalhoList() {
        return localTrabalhoList;
    }

    public void setLocalTrabalhoList(List<LocalTrabalho> localTrabalhoList) {
        this.localTrabalhoList = localTrabalhoList;
    }

    public Contrato getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Contrato idContrato) {
        this.idContrato = idContrato;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    @XmlTransient
    public List<CentroCustoAgrupamento> getCentroCustoAgrupamentoList() {
        return centroCustoAgrupamentoList;
    }

    public void setCentroCustoAgrupamentoList(List<CentroCustoAgrupamento> centroCustoAgrupamentoList) {
        this.centroCustoAgrupamentoList = centroCustoAgrupamentoList;
    }

    @XmlTransient
    public List<Centrocustoplanocontas> getCentrocustoplanocontasList() {
        return centrocustoplanocontasList;
    }

    public void setCentrocustoplanocontasList(List<Centrocustoplanocontas> centrocustoplanocontasList) {
        this.centrocustoplanocontasList = centrocustoplanocontasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCentroCusto != null ? idCentroCusto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CentroCusto)) {
            return false;
        }
        CentroCusto other = (CentroCusto) object;
        if ((this.idCentroCusto == null && other.idCentroCusto != null) || (this.idCentroCusto != null && !this.idCentroCusto.equals(other.idCentroCusto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.CentroCusto[ idCentroCusto=" + idCentroCusto + " ]";
    }
    
}
