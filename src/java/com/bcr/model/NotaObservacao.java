/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "nota_observacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotaObservacao.findAll", query = "SELECT n FROM NotaObservacao n"),
    @NamedQuery(name = "NotaObservacao.findByIdNotaObservacao", query = "SELECT n FROM NotaObservacao n WHERE n.idNotaObservacao = :idNotaObservacao"),
    @NamedQuery(name = "NotaObservacao.findByDescricao", query = "SELECT n FROM NotaObservacao n WHERE n.descricao = :descricao"),
    @NamedQuery(name = "NotaObservacao.findByMensagem", query = "SELECT n FROM NotaObservacao n WHERE n.mensagem = :mensagem")})
public class NotaObservacao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_nota_observacao")
    private Integer idNotaObservacao;
    @Size(max = 100)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 255)
    @Column(name = "mensagem")
    private String mensagem;
    @OneToMany(mappedBy = "idNotaObservacao")
    private List<NotaEntrada> notaEntradaList;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @OneToMany(mappedBy = "idNotaObservacao")
    private List<NotaSaida> notaSaidaList;

    public NotaObservacao() {
    }

    public NotaObservacao(Integer idNotaObservacao) {
        this.idNotaObservacao = idNotaObservacao;
    }

    public Integer getIdNotaObservacao() {
        return idNotaObservacao;
    }

    public void setIdNotaObservacao(Integer idNotaObservacao) {
        this.idNotaObservacao = idNotaObservacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    @XmlTransient
    public List<NotaEntrada> getNotaEntradaList() {
        return notaEntradaList;
    }

    public void setNotaEntradaList(List<NotaEntrada> notaEntradaList) {
        this.notaEntradaList = notaEntradaList;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @XmlTransient
    public List<NotaSaida> getNotaSaidaList() {
        return notaSaidaList;
    }

    public void setNotaSaidaList(List<NotaSaida> notaSaidaList) {
        this.notaSaidaList = notaSaidaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotaObservacao != null ? idNotaObservacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaObservacao)) {
            return false;
        }
        NotaObservacao other = (NotaObservacao) object;
        if ((this.idNotaObservacao == null && other.idNotaObservacao != null) || (this.idNotaObservacao != null && !this.idNotaObservacao.equals(other.idNotaObservacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.NotaObservacao[ idNotaObservacao=" + idNotaObservacao + " ]";
    }
    
}
