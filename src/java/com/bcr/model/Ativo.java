/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "ativo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ativo.findAll", query = "SELECT a FROM Ativo a"),
    @NamedQuery(name = "Ativo.findByIdAtivo", query = "SELECT a FROM Ativo a WHERE a.idAtivo = :idAtivo"),
    @NamedQuery(name = "Ativo.findByTipo", query = "SELECT a FROM Ativo a WHERE a.tipo = :tipo"),
    @NamedQuery(name = "Ativo.findByDescricao", query = "SELECT a FROM Ativo a WHERE a.descricao = :descricao"),
    @NamedQuery(name = "Ativo.findByStatus", query = "SELECT a FROM Ativo a WHERE a.status = :status"),
    @NamedQuery(name = "Ativo.findByValorCompra", query = "SELECT a FROM Ativo a WHERE a.valorCompra = :valorCompra"),
    @NamedQuery(name = "Ativo.findByMesesMaxDepreciacao", query = "SELECT a FROM Ativo a WHERE a.mesesMaxDepreciacao = :mesesMaxDepreciacao"),
    @NamedQuery(name = "Ativo.findByMesesDepreciado", query = "SELECT a FROM Ativo a WHERE a.mesesDepreciado = :mesesDepreciado"),
    @NamedQuery(name = "Ativo.findByValorRateioDepreciacao", query = "SELECT a FROM Ativo a WHERE a.valorRateioDepreciacao = :valorRateioDepreciacao"),
    @NamedQuery(name = "Ativo.findByValorCustoMensal", query = "SELECT a FROM Ativo a WHERE a.valorCustoMensal = :valorCustoMensal"),
    @NamedQuery(name = "Ativo.findByValorResidual", query = "SELECT a FROM Ativo a WHERE a.valorResidual = :valorResidual"),
    @NamedQuery(name = "Ativo.findByDtAquisicao", query = "SELECT a FROM Ativo a WHERE a.dtAquisicao = :dtAquisicao"),
    @NamedQuery(name = "Ativo.findByNumeroSerie", query = "SELECT a FROM Ativo a WHERE a.numeroSerie = :numeroSerie"),
    @NamedQuery(name = "Ativo.findByNumeroNf", query = "SELECT a FROM Ativo a WHERE a.numeroNf = :numeroNf"),
    @NamedQuery(name = "Ativo.findByTipoVeiculo", query = "SELECT a FROM Ativo a WHERE a.tipoVeiculo = :tipoVeiculo"),
    @NamedQuery(name = "Ativo.findByMarca", query = "SELECT a FROM Ativo a WHERE a.marca = :marca"),
    @NamedQuery(name = "Ativo.findByPlacaVeiculo", query = "SELECT a FROM Ativo a WHERE a.placaVeiculo = :placaVeiculo"),
    @NamedQuery(name = "Ativo.findByCombustivel", query = "SELECT a FROM Ativo a WHERE a.combustivel = :combustivel"),
    @NamedQuery(name = "Ativo.findByInativo", query = "SELECT a FROM Ativo a WHERE a.inativo = :inativo"),
    @NamedQuery(name = "Ativo.findByChassi", query = "SELECT a FROM Ativo a WHERE a.chassi = :chassi"),
    @NamedQuery(name = "Ativo.findByRenavam", query = "SELECT a FROM Ativo a WHERE a.renavam = :renavam"),
    @NamedQuery(name = "Ativo.findByDtTerminoUso", query = "SELECT a FROM Ativo a WHERE a.dtTerminoUso = :dtTerminoUso"),
    @NamedQuery(name = "Ativo.findByObservacao", query = "SELECT a FROM Ativo a WHERE a.observacao = :observacao")})
public class Ativo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_ativo")
    private Integer idAtivo;
    @Size(max = 20)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_compra")
    private BigDecimal valorCompra;
    @Column(name = "meses_max_depreciacao")
    private Integer mesesMaxDepreciacao;
    @Column(name = "meses_depreciado")
    private Integer mesesDepreciado;
    @Column(name = "valor_rateio_depreciacao")
    private BigDecimal valorRateioDepreciacao;
    @Column(name = "valor_custo_mensal")
    private BigDecimal valorCustoMensal;
    @Column(name = "valor_residual")
    private BigDecimal valorResidual;
    @Column(name = "dt_aquisicao")
    @Temporal(TemporalType.DATE)
    private Date dtAquisicao;
    @Size(max = 200)
    @Column(name = "numero_serie")
    private String numeroSerie;
    @Size(max = 50)
    @Column(name = "numero_nf")
    private String numeroNf;
    @Size(max = 100)
    @Column(name = "tipo_veiculo")
    private String tipoVeiculo;
    @Size(max = 50)
    @Column(name = "marca")
    private String marca;
    @Size(max = 20)
    @Column(name = "placa_veiculo")
    private String placaVeiculo;
    @Size(max = 30)
    @Column(name = "combustivel")
    private String combustivel;
    @Column(name = "inativo")
    private Boolean inativo;
    @Size(max = 200)
    @Column(name = "chassi")
    private String chassi;
    @Size(max = 100)
    @Column(name = "renavam")
    private String renavam;
    @Column(name = "dt_termino_uso")
    @Temporal(TemporalType.DATE)
    private Date dtTerminoUso;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @OneToMany(mappedBy = "idAtivo")
    private List<PlanilhaItem> planilhaItemList;
    @OneToMany(mappedBy = "idAtivo")
    private List<RequisicaoMaterialItem> requisicaoMaterialItemList;
    @JoinColumn(name = "id_tipo_ativo", referencedColumnName = "id_tipo_ativo")
    @ManyToOne
    private TipoAtivo idTipoAtivo;
    @JoinColumn(name = "id_unidade", referencedColumnName = "id_unidade")
    @ManyToOne
    private Unidade idUnidade;
    @JoinColumn(name = "id_modelo_veiculo", referencedColumnName = "id_modelo_veiculo")
    @ManyToOne
    private ModeloAtivo idModeloVeiculo;
    @JoinColumn(name = "id_grupo_ativo", referencedColumnName = "id_grupo_ativo")
    @ManyToOne
    private GrupoAtivo idGrupoAtivo;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_local_trabalho", referencedColumnName = "id_local_trabalho")
    @ManyToOne(optional = false)
    private LocalTrabalho idLocalTrabalho;
    @OneToMany(mappedBy = "idAtivo")
    private List<PropostaItemProduto> propostaItemProdutoList;
    @OneToMany(mappedBy = "idAtivo")
    private List<ReservaAtivoItem> reservaAtivoItemList;

    public Ativo() {
    }

    public Ativo(Integer idAtivo) {
        this.idAtivo = idAtivo;
    }

    public Integer getIdAtivo() {
        return idAtivo;
    }

    public void setIdAtivo(Integer idAtivo) {
        this.idAtivo = idAtivo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(BigDecimal valorCompra) {
        this.valorCompra = valorCompra;
    }

    public Integer getMesesMaxDepreciacao() {
        return mesesMaxDepreciacao;
    }

    public void setMesesMaxDepreciacao(Integer mesesMaxDepreciacao) {
        this.mesesMaxDepreciacao = mesesMaxDepreciacao;
    }

    public Integer getMesesDepreciado() {
        return mesesDepreciado;
    }

    public void setMesesDepreciado(Integer mesesDepreciado) {
        this.mesesDepreciado = mesesDepreciado;
    }

    public BigDecimal getValorRateioDepreciacao() {
        return valorRateioDepreciacao;
    }

    public void setValorRateioDepreciacao(BigDecimal valorRateioDepreciacao) {
        this.valorRateioDepreciacao = valorRateioDepreciacao;
    }

    public BigDecimal getValorCustoMensal() {
        return valorCustoMensal;
    }

    public void setValorCustoMensal(BigDecimal valorCustoMensal) {
        this.valorCustoMensal = valorCustoMensal;
    }

    public BigDecimal getValorResidual() {
        return valorResidual;
    }

    public void setValorResidual(BigDecimal valorResidual) {
        this.valorResidual = valorResidual;
    }

    public Date getDtAquisicao() {
        return dtAquisicao;
    }

    public void setDtAquisicao(Date dtAquisicao) {
        this.dtAquisicao = dtAquisicao;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getNumeroNf() {
        return numeroNf;
    }

    public void setNumeroNf(String numeroNf) {
        this.numeroNf = numeroNf;
    }

    public String getTipoVeiculo() {
        return tipoVeiculo;
    }

    public void setTipoVeiculo(String tipoVeiculo) {
        this.tipoVeiculo = tipoVeiculo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPlacaVeiculo() {
        return placaVeiculo;
    }

    public void setPlacaVeiculo(String placaVeiculo) {
        this.placaVeiculo = placaVeiculo;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public Boolean getInativo() {
        return inativo;
    }

    public void setInativo(Boolean inativo) {
        this.inativo = inativo;
    }

    public String getChassi() {
        return chassi;
    }

    public void setChassi(String chassi) {
        this.chassi = chassi;
    }

    public String getRenavam() {
        return renavam;
    }

    public void setRenavam(String renavam) {
        this.renavam = renavam;
    }

    public Date getDtTerminoUso() {
        return dtTerminoUso;
    }

    public void setDtTerminoUso(Date dtTerminoUso) {
        this.dtTerminoUso = dtTerminoUso;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @XmlTransient
    public List<PlanilhaItem> getPlanilhaItemList() {
        return planilhaItemList;
    }

    public void setPlanilhaItemList(List<PlanilhaItem> planilhaItemList) {
        this.planilhaItemList = planilhaItemList;
    }

    @XmlTransient
    public List<RequisicaoMaterialItem> getRequisicaoMaterialItemList() {
        return requisicaoMaterialItemList;
    }

    public void setRequisicaoMaterialItemList(List<RequisicaoMaterialItem> requisicaoMaterialItemList) {
        this.requisicaoMaterialItemList = requisicaoMaterialItemList;
    }

    public TipoAtivo getIdTipoAtivo() {
        return idTipoAtivo;
    }

    public void setIdTipoAtivo(TipoAtivo idTipoAtivo) {
        this.idTipoAtivo = idTipoAtivo;
    }

    public Unidade getIdUnidade() {
        return idUnidade;
    }

    public void setIdUnidade(Unidade idUnidade) {
        this.idUnidade = idUnidade;
    }

    public ModeloAtivo getIdModeloVeiculo() {
        return idModeloVeiculo;
    }

    public void setIdModeloVeiculo(ModeloAtivo idModeloVeiculo) {
        this.idModeloVeiculo = idModeloVeiculo;
    }

    public GrupoAtivo getIdGrupoAtivo() {
        return idGrupoAtivo;
    }

    public void setIdGrupoAtivo(GrupoAtivo idGrupoAtivo) {
        this.idGrupoAtivo = idGrupoAtivo;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public LocalTrabalho getIdLocalTrabalho() {
        return idLocalTrabalho;
    }

    public void setIdLocalTrabalho(LocalTrabalho idLocalTrabalho) {
        this.idLocalTrabalho = idLocalTrabalho;
    }

    @XmlTransient
    public List<PropostaItemProduto> getPropostaItemProdutoList() {
        return propostaItemProdutoList;
    }

    public void setPropostaItemProdutoList(List<PropostaItemProduto> propostaItemProdutoList) {
        this.propostaItemProdutoList = propostaItemProdutoList;
    }

    @XmlTransient
    public List<ReservaAtivoItem> getReservaAtivoItemList() {
        return reservaAtivoItemList;
    }

    public void setReservaAtivoItemList(List<ReservaAtivoItem> reservaAtivoItemList) {
        this.reservaAtivoItemList = reservaAtivoItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAtivo != null ? idAtivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ativo)) {
            return false;
        }
        Ativo other = (Ativo) object;
        if ((this.idAtivo == null && other.idAtivo != null) || (this.idAtivo != null && !this.idAtivo.equals(other.idAtivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Ativo[ idAtivo=" + idAtivo + " ]";
    }
    
}
