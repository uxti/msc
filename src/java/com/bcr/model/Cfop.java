/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "cfop")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cfop.findAll", query = "SELECT c FROM Cfop c"),
    @NamedQuery(name = "Cfop.findByIdCfop", query = "SELECT c FROM Cfop c WHERE c.idCfop = :idCfop"),
    @NamedQuery(name = "Cfop.findByTipo", query = "SELECT c FROM Cfop c WHERE c.tipo = :tipo"),
    @NamedQuery(name = "Cfop.findByCfopInterno", query = "SELECT c FROM Cfop c WHERE c.cfopInterno = :cfopInterno"),
    @NamedQuery(name = "Cfop.findByCfopExterno", query = "SELECT c FROM Cfop c WHERE c.cfopExterno = :cfopExterno"),
    @NamedQuery(name = "Cfop.findByCfopExterior", query = "SELECT c FROM Cfop c WHERE c.cfopExterior = :cfopExterior"),
    @NamedQuery(name = "Cfop.findByDescricao", query = "SELECT c FROM Cfop c WHERE c.descricao = :descricao"),
    @NamedQuery(name = "Cfop.findByEstado", query = "SELECT c FROM Cfop c WHERE c.estado = :estado"),
    @NamedQuery(name = "Cfop.findByObs", query = "SELECT c FROM Cfop c WHERE c.obs = :obs"),
    @NamedQuery(name = "Cfop.findByTipoCfop", query = "SELECT c FROM Cfop c WHERE c.tipoCfop = :tipoCfop")})
public class Cfop implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_CFOP")
    private Integer idCfop;
    @Size(max = 10)
    @Column(name = "TIPO")
    private String tipo;
    @Size(max = 7)
    @Column(name = "CFOP_INTERNO")
    private String cfopInterno;
    @Size(max = 7)
    @Column(name = "CFOP_EXTERNO")
    private String cfopExterno;
    @Size(max = 7)
    @Column(name = "CFOP_EXTERIOR")
    private String cfopExterior;
    @Size(max = 255)
    @Column(name = "DESCRICAO")
    private String descricao;
    @Size(max = 2)
    @Column(name = "ESTADO")
    private String estado;
    @Size(max = 500)
    @Column(name = "OBS")
    private String obs;
    @Size(max = 20)
    @Column(name = "TIPO_CFOP")
    private String tipoCfop;
    @OneToMany(mappedBy = "idCFOP")
    private List<NotaSaidaItem> notaSaidaItemList;
    @OneToMany(mappedBy = "idCFOP")
    private List<NotaEntrada> notaEntradaList;
    @OneToMany(mappedBy = "idCFOP")
    private List<NotaEntradaItem> notaEntradaItemList;
    @OneToMany(mappedBy = "idCFOP")
    private List<NotaSaida> notaSaidaList;

    public Cfop() {
    }

    public Cfop(Integer idCfop) {
        this.idCfop = idCfop;
    }

    public Integer getIdCfop() {
        return idCfop;
    }

    public void setIdCfop(Integer idCfop) {
        this.idCfop = idCfop;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getCfopInterno() {
        return cfopInterno;
    }

    public void setCfopInterno(String cfopInterno) {
        this.cfopInterno = cfopInterno;
    }

    public String getCfopExterno() {
        return cfopExterno;
    }

    public void setCfopExterno(String cfopExterno) {
        this.cfopExterno = cfopExterno;
    }

    public String getCfopExterior() {
        return cfopExterior;
    }

    public void setCfopExterior(String cfopExterior) {
        this.cfopExterior = cfopExterior;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getTipoCfop() {
        return tipoCfop;
    }

    public void setTipoCfop(String tipoCfop) {
        this.tipoCfop = tipoCfop;
    }

    @XmlTransient
    public List<NotaSaidaItem> getNotaSaidaItemList() {
        return notaSaidaItemList;
    }

    public void setNotaSaidaItemList(List<NotaSaidaItem> notaSaidaItemList) {
        this.notaSaidaItemList = notaSaidaItemList;
    }

    @XmlTransient
    public List<NotaEntrada> getNotaEntradaList() {
        return notaEntradaList;
    }

    public void setNotaEntradaList(List<NotaEntrada> notaEntradaList) {
        this.notaEntradaList = notaEntradaList;
    }

    @XmlTransient
    public List<NotaEntradaItem> getNotaEntradaItemList() {
        return notaEntradaItemList;
    }

    public void setNotaEntradaItemList(List<NotaEntradaItem> notaEntradaItemList) {
        this.notaEntradaItemList = notaEntradaItemList;
    }

    @XmlTransient
    public List<NotaSaida> getNotaSaidaList() {
        return notaSaidaList;
    }

    public void setNotaSaidaList(List<NotaSaida> notaSaidaList) {
        this.notaSaidaList = notaSaidaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCfop != null ? idCfop.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cfop)) {
            return false;
        }
        Cfop other = (Cfop) object;
        if ((this.idCfop == null && other.idCfop != null) || (this.idCfop != null && !this.idCfop.equals(other.idCfop))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Cfop[ idCfop=" + idCfop + " ]";
    }
    
}
