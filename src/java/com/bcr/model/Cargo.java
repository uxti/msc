/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "cargo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cargo.findAll", query = "SELECT c FROM Cargo c"),
    @NamedQuery(name = "Cargo.findByIdCargo", query = "SELECT c FROM Cargo c WHERE c.idCargo = :idCargo"),
    @NamedQuery(name = "Cargo.findByDescricao", query = "SELECT c FROM Cargo c WHERE c.descricao = :descricao"),
    @NamedQuery(name = "Cargo.findByCbo", query = "SELECT c FROM Cargo c WHERE c.cbo = :cbo"),
    @NamedQuery(name = "Cargo.findBySalarioBase", query = "SELECT c FROM Cargo c WHERE c.salarioBase = :salarioBase"),
    @NamedQuery(name = "Cargo.findByAdicionalInsalubridade", query = "SELECT c FROM Cargo c WHERE c.adicionalInsalubridade = :adicionalInsalubridade"),
    @NamedQuery(name = "Cargo.findByAdicionalPericulosidade", query = "SELECT c FROM Cargo c WHERE c.adicionalPericulosidade = :adicionalPericulosidade"),
    @NamedQuery(name = "Cargo.findByAlimentacao", query = "SELECT c FROM Cargo c WHERE c.alimentacao = :alimentacao"),
    @NamedQuery(name = "Cargo.findByAcumuloFuncao", query = "SELECT c FROM Cargo c WHERE c.acumuloFuncao = :acumuloFuncao"),
    @NamedQuery(name = "Cargo.findByValeTransporte", query = "SELECT c FROM Cargo c WHERE c.valeTransporte = :valeTransporte"),
    @NamedQuery(name = "Cargo.findBySeguroVida", query = "SELECT c FROM Cargo c WHERE c.seguroVida = :seguroVida")})
public class Cargo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cargo")
    private Integer idCargo;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 20)
    @Column(name = "CBO")
    private String cbo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "salario_base")
    private BigDecimal salarioBase;
    @Column(name = "adicional_insalubridade")
    private BigDecimal adicionalInsalubridade;
    @Column(name = "adicional_periculosidade")
    private BigDecimal adicionalPericulosidade;
    @Column(name = "alimentacao")
    private BigDecimal alimentacao;
    @Column(name = "acumulo_funcao")
    private BigDecimal acumuloFuncao;
    @Column(name = "vale_transporte")
    private BigDecimal valeTransporte;
    @Column(name = "seguro_vida")
    private BigDecimal seguroVida;
    @OneToMany(mappedBy = "idCargo")
    private List<PropostaPessoal> propostaPessoalList;
    @OneToMany(mappedBy = "idCargo")
    private List<Colaborador> colaboradorList;
    @OneToMany(mappedBy = "idCargo")
    private List<Planilha> planilhaList;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;

    public Cargo() {
    }

    public Cargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCbo() {
        return cbo;
    }

    public void setCbo(String cbo) {
        this.cbo = cbo;
    }

    public BigDecimal getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(BigDecimal salarioBase) {
        this.salarioBase = salarioBase;
    }

    public BigDecimal getAdicionalInsalubridade() {
        return adicionalInsalubridade;
    }

    public void setAdicionalInsalubridade(BigDecimal adicionalInsalubridade) {
        this.adicionalInsalubridade = adicionalInsalubridade;
    }

    public BigDecimal getAdicionalPericulosidade() {
        return adicionalPericulosidade;
    }

    public void setAdicionalPericulosidade(BigDecimal adicionalPericulosidade) {
        this.adicionalPericulosidade = adicionalPericulosidade;
    }

    public BigDecimal getAlimentacao() {
        return alimentacao;
    }

    public void setAlimentacao(BigDecimal alimentacao) {
        this.alimentacao = alimentacao;
    }

    public BigDecimal getAcumuloFuncao() {
        return acumuloFuncao;
    }

    public void setAcumuloFuncao(BigDecimal acumuloFuncao) {
        this.acumuloFuncao = acumuloFuncao;
    }

    public BigDecimal getValeTransporte() {
        return valeTransporte;
    }

    public void setValeTransporte(BigDecimal valeTransporte) {
        this.valeTransporte = valeTransporte;
    }

    public BigDecimal getSeguroVida() {
        return seguroVida;
    }

    public void setSeguroVida(BigDecimal seguroVida) {
        this.seguroVida = seguroVida;
    }

    @XmlTransient
    public List<PropostaPessoal> getPropostaPessoalList() {
        return propostaPessoalList;
    }

    public void setPropostaPessoalList(List<PropostaPessoal> propostaPessoalList) {
        this.propostaPessoalList = propostaPessoalList;
    }

    @XmlTransient
    public List<Colaborador> getColaboradorList() {
        return colaboradorList;
    }

    public void setColaboradorList(List<Colaborador> colaboradorList) {
        this.colaboradorList = colaboradorList;
    }

    @XmlTransient
    public List<Planilha> getPlanilhaList() {
        return planilhaList;
    }

    public void setPlanilhaList(List<Planilha> planilhaList) {
        this.planilhaList = planilhaList;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCargo != null ? idCargo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cargo)) {
            return false;
        }
        Cargo other = (Cargo) object;
        if ((this.idCargo == null && other.idCargo != null) || (this.idCargo != null && !this.idCargo.equals(other.idCargo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Cargo[ idCargo=" + idCargo + " ]";
    }
    
}
