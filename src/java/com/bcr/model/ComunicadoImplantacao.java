/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "comunicado_implantacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ComunicadoImplantacao.findAll", query = "SELECT c FROM ComunicadoImplantacao c"),
    @NamedQuery(name = "ComunicadoImplantacao.findByIdComunicado", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.idComunicado = :idComunicado"),
    @NamedQuery(name = "ComunicadoImplantacao.findByObservacaoComunicado", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.observacaoComunicado = :observacaoComunicado"),
    @NamedQuery(name = "ComunicadoImplantacao.findByDtComunicado", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.dtComunicado = :dtComunicado"),
    @NamedQuery(name = "ComunicadoImplantacao.findByDtFaturar", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.dtFaturar = :dtFaturar"),
    @NamedQuery(name = "ComunicadoImplantacao.findByAoDia", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.aoDia = :aoDia"),
    @NamedQuery(name = "ComunicadoImplantacao.findByValorFaturar", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.valorFaturar = :valorFaturar"),
    @NamedQuery(name = "ComunicadoImplantacao.findByObservacaoFinanceiro", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.observacaoFinanceiro = :observacaoFinanceiro"),
    @NamedQuery(name = "ComunicadoImplantacao.findByObservacaoRh", query = "SELECT c FROM ComunicadoImplantacao c WHERE c.observacaoRh = :observacaoRh")})
public class ComunicadoImplantacao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_comunicado")
    private Integer idComunicado;
    @Size(max = 255)
    @Column(name = "observacao_comunicado")
    private String observacaoComunicado;
    @Column(name = "dt_comunicado")
    @Temporal(TemporalType.DATE)
    private Date dtComunicado;
    @Column(name = "dt_faturar")
    private Integer dtFaturar;
    @Column(name = "ao_dia")
    private Integer aoDia;
    @Column(name = "valor_faturar")
    private BigDecimal valorFaturar;
    @Size(max = 255)
    @Column(name = "observacao_financeiro")
    private String observacaoFinanceiro;
    @Size(max = 255)
    @Column(name = "observacao_rh")
    private String observacaoRh;
    @Column(name = "mes_corrente")
    @Temporal(TemporalType.DATE)
    private Date mesCorrente;
    @Column(name = "mes_gerado")
    @Temporal(TemporalType.DATE)
    private Date mesGerado;
    @JoinColumn(name = "id_contrato", referencedColumnName = "id_contrato")
    @ManyToOne(optional = false)
    private Contrato idContrato;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idComunicado")
    private List<NotaSaida> notaSaidaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idComunicado")
    private List<RequisicaoMaterial> requisicaoMaterialList;

    public ComunicadoImplantacao() {
    }

    public ComunicadoImplantacao(Integer idComunicado) {
        this.idComunicado = idComunicado;
    }

    public Integer getIdComunicado() {
        return idComunicado;
    }

    public void setIdComunicado(Integer idComunicado) {
        this.idComunicado = idComunicado;
    }

    public String getObservacaoComunicado() {
        return observacaoComunicado;
    }

    public void setObservacaoComunicado(String observacaoComunicado) {
        this.observacaoComunicado = observacaoComunicado;
    }

    public Date getDtComunicado() {
        return dtComunicado;
    }

    public void setDtComunicado(Date dtComunicado) {
        this.dtComunicado = dtComunicado;
    }

    public Integer getDtFaturar() {
        return dtFaturar;
    }

    public void setDtFaturar(Integer dtFaturar) {
        this.dtFaturar = dtFaturar;
    }

    public Integer getAoDia() {
        return aoDia;
    }

    public void setAoDia(Integer aoDia) {
        this.aoDia = aoDia;
    }

    public BigDecimal getValorFaturar() {
        return valorFaturar;
    }

    public void setValorFaturar(BigDecimal valorFaturar) {
        this.valorFaturar = valorFaturar;
    }

    public String getObservacaoFinanceiro() {
        return observacaoFinanceiro;
    }

    public void setObservacaoFinanceiro(String observacaoFinanceiro) {
        this.observacaoFinanceiro = observacaoFinanceiro;
    }

    public String getObservacaoRh() {
        return observacaoRh;
    }

    public void setObservacaoRh(String observacaoRh) {
        this.observacaoRh = observacaoRh;
    }

    public Contrato getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Contrato idContrato) {
        this.idContrato = idContrato;
    }

    public List<NotaSaida> getNotaSaidaList() {
        return notaSaidaList;
    }

    public void setNotaSaidaList(List<NotaSaida> notaSaidaList) {
        this.notaSaidaList = notaSaidaList;
    }

    public List<RequisicaoMaterial> getRequisicaoMaterialList() {
        return requisicaoMaterialList;
    }

    public void setRequisicaoMaterialList(List<RequisicaoMaterial> requisicaoMaterialList) {
        this.requisicaoMaterialList = requisicaoMaterialList;
    }

    public Date getMesCorrente() {
        return mesCorrente;
    }

    public void setMesCorrente(Date mesCorrente) {
        this.mesCorrente = mesCorrente;
    }

    public Date getMesGerado() {
        return mesGerado;
    }

    public void setMesGerado(Date mesGerado) {
        this.mesGerado = mesGerado;
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idComunicado != null ? idComunicado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComunicadoImplantacao)) {
            return false;
        }
        ComunicadoImplantacao other = (ComunicadoImplantacao) object;
        if ((this.idComunicado == null && other.idComunicado != null) || (this.idComunicado != null && !this.idComunicado.equals(other.idComunicado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ComunicadoImplantacao[ idComunicado=" + idComunicado + " ]";
    }
}
