/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "cotacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cotacao.findAll", query = "SELECT c FROM Cotacao c"),
    @NamedQuery(name = "Cotacao.findByIdCotacao", query = "SELECT c FROM Cotacao c WHERE c.idCotacao = :idCotacao"),
    @NamedQuery(name = "Cotacao.findByDtCadastro", query = "SELECT c FROM Cotacao c WHERE c.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "Cotacao.findByDtAutorizacao", query = "SELECT c FROM Cotacao c WHERE c.dtAutorizacao = :dtAutorizacao"),
    @NamedQuery(name = "Cotacao.findByStatus", query = "SELECT c FROM Cotacao c WHERE c.status = :status"),
    @NamedQuery(name = "Cotacao.findByValortotal", query = "SELECT c FROM Cotacao c WHERE c.valortotal = :valortotal"),
    @NamedQuery(name = "Cotacao.findByLote", query = "SELECT c FROM Cotacao c WHERE c.lote = :lote"),
    @NamedQuery(name = "Cotacao.findByLoteNumerico", query = "SELECT c FROM Cotacao c WHERE c.loteNumerico = :loteNumerico")})
public class Cotacao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cotacao")
    private Integer idCotacao;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtCadastro;
    @Column(name = "dt_autorizacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtAutorizacao;
    @Column(name = "status")
    private String status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "Valor_total")
    private BigDecimal valortotal;
    @Column(name = "lote")
    private String lote;
    @Column(name = "lote_numerico")
    private Integer loteNumerico;
    @OneToMany(mappedBy = "idCotacao")
    private List<CotacaoItem> cotacaoItemList;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "id_fornecedor", referencedColumnName = "id_fornecedor")
    @ManyToOne
    private Fornecedor idFornecedor;
    @JoinColumn(name = "id_requisicao_material", referencedColumnName = "id_requisicao_material")
    @ManyToOne
    private RequisicaoMaterial idRequisicaoMaterial;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCotacao")
    private List<CotacaoItemFornecedor> cotacaoItemFornecedorList;

    public Cotacao() {
    }

    public Cotacao(Integer idCotacao) {
        this.idCotacao = idCotacao;
    }

    public Integer getIdCotacao() {
        return idCotacao;
    }

    public void setIdCotacao(Integer idCotacao) {
        this.idCotacao = idCotacao;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public Date getDtAutorizacao() {
        return dtAutorizacao;
    }

    public void setDtAutorizacao(Date dtAutorizacao) {
        this.dtAutorizacao = dtAutorizacao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getValortotal() {
        return valortotal;
    }

    public void setValortotal(BigDecimal valortotal) {
        this.valortotal = valortotal;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public Integer getLoteNumerico() {
        return loteNumerico;
    }

    public void setLoteNumerico(Integer loteNumerico) {
        this.loteNumerico = loteNumerico;
    }

    @XmlTransient
    public List<CotacaoItem> getCotacaoItemList() {
        return cotacaoItemList;
    }

    public void setCotacaoItemList(List<CotacaoItem> cotacaoItemList) {
        this.cotacaoItemList = cotacaoItemList;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Fornecedor getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(Fornecedor idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public RequisicaoMaterial getIdRequisicaoMaterial() {
        return idRequisicaoMaterial;
    }

    public void setIdRequisicaoMaterial(RequisicaoMaterial idRequisicaoMaterial) {
        this.idRequisicaoMaterial = idRequisicaoMaterial;
    }

    @XmlTransient
    public List<CotacaoItemFornecedor> getCotacaoItemFornecedorList() {
        return cotacaoItemFornecedorList;
    }

    public void setCotacaoItemFornecedorList(List<CotacaoItemFornecedor> cotacaoItemFornecedorList) {
        this.cotacaoItemFornecedorList = cotacaoItemFornecedorList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCotacao != null ? idCotacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cotacao)) {
            return false;
        }
        Cotacao other = (Cotacao) object;
        if ((this.idCotacao == null && other.idCotacao != null) || (this.idCotacao != null && !this.idCotacao.equals(other.idCotacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Cotacao[ idCotacao=" + idCotacao + " ]";
    }
    
}
