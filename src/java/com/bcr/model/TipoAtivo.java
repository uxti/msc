/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "tipo_ativo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoAtivo.findAll", query = "SELECT t FROM TipoAtivo t"),
    @NamedQuery(name = "TipoAtivo.findByIdTipoAtivo", query = "SELECT t FROM TipoAtivo t WHERE t.idTipoAtivo = :idTipoAtivo"),
    @NamedQuery(name = "TipoAtivo.findByDescricao", query = "SELECT t FROM TipoAtivo t WHERE t.descricao = :descricao")})
public class TipoAtivo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_ativo")
    private Integer idTipoAtivo;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @OneToMany(mappedBy = "idTipoAtivo")
    private List<GrupoAtivo> grupoAtivoList;
    @OneToMany(mappedBy = "idTipoAtivo")
    private List<Ativo> ativoList;

    public TipoAtivo() {
    }

    public TipoAtivo(Integer idTipoAtivo) {
        this.idTipoAtivo = idTipoAtivo;
    }

    public Integer getIdTipoAtivo() {
        return idTipoAtivo;
    }

    public void setIdTipoAtivo(Integer idTipoAtivo) {
        this.idTipoAtivo = idTipoAtivo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public List<GrupoAtivo> getGrupoAtivoList() {
        return grupoAtivoList;
    }

    public void setGrupoAtivoList(List<GrupoAtivo> grupoAtivoList) {
        this.grupoAtivoList = grupoAtivoList;
    }

    @XmlTransient
    public List<Ativo> getAtivoList() {
        return ativoList;
    }

    public void setAtivoList(List<Ativo> ativoList) {
        this.ativoList = ativoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoAtivo != null ? idTipoAtivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoAtivo)) {
            return false;
        }
        TipoAtivo other = (TipoAtivo) object;
        if ((this.idTipoAtivo == null && other.idTipoAtivo != null) || (this.idTipoAtivo != null && !this.idTipoAtivo.equals(other.idTipoAtivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.TipoAtivo[ idTipoAtivo=" + idTipoAtivo + " ]";
    }
    
}
