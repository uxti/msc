/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "reserva_ativo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReservaAtivo.findAll", query = "SELECT r FROM ReservaAtivo r"),
    @NamedQuery(name = "ReservaAtivo.findByIdReservaAtivo", query = "SELECT r FROM ReservaAtivo r WHERE r.idReservaAtivo = :idReservaAtivo"),
    @NamedQuery(name = "ReservaAtivo.findByTipo", query = "SELECT r FROM ReservaAtivo r WHERE r.tipo = :tipo"),
    @NamedQuery(name = "ReservaAtivo.findByStatus", query = "SELECT r FROM ReservaAtivo r WHERE r.status = :status"),
    @NamedQuery(name = "ReservaAtivo.findByDtCadastro", query = "SELECT r FROM ReservaAtivo r WHERE r.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "ReservaAtivo.findByDtReservaInicio", query = "SELECT r FROM ReservaAtivo r WHERE r.dtReservaInicio = :dtReservaInicio"),
    @NamedQuery(name = "ReservaAtivo.findByDtReservaFim", query = "SELECT r FROM ReservaAtivo r WHERE r.dtReservaFim = :dtReservaFim"),
    @NamedQuery(name = "ReservaAtivo.findByMotivo", query = "SELECT r FROM ReservaAtivo r WHERE r.motivo = :motivo"),
    @NamedQuery(name = "ReservaAtivo.findByObservacao", query = "SELECT r FROM ReservaAtivo r WHERE r.observacao = :observacao")})
public class ReservaAtivo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_reserva_ativo")
    private Integer idReservaAtivo;
    @Size(max = 100)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.DATE)
    private Date dtCadastro;
    @Column(name = "dt_reserva_inicio")
    @Temporal(TemporalType.DATE)
    private Date dtReservaInicio;
    @Column(name = "dt_reserva_fim")
    @Temporal(TemporalType.DATE)
    private Date dtReservaFim;
    @Size(max = 255)
    @Column(name = "motivo")
    private String motivo;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "id_colaborador", referencedColumnName = "id_colaborador")
    @ManyToOne
    private Colaborador idColaborador;
    @OneToMany(mappedBy = "idReservaAtivo")
    private List<ReservaAtivoItem> reservaAtivoItemList;

    public ReservaAtivo() {
    }

    public ReservaAtivo(Integer idReservaAtivo) {
        this.idReservaAtivo = idReservaAtivo;
    }

    public Integer getIdReservaAtivo() {
        return idReservaAtivo;
    }

    public void setIdReservaAtivo(Integer idReservaAtivo) {
        this.idReservaAtivo = idReservaAtivo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public Date getDtReservaInicio() {
        return dtReservaInicio;
    }

    public void setDtReservaInicio(Date dtReservaInicio) {
        this.dtReservaInicio = dtReservaInicio;
    }

    public Date getDtReservaFim() {
        return dtReservaFim;
    }

    public void setDtReservaFim(Date dtReservaFim) {
        this.dtReservaFim = dtReservaFim;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Colaborador getIdColaborador() {
        return idColaborador;
    }

    public void setIdColaborador(Colaborador idColaborador) {
        this.idColaborador = idColaborador;
    }

    @XmlTransient
    public List<ReservaAtivoItem> getReservaAtivoItemList() {
        return reservaAtivoItemList;
    }

    public void setReservaAtivoItemList(List<ReservaAtivoItem> reservaAtivoItemList) {
        this.reservaAtivoItemList = reservaAtivoItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReservaAtivo != null ? idReservaAtivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservaAtivo)) {
            return false;
        }
        ReservaAtivo other = (ReservaAtivo) object;
        if ((this.idReservaAtivo == null && other.idReservaAtivo != null) || (this.idReservaAtivo != null && !this.idReservaAtivo.equals(other.idReservaAtivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ReservaAtivo[ idReservaAtivo=" + idReservaAtivo + " ]";
    }
    
}
