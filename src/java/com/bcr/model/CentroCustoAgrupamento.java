/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "centro_custo_agrupamento")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CentroCustoAgrupamento.findAll", query = "SELECT c FROM CentroCustoAgrupamento c"),
    @NamedQuery(name = "CentroCustoAgrupamento.findByIdCentroCustoAgrupamento", query = "SELECT c FROM CentroCustoAgrupamento c WHERE c.idCentroCustoAgrupamento = :idCentroCustoAgrupamento")})
public class CentroCustoAgrupamento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_centro_custo_agrupamento")
    private Integer idCentroCustoAgrupamento;
    @JoinColumn(name = "id_centro_custo", referencedColumnName = "id_centro_custo")
    @ManyToOne(optional = false)
    private CentroCusto idCentroCusto;
    @JoinColumn(name = "id_agrupamento", referencedColumnName = "id_agrupamento")
    @ManyToOne(optional = false)
    private Agrupamento idAgrupamento;

    public CentroCustoAgrupamento() {
    }

    public CentroCustoAgrupamento(Integer idCentroCustoAgrupamento) {
        this.idCentroCustoAgrupamento = idCentroCustoAgrupamento;
    }

    public Integer getIdCentroCustoAgrupamento() {
        return idCentroCustoAgrupamento;
    }

    public void setIdCentroCustoAgrupamento(Integer idCentroCustoAgrupamento) {
        this.idCentroCustoAgrupamento = idCentroCustoAgrupamento;
    }

    public CentroCusto getIdCentroCusto() {
        return idCentroCusto;
    }

    public void setIdCentroCusto(CentroCusto idCentroCusto) {
        this.idCentroCusto = idCentroCusto;
    }

    public Agrupamento getIdAgrupamento() {
        return idAgrupamento;
    }

    public void setIdAgrupamento(Agrupamento idAgrupamento) {
        this.idAgrupamento = idAgrupamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCentroCustoAgrupamento != null ? idCentroCustoAgrupamento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CentroCustoAgrupamento)) {
            return false;
        }
        CentroCustoAgrupamento other = (CentroCustoAgrupamento) object;
        if ((this.idCentroCustoAgrupamento == null && other.idCentroCustoAgrupamento != null) || (this.idCentroCustoAgrupamento != null && !this.idCentroCustoAgrupamento.equals(other.idCentroCustoAgrupamento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.CentroCustoAgrupamento[ idCentroCustoAgrupamento=" + idCentroCustoAgrupamento + " ]";
    }
    
}
