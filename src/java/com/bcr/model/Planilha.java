/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "planilha")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Planilha.findAll", query = "SELECT p FROM Planilha p"),
    @NamedQuery(name = "Planilha.findByIdPlanilha", query = "SELECT p FROM Planilha p WHERE p.idPlanilha = :idPlanilha"),
    @NamedQuery(name = "Planilha.findByDescricao", query = "SELECT p FROM Planilha p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "Planilha.findByTipo", query = "SELECT p FROM Planilha p WHERE p.tipo = :tipo"),
    @NamedQuery(name = "Planilha.findByValorBase", query = "SELECT p FROM Planilha p WHERE p.valorBase = :valorBase"),
    @NamedQuery(name = "Planilha.findByAdicional1", query = "SELECT p FROM Planilha p WHERE p.adicional1 = :adicional1"),
    @NamedQuery(name = "Planilha.findByAdicional2", query = "SELECT p FROM Planilha p WHERE p.adicional2 = :adicional2"),
    @NamedQuery(name = "Planilha.findByAdicional3", query = "SELECT p FROM Planilha p WHERE p.adicional3 = :adicional3"),
    @NamedQuery(name = "Planilha.findByOutros", query = "SELECT p FROM Planilha p WHERE p.outros = :outros")})
public class Planilha implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_planilha")
    private Integer idPlanilha;
    @Size(max = 200)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 100)
    @Column(name = "tipo")
    private String tipo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_base")
    private BigDecimal valorBase;
    @Column(name = "adicional1")
    private BigDecimal adicional1;
    @Column(name = "adicional2")
    private BigDecimal adicional2;
    @Column(name = "adicional3")
    private BigDecimal adicional3;
    @Column(name = "outros")
    private BigDecimal outros;
    @OneToMany(mappedBy = "idPlanilha")
    private List<PropostaPessoal> propostaPessoalList;
    @OneToMany(mappedBy = "idPlanilha")
    private List<PlanilhaItem> planilhaItemList;
    @OneToMany(mappedBy = "idPlanilha")
    private List<PlanilhaMateriais> planilhaMateriaisList;
    @JoinColumn(name = "id_setor", referencedColumnName = "id_setor")
    @ManyToOne
    private Setor idSetor;
    @JoinColumn(name = "id_cargo", referencedColumnName = "id_cargo")
    @ManyToOne
    private Cargo idCargo;
    @OneToMany(mappedBy = "idPlanilha")
    private List<PropostaItem> propostaItemList;
    @OneToMany(mappedBy = "idPlanilha")
    private List<PropostaItemProduto> propostaItemProdutoList;

    public Planilha() {
    }

    public Planilha(Integer idPlanilha) {
        this.idPlanilha = idPlanilha;
    }

    public Integer getIdPlanilha() {
        return idPlanilha;
    }

    public void setIdPlanilha(Integer idPlanilha) {
        this.idPlanilha = idPlanilha;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public BigDecimal getValorBase() {
        return valorBase;
    }

    public void setValorBase(BigDecimal valorBase) {
        this.valorBase = valorBase;
    }

    public BigDecimal getAdicional1() {
        return adicional1;
    }

    public void setAdicional1(BigDecimal adicional1) {
        this.adicional1 = adicional1;
    }

    public BigDecimal getAdicional2() {
        return adicional2;
    }

    public void setAdicional2(BigDecimal adicional2) {
        this.adicional2 = adicional2;
    }

    public BigDecimal getAdicional3() {
        return adicional3;
    }

    public void setAdicional3(BigDecimal adicional3) {
        this.adicional3 = adicional3;
    }

    public BigDecimal getOutros() {
        return outros;
    }

    public void setOutros(BigDecimal outros) {
        this.outros = outros;
    }

    @XmlTransient
    public List<PropostaPessoal> getPropostaPessoalList() {
        return propostaPessoalList;
    }

    public void setPropostaPessoalList(List<PropostaPessoal> propostaPessoalList) {
        this.propostaPessoalList = propostaPessoalList;
    }

    @XmlTransient
    public List<PlanilhaItem> getPlanilhaItemList() {
        return planilhaItemList;
    }

    public void setPlanilhaItemList(List<PlanilhaItem> planilhaItemList) {
        this.planilhaItemList = planilhaItemList;
    }

    @XmlTransient
    public List<PlanilhaMateriais> getPlanilhaMateriaisList() {
        return planilhaMateriaisList;
    }

    public void setPlanilhaMateriaisList(List<PlanilhaMateriais> planilhaMateriaisList) {
        this.planilhaMateriaisList = planilhaMateriaisList;
    }

    public Setor getIdSetor() {
        return idSetor;
    }

    public void setIdSetor(Setor idSetor) {
        this.idSetor = idSetor;
    }

    public Cargo getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Cargo idCargo) {
        this.idCargo = idCargo;
    }

    @XmlTransient
    public List<PropostaItem> getPropostaItemList() {
        return propostaItemList;
    }

    public void setPropostaItemList(List<PropostaItem> propostaItemList) {
        this.propostaItemList = propostaItemList;
    }

    @XmlTransient
    public List<PropostaItemProduto> getPropostaItemProdutoList() {
        return propostaItemProdutoList;
    }

    public void setPropostaItemProdutoList(List<PropostaItemProduto> propostaItemProdutoList) {
        this.propostaItemProdutoList = propostaItemProdutoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlanilha != null ? idPlanilha.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Planilha)) {
            return false;
        }
        Planilha other = (Planilha) object;
        if ((this.idPlanilha == null && other.idPlanilha != null) || (this.idPlanilha != null && !this.idPlanilha.equals(other.idPlanilha))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Planilha[ idPlanilha=" + idPlanilha + " ]";
    }
    
}
