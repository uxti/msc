/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "ncm")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ncm.findAll", query = "SELECT n FROM Ncm n"),
    @NamedQuery(name = "Ncm.findByIdNcm", query = "SELECT n FROM Ncm n WHERE n.idNcm = :idNcm"),
    @NamedQuery(name = "Ncm.findByDescricao", query = "SELECT n FROM Ncm n WHERE n.descricao = :descricao"),
    @NamedQuery(name = "Ncm.findByNomeclaturaNcm", query = "SELECT n FROM Ncm n WHERE n.nomeclaturaNcm = :nomeclaturaNcm")})
public class Ncm implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_NCM")
    private Integer idNcm;
    @Size(max = 255)
    @Column(name = "DESCRICAO")
    private String descricao;
    @Size(max = 11)
    @Column(name = "NOMECLATURA_NCM")
    private String nomeclaturaNcm;
    @OneToMany(mappedBy = "idNCM")
    private List<Produto> produtoList;

    public Ncm() {
    }

    public Ncm(Integer idNcm) {
        this.idNcm = idNcm;
    }

    public Integer getIdNcm() {
        return idNcm;
    }

    public void setIdNcm(Integer idNcm) {
        this.idNcm = idNcm;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getNomeclaturaNcm() {
        return nomeclaturaNcm;
    }

    public void setNomeclaturaNcm(String nomeclaturaNcm) {
        this.nomeclaturaNcm = nomeclaturaNcm;
    }

    @XmlTransient
    public List<Produto> getProdutoList() {
        return produtoList;
    }

    public void setProdutoList(List<Produto> produtoList) {
        this.produtoList = produtoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNcm != null ? idNcm.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ncm)) {
            return false;
        }
        Ncm other = (Ncm) object;
        if ((this.idNcm == null && other.idNcm != null) || (this.idNcm != null && !this.idNcm.equals(other.idNcm))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Ncm[ idNcm=" + idNcm + " ]";
    }
    
}
