/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "tipo_proposta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoProposta.findAll", query = "SELECT t FROM TipoProposta t"),
    @NamedQuery(name = "TipoProposta.findByIdTipoProposta", query = "SELECT t FROM TipoProposta t WHERE t.idTipoProposta = :idTipoProposta"),
    @NamedQuery(name = "TipoProposta.findByDescricao", query = "SELECT t FROM TipoProposta t WHERE t.descricao = :descricao")})
public class TipoProposta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_proposta")
    private Integer idTipoProposta;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @OneToMany(mappedBy = "idTipoProposta")
    private List<Proposta> propostaList;

    public TipoProposta() {
    }

    public TipoProposta(Integer idTipoProposta) {
        this.idTipoProposta = idTipoProposta;
    }

    public Integer getIdTipoProposta() {
        return idTipoProposta;
    }

    public void setIdTipoProposta(Integer idTipoProposta) {
        this.idTipoProposta = idTipoProposta;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @XmlTransient
    public List<Proposta> getPropostaList() {
        return propostaList;
    }

    public void setPropostaList(List<Proposta> propostaList) {
        this.propostaList = propostaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoProposta != null ? idTipoProposta.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoProposta)) {
            return false;
        }
        TipoProposta other = (TipoProposta) object;
        if ((this.idTipoProposta == null && other.idTipoProposta != null) || (this.idTipoProposta != null && !this.idTipoProposta.equals(other.idTipoProposta))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.TipoProposta[ idTipoProposta=" + idTipoProposta + " ]";
    }
    
}
