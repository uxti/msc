/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "contrato")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contrato.findAll", query = "SELECT c FROM Contrato c"),
    @NamedQuery(name = "Contrato.findByIdContrato", query = "SELECT c FROM Contrato c WHERE c.idContrato = :idContrato"),
    @NamedQuery(name = "Contrato.findByDescricao", query = "SELECT c FROM Contrato c WHERE c.descricao = :descricao"),
    @NamedQuery(name = "Contrato.findByDtInicio", query = "SELECT c FROM Contrato c WHERE c.dtInicio = :dtInicio"),
    @NamedQuery(name = "Contrato.findByDtFim", query = "SELECT c FROM Contrato c WHERE c.dtFim = :dtFim"),
    @NamedQuery(name = "Contrato.findByStatus", query = "SELECT c FROM Contrato c WHERE c.status = :status"),
    @NamedQuery(name = "Contrato.findByValor", query = "SELECT c FROM Contrato c WHERE c.valor = :valor"),
    @NamedQuery(name = "Contrato.findByObserva\u00e7\u00e3o", query = "SELECT c FROM Contrato c WHERE c.observa\u00e7\u00e3o = :observa\u00e7\u00e3o")})
public class Contrato implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_contrato")
    private Integer idContrato;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "dt_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtInicio;
    @Column(name = "dt_fim")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtFim;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private BigDecimal valor;
    @Column(name = "comunicado_gerado")
    private String comunicadoGerado;
    @Size(max = 255)
    @Column(name = "observa\u00e7\u00e3o")
    private String observação;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idContrato")
    private List<ComunicadoImplantacao> comunicadoImplantacaoList;
    @OneToMany(mappedBy = "idContrato")
    private List<CentroCusto> centroCustoList;
    @JoinColumn(name = "id_categoria", referencedColumnName = "id_categoria")
    @ManyToOne
    private Categoria idCategoria;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @JoinColumn(name = "id_proposta_financeira", referencedColumnName = "id_proposta_financeira")
    @ManyToOne
    private PropostaFinanceira idPropostaFinanceira;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "id_forma_pagamento", referencedColumnName = "id_forma_pagamento")
    @ManyToOne
    private FormaPagamento idFormaPagamento;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @OneToMany(mappedBy = "iDContratoAditivo")
    private List<Contrato> contratoList;
    @JoinColumn(name = "ID_Contrato_Aditivo", referencedColumnName = "id_contrato")
    @ManyToOne
    private Contrato iDContratoAditivo;
    @JoinColumn(name = "id_proposta", referencedColumnName = "id_proposta")
    @ManyToOne
    private Proposta idProposta;
    @OneToMany(mappedBy = "idContrato")
    private List<Arquivo> arquivoList;

    public Contrato() {
    }

    public Contrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public Integer getIdContrato() {
        return idContrato;
    }

    public void setIdContrato(Integer idContrato) {
        this.idContrato = idContrato;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDtInicio() {
        return dtInicio;
    }

    public void setDtInicio(Date dtInicio) {
        this.dtInicio = dtInicio;
    }

    public Date getDtFim() {
        return dtFim;
    }

    public void setDtFim(Date dtFim) {
        this.dtFim = dtFim;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getObservação() {
        return observação;
    }

    public void setObservação(String observação) {
        this.observação = observação;
    }

    @XmlTransient
    public List<ComunicadoImplantacao> getComunicadoImplantacaoList() {
        return comunicadoImplantacaoList;
    }

    public void setComunicadoImplantacaoList(List<ComunicadoImplantacao> comunicadoImplantacaoList) {
        this.comunicadoImplantacaoList = comunicadoImplantacaoList;
    }

    @XmlTransient
    public List<CentroCusto> getCentroCustoList() {
        return centroCustoList;
    }

    public void setCentroCustoList(List<CentroCusto> centroCustoList) {
        this.centroCustoList = centroCustoList;
    }

    public Categoria getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Categoria idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public String getComunicadoGerado() {
        return comunicadoGerado;
    }

    public void setComunicadoGerado(String comunicadoGerado) {
        this.comunicadoGerado = comunicadoGerado;
    }
    
    

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public PropostaFinanceira getIdPropostaFinanceira() {
        return idPropostaFinanceira;
    }

    public void setIdPropostaFinanceira(PropostaFinanceira idPropostaFinanceira) {
        this.idPropostaFinanceira = idPropostaFinanceira;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public FormaPagamento getIdFormaPagamento() {
        return idFormaPagamento;
    }

    public void setIdFormaPagamento(FormaPagamento idFormaPagamento) {
        this.idFormaPagamento = idFormaPagamento;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @XmlTransient
    public List<Contrato> getContratoList() {
        return contratoList;
    }

    public void setContratoList(List<Contrato> contratoList) {
        this.contratoList = contratoList;
    }

    public Contrato getIDContratoAditivo() {
        return iDContratoAditivo;
    }

    public void setIDContratoAditivo(Contrato iDContratoAditivo) {
        this.iDContratoAditivo = iDContratoAditivo;
    }

    public Proposta getIdProposta() {
        return idProposta;
    }

    public void setIdProposta(Proposta idProposta) {
        this.idProposta = idProposta;
    }

    @XmlTransient
    public List<Arquivo> getArquivoList() {
        return arquivoList;
    }

    public void setArquivoList(List<Arquivo> arquivoList) {
        this.arquivoList = arquivoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContrato != null ? idContrato.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contrato)) {
            return false;
        }
        Contrato other = (Contrato) object;
        if ((this.idContrato == null && other.idContrato != null) || (this.idContrato != null && !this.idContrato.equals(other.idContrato))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Contrato[ idContrato=" + idContrato + " ]";
    }
    
}
