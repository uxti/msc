/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "caixa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Caixa.findAll", query = "SELECT c FROM Caixa c"),
    @NamedQuery(name = "Caixa.findByIdCaixa", query = "SELECT c FROM Caixa c WHERE c.idCaixa = :idCaixa"),
    @NamedQuery(name = "Caixa.findByDtCaixa", query = "SELECT c FROM Caixa c WHERE c.dtCaixa = :dtCaixa"),
    @NamedQuery(name = "Caixa.findBySaldoAnterior", query = "SELECT c FROM Caixa c WHERE c.saldoAnterior = :saldoAnterior"),
    @NamedQuery(name = "Caixa.findByTotalEntradas", query = "SELECT c FROM Caixa c WHERE c.totalEntradas = :totalEntradas"),
    @NamedQuery(name = "Caixa.findByTotalSaidas", query = "SELECT c FROM Caixa c WHERE c.totalSaidas = :totalSaidas"),
    @NamedQuery(name = "Caixa.findBySaldoAtual", query = "SELECT c FROM Caixa c WHERE c.saldoAtual = :saldoAtual")})
public class Caixa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_caixa")
    private Integer idCaixa;
    @Column(name = "dt_caixa")
    @Temporal(TemporalType.DATE)
    private Date dtCaixa;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "saldo_anterior")
    private BigDecimal saldoAnterior;
    @Column(name = "total_entradas")
    private BigDecimal totalEntradas;
    @Column(name = "total_saidas")
    private BigDecimal totalSaidas;
    @Column(name = "saldo_atual")
    private BigDecimal saldoAtual;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @OneToMany(mappedBy = "idCaixa")
    private List<CaixaItem> caixaItemList;

    public Caixa() {
    }

    public Caixa(Integer idCaixa) {
        this.idCaixa = idCaixa;
    }

    public Integer getIdCaixa() {
        return idCaixa;
    }

    public void setIdCaixa(Integer idCaixa) {
        this.idCaixa = idCaixa;
    }

    public Date getDtCaixa() {
        return dtCaixa;
    }

    public void setDtCaixa(Date dtCaixa) {
        this.dtCaixa = dtCaixa;
    }

    public BigDecimal getSaldoAnterior() {
        return saldoAnterior;
    }

    public void setSaldoAnterior(BigDecimal saldoAnterior) {
        this.saldoAnterior = saldoAnterior;
    }

    public BigDecimal getTotalEntradas() {
        return totalEntradas;
    }

    public void setTotalEntradas(BigDecimal totalEntradas) {
        this.totalEntradas = totalEntradas;
    }

    public BigDecimal getTotalSaidas() {
        return totalSaidas;
    }

    public void setTotalSaidas(BigDecimal totalSaidas) {
        this.totalSaidas = totalSaidas;
    }

    public BigDecimal getSaldoAtual() {
        return saldoAtual;
    }

    public void setSaldoAtual(BigDecimal saldoAtual) {
        this.saldoAtual = saldoAtual;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @XmlTransient
    public List<CaixaItem> getCaixaItemList() {
        return caixaItemList;
    }

    public void setCaixaItemList(List<CaixaItem> caixaItemList) {
        this.caixaItemList = caixaItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCaixa != null ? idCaixa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Caixa)) {
            return false;
        }
        Caixa other = (Caixa) object;
        if ((this.idCaixa == null && other.idCaixa != null) || (this.idCaixa != null && !this.idCaixa.equals(other.idCaixa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Caixa[ idCaixa=" + idCaixa + " ]";
    }
    
}
