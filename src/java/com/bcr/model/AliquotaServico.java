/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "aliquota_servico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AliquotaServico.findAll", query = "SELECT a FROM AliquotaServico a"),
    @NamedQuery(name = "AliquotaServico.findByIdAliquotaServico", query = "SELECT a FROM AliquotaServico a WHERE a.idAliquotaServico = :idAliquotaServico"),
    @NamedQuery(name = "AliquotaServico.findByDescricao", query = "SELECT a FROM AliquotaServico a WHERE a.descricao = :descricao"),
    @NamedQuery(name = "AliquotaServico.findByAliquota", query = "SELECT a FROM AliquotaServico a WHERE a.aliquota = :aliquota")})
public class AliquotaServico implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_aliquota_servico")
    private Integer idAliquotaServico;
    @Column(name = "descricao")
    private String descricao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "aliquota")
    private BigDecimal aliquota;
    @JoinColumn(name = "id_servico", referencedColumnName = "id_servico")
    @ManyToOne
    private Servico idServico;
    @JoinColumn(name = "id_cidade", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade idCidade;

    public AliquotaServico() {
    }

    public AliquotaServico(Integer idAliquotaServico) {
        this.idAliquotaServico = idAliquotaServico;
    }

    public Integer getIdAliquotaServico() {
        return idAliquotaServico;
    }

    public void setIdAliquotaServico(Integer idAliquotaServico) {
        this.idAliquotaServico = idAliquotaServico;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getAliquota() {
        return aliquota;
    }

    public void setAliquota(BigDecimal aliquota) {
        this.aliquota = aliquota;
    }

    public Servico getIdServico() {
        return idServico;
    }

    public void setIdServico(Servico idServico) {
        this.idServico = idServico;
    }

    public Cidade getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(Cidade idCidade) {
        this.idCidade = idCidade;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAliquotaServico != null ? idAliquotaServico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AliquotaServico)) {
            return false;
        }
        AliquotaServico other = (AliquotaServico) object;
        if ((this.idAliquotaServico == null && other.idAliquotaServico != null) || (this.idAliquotaServico != null && !this.idAliquotaServico.equals(other.idAliquotaServico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.AliquotaServico[ idAliquotaServico=" + idAliquotaServico + " ]";
    }
    
}
