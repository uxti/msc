/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "proposta_item_produto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PropostaItemProduto.findAll", query = "SELECT p FROM PropostaItemProduto p"),
    @NamedQuery(name = "PropostaItemProduto.findByIDPropostaItemProduto", query = "SELECT p FROM PropostaItemProduto p WHERE p.iDPropostaItemProduto = :iDPropostaItemProduto"),
    @NamedQuery(name = "PropostaItemProduto.findByQuantidade", query = "SELECT p FROM PropostaItemProduto p WHERE p.quantidade = :quantidade"),
    @NamedQuery(name = "PropostaItemProduto.findByTotal", query = "SELECT p FROM PropostaItemProduto p WHERE p.total = :total"),
    @NamedQuery(name = "PropostaItemProduto.findByMesesDepreciacao", query = "SELECT p FROM PropostaItemProduto p WHERE p.mesesDepreciacao = :mesesDepreciacao"),
    @NamedQuery(name = "PropostaItemProduto.findByValorUnitario", query = "SELECT p FROM PropostaItemProduto p WHERE p.valorUnitario = :valorUnitario"),
    @NamedQuery(name = "PropostaItemProduto.findByValorDepreciacao", query = "SELECT p FROM PropostaItemProduto p WHERE p.valorDepreciacao = :valorDepreciacao")})
public class PropostaItemProduto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_Proposta_Item_Produto")
    private Integer iDPropostaItemProduto;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "quantidade")
    private BigDecimal quantidade;
    @Column(name = "total")
    private BigDecimal total;
    @Column(name = "meses_depreciacao")
    private BigDecimal mesesDepreciacao;
    @Column(name = "valor_unitario")
    private BigDecimal valorUnitario;
    @Column(name = "valor_depreciacao")
    private BigDecimal valorDepreciacao;
    @Column(name = "valor_total_depreciacao")
    private BigDecimal valorTotalDepreciacao;
    @JoinColumn(name = "id_planilha", referencedColumnName = "id_planilha")
    @ManyToOne
    private Planilha idPlanilha;
    @JoinColumn(name = "id_proposta_item", referencedColumnName = "id_proposta_item")
    @ManyToOne
    private PropostaItem idPropostaItem;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne
    private Produto idProduto;
    @JoinColumn(name = "id_planilha_item", referencedColumnName = "id_planilha_item")
    @ManyToOne
    private PlanilhaItem idPlanilhaItem;
    @JoinColumn(name = "id_ativo", referencedColumnName = "id_ativo")
    @ManyToOne
    private Ativo idAtivo;
    @JoinColumn(name = "id_proposta", referencedColumnName = "id_proposta")
    @ManyToOne
    private Proposta idProposta;

    public PropostaItemProduto() {
    }

    public PropostaItemProduto(Integer iDPropostaItemProduto) {
        this.iDPropostaItemProduto = iDPropostaItemProduto;
    }

    public Integer getIDPropostaItemProduto() {
        return iDPropostaItemProduto;
    }

    public void setIDPropostaItemProduto(Integer iDPropostaItemProduto) {
        this.iDPropostaItemProduto = iDPropostaItemProduto;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getMesesDepreciacao() {
        return mesesDepreciacao;
    }

    public void setMesesDepreciacao(BigDecimal mesesDepreciacao) {
        this.mesesDepreciacao = mesesDepreciacao;
    }

    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public BigDecimal getValorDepreciacao() {
        return valorDepreciacao;
    }

    public void setValorDepreciacao(BigDecimal valorDepreciacao) {
        this.valorDepreciacao = valorDepreciacao;
    }

    public Planilha getIdPlanilha() {
        return idPlanilha;
    }

    public void setIdPlanilha(Planilha idPlanilha) {
        this.idPlanilha = idPlanilha;
    }

    public PropostaItem getIdPropostaItem() {
        return idPropostaItem;
    }

    public void setIdPropostaItem(PropostaItem idPropostaItem) {
        this.idPropostaItem = idPropostaItem;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    public PlanilhaItem getIdPlanilhaItem() {
        return idPlanilhaItem;
    }

    public void setIdPlanilhaItem(PlanilhaItem idPlanilhaItem) {
        this.idPlanilhaItem = idPlanilhaItem;
    }

    public Ativo getIdAtivo() {
        return idAtivo;
    }

    public void setIdAtivo(Ativo idAtivo) {
        this.idAtivo = idAtivo;
    }

    public Proposta getIdProposta() {
        return idProposta;
    }

    public void setIdProposta(Proposta idProposta) {
        this.idProposta = idProposta;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iDPropostaItemProduto != null ? iDPropostaItemProduto.hashCode() : 0);
        return hash;
    }

    public BigDecimal getValorTotalDepreciacao() {
        return valorTotalDepreciacao;
    }

    public void setValorTotalDepreciacao(BigDecimal valorTotalDepreciacao) {
        this.valorTotalDepreciacao = valorTotalDepreciacao;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropostaItemProduto)) {
            return false;
        }
        PropostaItemProduto other = (PropostaItemProduto) object;
        if ((this.iDPropostaItemProduto == null && other.iDPropostaItemProduto != null) || (this.iDPropostaItemProduto != null && !this.iDPropostaItemProduto.equals(other.iDPropostaItemProduto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PropostaItemProduto[ iDPropostaItemProduto=" + iDPropostaItemProduto + " ]";
    }
    
}
