/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "grupo_ativo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GrupoAtivo.findAll", query = "SELECT g FROM GrupoAtivo g"),
    @NamedQuery(name = "GrupoAtivo.findByIdGrupoAtivo", query = "SELECT g FROM GrupoAtivo g WHERE g.idGrupoAtivo = :idGrupoAtivo"),
    @NamedQuery(name = "GrupoAtivo.findByDescricao", query = "SELECT g FROM GrupoAtivo g WHERE g.descricao = :descricao")})
public class GrupoAtivo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_grupo_ativo")
    private Integer idGrupoAtivo;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @JoinColumn(name = "id_tipo_ativo", referencedColumnName = "id_tipo_ativo")
    @ManyToOne
    private TipoAtivo idTipoAtivo;
    @OneToMany(mappedBy = "idGrupoAtivo")
    private List<ModeloAtivo> modeloAtivoList;
    @OneToMany(mappedBy = "idGrupoAtivo")
    private List<Ativo> ativoList;

    public GrupoAtivo() {
    }

    public GrupoAtivo(Integer idGrupoAtivo) {
        this.idGrupoAtivo = idGrupoAtivo;
    }

    public Integer getIdGrupoAtivo() {
        return idGrupoAtivo;
    }

    public void setIdGrupoAtivo(Integer idGrupoAtivo) {
        this.idGrupoAtivo = idGrupoAtivo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoAtivo getIdTipoAtivo() {
        return idTipoAtivo;
    }

    public void setIdTipoAtivo(TipoAtivo idTipoAtivo) {
        this.idTipoAtivo = idTipoAtivo;
    }

    @XmlTransient
    public List<ModeloAtivo> getModeloAtivoList() {
        return modeloAtivoList;
    }

    public void setModeloAtivoList(List<ModeloAtivo> modeloAtivoList) {
        this.modeloAtivoList = modeloAtivoList;
    }

    @XmlTransient
    public List<Ativo> getAtivoList() {
        return ativoList;
    }

    public void setAtivoList(List<Ativo> ativoList) {
        this.ativoList = ativoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrupoAtivo != null ? idGrupoAtivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GrupoAtivo)) {
            return false;
        }
        GrupoAtivo other = (GrupoAtivo) object;
        if ((this.idGrupoAtivo == null && other.idGrupoAtivo != null) || (this.idGrupoAtivo != null && !this.idGrupoAtivo.equals(other.idGrupoAtivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.GrupoAtivo[ idGrupoAtivo=" + idGrupoAtivo + " ]";
    }
    
}
