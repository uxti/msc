/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "nota_entrada")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "NotaEntrada.findAll", query = "SELECT n FROM NotaEntrada n"),
    @NamedQuery(name = "NotaEntrada.findByIdNotaEntrada", query = "SELECT n FROM NotaEntrada n WHERE n.idNotaEntrada = :idNotaEntrada"),
    @NamedQuery(name = "NotaEntrada.findByNumero", query = "SELECT n FROM NotaEntrada n WHERE n.numero = :numero"),
    @NamedQuery(name = "NotaEntrada.findBySerie", query = "SELECT n FROM NotaEntrada n WHERE n.serie = :serie"),
    @NamedQuery(name = "NotaEntrada.findByStatus", query = "SELECT n FROM NotaEntrada n WHERE n.status = :status"),
    @NamedQuery(name = "NotaEntrada.findByTipoEntrada", query = "SELECT n FROM NotaEntrada n WHERE n.tipoEntrada = :tipoEntrada"),
    @NamedQuery(name = "NotaEntrada.findByTipoEmissao", query = "SELECT n FROM NotaEntrada n WHERE n.tipoEmissao = :tipoEmissao"),
    @NamedQuery(name = "NotaEntrada.findByChaveAcesso", query = "SELECT n FROM NotaEntrada n WHERE n.chaveAcesso = :chaveAcesso"),
    @NamedQuery(name = "NotaEntrada.findByProtocoloAutorizacao", query = "SELECT n FROM NotaEntrada n WHERE n.protocoloAutorizacao = :protocoloAutorizacao"),
    @NamedQuery(name = "NotaEntrada.findByDtEmissao", query = "SELECT n FROM NotaEntrada n WHERE n.dtEmissao = :dtEmissao"),
    @NamedQuery(name = "NotaEntrada.findByDtEntrada", query = "SELECT n FROM NotaEntrada n WHERE n.dtEntrada = :dtEntrada"),
    @NamedQuery(name = "NotaEntrada.findByHoraSaida", query = "SELECT n FROM NotaEntrada n WHERE n.horaSaida = :horaSaida"),
    @NamedQuery(name = "NotaEntrada.findByBasecalculoICMS", query = "SELECT n FROM NotaEntrada n WHERE n.basecalculoICMS = :basecalculoICMS"),
    @NamedQuery(name = "NotaEntrada.findByValorICMS", query = "SELECT n FROM NotaEntrada n WHERE n.valorICMS = :valorICMS"),
    @NamedQuery(name = "NotaEntrada.findByBasecalculoICMSST", query = "SELECT n FROM NotaEntrada n WHERE n.basecalculoICMSST = :basecalculoICMSST"),
    @NamedQuery(name = "NotaEntrada.findByValorICMSST", query = "SELECT n FROM NotaEntrada n WHERE n.valorICMSST = :valorICMSST"),
    @NamedQuery(name = "NotaEntrada.findByValorFrete", query = "SELECT n FROM NotaEntrada n WHERE n.valorFrete = :valorFrete"),
    @NamedQuery(name = "NotaEntrada.findByValorSeguro", query = "SELECT n FROM NotaEntrada n WHERE n.valorSeguro = :valorSeguro"),
    @NamedQuery(name = "NotaEntrada.findByValorDesconto", query = "SELECT n FROM NotaEntrada n WHERE n.valorDesconto = :valorDesconto"),
    @NamedQuery(name = "NotaEntrada.findByOutrasDespesas", query = "SELECT n FROM NotaEntrada n WHERE n.outrasDespesas = :outrasDespesas"),
    @NamedQuery(name = "NotaEntrada.findByValorIPI", query = "SELECT n FROM NotaEntrada n WHERE n.valorIPI = :valorIPI"),
    @NamedQuery(name = "NotaEntrada.findByValortotalNF", query = "SELECT n FROM NotaEntrada n WHERE n.valortotalNF = :valortotalNF"),
    @NamedQuery(name = "NotaEntrada.findByValorTotalProdutos", query = "SELECT n FROM NotaEntrada n WHERE n.valorTotalProdutos = :valorTotalProdutos"),
    @NamedQuery(name = "NotaEntrada.findByValorTotalServicos", query = "SELECT n FROM NotaEntrada n WHERE n.valorTotalServicos = :valorTotalServicos"),
    @NamedQuery(name = "NotaEntrada.findByBasecalculoISSQN", query = "SELECT n FROM NotaEntrada n WHERE n.basecalculoISSQN = :basecalculoISSQN"),
    @NamedQuery(name = "NotaEntrada.findByValorISSQN", query = "SELECT n FROM NotaEntrada n WHERE n.valorISSQN = :valorISSQN"),
    @NamedQuery(name = "NotaEntrada.findByTipoFrete", query = "SELECT n FROM NotaEntrada n WHERE n.tipoFrete = :tipoFrete"),
    @NamedQuery(name = "NotaEntrada.findByPlacaFrete", query = "SELECT n FROM NotaEntrada n WHERE n.placaFrete = :placaFrete"),
    @NamedQuery(name = "NotaEntrada.findByPlacaUFfrete", query = "SELECT n FROM NotaEntrada n WHERE n.placaUFfrete = :placaUFfrete"),
    @NamedQuery(name = "NotaEntrada.findByQuantidadeFrete", query = "SELECT n FROM NotaEntrada n WHERE n.quantidadeFrete = :quantidadeFrete"),
    @NamedQuery(name = "NotaEntrada.findByModalidadeFrete", query = "SELECT n FROM NotaEntrada n WHERE n.modalidadeFrete = :modalidadeFrete"),
    @NamedQuery(name = "NotaEntrada.findByMarcaFrete", query = "SELECT n FROM NotaEntrada n WHERE n.marcaFrete = :marcaFrete"),
    @NamedQuery(name = "NotaEntrada.findByNumeroFrete", query = "SELECT n FROM NotaEntrada n WHERE n.numeroFrete = :numeroFrete"),
    @NamedQuery(name = "NotaEntrada.findByPesoBruto", query = "SELECT n FROM NotaEntrada n WHERE n.pesoBruto = :pesoBruto"),
    @NamedQuery(name = "NotaEntrada.findByPesoLiquido", query = "SELECT n FROM NotaEntrada n WHERE n.pesoLiquido = :pesoLiquido"),
    @NamedQuery(name = "NotaEntrada.findByInformacoesComplementares", query = "SELECT n FROM NotaEntrada n WHERE n.informacoesComplementares = :informacoesComplementares"),
    @NamedQuery(name = "NotaEntrada.findByObservacoes", query = "SELECT n FROM NotaEntrada n WHERE n.observacoes = :observacoes"),
    @NamedQuery(name = "NotaEntrada.findByCodigoNumerico", query = "SELECT n FROM NotaEntrada n WHERE n.codigoNumerico = :codigoNumerico"),
    @NamedQuery(name = "NotaEntrada.findByCodMunicipioGerador", query = "SELECT n FROM NotaEntrada n WHERE n.codMunicipioGerador = :codMunicipioGerador"),
    @NamedQuery(name = "NotaEntrada.findByImpressaoDanf", query = "SELECT n FROM NotaEntrada n WHERE n.impressaoDanf = :impressaoDanf"),
    @NamedQuery(name = "NotaEntrada.findByTipoProcesso", query = "SELECT n FROM NotaEntrada n WHERE n.tipoProcesso = :tipoProcesso")})
public class NotaEntrada implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_nota_entrada")
    private Integer idNotaEntrada;
    @Size(max = 20)
    @Column(name = "numero")
    private String numero;
    @Size(max = 3)
    @Column(name = "serie")
    private String serie;
    @Size(max = 50)
    @Column(name = "status")
    private String status;
    @Size(max = 100)
    @Column(name = "tipo_entrada")
    private String tipoEntrada;
    @Size(max = 100)
    @Column(name = "tipo_emissao")
    private String tipoEmissao;
    @Column(name = "chave_acesso")
    private Integer chaveAcesso;
    @Size(max = 99)
    @Column(name = "protocolo_autorizacao")
    private String protocoloAutorizacao;
    @Basic(optional = false)
    @Column(name = "dt_emissao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEmissao;
    @Basic(optional = false)
    @Column(name = "dt_entrada")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEntrada;
    @Basic(optional = false)
    @Column(name = "hora_saida")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horaSaida;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "base_calculo_ICMS")
    private BigDecimal basecalculoICMS;
    @Column(name = "valor_ICMS")
    private BigDecimal valorICMS;
    @Column(name = "base_calculo_ICMS_ST")
    private BigDecimal basecalculoICMSST;
    @Column(name = "valor_ICMS_ST")
    private BigDecimal valorICMSST;
    @Column(name = "valor_frete")
    private BigDecimal valorFrete;
    @Column(name = "valor_seguro")
    private BigDecimal valorSeguro;
    @Column(name = "valor_desconto")
    private BigDecimal valorDesconto;
    @Column(name = "outras_despesas")
    private BigDecimal outrasDespesas;
    @Column(name = "valor_IPI")
    private BigDecimal valorIPI;
    @Column(name = "valor_total_NF")
    private BigDecimal valortotalNF;
    @Column(name = "valor_total_produtos")
    private BigDecimal valorTotalProdutos;
    @Column(name = "valor_total_servicos")
    private BigDecimal valorTotalServicos;
    @Column(name = "base_calculo_ISSQN")
    private BigDecimal basecalculoISSQN;
    @Column(name = "valor_ISSQN")
    private BigDecimal valorISSQN;
    @Size(max = 100)
    @Column(name = "tipo_frete")
    private String tipoFrete;
    @Size(max = 8)
    @Column(name = "placa_frete")
    private String placaFrete;
    @Size(max = 2)
    @Column(name = "placa_UF_frete")
    private String placaUFfrete;
    @Column(name = "quantidade_frete")
    private Short quantidadeFrete;
    @Size(max = 1)
    @Column(name = "modalidade_frete")
    private String modalidadeFrete;
    @Size(max = 60)
    @Column(name = "marca_frete")
    private String marcaFrete;
    @Column(name = "numero_frete")
    private Integer numeroFrete;
    @Column(name = "peso_bruto")
    private BigDecimal pesoBruto;
    @Column(name = "peso_liquido")
    private BigDecimal pesoLiquido;
    @Size(max = 255)
    @Column(name = "informacoes_complementares")
    private String informacoesComplementares;
    @Size(max = 255)
    @Column(name = "observacoes")
    private String observacoes;
    @Column(name = "codigo_numerico")
    private Integer codigoNumerico;
    @Size(max = 7)
    @Column(name = "cod_municipio_gerador")
    private String codMunicipioGerador;
    @Size(max = 1)
    @Column(name = "impressao_danf")
    private String impressaoDanf;
    @Size(max = 100)
    @Column(name = "Tipo_Processo")
    private String tipoProcesso;
    @Lob
    @Size(max = 16777215)
    @Column(name = "Observacao")
    private String observacao;
    @OneToMany(mappedBy = "idNotaEntrada")
    private List<ContasPagar> contasPagarList;
    @JoinColumn(name = "id_transportadora", referencedColumnName = "id_transportadora")
    @ManyToOne
    private Transportadora idTransportadora;
    @JoinColumn(name = "id_local_trabalho", referencedColumnName = "id_local_trabalho")
    @ManyToOne
    private LocalTrabalho idLocalTrabalho;
    @JoinColumn(name = "id_CFOP", referencedColumnName = "ID_CFOP")
    @ManyToOne
    private Cfop idCFOP;
    @JoinColumn(name = "id_forma_pagamento", referencedColumnName = "id_forma_pagamento")
    @ManyToOne
    private FormaPagamento idFormaPagamento;
    @JoinColumn(name = "id_nota_observacao", referencedColumnName = "id_nota_observacao")
    @ManyToOne
    private NotaObservacao idNotaObservacao;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_fornecedor", referencedColumnName = "id_fornecedor")
    @ManyToOne
    private Fornecedor idFornecedor;
    @JoinColumn(name = "id_modelo", referencedColumnName = "id_modelo")
    @ManyToOne
    private ModeloNfe idModelo;
    @JoinColumn(name = "id_plano_contas", referencedColumnName = "id_plano_contas")
    @ManyToOne
    private PlanoContas idPlanoContas;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @JoinColumn(name = "id_pedido_compra", referencedColumnName = "id_pedido_compra")
    @ManyToOne
    private PedidoCompra idPedidoCompra;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNotaEntrada")
    private List<NotaEntradaItem> notaEntradaItemList;
    @OneToMany(mappedBy = "idNotaEntrada")
    private List<PedidoCompra> pedidoCompraList;

    public NotaEntrada() {
    }

    public NotaEntrada(Integer idNotaEntrada) {
        this.idNotaEntrada = idNotaEntrada;
    }

    public NotaEntrada(Integer idNotaEntrada, Date dtEmissao, Date dtEntrada, Date horaSaida) {
        this.idNotaEntrada = idNotaEntrada;
        this.dtEmissao = dtEmissao;
        this.dtEntrada = dtEntrada;
        this.horaSaida = horaSaida;
    }

    public Integer getIdNotaEntrada() {
        return idNotaEntrada;
    }

    public void setIdNotaEntrada(Integer idNotaEntrada) {
        this.idNotaEntrada = idNotaEntrada;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTipoEntrada() {
        return tipoEntrada;
    }

    public void setTipoEntrada(String tipoEntrada) {
        this.tipoEntrada = tipoEntrada;
    }

    public String getTipoEmissao() {
        return tipoEmissao;
    }

    public void setTipoEmissao(String tipoEmissao) {
        this.tipoEmissao = tipoEmissao;
    }

    public Integer getChaveAcesso() {
        return chaveAcesso;
    }

    public void setChaveAcesso(Integer chaveAcesso) {
        this.chaveAcesso = chaveAcesso;
    }

    public String getProtocoloAutorizacao() {
        return protocoloAutorizacao;
    }

    public void setProtocoloAutorizacao(String protocoloAutorizacao) {
        this.protocoloAutorizacao = protocoloAutorizacao;
    }

    public Date getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Date dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public Date getDtEntrada() {
        return dtEntrada;
    }

    public void setDtEntrada(Date dtEntrada) {
        this.dtEntrada = dtEntrada;
    }

    public Date getHoraSaida() {
        return horaSaida;
    }

    public void setHoraSaida(Date horaSaida) {
        this.horaSaida = horaSaida;
    }

    public BigDecimal getBasecalculoICMS() {
        return basecalculoICMS;
    }

    public void setBasecalculoICMS(BigDecimal basecalculoICMS) {
        this.basecalculoICMS = basecalculoICMS;
    }

    public BigDecimal getValorICMS() {
        return valorICMS;
    }

    public void setValorICMS(BigDecimal valorICMS) {
        this.valorICMS = valorICMS;
    }

    public BigDecimal getBasecalculoICMSST() {
        return basecalculoICMSST;
    }

    public void setBasecalculoICMSST(BigDecimal basecalculoICMSST) {
        this.basecalculoICMSST = basecalculoICMSST;
    }

    public BigDecimal getValorICMSST() {
        return valorICMSST;
    }

    public void setValorICMSST(BigDecimal valorICMSST) {
        this.valorICMSST = valorICMSST;
    }

    public BigDecimal getValorFrete() {
        return valorFrete;
    }

    public void setValorFrete(BigDecimal valorFrete) {
        this.valorFrete = valorFrete;
    }

    public BigDecimal getValorSeguro() {
        return valorSeguro;
    }

    public void setValorSeguro(BigDecimal valorSeguro) {
        this.valorSeguro = valorSeguro;
    }

    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public BigDecimal getOutrasDespesas() {
        return outrasDespesas;
    }

    public void setOutrasDespesas(BigDecimal outrasDespesas) {
        this.outrasDespesas = outrasDespesas;
    }

    public BigDecimal getValorIPI() {
        return valorIPI;
    }

    public void setValorIPI(BigDecimal valorIPI) {
        this.valorIPI = valorIPI;
    }

    public BigDecimal getValortotalNF() {
        return valortotalNF;
    }

    public void setValortotalNF(BigDecimal valortotalNF) {
        this.valortotalNF = valortotalNF;
    }

    public BigDecimal getValorTotalProdutos() {
        return valorTotalProdutos;
    }

    public void setValorTotalProdutos(BigDecimal valorTotalProdutos) {
        this.valorTotalProdutos = valorTotalProdutos;
    }

    public BigDecimal getValorTotalServicos() {
        return valorTotalServicos;
    }

    public void setValorTotalServicos(BigDecimal valorTotalServicos) {
        this.valorTotalServicos = valorTotalServicos;
    }

    public BigDecimal getBasecalculoISSQN() {
        return basecalculoISSQN;
    }

    public void setBasecalculoISSQN(BigDecimal basecalculoISSQN) {
        this.basecalculoISSQN = basecalculoISSQN;
    }

    public BigDecimal getValorISSQN() {
        return valorISSQN;
    }

    public void setValorISSQN(BigDecimal valorISSQN) {
        this.valorISSQN = valorISSQN;
    }

    public String getTipoFrete() {
        return tipoFrete;
    }

    public void setTipoFrete(String tipoFrete) {
        this.tipoFrete = tipoFrete;
    }

    public String getPlacaFrete() {
        return placaFrete;
    }

    public void setPlacaFrete(String placaFrete) {
        this.placaFrete = placaFrete;
    }

    public String getPlacaUFfrete() {
        return placaUFfrete;
    }

    public void setPlacaUFfrete(String placaUFfrete) {
        this.placaUFfrete = placaUFfrete;
    }

    public Short getQuantidadeFrete() {
        return quantidadeFrete;
    }

    public void setQuantidadeFrete(Short quantidadeFrete) {
        this.quantidadeFrete = quantidadeFrete;
    }

    public String getModalidadeFrete() {
        return modalidadeFrete;
    }

    public void setModalidadeFrete(String modalidadeFrete) {
        this.modalidadeFrete = modalidadeFrete;
    }

    public String getMarcaFrete() {
        return marcaFrete;
    }

    public void setMarcaFrete(String marcaFrete) {
        this.marcaFrete = marcaFrete;
    }

    public Integer getNumeroFrete() {
        return numeroFrete;
    }

    public void setNumeroFrete(Integer numeroFrete) {
        this.numeroFrete = numeroFrete;
    }

    public BigDecimal getPesoBruto() {
        return pesoBruto;
    }

    public void setPesoBruto(BigDecimal pesoBruto) {
        this.pesoBruto = pesoBruto;
    }

    public BigDecimal getPesoLiquido() {
        return pesoLiquido;
    }

    public void setPesoLiquido(BigDecimal pesoLiquido) {
        this.pesoLiquido = pesoLiquido;
    }

    public String getInformacoesComplementares() {
        return informacoesComplementares;
    }

    public void setInformacoesComplementares(String informacoesComplementares) {
        this.informacoesComplementares = informacoesComplementares;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Integer getCodigoNumerico() {
        return codigoNumerico;
    }

    public void setCodigoNumerico(Integer codigoNumerico) {
        this.codigoNumerico = codigoNumerico;
    }

    public String getCodMunicipioGerador() {
        return codMunicipioGerador;
    }

    public void setCodMunicipioGerador(String codMunicipioGerador) {
        this.codMunicipioGerador = codMunicipioGerador;
    }

    public String getImpressaoDanf() {
        return impressaoDanf;
    }

    public void setImpressaoDanf(String impressaoDanf) {
        this.impressaoDanf = impressaoDanf;
    }

    public String getTipoProcesso() {
        return tipoProcesso;
    }

    public void setTipoProcesso(String tipoProcesso) {
        this.tipoProcesso = tipoProcesso;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @XmlTransient
    public List<ContasPagar> getContasPagarList() {
        return contasPagarList;
    }

    public void setContasPagarList(List<ContasPagar> contasPagarList) {
        this.contasPagarList = contasPagarList;
    }

    public Transportadora getIdTransportadora() {
        return idTransportadora;
    }

    public void setIdTransportadora(Transportadora idTransportadora) {
        this.idTransportadora = idTransportadora;
    }

    public LocalTrabalho getIdLocalTrabalho() {
        return idLocalTrabalho;
    }

    public void setIdLocalTrabalho(LocalTrabalho idLocalTrabalho) {
        this.idLocalTrabalho = idLocalTrabalho;
    }

    public Cfop getIdCFOP() {
        return idCFOP;
    }

    public void setIdCFOP(Cfop idCFOP) {
        this.idCFOP = idCFOP;
    }

    public FormaPagamento getIdFormaPagamento() {
        return idFormaPagamento;
    }

    public void setIdFormaPagamento(FormaPagamento idFormaPagamento) {
        this.idFormaPagamento = idFormaPagamento;
    }

    public NotaObservacao getIdNotaObservacao() {
        return idNotaObservacao;
    }

    public void setIdNotaObservacao(NotaObservacao idNotaObservacao) {
        this.idNotaObservacao = idNotaObservacao;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Fornecedor getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(Fornecedor idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    public ModeloNfe getIdModelo() {
        return idModelo;
    }

    public void setIdModelo(ModeloNfe idModelo) {
        this.idModelo = idModelo;
    }

    public PlanoContas getIdPlanoContas() {
        return idPlanoContas;
    }

    public void setIdPlanoContas(PlanoContas idPlanoContas) {
        this.idPlanoContas = idPlanoContas;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public PedidoCompra getIdPedidoCompra() {
        return idPedidoCompra;
    }

    public void setIdPedidoCompra(PedidoCompra idPedidoCompra) {
        this.idPedidoCompra = idPedidoCompra;
    }

    @XmlTransient
    public List<NotaEntradaItem> getNotaEntradaItemList() {
        return notaEntradaItemList;
    }

    public void setNotaEntradaItemList(List<NotaEntradaItem> notaEntradaItemList) {
        this.notaEntradaItemList = notaEntradaItemList;
    }

    @XmlTransient
    public List<PedidoCompra> getPedidoCompraList() {
        return pedidoCompraList;
    }

    public void setPedidoCompraList(List<PedidoCompra> pedidoCompraList) {
        this.pedidoCompraList = pedidoCompraList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idNotaEntrada != null ? idNotaEntrada.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotaEntrada)) {
            return false;
        }
        NotaEntrada other = (NotaEntrada) object;
        if ((this.idNotaEntrada == null && other.idNotaEntrada != null) || (this.idNotaEntrada != null && !this.idNotaEntrada.equals(other.idNotaEntrada))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.NotaEntrada[ idNotaEntrada=" + idNotaEntrada + " ]";
    }
    
}
