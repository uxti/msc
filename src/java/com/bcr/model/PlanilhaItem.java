/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "planilha_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanilhaItem.findAll", query = "SELECT p FROM PlanilhaItem p"),
    @NamedQuery(name = "PlanilhaItem.findByIdPlanilhaItem", query = "SELECT p FROM PlanilhaItem p WHERE p.idPlanilhaItem = :idPlanilhaItem"),
    @NamedQuery(name = "PlanilhaItem.findByDescricao", query = "SELECT p FROM PlanilhaItem p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "PlanilhaItem.findByPosicao", query = "SELECT p FROM PlanilhaItem p WHERE p.posicao = :posicao"),
    @NamedQuery(name = "PlanilhaItem.findByPercentual", query = "SELECT p FROM PlanilhaItem p WHERE p.percentual = :percentual"),
    @NamedQuery(name = "PlanilhaItem.findByAcao", query = "SELECT p FROM PlanilhaItem p WHERE p.acao = :acao"),
    @NamedQuery(name = "PlanilhaItem.findByPosicaoInicial", query = "SELECT p FROM PlanilhaItem p WHERE p.posicaoInicial = :posicaoInicial"),
    @NamedQuery(name = "PlanilhaItem.findByPosicaoFinal", query = "SELECT p FROM PlanilhaItem p WHERE p.posicaoFinal = :posicaoFinal"),
    @NamedQuery(name = "PlanilhaItem.findByValorEditavel", query = "SELECT p FROM PlanilhaItem p WHERE p.valorEditavel = :valorEditavel"),
    @NamedQuery(name = "PlanilhaItem.findByPercentualEditavel", query = "SELECT p FROM PlanilhaItem p WHERE p.percentualEditavel = :percentualEditavel"),
    @NamedQuery(name = "PlanilhaItem.findByTipo", query = "SELECT p FROM PlanilhaItem p WHERE p.tipo = :tipo"),
    @NamedQuery(name = "PlanilhaItem.findByGrupo", query = "SELECT p FROM PlanilhaItem p WHERE p.grupo = :grupo"),
    @NamedQuery(name = "PlanilhaItem.findByValor", query = "SELECT p FROM PlanilhaItem p WHERE p.valor = :valor"),
    @NamedQuery(name = "PlanilhaItem.findByRegistroTotalizador", query = "SELECT p FROM PlanilhaItem p WHERE p.registroTotalizador = :registroTotalizador")})
public class PlanilhaItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_planilha_item")
    private Integer idPlanilhaItem;
    @Size(max = 200)
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "posicao")
    private Integer posicao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "percentual")
    private BigDecimal percentual;
    @Size(max = 100)
    @Column(name = "acao")
    private String acao;
    @Size(max = 100)
    @Column(name = "posicao_inicial")
    private String posicaoInicial;
    @Size(max = 100)
    @Column(name = "posicao_final")
    private String posicaoFinal;
    @Size(max = 1)
    @Column(name = "valor_editavel")
    private String valorEditavel;
    @Size(max = 1)
    @Column(name = "percentual_editavel")
    private String percentualEditavel;
    @Size(max = 100)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 100)
    @Column(name = "grupo")
    private String grupo;
    @Column(name = "valor")
    private BigDecimal valor;
    @Size(max = 1)
    @Column(name = "registro_totalizador")
    private String registroTotalizador;
    @JoinColumn(name = "id_grupo", referencedColumnName = "id_grupo")
    @ManyToOne
    private Grupo idGrupo;
    @JoinColumn(name = "id_ativo", referencedColumnName = "id_ativo")
    @ManyToOne
    private Ativo idAtivo;
    @JoinColumn(name = "id_planilha", referencedColumnName = "id_planilha")
    @ManyToOne
    private Planilha idPlanilha;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne
    private Produto idProduto;
    @OneToMany(mappedBy = "idPlanilhaItem")
    private List<PropostaItem> propostaItemList;
    @OneToMany(mappedBy = "idPlanilhaItem")
    private List<PropostaItemProduto> propostaItemProdutoList;

    public PlanilhaItem() {
    }

    public PlanilhaItem(Integer idPlanilhaItem) {
        this.idPlanilhaItem = idPlanilhaItem;
    }

    public Integer getIdPlanilhaItem() {
        return idPlanilhaItem;
    }

    public void setIdPlanilhaItem(Integer idPlanilhaItem) {
        this.idPlanilhaItem = idPlanilhaItem;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }

    public BigDecimal getPercentual() {
        return percentual;
    }

    public void setPercentual(BigDecimal percentual) {
        this.percentual = percentual;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public String getPosicaoInicial() {
        return posicaoInicial;
    }

    public void setPosicaoInicial(String posicaoInicial) {
        this.posicaoInicial = posicaoInicial;
    }

    public String getPosicaoFinal() {
        return posicaoFinal;
    }

    public void setPosicaoFinal(String posicaoFinal) {
        this.posicaoFinal = posicaoFinal;
    }

    public String getValorEditavel() {
        return valorEditavel;
    }

    public void setValorEditavel(String valorEditavel) {
        this.valorEditavel = valorEditavel;
    }

    public String getPercentualEditavel() {
        return percentualEditavel;
    }

    public void setPercentualEditavel(String percentualEditavel) {
        this.percentualEditavel = percentualEditavel;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getRegistroTotalizador() {
        return registroTotalizador;
    }

    public void setRegistroTotalizador(String registroTotalizador) {
        this.registroTotalizador = registroTotalizador;
    }

    public Grupo getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Grupo idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Ativo getIdAtivo() {
        return idAtivo;
    }

    public void setIdAtivo(Ativo idAtivo) {
        this.idAtivo = idAtivo;
    }

    public Planilha getIdPlanilha() {
        return idPlanilha;
    }

    public void setIdPlanilha(Planilha idPlanilha) {
        this.idPlanilha = idPlanilha;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    @XmlTransient
    public List<PropostaItem> getPropostaItemList() {
        return propostaItemList;
    }

    public void setPropostaItemList(List<PropostaItem> propostaItemList) {
        this.propostaItemList = propostaItemList;
    }

    @XmlTransient
    public List<PropostaItemProduto> getPropostaItemProdutoList() {
        return propostaItemProdutoList;
    }

    public void setPropostaItemProdutoList(List<PropostaItemProduto> propostaItemProdutoList) {
        this.propostaItemProdutoList = propostaItemProdutoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlanilhaItem != null ? idPlanilhaItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanilhaItem)) {
            return false;
        }
        PlanilhaItem other = (PlanilhaItem) object;
        if ((this.idPlanilhaItem == null && other.idPlanilhaItem != null) || (this.idPlanilhaItem != null && !this.idPlanilhaItem.equals(other.idPlanilhaItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PlanilhaItem[ idPlanilhaItem=" + idPlanilhaItem + " ]";
    }
    
}
