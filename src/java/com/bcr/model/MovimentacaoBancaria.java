/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "movimentacao_bancaria")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MovimentacaoBancaria.findAll", query = "SELECT m FROM MovimentacaoBancaria m"),
    @NamedQuery(name = "MovimentacaoBancaria.findByIdmovimentacaobancariaItem", query = "SELECT m FROM MovimentacaoBancaria m WHERE m.idmovimentacaobancariaItem = :idmovimentacaobancariaItem"),
    @NamedQuery(name = "MovimentacaoBancaria.findByDtMovimentacaoBancariaItem", query = "SELECT m FROM MovimentacaoBancaria m WHERE m.dtMovimentacaoBancariaItem = :dtMovimentacaoBancariaItem"),
    @NamedQuery(name = "MovimentacaoBancaria.findByHistorico", query = "SELECT m FROM MovimentacaoBancaria m WHERE m.historico = :historico"),
    @NamedQuery(name = "MovimentacaoBancaria.findByTipo", query = "SELECT m FROM MovimentacaoBancaria m WHERE m.tipo = :tipo"),
    @NamedQuery(name = "MovimentacaoBancaria.findByTipoMovimento", query = "SELECT m FROM MovimentacaoBancaria m WHERE m.tipoMovimento = :tipoMovimento"),
    @NamedQuery(name = "MovimentacaoBancaria.findByValor", query = "SELECT m FROM MovimentacaoBancaria m WHERE m.valor = :valor"),
    @NamedQuery(name = "MovimentacaoBancaria.findByObservacao", query = "SELECT m FROM MovimentacaoBancaria m WHERE m.observacao = :observacao")})
public class MovimentacaoBancaria implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_movimentacao_bancaria_Item")
    private Integer idmovimentacaobancariaItem;
    @Column(name = "dt_movimentacao_bancaria_item")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtMovimentacaoBancariaItem;
    @Size(max = 255)
    @Column(name = "historico")
    private String historico;
    @Size(max = 100)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 100)
    @Column(name = "tipo_movimento")
    private String tipoMovimento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private BigDecimal valor;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @JoinColumn(name = "id_contas_receber", referencedColumnName = "id_contas_receber")
    @ManyToOne
    private ContasReceber idContasReceber;
    @JoinColumn(name = "id_conta_corrente", referencedColumnName = "id_conta_corrente")
    @ManyToOne
    private ContaCorrente idContaCorrente;
    @JoinColumn(name = "id_cheque", referencedColumnName = "id_cheque")
    @ManyToOne
    private Cheque idCheque;
    @JoinColumn(name = "id_plano_contas", referencedColumnName = "id_plano_contas")
    @ManyToOne
    private PlanoContas idPlanoContas;
    @JoinColumn(name = "id_banco", referencedColumnName = "id_banco")
    @ManyToOne
    private Banco idBanco;
    @JoinColumn(name = "id_contas_pagar", referencedColumnName = "id_contas_pagar")
    @ManyToOne
    private ContasPagar idContasPagar;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;

    public MovimentacaoBancaria() {
    }

    public MovimentacaoBancaria(Integer idmovimentacaobancariaItem) {
        this.idmovimentacaobancariaItem = idmovimentacaobancariaItem;
    }

    public Integer getIdmovimentacaobancariaItem() {
        return idmovimentacaobancariaItem;
    }

    public void setIdmovimentacaobancariaItem(Integer idmovimentacaobancariaItem) {
        this.idmovimentacaobancariaItem = idmovimentacaobancariaItem;
    }

    public Date getDtMovimentacaoBancariaItem() {
        return dtMovimentacaoBancariaItem;
    }

    public void setDtMovimentacaoBancariaItem(Date dtMovimentacaoBancariaItem) {
        this.dtMovimentacaoBancariaItem = dtMovimentacaoBancariaItem;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipoMovimento() {
        return tipoMovimento;
    }

    public void setTipoMovimento(String tipoMovimento) {
        this.tipoMovimento = tipoMovimento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public ContasReceber getIdContasReceber() {
        return idContasReceber;
    }

    public void setIdContasReceber(ContasReceber idContasReceber) {
        this.idContasReceber = idContasReceber;
    }

    public ContaCorrente getIdContaCorrente() {
        return idContaCorrente;
    }

    public void setIdContaCorrente(ContaCorrente idContaCorrente) {
        this.idContaCorrente = idContaCorrente;
    }

    public Cheque getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Cheque idCheque) {
        this.idCheque = idCheque;
    }

    public PlanoContas getIdPlanoContas() {
        return idPlanoContas;
    }

    public void setIdPlanoContas(PlanoContas idPlanoContas) {
        this.idPlanoContas = idPlanoContas;
    }

    public Banco getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Banco idBanco) {
        this.idBanco = idBanco;
    }

    public ContasPagar getIdContasPagar() {
        return idContasPagar;
    }

    public void setIdContasPagar(ContasPagar idContasPagar) {
        this.idContasPagar = idContasPagar;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmovimentacaobancariaItem != null ? idmovimentacaobancariaItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MovimentacaoBancaria)) {
            return false;
        }
        MovimentacaoBancaria other = (MovimentacaoBancaria) object;
        if ((this.idmovimentacaobancariaItem == null && other.idmovimentacaobancariaItem != null) || (this.idmovimentacaobancariaItem != null && !this.idmovimentacaobancariaItem.equals(other.idmovimentacaobancariaItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.MovimentacaoBancaria[ idmovimentacaobancariaItem=" + idmovimentacaobancariaItem + " ]";
    }
    
}
