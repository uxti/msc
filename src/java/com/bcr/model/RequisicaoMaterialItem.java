/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "requisicao_material_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RequisicaoMaterialItem.findAll", query = "SELECT r FROM RequisicaoMaterialItem r"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByIdRequisicaoMaterialItem", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.idRequisicaoMaterialItem = :idRequisicaoMaterialItem"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByDescricao", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.descricao = :descricao"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByQuantidade", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.quantidade = :quantidade"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByQuantidadeAprovada", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.quantidadeAprovada = :quantidadeAprovada"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByVlrUnitario", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.vlrUnitario = :vlrUnitario"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByStatus", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.status = :status"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByQuantidadeCotada", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.quantidadeCotada = :quantidadeCotada"),
    @NamedQuery(name = "RequisicaoMaterialItem.findByIdCotacaoItem", query = "SELECT r FROM RequisicaoMaterialItem r WHERE r.idCotacaoItem = :idCotacaoItem")})
public class RequisicaoMaterialItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_requisicao_material_item")
    private Integer idRequisicaoMaterialItem;
    @Column(name = "descricao")
    private String descricao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "quantidade")
    private BigDecimal quantidade;
    @Column(name = "quantidade_aprovada")
    private BigDecimal quantidadeAprovada;
    @Column(name = "vlr_unitario")
    private BigDecimal vlrUnitario;
    @Column(name = "Status")
    private String status;
    @Column(name = "quantidade_cotada")
    private BigDecimal quantidadeCotada;
    @Column(name = "id_cotacao_item")
    private Integer idCotacaoItem;
    @JoinColumn(name = "id_ativo", referencedColumnName = "id_ativo")
    @ManyToOne
    private Ativo idAtivo;
    @JoinColumn(name = "id_requisicao_material", referencedColumnName = "id_requisicao_material")
    @ManyToOne(optional = false)
    private RequisicaoMaterial idRequisicaoMaterial;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne(optional = false)
    private Produto idProduto;

    public RequisicaoMaterialItem() {
    }

    public RequisicaoMaterialItem(Integer idRequisicaoMaterialItem) {
        this.idRequisicaoMaterialItem = idRequisicaoMaterialItem;
    }

    public Integer getIdRequisicaoMaterialItem() {
        return idRequisicaoMaterialItem;
    }

    public void setIdRequisicaoMaterialItem(Integer idRequisicaoMaterialItem) {
        this.idRequisicaoMaterialItem = idRequisicaoMaterialItem;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getQuantidadeAprovada() {
        return quantidadeAprovada;
    }

    public void setQuantidadeAprovada(BigDecimal quantidadeAprovada) {
        this.quantidadeAprovada = quantidadeAprovada;
    }

    public BigDecimal getVlrUnitario() {
        return vlrUnitario;
    }

    public void setVlrUnitario(BigDecimal vlrUnitario) {
        this.vlrUnitario = vlrUnitario;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getQuantidadeCotada() {
        return quantidadeCotada;
    }

    public void setQuantidadeCotada(BigDecimal quantidadeCotada) {
        this.quantidadeCotada = quantidadeCotada;
    }

    public Integer getIdCotacaoItem() {
        return idCotacaoItem;
    }

    public void setIdCotacaoItem(Integer idCotacaoItem) {
        this.idCotacaoItem = idCotacaoItem;
    }

    public Ativo getIdAtivo() {
        return idAtivo;
    }

    public void setIdAtivo(Ativo idAtivo) {
        this.idAtivo = idAtivo;
    }

    public RequisicaoMaterial getIdRequisicaoMaterial() {
        return idRequisicaoMaterial;
    }

    public void setIdRequisicaoMaterial(RequisicaoMaterial idRequisicaoMaterial) {
        this.idRequisicaoMaterial = idRequisicaoMaterial;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRequisicaoMaterialItem != null ? idRequisicaoMaterialItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequisicaoMaterialItem)) {
            return false;
        }
        RequisicaoMaterialItem other = (RequisicaoMaterialItem) object;
        if ((this.idRequisicaoMaterialItem == null && other.idRequisicaoMaterialItem != null) || (this.idRequisicaoMaterialItem != null && !this.idRequisicaoMaterialItem.equals(other.idRequisicaoMaterialItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.RequisicaoMaterialItem[ idRequisicaoMaterialItem=" + idRequisicaoMaterialItem + " ]";
    }
    
}
