/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "contas_receber")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContasReceber.findAll", query = "SELECT c FROM ContasReceber c"),
    @NamedQuery(name = "ContasReceber.findByIdContasReceber", query = "SELECT c FROM ContasReceber c WHERE c.idContasReceber = :idContasReceber"),
    @NamedQuery(name = "ContasReceber.findByDtLancamento", query = "SELECT c FROM ContasReceber c WHERE c.dtLancamento = :dtLancamento"),
    @NamedQuery(name = "ContasReceber.findByDtPagamento", query = "SELECT c FROM ContasReceber c WHERE c.dtPagamento = :dtPagamento"),
    @NamedQuery(name = "ContasReceber.findByDtVencimento", query = "SELECT c FROM ContasReceber c WHERE c.dtVencimento = :dtVencimento"),
    @NamedQuery(name = "ContasReceber.findByDtEstorno", query = "SELECT c FROM ContasReceber c WHERE c.dtEstorno = :dtEstorno"),
    @NamedQuery(name = "ContasReceber.findByNumParcela", query = "SELECT c FROM ContasReceber c WHERE c.numParcela = :numParcela"),
    @NamedQuery(name = "ContasReceber.findByNumDocto", query = "SELECT c FROM ContasReceber c WHERE c.numDocto = :numDocto"),
    @NamedQuery(name = "ContasReceber.findByStatus", query = "SELECT c FROM ContasReceber c WHERE c.status = :status"),
    @NamedQuery(name = "ContasReceber.findByValorDuplicata", query = "SELECT c FROM ContasReceber c WHERE c.valorDuplicata = :valorDuplicata"),
    @NamedQuery(name = "ContasReceber.findByValorRecebido", query = "SELECT c FROM ContasReceber c WHERE c.valorRecebido = :valorRecebido"),
    @NamedQuery(name = "ContasReceber.findByObservacao", query = "SELECT c FROM ContasReceber c WHERE c.observacao = :observacao"),
    @NamedQuery(name = "ContasReceber.findByNumDuplicata", query = "SELECT c FROM ContasReceber c WHERE c.numDuplicata = :numDuplicata"),
    @NamedQuery(name = "ContasReceber.findByDtDuplicata", query = "SELECT c FROM ContasReceber c WHERE c.dtDuplicata = :dtDuplicata"),
    @NamedQuery(name = "ContasReceber.findByIdCentroCustoVariavel", query = "SELECT c FROM ContasReceber c WHERE c.idCentroCustoVariavel = :idCentroCustoVariavel"),
    @NamedQuery(name = "ContasReceber.findByRecebidoCom", query = "SELECT c FROM ContasReceber c WHERE c.recebidoCom = :recebidoCom"),
    @NamedQuery(name = "ContasReceber.findByTipodocumento", query = "SELECT c FROM ContasReceber c WHERE c.tipodocumento = :tipodocumento"),
    @NamedQuery(name = "ContasReceber.findByNumLote", query = "SELECT c FROM ContasReceber c WHERE c.numLote = :numLote"),
    @NamedQuery(name = "ContasReceber.findByParcelaSistema", query = "SELECT c FROM ContasReceber c WHERE c.parcelaSistema = :parcelaSistema"),
    @NamedQuery(name = "ContasReceber.findByTotalParcelas", query = "SELECT c FROM ContasReceber c WHERE c.totalParcelas = :totalParcelas")})
public class ContasReceber implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_contas_receber")
    private Integer idContasReceber;
    @Column(name = "dt_lancamento")
    @Temporal(TemporalType.DATE)
    private Date dtLancamento;
    @Column(name = "dt_pagamento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtPagamento;
    @Column(name = "dt_vencimento")
    @Temporal(TemporalType.DATE)
    private Date dtVencimento;
    @Column(name = "dt_estorno")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtEstorno;
    @Column(name = "num_parcela")
    private Integer numParcela;
    @Size(max = 60)
    @Column(name = "num_docto")
    private String numDocto;
    @Column(name = "status")
    private Boolean status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_duplicata")
    private BigDecimal valorDuplicata;
    @Column(name = "valor_recebido")
    private BigDecimal valorRecebido;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @Size(max = 60)
    @Column(name = "num_duplicata")
    private String numDuplicata;
    @Column(name = "dt_duplicata")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dtDuplicata;
    @Column(name = "id_centro_custo_variavel")
    private Integer idCentroCustoVariavel;
    @Size(max = 10)
    @Column(name = "recebido_com")
    private String recebidoCom;
    @Size(max = 100)
    @Column(name = "Tipo_documento")
    private String tipodocumento;
    @Size(max = 50)
    @Column(name = "num_lote")
    private String numLote;
    @Size(max = 50)
    @Column(name = "parcela_sistema")
    private String parcelaSistema;
    @Column(name = "total_parcelas")
    private Integer totalParcelas;
    @OneToMany(mappedBy = "idContasReceber")
    private List<MovimentacaoBancaria> movimentacaoBancariaList;
    @OneToMany(mappedBy = "idContasReceber")
    private List<Cheque> chequeList;
    @OneToMany(mappedBy = "idContasReceber")
    private List<CaixaItem> caixaItemList;
    @OneToMany(mappedBy = "idContasReceber")
    private List<Centrocustoplanocontas> centrocustoplanocontasList;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;
    @JoinColumn(name = "id_forma_pagamento", referencedColumnName = "id_forma_pagamento")
    @ManyToOne
    private FormaPagamento idFormaPagamento;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_nota_saida", referencedColumnName = "id_nota_saida")
    @ManyToOne
    private NotaSaida idNotaSaida;
    @JoinColumn(name = "id_plano_contas", referencedColumnName = "id_plano_contas")
    @ManyToOne
    private PlanoContas idPlanoContas;
    @JoinColumn(name = "id_banco", referencedColumnName = "id_banco")
    @ManyToOne
    private Banco idBanco;
    @JoinColumn(name = "id_cheque", referencedColumnName = "id_cheque")
    @ManyToOne
    private Cheque idCheque;
    @JoinColumn(name = "id_conta_corrente", referencedColumnName = "id_conta_corrente")
    @ManyToOne
    private ContaCorrente idContaCorrente;

    public ContasReceber() {
    }

    public ContasReceber(Integer idContasReceber) {
        this.idContasReceber = idContasReceber;
    }

    public Integer getIdContasReceber() {
        return idContasReceber;
    }

    public void setIdContasReceber(Integer idContasReceber) {
        this.idContasReceber = idContasReceber;
    }

    public Date getDtLancamento() {
        return dtLancamento;
    }

    public void setDtLancamento(Date dtLancamento) {
        this.dtLancamento = dtLancamento;
    }

    public Date getDtPagamento() {
        return dtPagamento;
    }

    public void setDtPagamento(Date dtPagamento) {
        this.dtPagamento = dtPagamento;
    }

    public Date getDtVencimento() {
        return dtVencimento;
    }

    public void setDtVencimento(Date dtVencimento) {
        this.dtVencimento = dtVencimento;
    }

    public Date getDtEstorno() {
        return dtEstorno;
    }

    public void setDtEstorno(Date dtEstorno) {
        this.dtEstorno = dtEstorno;
    }

    public Integer getNumParcela() {
        return numParcela;
    }

    public void setNumParcela(Integer numParcela) {
        this.numParcela = numParcela;
    }

    public String getNumDocto() {
        return numDocto;
    }

    public void setNumDocto(String numDocto) {
        this.numDocto = numDocto;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public BigDecimal getValorDuplicata() {
        return valorDuplicata;
    }

    public void setValorDuplicata(BigDecimal valorDuplicata) {
        this.valorDuplicata = valorDuplicata;
    }

    public BigDecimal getValorRecebido() {
        return valorRecebido;
    }

    public void setValorRecebido(BigDecimal valorRecebido) {
        this.valorRecebido = valorRecebido;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getNumDuplicata() {
        return numDuplicata;
    }

    public void setNumDuplicata(String numDuplicata) {
        this.numDuplicata = numDuplicata;
    }

    public Date getDtDuplicata() {
        return dtDuplicata;
    }

    public void setDtDuplicata(Date dtDuplicata) {
        this.dtDuplicata = dtDuplicata;
    }

    public Integer getIdCentroCustoVariavel() {
        return idCentroCustoVariavel;
    }

    public void setIdCentroCustoVariavel(Integer idCentroCustoVariavel) {
        this.idCentroCustoVariavel = idCentroCustoVariavel;
    }

    public String getRecebidoCom() {
        return recebidoCom;
    }

    public void setRecebidoCom(String recebidoCom) {
        this.recebidoCom = recebidoCom;
    }

    public String getTipodocumento() {
        return tipodocumento;
    }

    public void setTipodocumento(String tipodocumento) {
        this.tipodocumento = tipodocumento;
    }

    public String getNumLote() {
        return numLote;
    }

    public void setNumLote(String numLote) {
        this.numLote = numLote;
    }

    public String getParcelaSistema() {
        return parcelaSistema;
    }

    public void setParcelaSistema(String parcelaSistema) {
        this.parcelaSistema = parcelaSistema;
    }

    public Integer getTotalParcelas() {
        return totalParcelas;
    }

    public void setTotalParcelas(Integer totalParcelas) {
        this.totalParcelas = totalParcelas;
    }

    @XmlTransient
    public List<MovimentacaoBancaria> getMovimentacaoBancariaList() {
        return movimentacaoBancariaList;
    }

    public void setMovimentacaoBancariaList(List<MovimentacaoBancaria> movimentacaoBancariaList) {
        this.movimentacaoBancariaList = movimentacaoBancariaList;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    @XmlTransient
    public List<CaixaItem> getCaixaItemList() {
        return caixaItemList;
    }

    public void setCaixaItemList(List<CaixaItem> caixaItemList) {
        this.caixaItemList = caixaItemList;
    }

    @XmlTransient
    public List<Centrocustoplanocontas> getCentrocustoplanocontasList() {
        return centrocustoplanocontasList;
    }

    public void setCentrocustoplanocontasList(List<Centrocustoplanocontas> centrocustoplanocontasList) {
        this.centrocustoplanocontasList = centrocustoplanocontasList;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    public FormaPagamento getIdFormaPagamento() {
        return idFormaPagamento;
    }

    public void setIdFormaPagamento(FormaPagamento idFormaPagamento) {
        this.idFormaPagamento = idFormaPagamento;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public NotaSaida getIdNotaSaida() {
        return idNotaSaida;
    }

    public void setIdNotaSaida(NotaSaida idNotaSaida) {
        this.idNotaSaida = idNotaSaida;
    }

    public PlanoContas getIdPlanoContas() {
        return idPlanoContas;
    }

    public void setIdPlanoContas(PlanoContas idPlanoContas) {
        this.idPlanoContas = idPlanoContas;
    }

    public Banco getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Banco idBanco) {
        this.idBanco = idBanco;
    }

    public Cheque getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Cheque idCheque) {
        this.idCheque = idCheque;
    }

    public ContaCorrente getIdContaCorrente() {
        return idContaCorrente;
    }

    public void setIdContaCorrente(ContaCorrente idContaCorrente) {
        this.idContaCorrente = idContaCorrente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContasReceber != null ? idContasReceber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContasReceber)) {
            return false;
        }
        ContasReceber other = (ContasReceber) object;
        if ((this.idContasReceber == null && other.idContasReceber != null) || (this.idContasReceber != null && !this.idContasReceber.equals(other.idContasReceber))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ContasReceber[ idContasReceber=" + idContasReceber + " ]";
    }
    
}
