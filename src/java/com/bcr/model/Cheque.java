/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "cheque")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cheque.findAll", query = "SELECT c FROM Cheque c"),
    @NamedQuery(name = "Cheque.findByIdCheque", query = "SELECT c FROM Cheque c WHERE c.idCheque = :idCheque"),
    @NamedQuery(name = "Cheque.findByTipo", query = "SELECT c FROM Cheque c WHERE c.tipo = :tipo"),
    @NamedQuery(name = "Cheque.findByConta", query = "SELECT c FROM Cheque c WHERE c.conta = :conta"),
    @NamedQuery(name = "Cheque.findByNumeroCheque", query = "SELECT c FROM Cheque c WHERE c.numeroCheque = :numeroCheque"),
    @NamedQuery(name = "Cheque.findByValor", query = "SELECT c FROM Cheque c WHERE c.valor = :valor"),
    @NamedQuery(name = "Cheque.findByDtRecebimentoEmissao", query = "SELECT c FROM Cheque c WHERE c.dtRecebimentoEmissao = :dtRecebimentoEmissao"),
    @NamedQuery(name = "Cheque.findByDtCompensacaoVencimento", query = "SELECT c FROM Cheque c WHERE c.dtCompensacaoVencimento = :dtCompensacaoVencimento"),
    @NamedQuery(name = "Cheque.findByObservacao", query = "SELECT c FROM Cheque c WHERE c.observacao = :observacao"),
    @NamedQuery(name = "Cheque.findByCompensado", query = "SELECT c FROM Cheque c WHERE c.compensado = :compensado"),
    @NamedQuery(name = "Cheque.findByNominal", query = "SELECT c FROM Cheque c WHERE c.nominal = :nominal")})
public class Cheque implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_cheque")
    private Integer idCheque;
    @Size(max = 100)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 99)
    @Column(name = "conta")
    private String conta;
    @Column(name = "numero_cheque")
    private Integer numeroCheque;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private BigDecimal valor;
    @Column(name = "dt_recebimento_emissao")
    @Temporal(TemporalType.DATE)
    private Date dtRecebimentoEmissao;
    @Column(name = "dt_compensacao_vencimento")
    @Temporal(TemporalType.DATE)
    private Date dtCompensacaoVencimento;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @Size(max = 1)
    @Column(name = "compensado")
    private String compensado;
    @Size(max = 255)
    @Column(name = "nominal")
    private String nominal;
    @OneToMany(mappedBy = "idCheque")
    private List<ContasPagar> contasPagarList;
    @OneToMany(mappedBy = "idCheque")
    private List<MovimentacaoBancaria> movimentacaoBancariaList;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_contas_pagar", referencedColumnName = "id_contas_pagar")
    @ManyToOne
    private ContasPagar idContasPagar;
    @JoinColumn(name = "id_conta_corrente", referencedColumnName = "id_conta_corrente")
    @ManyToOne
    private ContaCorrente idContaCorrente;
    @JoinColumn(name = "id_contas_receber", referencedColumnName = "id_contas_receber")
    @ManyToOne
    private ContasReceber idContasReceber;
    @JoinColumn(name = "id_colaborador", referencedColumnName = "id_colaborador")
    @ManyToOne
    private Colaborador idColaborador;
    @JoinColumn(name = "id_fornecedor", referencedColumnName = "id_fornecedor")
    @ManyToOne
    private Fornecedor idFornecedor;
    @OneToMany(mappedBy = "idCheque")
    private List<CaixaItem> caixaItemList;
    @OneToMany(mappedBy = "idCheque")
    private List<ContasReceber> contasReceberList;

    public Cheque() {
    }

    public Cheque(Integer idCheque) {
        this.idCheque = idCheque;
    }

    public Integer getIdCheque() {
        return idCheque;
    }

    public void setIdCheque(Integer idCheque) {
        this.idCheque = idCheque;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public Integer getNumeroCheque() {
        return numeroCheque;
    }

    public void setNumeroCheque(Integer numeroCheque) {
        this.numeroCheque = numeroCheque;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Date getDtRecebimentoEmissao() {
        return dtRecebimentoEmissao;
    }

    public void setDtRecebimentoEmissao(Date dtRecebimentoEmissao) {
        this.dtRecebimentoEmissao = dtRecebimentoEmissao;
    }

    public Date getDtCompensacaoVencimento() {
        return dtCompensacaoVencimento;
    }

    public void setDtCompensacaoVencimento(Date dtCompensacaoVencimento) {
        this.dtCompensacaoVencimento = dtCompensacaoVencimento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getCompensado() {
        return compensado;
    }

    public void setCompensado(String compensado) {
        this.compensado = compensado;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    @XmlTransient
    public List<ContasPagar> getContasPagarList() {
        return contasPagarList;
    }

    public void setContasPagarList(List<ContasPagar> contasPagarList) {
        this.contasPagarList = contasPagarList;
    }

    @XmlTransient
    public List<MovimentacaoBancaria> getMovimentacaoBancariaList() {
        return movimentacaoBancariaList;
    }

    public void setMovimentacaoBancariaList(List<MovimentacaoBancaria> movimentacaoBancariaList) {
        this.movimentacaoBancariaList = movimentacaoBancariaList;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public ContasPagar getIdContasPagar() {
        return idContasPagar;
    }

    public void setIdContasPagar(ContasPagar idContasPagar) {
        this.idContasPagar = idContasPagar;
    }

    public ContaCorrente getIdContaCorrente() {
        return idContaCorrente;
    }

    public void setIdContaCorrente(ContaCorrente idContaCorrente) {
        this.idContaCorrente = idContaCorrente;
    }

    public ContasReceber getIdContasReceber() {
        return idContasReceber;
    }

    public void setIdContasReceber(ContasReceber idContasReceber) {
        this.idContasReceber = idContasReceber;
    }

    public Colaborador getIdColaborador() {
        return idColaborador;
    }

    public void setIdColaborador(Colaborador idColaborador) {
        this.idColaborador = idColaborador;
    }

    public Fornecedor getIdFornecedor() {
        return idFornecedor;
    }

    public void setIdFornecedor(Fornecedor idFornecedor) {
        this.idFornecedor = idFornecedor;
    }

    @XmlTransient
    public List<CaixaItem> getCaixaItemList() {
        return caixaItemList;
    }

    public void setCaixaItemList(List<CaixaItem> caixaItemList) {
        this.caixaItemList = caixaItemList;
    }

    @XmlTransient
    public List<ContasReceber> getContasReceberList() {
        return contasReceberList;
    }

    public void setContasReceberList(List<ContasReceber> contasReceberList) {
        this.contasReceberList = contasReceberList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCheque != null ? idCheque.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cheque)) {
            return false;
        }
        Cheque other = (Cheque) object;
        if ((this.idCheque == null && other.idCheque != null) || (this.idCheque != null && !this.idCheque.equals(other.idCheque))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Cheque[ idCheque=" + idCheque + " ]";
    }
    
}
