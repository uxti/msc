/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "proposta_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PropostaItem.findAll", query = "SELECT p FROM PropostaItem p"),
    @NamedQuery(name = "PropostaItem.findByIdPropostaItem", query = "SELECT p FROM PropostaItem p WHERE p.idPropostaItem = :idPropostaItem"),
    @NamedQuery(name = "PropostaItem.findByDescricao", query = "SELECT p FROM PropostaItem p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "PropostaItem.findByPercentual", query = "SELECT p FROM PropostaItem p WHERE p.percentual = :percentual"),
    @NamedQuery(name = "PropostaItem.findByAcao", query = "SELECT p FROM PropostaItem p WHERE p.acao = :acao"),
    @NamedQuery(name = "PropostaItem.findByPosicao", query = "SELECT p FROM PropostaItem p WHERE p.posicao = :posicao"),
    @NamedQuery(name = "PropostaItem.findByPosicaoInicial", query = "SELECT p FROM PropostaItem p WHERE p.posicaoInicial = :posicaoInicial"),
    @NamedQuery(name = "PropostaItem.findByPosicaoFinal", query = "SELECT p FROM PropostaItem p WHERE p.posicaoFinal = :posicaoFinal"),
    @NamedQuery(name = "PropostaItem.findByValorEditavel", query = "SELECT p FROM PropostaItem p WHERE p.valorEditavel = :valorEditavel"),
    @NamedQuery(name = "PropostaItem.findByPercentualEditavel", query = "SELECT p FROM PropostaItem p WHERE p.percentualEditavel = :percentualEditavel"),
    @NamedQuery(name = "PropostaItem.findByValor", query = "SELECT p FROM PropostaItem p WHERE p.valor = :valor"),
    @NamedQuery(name = "PropostaItem.findByTipo", query = "SELECT p FROM PropostaItem p WHERE p.tipo = :tipo"),
    @NamedQuery(name = "PropostaItem.findByGrupo", query = "SELECT p FROM PropostaItem p WHERE p.grupo = :grupo"),
    @NamedQuery(name = "PropostaItem.findBySubTotal", query = "SELECT p FROM PropostaItem p WHERE p.subTotal = :subTotal"),
    @NamedQuery(name = "PropostaItem.findByTotal", query = "SELECT p FROM PropostaItem p WHERE p.total = :total"),
    @NamedQuery(name = "PropostaItem.findByPercentualSubgrupo", query = "SELECT p FROM PropostaItem p WHERE p.percentualSubgrupo = :percentualSubgrupo"),
    @NamedQuery(name = "PropostaItem.findByQuantidade", query = "SELECT p FROM PropostaItem p WHERE p.quantidade = :quantidade"),
    @NamedQuery(name = "PropostaItem.findByRegistroTotalizador", query = "SELECT p FROM PropostaItem p WHERE p.registroTotalizador = :registroTotalizador")})
public class PropostaItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proposta_item")
    private Integer idPropostaItem;
   
    @Column(name = "descricao")
    private String descricao;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "percentual")
    private BigDecimal percentual;
   
    @Column(name = "acao")
    private String acao;
    @Column(name = "posicao")
    private Integer posicao;
    @Size(max = 100)
    @Column(name = "posicao_inicial")
    private String posicaoInicial;
    @Size(max = 100)
    @Column(name = "posicao_final")
    private String posicaoFinal;
    @Size(max = 1)
    @Column(name = "valor_editavel")
    private String valorEditavel;
    @Size(max = 1)
    @Column(name = "percentual_editavel")
    private String percentualEditavel;
    @Column(name = "valor")
    private BigDecimal valor;
    @Size(max = 100)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 100)
    @Column(name = "grupo")
    private String grupo;
    @Column(name = "sub_total")
    private BigDecimal subTotal;
    @Column(name = "total")
    private BigDecimal total;
    @Column(name = "percentual_subgrupo")
    private BigDecimal percentualSubgrupo;
    @Column(name = "quantidade")
    private Integer quantidade;
    @Size(max = 1)
    @Column(name = "registro_totalizador")
    private String registroTotalizador;
    @JoinColumn(name = "id_planilha", referencedColumnName = "id_planilha")
    @ManyToOne
    private Planilha idPlanilha;
    @JoinColumn(name = "id_grupo", referencedColumnName = "id_grupo")
    @ManyToOne
    private Grupo idGrupo;
    @JoinColumn(name = "id_proposta", referencedColumnName = "id_proposta")
    @ManyToOne
    private Proposta idProposta;
    @JoinColumn(name = "id_planilha_item", referencedColumnName = "id_planilha_item")
    @ManyToOne
    private PlanilhaItem idPlanilhaItem;
    @OneToMany(mappedBy = "idPropostaItem")
    private List<PropostaItemProduto> propostaItemProdutoList;

    public PropostaItem() {
    }

    public PropostaItem(Integer idPropostaItem) {
        this.idPropostaItem = idPropostaItem;
    }

    public Integer getIdPropostaItem() {
        return idPropostaItem;
    }

    public void setIdPropostaItem(Integer idPropostaItem) {
        this.idPropostaItem = idPropostaItem;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getPercentual() {
        return percentual;
    }

    public void setPercentual(BigDecimal percentual) {
        this.percentual = percentual;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }

    public Integer getPosicao() {
        return posicao;
    }

    public void setPosicao(Integer posicao) {
        this.posicao = posicao;
    }

    public String getPosicaoInicial() {
        return posicaoInicial;
    }

    public void setPosicaoInicial(String posicaoInicial) {
        this.posicaoInicial = posicaoInicial;
    }

    public String getPosicaoFinal() {
        return posicaoFinal;
    }

    public void setPosicaoFinal(String posicaoFinal) {
        this.posicaoFinal = posicaoFinal;
    }

    public String getValorEditavel() {
        return valorEditavel;
    }

    public void setValorEditavel(String valorEditavel) {
        this.valorEditavel = valorEditavel;
    }

    public String getPercentualEditavel() {
        return percentualEditavel;
    }

    public void setPercentualEditavel(String percentualEditavel) {
        this.percentualEditavel = percentualEditavel;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public BigDecimal getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(BigDecimal subTotal) {
        this.subTotal = subTotal;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getPercentualSubgrupo() {
        return percentualSubgrupo;
    }

    public void setPercentualSubgrupo(BigDecimal percentualSubgrupo) {
        this.percentualSubgrupo = percentualSubgrupo;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public String getRegistroTotalizador() {
        return registroTotalizador;
    }

    public void setRegistroTotalizador(String registroTotalizador) {
        this.registroTotalizador = registroTotalizador;
    }

    public Planilha getIdPlanilha() {
        return idPlanilha;
    }

    public void setIdPlanilha(Planilha idPlanilha) {
        this.idPlanilha = idPlanilha;
    }

    public Grupo getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(Grupo idGrupo) {
        this.idGrupo = idGrupo;
    }

    public Proposta getIdProposta() {
        return idProposta;
    }

    public void setIdProposta(Proposta idProposta) {
        this.idProposta = idProposta;
    }

    public PlanilhaItem getIdPlanilhaItem() {
        return idPlanilhaItem;
    }

    public void setIdPlanilhaItem(PlanilhaItem idPlanilhaItem) {
        this.idPlanilhaItem = idPlanilhaItem;
    }

    @XmlTransient
    public List<PropostaItemProduto> getPropostaItemProdutoList() {
        return propostaItemProdutoList;
    }

    public void setPropostaItemProdutoList(List<PropostaItemProduto> propostaItemProdutoList) {
        this.propostaItemProdutoList = propostaItemProdutoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPropostaItem != null ? idPropostaItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropostaItem)) {
            return false;
        }
        PropostaItem other = (PropostaItem) object;
        if ((this.idPropostaItem == null && other.idPropostaItem != null) || (this.idPropostaItem != null && !this.idPropostaItem.equals(other.idPropostaItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PropostaItem[ idPropostaItem=" + idPropostaItem + " ]";
    }
    
}
