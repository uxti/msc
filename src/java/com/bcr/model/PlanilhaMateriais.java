/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "planilha_materiais")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlanilhaMateriais.findAll", query = "SELECT p FROM PlanilhaMateriais p"),
    @NamedQuery(name = "PlanilhaMateriais.findByIdPlanilhaMateriais", query = "SELECT p FROM PlanilhaMateriais p WHERE p.idPlanilhaMateriais = :idPlanilhaMateriais"),
    @NamedQuery(name = "PlanilhaMateriais.findByQuantidade", query = "SELECT p FROM PlanilhaMateriais p WHERE p.quantidade = :quantidade"),
    @NamedQuery(name = "PlanilhaMateriais.findByMesesDepreciado", query = "SELECT p FROM PlanilhaMateriais p WHERE p.mesesDepreciado = :mesesDepreciado")})
public class PlanilhaMateriais implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_planilha_materiais")
    private Integer idPlanilhaMateriais;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "quantidade")
    private BigDecimal quantidade;
    @Column(name = "meses_depreciado")
    private Integer mesesDepreciado;
    @JoinColumn(name = "id_planilha", referencedColumnName = "id_planilha")
    @ManyToOne
    private Planilha idPlanilha;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne(optional = false)
    private Produto idProduto;

    public PlanilhaMateriais() {
    }

    public PlanilhaMateriais(Integer idPlanilhaMateriais) {
        this.idPlanilhaMateriais = idPlanilhaMateriais;
    }

    public Integer getIdPlanilhaMateriais() {
        return idPlanilhaMateriais;
    }

    public void setIdPlanilhaMateriais(Integer idPlanilhaMateriais) {
        this.idPlanilhaMateriais = idPlanilhaMateriais;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getMesesDepreciado() {
        return mesesDepreciado;
    }

    public void setMesesDepreciado(Integer mesesDepreciado) {
        this.mesesDepreciado = mesesDepreciado;
    }

    public Planilha getIdPlanilha() {
        return idPlanilha;
    }

    public void setIdPlanilha(Planilha idPlanilha) {
        this.idPlanilha = idPlanilha;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPlanilhaMateriais != null ? idPlanilhaMateriais.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlanilhaMateriais)) {
            return false;
        }
        PlanilhaMateriais other = (PlanilhaMateriais) object;
        if ((this.idPlanilhaMateriais == null && other.idPlanilhaMateriais != null) || (this.idPlanilhaMateriais != null && !this.idPlanilhaMateriais.equals(other.idPlanilhaMateriais))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PlanilhaMateriais[ idPlanilhaMateriais=" + idPlanilhaMateriais + " ]";
    }
    
}
