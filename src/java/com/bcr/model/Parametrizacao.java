/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "parametrizacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parametrizacao.findAll", query = "SELECT p FROM Parametrizacao p"),
    @NamedQuery(name = "Parametrizacao.findByIdParametrizacao", query = "SELECT p FROM Parametrizacao p WHERE p.idParametrizacao = :idParametrizacao"),
    @NamedQuery(name = "Parametrizacao.findByEnquadramento", query = "SELECT p FROM Parametrizacao p WHERE p.enquadramento = :enquadramento"),
    @NamedQuery(name = "Parametrizacao.findByMsgIntroducao", query = "SELECT p FROM Parametrizacao p WHERE p.msgIntroducao = :msgIntroducao"),
    @NamedQuery(name = "Parametrizacao.findByTabelaPreco", query = "SELECT p FROM Parametrizacao p WHERE p.tabelaPreco = :tabelaPreco"),
    @NamedQuery(name = "Parametrizacao.findByVersaoNFE", query = "SELECT p FROM Parametrizacao p WHERE p.versaoNFE = :versaoNFE"),
    @NamedQuery(name = "Parametrizacao.findByAmbienteNFE", query = "SELECT p FROM Parametrizacao p WHERE p.ambienteNFE = :ambienteNFE"),
    @NamedQuery(name = "Parametrizacao.findByCPFCNPJDuplicado", query = "SELECT p FROM Parametrizacao p WHERE p.cPFCNPJDuplicado = :cPFCNPJDuplicado"),
    @NamedQuery(name = "Parametrizacao.findByCorTopo", query = "SELECT p FROM Parametrizacao p WHERE p.corTopo = :corTopo"),
    @NamedQuery(name = "Parametrizacao.findByCorLateral", query = "SELECT p FROM Parametrizacao p WHERE p.corLateral = :corLateral"),
    @NamedQuery(name = "Parametrizacao.findByCorBarras", query = "SELECT p FROM Parametrizacao p WHERE p.corBarras = :corBarras"),
    @NamedQuery(name = "Parametrizacao.findBySchemaSelecionado", query = "SELECT p FROM Parametrizacao p WHERE p.schemaSelecionado = :schemaSelecionado"),
    @NamedQuery(name = "Parametrizacao.findByTipoSistema", query = "SELECT p FROM Parametrizacao p WHERE p.tipoSistema = :tipoSistema"),
    @NamedQuery(name = "Parametrizacao.findByDiasCompensacaoCheque", query = "SELECT p FROM Parametrizacao p WHERE p.diasCompensacaoCheque = :diasCompensacaoCheque"),
    @NamedQuery(name = "Parametrizacao.findByPagamentoRetroativo", query = "SELECT p FROM Parametrizacao p WHERE p.pagamentoRetroativo = :pagamentoRetroativo"),
    @NamedQuery(name = "Parametrizacao.findBySchemaBanco", query = "SELECT p FROM Parametrizacao p WHERE p.schemaBanco = :schemaBanco"),
    @NamedQuery(name = "Parametrizacao.findByCaminhoLog", query = "SELECT p FROM Parametrizacao p WHERE p.caminhoLog = :caminhoLog"),
    @NamedQuery(name = "Parametrizacao.findByExportarChequeSoftcheque", query = "SELECT p FROM Parametrizacao p WHERE p.exportarChequeSoftcheque = :exportarChequeSoftcheque"),
    @NamedQuery(name = "Parametrizacao.findByChequeMovimentaCompensacao", query = "SELECT p FROM Parametrizacao p WHERE p.chequeMovimentaCompensacao = :chequeMovimentaCompensacao")})
public class Parametrizacao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_parametrizacao")
    private Integer idParametrizacao;
    @Size(max = 1)
    @Column(name = "enquadramento")
    private String enquadramento;
    @Size(max = 255)
    @Column(name = "msg_introducao")
    private String msgIntroducao;
    @Size(max = 1)
    @Column(name = "tabela_preco")
    private String tabelaPreco;
    @Size(max = 4)
    @Column(name = "versao_NFE")
    private String versaoNFE;
    @Size(max = 1)
    @Column(name = "ambiente_NFE")
    private String ambienteNFE;
    @Lob
    @Column(name = "assinatura_XML")
    private byte[] assinaturaXML;
    @Column(name = "CPFCNPJ_Duplicado")
    private Boolean cPFCNPJDuplicado;
    @Size(max = 20)
    @Column(name = "COR_TOPO")
    private String corTopo;
    @Size(max = 20)
    @Column(name = "COR_LATERAL")
    private String corLateral;
    @Size(max = 20)
    @Column(name = "COR_BARRAS")
    private String corBarras;
    @Lob
    @Size(max = 65535)
    @Column(name = "caminho_mysql")
    private String caminhoMysql;
    @Lob
    @Size(max = 65535)
    @Column(name = "destino")
    private String destino;
    @Size(max = 100)
    @Column(name = "schema_selecionado")
    private String schemaSelecionado;
    @Size(max = 1)
    @Column(name = "tipo_sistema")
    private String tipoSistema;
    @Column(name = "dias_compensacao_cheque")
    private Integer diasCompensacaoCheque;
    @Column(name = "pagamento_retroativo")
    private Boolean pagamentoRetroativo;
    @Lob
    @Size(max = 65535)
    @Column(name = "destino_bkp")
    private String destinoBkp;
    @Size(max = 255)
    @Column(name = "schema_banco")
    private String schemaBanco;
    @Size(max = 255)
    @Column(name = "caminho_log")
    private String caminhoLog;
    @Column(name = "exportar_cheque_softcheque")
    private Boolean exportarChequeSoftcheque;
    @Size(max = 1)
    @Column(name = "cheque_movimenta_compensacao")
    private String chequeMovimentaCompensacao;
    @OneToMany(mappedBy = "idParametrizacao")
    private List<Empresa> empresaList;

    public Parametrizacao() {
    }

    public Parametrizacao(Integer idParametrizacao) {
        this.idParametrizacao = idParametrizacao;
    }

    public Integer getIdParametrizacao() {
        return idParametrizacao;
    }

    public void setIdParametrizacao(Integer idParametrizacao) {
        this.idParametrizacao = idParametrizacao;
    }

    public String getEnquadramento() {
        return enquadramento;
    }

    public void setEnquadramento(String enquadramento) {
        this.enquadramento = enquadramento;
    }

    public String getMsgIntroducao() {
        return msgIntroducao;
    }

    public void setMsgIntroducao(String msgIntroducao) {
        this.msgIntroducao = msgIntroducao;
    }

    public String getTabelaPreco() {
        return tabelaPreco;
    }

    public void setTabelaPreco(String tabelaPreco) {
        this.tabelaPreco = tabelaPreco;
    }

    public String getVersaoNFE() {
        return versaoNFE;
    }

    public void setVersaoNFE(String versaoNFE) {
        this.versaoNFE = versaoNFE;
    }

    public String getAmbienteNFE() {
        return ambienteNFE;
    }

    public void setAmbienteNFE(String ambienteNFE) {
        this.ambienteNFE = ambienteNFE;
    }

    public byte[] getAssinaturaXML() {
        return assinaturaXML;
    }

    public void setAssinaturaXML(byte[] assinaturaXML) {
        this.assinaturaXML = assinaturaXML;
    }

    public Boolean getCPFCNPJDuplicado() {
        return cPFCNPJDuplicado;
    }

    public void setCPFCNPJDuplicado(Boolean cPFCNPJDuplicado) {
        this.cPFCNPJDuplicado = cPFCNPJDuplicado;
    }

    public String getCorTopo() {
        return corTopo;
    }

    public void setCorTopo(String corTopo) {
        this.corTopo = corTopo;
    }

    public String getCorLateral() {
        return corLateral;
    }

    public void setCorLateral(String corLateral) {
        this.corLateral = corLateral;
    }

    public String getCorBarras() {
        return corBarras;
    }

    public void setCorBarras(String corBarras) {
        this.corBarras = corBarras;
    }

    public String getCaminhoMysql() {
        return caminhoMysql;
    }

    public void setCaminhoMysql(String caminhoMysql) {
        this.caminhoMysql = caminhoMysql;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getSchemaSelecionado() {
        return schemaSelecionado;
    }

    public void setSchemaSelecionado(String schemaSelecionado) {
        this.schemaSelecionado = schemaSelecionado;
    }

    public String getTipoSistema() {
        return tipoSistema;
    }

    public void setTipoSistema(String tipoSistema) {
        this.tipoSistema = tipoSistema;
    }

    public Integer getDiasCompensacaoCheque() {
        return diasCompensacaoCheque;
    }

    public void setDiasCompensacaoCheque(Integer diasCompensacaoCheque) {
        this.diasCompensacaoCheque = diasCompensacaoCheque;
    }

    public Boolean getPagamentoRetroativo() {
        return pagamentoRetroativo;
    }

    public void setPagamentoRetroativo(Boolean pagamentoRetroativo) {
        this.pagamentoRetroativo = pagamentoRetroativo;
    }

    public String getDestinoBkp() {
        return destinoBkp;
    }

    public void setDestinoBkp(String destinoBkp) {
        this.destinoBkp = destinoBkp;
    }

    public String getSchemaBanco() {
        return schemaBanco;
    }

    public void setSchemaBanco(String schemaBanco) {
        this.schemaBanco = schemaBanco;
    }

    public String getCaminhoLog() {
        return caminhoLog;
    }

    public void setCaminhoLog(String caminhoLog) {
        this.caminhoLog = caminhoLog;
    }

    public Boolean getExportarChequeSoftcheque() {
        return exportarChequeSoftcheque;
    }

    public void setExportarChequeSoftcheque(Boolean exportarChequeSoftcheque) {
        this.exportarChequeSoftcheque = exportarChequeSoftcheque;
    }

    public String getChequeMovimentaCompensacao() {
        return chequeMovimentaCompensacao;
    }

    public void setChequeMovimentaCompensacao(String chequeMovimentaCompensacao) {
        this.chequeMovimentaCompensacao = chequeMovimentaCompensacao;
    }

    @XmlTransient
    public List<Empresa> getEmpresaList() {
        return empresaList;
    }

    public void setEmpresaList(List<Empresa> empresaList) {
        this.empresaList = empresaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idParametrizacao != null ? idParametrizacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parametrizacao)) {
            return false;
        }
        Parametrizacao other = (Parametrizacao) object;
        if ((this.idParametrizacao == null && other.idParametrizacao != null) || (this.idParametrizacao != null && !this.idParametrizacao.equals(other.idParametrizacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Parametrizacao[ idParametrizacao=" + idParametrizacao + " ]";
    }
    
}
