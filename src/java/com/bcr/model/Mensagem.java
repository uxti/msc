/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "mensagem")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mensagem.findAll", query = "SELECT m FROM Mensagem m"),
    @NamedQuery(name = "Mensagem.findByIdMensagem", query = "SELECT m FROM Mensagem m WHERE m.idMensagem = :idMensagem"),
    @NamedQuery(name = "Mensagem.findByMensagem", query = "SELECT m FROM Mensagem m WHERE m.mensagem = :mensagem"),
    @NamedQuery(name = "Mensagem.findByVista", query = "SELECT m FROM Mensagem m WHERE m.vista = :vista")})
public class Mensagem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_MENSAGEM")
    private Integer idMensagem;
    @Size(max = 255)
    @Column(name = "MENSAGEM")
    private String mensagem;
    @Column(name = "VISTA")
    private Character vista;
    @JoinColumn(name = "id_usuario_receiver", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuarioReceiver;
    @JoinColumn(name = "id_usuario_sender", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuarioSender;

    public Mensagem() {
    }

    public Mensagem(Integer idMensagem) {
        this.idMensagem = idMensagem;
    }

    public Integer getIdMensagem() {
        return idMensagem;
    }

    public void setIdMensagem(Integer idMensagem) {
        this.idMensagem = idMensagem;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Character getVista() {
        return vista;
    }

    public void setVista(Character vista) {
        this.vista = vista;
    }

    public Usuario getIdUsuarioReceiver() {
        return idUsuarioReceiver;
    }

    public void setIdUsuarioReceiver(Usuario idUsuarioReceiver) {
        this.idUsuarioReceiver = idUsuarioReceiver;
    }

    public Usuario getIdUsuarioSender() {
        return idUsuarioSender;
    }

    public void setIdUsuarioSender(Usuario idUsuarioSender) {
        this.idUsuarioSender = idUsuarioSender;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMensagem != null ? idMensagem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mensagem)) {
            return false;
        }
        Mensagem other = (Mensagem) object;
        if ((this.idMensagem == null && other.idMensagem != null) || (this.idMensagem != null && !this.idMensagem.equals(other.idMensagem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Mensagem[ idMensagem=" + idMensagem + " ]";
    }
    
}
