/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "reserva_ativo_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReservaAtivoItem.findAll", query = "SELECT r FROM ReservaAtivoItem r"),
    @NamedQuery(name = "ReservaAtivoItem.findByIdReservaAtivoItem", query = "SELECT r FROM ReservaAtivoItem r WHERE r.idReservaAtivoItem = :idReservaAtivoItem"),
    @NamedQuery(name = "ReservaAtivoItem.findByQde", query = "SELECT r FROM ReservaAtivoItem r WHERE r.qde = :qde"),
    @NamedQuery(name = "ReservaAtivoItem.findByValorConsiderado", query = "SELECT r FROM ReservaAtivoItem r WHERE r.valorConsiderado = :valorConsiderado")})
public class ReservaAtivoItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_reserva_ativo_item")
    private Integer idReservaAtivoItem;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "qde")
    private BigDecimal qde;
    @Column(name = "valor_considerado")
    private BigDecimal valorConsiderado;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne(optional = false)
    private Produto idProduto;
    @JoinColumn(name = "id_reserva_ativo", referencedColumnName = "id_reserva_ativo")
    @ManyToOne
    private ReservaAtivo idReservaAtivo;
    @JoinColumn(name = "id_ativo", referencedColumnName = "id_ativo")
    @ManyToOne
    private Ativo idAtivo;

    public ReservaAtivoItem() {
    }

    public ReservaAtivoItem(Integer idReservaAtivoItem) {
        this.idReservaAtivoItem = idReservaAtivoItem;
    }

    public Integer getIdReservaAtivoItem() {
        return idReservaAtivoItem;
    }

    public void setIdReservaAtivoItem(Integer idReservaAtivoItem) {
        this.idReservaAtivoItem = idReservaAtivoItem;
    }

    public BigDecimal getQde() {
        return qde;
    }

    public void setQde(BigDecimal qde) {
        this.qde = qde;
    }

    public BigDecimal getValorConsiderado() {
        return valorConsiderado;
    }

    public void setValorConsiderado(BigDecimal valorConsiderado) {
        this.valorConsiderado = valorConsiderado;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    public ReservaAtivo getIdReservaAtivo() {
        return idReservaAtivo;
    }

    public void setIdReservaAtivo(ReservaAtivo idReservaAtivo) {
        this.idReservaAtivo = idReservaAtivo;
    }

    public Ativo getIdAtivo() {
        return idAtivo;
    }

    public void setIdAtivo(Ativo idAtivo) {
        this.idAtivo = idAtivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idReservaAtivoItem != null ? idReservaAtivoItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReservaAtivoItem)) {
            return false;
        }
        ReservaAtivoItem other = (ReservaAtivoItem) object;
        if ((this.idReservaAtivoItem == null && other.idReservaAtivoItem != null) || (this.idReservaAtivoItem != null && !this.idReservaAtivoItem.equals(other.idReservaAtivoItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ReservaAtivoItem[ idReservaAtivoItem=" + idReservaAtivoItem + " ]";
    }
    
}
