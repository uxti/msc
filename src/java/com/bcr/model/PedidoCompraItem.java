/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "pedido_compra_item")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PedidoCompraItem.findAll", query = "SELECT p FROM PedidoCompraItem p"),
    @NamedQuery(name = "PedidoCompraItem.findByIdPedidoCompraItem", query = "SELECT p FROM PedidoCompraItem p WHERE p.idPedidoCompraItem = :idPedidoCompraItem"),
    @NamedQuery(name = "PedidoCompraItem.findByStatus", query = "SELECT p FROM PedidoCompraItem p WHERE p.status = :status"),
    @NamedQuery(name = "PedidoCompraItem.findByQtdSolicitada", query = "SELECT p FROM PedidoCompraItem p WHERE p.qtdSolicitada = :qtdSolicitada"),
    @NamedQuery(name = "PedidoCompraItem.findByQtdChegada", query = "SELECT p FROM PedidoCompraItem p WHERE p.qtdChegada = :qtdChegada"),
    @NamedQuery(name = "PedidoCompraItem.findByQtdRestante", query = "SELECT p FROM PedidoCompraItem p WHERE p.qtdRestante = :qtdRestante"),
    @NamedQuery(name = "PedidoCompraItem.findByValorUnitario", query = "SELECT p FROM PedidoCompraItem p WHERE p.valorUnitario = :valorUnitario"),
    @NamedQuery(name = "PedidoCompraItem.findByValorTotal", query = "SELECT p FROM PedidoCompraItem p WHERE p.valorTotal = :valorTotal"),
    @NamedQuery(name = "PedidoCompraItem.findByValorUnitarioChegada", query = "SELECT p FROM PedidoCompraItem p WHERE p.valorUnitarioChegada = :valorUnitarioChegada"),
    @NamedQuery(name = "PedidoCompraItem.findByValorTotalChegada", query = "SELECT p FROM PedidoCompraItem p WHERE p.valorTotalChegada = :valorTotalChegada")})
public class PedidoCompraItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_pedido_compra_item")
    private Integer idPedidoCompraItem;
    @Lob
    @Size(max = 65535)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "qtd_solicitada")
    private BigDecimal qtdSolicitada;
    @Column(name = "qtd_chegada")
    private BigDecimal qtdChegada;
    @Column(name = "qtd_restante")
    private BigDecimal qtdRestante;
    @Column(name = "valor_unitario")
    private BigDecimal valorUnitario;
    @Column(name = "valor_total")
    private BigDecimal valorTotal;
    @Column(name = "valor_unitario_chegada")
    private BigDecimal valorUnitarioChegada;
    @Column(name = "valor_total_chegada")
    private BigDecimal valorTotalChegada;
    @JoinColumn(name = "id_pedido_compra", referencedColumnName = "id_pedido_compra")
    @ManyToOne(optional = false)
    private PedidoCompra idPedidoCompra;
    @JoinColumn(name = "id_cotacao_item", referencedColumnName = "id_cotacao_item")
    @ManyToOne
    private CotacaoItem idCotacaoItem;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne(optional = false)
    private Produto idProduto;

    public PedidoCompraItem() {
    }

    public PedidoCompraItem(Integer idPedidoCompraItem) {
        this.idPedidoCompraItem = idPedidoCompraItem;
    }

    public Integer getIdPedidoCompraItem() {
        return idPedidoCompraItem;
    }

    public void setIdPedidoCompraItem(Integer idPedidoCompraItem) {
        this.idPedidoCompraItem = idPedidoCompraItem;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getQtdSolicitada() {
        return qtdSolicitada;
    }

    public void setQtdSolicitada(BigDecimal qtdSolicitada) {
        this.qtdSolicitada = qtdSolicitada;
    }

    public BigDecimal getQtdChegada() {
        return qtdChegada;
    }

    public void setQtdChegada(BigDecimal qtdChegada) {
        this.qtdChegada = qtdChegada;
    }

    public BigDecimal getQtdRestante() {
        return qtdRestante;
    }

    public void setQtdRestante(BigDecimal qtdRestante) {
        this.qtdRestante = qtdRestante;
    }

    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public BigDecimal getValorUnitarioChegada() {
        return valorUnitarioChegada;
    }

    public void setValorUnitarioChegada(BigDecimal valorUnitarioChegada) {
        this.valorUnitarioChegada = valorUnitarioChegada;
    }

    public BigDecimal getValorTotalChegada() {
        return valorTotalChegada;
    }

    public void setValorTotalChegada(BigDecimal valorTotalChegada) {
        this.valorTotalChegada = valorTotalChegada;
    }

    public PedidoCompra getIdPedidoCompra() {
        return idPedidoCompra;
    }

    public void setIdPedidoCompra(PedidoCompra idPedidoCompra) {
        this.idPedidoCompra = idPedidoCompra;
    }

    public CotacaoItem getIdCotacaoItem() {
        return idCotacaoItem;
    }

    public void setIdCotacaoItem(CotacaoItem idCotacaoItem) {
        this.idCotacaoItem = idCotacaoItem;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPedidoCompraItem != null ? idPedidoCompraItem.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PedidoCompraItem)) {
            return false;
        }
        PedidoCompraItem other = (PedidoCompraItem) object;
        if ((this.idPedidoCompraItem == null && other.idPedidoCompraItem != null) || (this.idPedidoCompraItem != null && !this.idPedidoCompraItem.equals(other.idPedidoCompraItem))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PedidoCompraItem[ idPedidoCompraItem=" + idPedidoCompraItem + " ]";
    }
    
}
