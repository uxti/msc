/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "acesso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Acesso.findAll", query = "SELECT a FROM Acesso a"),
    @NamedQuery(name = "Acesso.findByIdAcesso", query = "SELECT a FROM Acesso a WHERE a.idAcesso = :idAcesso"),
    @NamedQuery(name = "Acesso.findByModuloFinanceiro", query = "SELECT a FROM Acesso a WHERE a.moduloFinanceiro = :moduloFinanceiro"),
    @NamedQuery(name = "Acesso.findByFinanceiroContasPagar", query = "SELECT a FROM Acesso a WHERE a.financeiroContasPagar = :financeiroContasPagar"),
    @NamedQuery(name = "Acesso.findByFinanceiroContasReceber", query = "SELECT a FROM Acesso a WHERE a.financeiroContasReceber = :financeiroContasReceber"),
    @NamedQuery(name = "Acesso.findByFinanceiroCaixa", query = "SELECT a FROM Acesso a WHERE a.financeiroCaixa = :financeiroCaixa"),
    @NamedQuery(name = "Acesso.findByFinanceiroBanco", query = "SELECT a FROM Acesso a WHERE a.financeiroBanco = :financeiroBanco"),
    @NamedQuery(name = "Acesso.findByFinanceiroCheque", query = "SELECT a FROM Acesso a WHERE a.financeiroCheque = :financeiroCheque"),
    @NamedQuery(name = "Acesso.findByContasPagarPesquisar", query = "SELECT a FROM Acesso a WHERE a.contasPagarPesquisar = :contasPagarPesquisar"),
    @NamedQuery(name = "Acesso.findByContasPagarInserir", query = "SELECT a FROM Acesso a WHERE a.contasPagarInserir = :contasPagarInserir"),
    @NamedQuery(name = "Acesso.findByContasPagarPagar", query = "SELECT a FROM Acesso a WHERE a.contasPagarPagar = :contasPagarPagar"),
    @NamedQuery(name = "Acesso.findByContasPagarExcluir", query = "SELECT a FROM Acesso a WHERE a.contasPagarExcluir = :contasPagarExcluir"),
    @NamedQuery(name = "Acesso.findByContasPagarEstornar", query = "SELECT a FROM Acesso a WHERE a.contasPagarEstornar = :contasPagarEstornar"),
    @NamedQuery(name = "Acesso.findByContasPagarVer", query = "SELECT a FROM Acesso a WHERE a.contasPagarVer = :contasPagarVer"),
    @NamedQuery(name = "Acesso.findByContasReceberPesquisar", query = "SELECT a FROM Acesso a WHERE a.contasReceberPesquisar = :contasReceberPesquisar"),
    @NamedQuery(name = "Acesso.findByContasReceberInserir", query = "SELECT a FROM Acesso a WHERE a.contasReceberInserir = :contasReceberInserir"),
    @NamedQuery(name = "Acesso.findByContasReceberReceber", query = "SELECT a FROM Acesso a WHERE a.contasReceberReceber = :contasReceberReceber"),
    @NamedQuery(name = "Acesso.findByContasReceberExcluir", query = "SELECT a FROM Acesso a WHERE a.contasReceberExcluir = :contasReceberExcluir"),
    @NamedQuery(name = "Acesso.findByContasReceberEstornar", query = "SELECT a FROM Acesso a WHERE a.contasReceberEstornar = :contasReceberEstornar"),
    @NamedQuery(name = "Acesso.findByContasReceberVer", query = "SELECT a FROM Acesso a WHERE a.contasReceberVer = :contasReceberVer"),
    @NamedQuery(name = "Acesso.findByCaixaPesquisar", query = "SELECT a FROM Acesso a WHERE a.caixaPesquisar = :caixaPesquisar"),
    @NamedQuery(name = "Acesso.findByCaixaSangria", query = "SELECT a FROM Acesso a WHERE a.caixaSangria = :caixaSangria"),
    @NamedQuery(name = "Acesso.findByCaixaSuprimento", query = "SELECT a FROM Acesso a WHERE a.caixaSuprimento = :caixaSuprimento"),
    @NamedQuery(name = "Acesso.findByCaixaTransferencia", query = "SELECT a FROM Acesso a WHERE a.caixaTransferencia = :caixaTransferencia"),
    @NamedQuery(name = "Acesso.findByBancoPesquisar", query = "SELECT a FROM Acesso a WHERE a.bancoPesquisar = :bancoPesquisar"),
    @NamedQuery(name = "Acesso.findByBancoSaque", query = "SELECT a FROM Acesso a WHERE a.bancoSaque = :bancoSaque"),
    @NamedQuery(name = "Acesso.findByBancoDeposito", query = "SELECT a FROM Acesso a WHERE a.bancoDeposito = :bancoDeposito"),
    @NamedQuery(name = "Acesso.findByBancoTransferencia", query = "SELECT a FROM Acesso a WHERE a.bancoTransferencia = :bancoTransferencia"),
    @NamedQuery(name = "Acesso.findByGestaoResultado", query = "SELECT a FROM Acesso a WHERE a.gestaoResultado = :gestaoResultado"),
    @NamedQuery(name = "Acesso.findByFinanceiroAcessoTotal", query = "SELECT a FROM Acesso a WHERE a.financeiroAcessoTotal = :financeiroAcessoTotal"),
    @NamedQuery(name = "Acesso.findByChequeCompensar", query = "SELECT a FROM Acesso a WHERE a.chequeCompensar = :chequeCompensar"),
    @NamedQuery(name = "Acesso.findByChequePesquisar", query = "SELECT a FROM Acesso a WHERE a.chequePesquisar = :chequePesquisar"),
    @NamedQuery(name = "Acesso.findByChequeInserir", query = "SELECT a FROM Acesso a WHERE a.chequeInserir = :chequeInserir"),
    @NamedQuery(name = "Acesso.findByChequeAlterar", query = "SELECT a FROM Acesso a WHERE a.chequeAlterar = :chequeAlterar"),
    @NamedQuery(name = "Acesso.findByChequeExcluir", query = "SELECT a FROM Acesso a WHERE a.chequeExcluir = :chequeExcluir"),
    @NamedQuery(name = "Acesso.findByModuloLogistica", query = "SELECT a FROM Acesso a WHERE a.moduloLogistica = :moduloLogistica"),
    @NamedQuery(name = "Acesso.findByModuloComercial", query = "SELECT a FROM Acesso a WHERE a.moduloComercial = :moduloComercial"),
    @NamedQuery(name = "Acesso.findByCadastroCliente", query = "SELECT a FROM Acesso a WHERE a.cadastroCliente = :cadastroCliente"),
    @NamedQuery(name = "Acesso.findByCadastroFornecedor", query = "SELECT a FROM Acesso a WHERE a.cadastroFornecedor = :cadastroFornecedor"),
    @NamedQuery(name = "Acesso.findByCadastroTransportadora", query = "SELECT a FROM Acesso a WHERE a.cadastroTransportadora = :cadastroTransportadora"),
    @NamedQuery(name = "Acesso.findByCadastroProduto", query = "SELECT a FROM Acesso a WHERE a.cadastroProduto = :cadastroProduto"),
    @NamedQuery(name = "Acesso.findByCadastroAtivo", query = "SELECT a FROM Acesso a WHERE a.cadastroAtivo = :cadastroAtivo"),
    @NamedQuery(name = "Acesso.findByCadastroPlanilha", query = "SELECT a FROM Acesso a WHERE a.cadastroPlanilha = :cadastroPlanilha"),
    @NamedQuery(name = "Acesso.findByCadastroPlanoContas", query = "SELECT a FROM Acesso a WHERE a.cadastroPlanoContas = :cadastroPlanoContas"),
    @NamedQuery(name = "Acesso.findByCadastroEmpresa", query = "SELECT a FROM Acesso a WHERE a.cadastroEmpresa = :cadastroEmpresa"),
    @NamedQuery(name = "Acesso.findByCadastroBanco", query = "SELECT a FROM Acesso a WHERE a.cadastroBanco = :cadastroBanco"),
    @NamedQuery(name = "Acesso.findByCadastroOutros", query = "SELECT a FROM Acesso a WHERE a.cadastroOutros = :cadastroOutros"),
    @NamedQuery(name = "Acesso.findByConfiguracao", query = "SELECT a FROM Acesso a WHERE a.configuracao = :configuracao"),
    @NamedQuery(name = "Acesso.findByLogisticaAcessoTotal", query = "SELECT a FROM Acesso a WHERE a.logisticaAcessoTotal = :logisticaAcessoTotal"),
    @NamedQuery(name = "Acesso.findByLogisticaRequisicao", query = "SELECT a FROM Acesso a WHERE a.logisticaRequisicao = :logisticaRequisicao"),
    @NamedQuery(name = "Acesso.findByLogisticaCompras", query = "SELECT a FROM Acesso a WHERE a.logisticaCompras = :logisticaCompras"),
    @NamedQuery(name = "Acesso.findByLogisticaLancamentos", query = "SELECT a FROM Acesso a WHERE a.logisticaLancamentos = :logisticaLancamentos"),
    @NamedQuery(name = "Acesso.findByLogisticaEstoque", query = "SELECT a FROM Acesso a WHERE a.logisticaEstoque = :logisticaEstoque"),
    @NamedQuery(name = "Acesso.findByLogisticaDepreciacao", query = "SELECT a FROM Acesso a WHERE a.logisticaDepreciacao = :logisticaDepreciacao"),
    @NamedQuery(name = "Acesso.findByCadastroTipoProposta", query = "SELECT a FROM Acesso a WHERE a.cadastroTipoProposta = :cadastroTipoProposta"),
    @NamedQuery(name = "Acesso.findByCadastroLocalTrabalho", query = "SELECT a FROM Acesso a WHERE a.cadastroLocalTrabalho = :cadastroLocalTrabalho"),
    @NamedQuery(name = "Acesso.findByCadastroColaborador", query = "SELECT a FROM Acesso a WHERE a.cadastroColaborador = :cadastroColaborador"),
    @NamedQuery(name = "Acesso.findByCadastroPerfil", query = "SELECT a FROM Acesso a WHERE a.cadastroPerfil = :cadastroPerfil"),
    @NamedQuery(name = "Acesso.findByCadastroSetor", query = "SELECT a FROM Acesso a WHERE a.cadastroSetor = :cadastroSetor"),
    @NamedQuery(name = "Acesso.findByCadastroCargo", query = "SELECT a FROM Acesso a WHERE a.cadastroCargo = :cadastroCargo"),
    @NamedQuery(name = "Acesso.findByCadastroGrupo", query = "SELECT a FROM Acesso a WHERE a.cadastroGrupo = :cadastroGrupo"),
    @NamedQuery(name = "Acesso.findByCadastroSubgrupo", query = "SELECT a FROM Acesso a WHERE a.cadastroSubgrupo = :cadastroSubgrupo"),
    @NamedQuery(name = "Acesso.findByCadastroGrupoativo", query = "SELECT a FROM Acesso a WHERE a.cadastroGrupoativo = :cadastroGrupoativo"),
    @NamedQuery(name = "Acesso.findByCadastroTipoativo", query = "SELECT a FROM Acesso a WHERE a.cadastroTipoativo = :cadastroTipoativo"),
    @NamedQuery(name = "Acesso.findByCadastroModeloativo", query = "SELECT a FROM Acesso a WHERE a.cadastroModeloativo = :cadastroModeloativo"),
    @NamedQuery(name = "Acesso.findByCadastroUnidade", query = "SELECT a FROM Acesso a WHERE a.cadastroUnidade = :cadastroUnidade"),
    @NamedQuery(name = "Acesso.findByCadastroNcm", query = "SELECT a FROM Acesso a WHERE a.cadastroNcm = :cadastroNcm"),
    @NamedQuery(name = "Acesso.findByCadastroLocalizacao", query = "SELECT a FROM Acesso a WHERE a.cadastroLocalizacao = :cadastroLocalizacao"),
    @NamedQuery(name = "Acesso.findByCadastroAgrupamento", query = "SELECT a FROM Acesso a WHERE a.cadastroAgrupamento = :cadastroAgrupamento"),
    @NamedQuery(name = "Acesso.findByCadastroCentrocusto", query = "SELECT a FROM Acesso a WHERE a.cadastroCentrocusto = :cadastroCentrocusto"),
    @NamedQuery(name = "Acesso.findByCadastroContacorrente", query = "SELECT a FROM Acesso a WHERE a.cadastroContacorrente = :cadastroContacorrente"),
    @NamedQuery(name = "Acesso.findByCadastroFormapagamento", query = "SELECT a FROM Acesso a WHERE a.cadastroFormapagamento = :cadastroFormapagamento"),
    @NamedQuery(name = "Acesso.findByCadastroEstado", query = "SELECT a FROM Acesso a WHERE a.cadastroEstado = :cadastroEstado"),
    @NamedQuery(name = "Acesso.findByCadastroCidade", query = "SELECT a FROM Acesso a WHERE a.cadastroCidade = :cadastroCidade"),
    @NamedQuery(name = "Acesso.findByLogisticaAnaliseRequisicao", query = "SELECT a FROM Acesso a WHERE a.logisticaAnaliseRequisicao = :logisticaAnaliseRequisicao"),
    @NamedQuery(name = "Acesso.findByLogisticaSaidaRequisicao", query = "SELECT a FROM Acesso a WHERE a.logisticaSaidaRequisicao = :logisticaSaidaRequisicao"),
    @NamedQuery(name = "Acesso.findByLogisticaCotacao", query = "SELECT a FROM Acesso a WHERE a.logisticaCotacao = :logisticaCotacao"),
    @NamedQuery(name = "Acesso.findByLogisticaNotaEntrada", query = "SELECT a FROM Acesso a WHERE a.logisticaNotaEntrada = :logisticaNotaEntrada"),
    @NamedQuery(name = "Acesso.findByLogisticaNotaSaida", query = "SELECT a FROM Acesso a WHERE a.logisticaNotaSaida = :logisticaNotaSaida"),
    @NamedQuery(name = "Acesso.findByComercialProposta", query = "SELECT a FROM Acesso a WHERE a.comercialProposta = :comercialProposta"),
    @NamedQuery(name = "Acesso.findByComercialContrato", query = "SELECT a FROM Acesso a WHERE a.comercialContrato = :comercialContrato"),
    @NamedQuery(name = "Acesso.findByCadastroUsuario", query = "SELECT a FROM Acesso a WHERE a.cadastroUsuario = :cadastroUsuario")})
public class Acesso implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID_ACESSO")
    private Integer idAcesso;
    @Column(name = "MODULO_FINANCEIRO")
    private Boolean moduloFinanceiro;
    @Column(name = "FINANCEIRO_CONTAS_PAGAR")
    private Boolean financeiroContasPagar;
    @Column(name = "FINANCEIRO_CONTAS_RECEBER")
    private Boolean financeiroContasReceber;
    @Column(name = "FINANCEIRO_CAIXA")
    private Boolean financeiroCaixa;
    @Column(name = "FINANCEIRO_BANCO")
    private Boolean financeiroBanco;
    @Column(name = "FINANCEIRO_CHEQUE")
    private Boolean financeiroCheque;
    @Column(name = "CONTAS_PAGAR_PESQUISAR")
    private Boolean contasPagarPesquisar;
    @Column(name = "CONTAS_PAGAR_INSERIR")
    private Boolean contasPagarInserir;
    @Column(name = "CONTAS_PAGAR_PAGAR")
    private Boolean contasPagarPagar;
    @Column(name = "CONTAS_PAGAR_EXCLUIR")
    private Boolean contasPagarExcluir;
    @Column(name = "CONTAS_PAGAR_ESTORNAR")
    private Boolean contasPagarEstornar;
    @Column(name = "CONTAS_PAGAR_VER")
    private Boolean contasPagarVer;
    @Column(name = "CONTAS_RECEBER_PESQUISAR")
    private Boolean contasReceberPesquisar;
    @Column(name = "CONTAS_RECEBER_INSERIR")
    private Boolean contasReceberInserir;
    @Column(name = "CONTAS_RECEBER_RECEBER")
    private Boolean contasReceberReceber;
    @Column(name = "CONTAS_RECEBER_EXCLUIR")
    private Boolean contasReceberExcluir;
    @Column(name = "CONTAS_RECEBER_ESTORNAR")
    private Boolean contasReceberEstornar;
    @Column(name = "CONTAS_RECEBER_VER")
    private Boolean contasReceberVer;
    @Column(name = "CAIXA_PESQUISAR")
    private Boolean caixaPesquisar;
    @Column(name = "CAIXA_SANGRIA")
    private Boolean caixaSangria;
    @Column(name = "CAIXA_SUPRIMENTO")
    private Boolean caixaSuprimento;
    @Column(name = "CAIXA_TRANSFERENCIA")
    private Boolean caixaTransferencia;
    @Column(name = "BANCO_PESQUISAR")
    private Boolean bancoPesquisar;
    @Column(name = "BANCO_SAQUE")
    private Boolean bancoSaque;
    @Column(name = "BANCO_DEPOSITO")
    private Boolean bancoDeposito;
    @Column(name = "BANCO_TRANSFERENCIA")
    private Boolean bancoTransferencia;
    @Column(name = "GESTAO_RESULTADO")
    private Boolean gestaoResultado;
    @Column(name = "FINANCEIRO_ACESSO_TOTAL")
    private Boolean financeiroAcessoTotal;
    @Column(name = "CHEQUE_COMPENSAR")
    private Boolean chequeCompensar;
    @Column(name = "CHEQUE_PESQUISAR")
    private Boolean chequePesquisar;
    @Column(name = "CHEQUE_INSERIR")
    private Boolean chequeInserir;
    @Column(name = "CHEQUE_ALTERAR")
    private Boolean chequeAlterar;
    @Column(name = "CHEQUE_EXCLUIR")
    private Boolean chequeExcluir;
    @Column(name = "MODULO_LOGISTICA")
    private Boolean moduloLogistica;
    @Column(name = "MODULO_COMERCIAL")
    private Boolean moduloComercial;
    @Column(name = "CADASTRO_CLIENTE")
    private Boolean cadastroCliente;
    @Column(name = "CADASTRO_FORNECEDOR")
    private Boolean cadastroFornecedor;
    @Column(name = "CADASTRO_TRANSPORTADORA")
    private Boolean cadastroTransportadora;
    @Column(name = "CADASTRO_PRODUTO")
    private Boolean cadastroProduto;
    @Column(name = "CADASTRO_ATIVO")
    private Boolean cadastroAtivo;
    @Column(name = "CADASTRO_PLANILHA")
    private Boolean cadastroPlanilha;
    @Column(name = "CADASTRO_PLANO_CONTAS")
    private Boolean cadastroPlanoContas;
    @Column(name = "CADASTRO_EMPRESA")
    private Boolean cadastroEmpresa;
    @Column(name = "CADASTRO_BANCO")
    private Boolean cadastroBanco;
    @Column(name = "CADASTRO_OUTROS")
    private Boolean cadastroOutros;
    @Column(name = "CONFIGURACAO")
    private Boolean configuracao;
    @Column(name = "LOGISTICA_ACESSO_TOTAL")
    private Boolean logisticaAcessoTotal;
    @Column(name = "LOGISTICA_REQUISICAO")
    private Boolean logisticaRequisicao;
    @Column(name = "LOGISTICA_COMPRAS")
    private Boolean logisticaCompras;
    @Column(name = "LOGISTICA_LANCAMENTOS")
    private Boolean logisticaLancamentos;
    @Column(name = "LOGISTICA_ESTOQUE")
    private Boolean logisticaEstoque;
    @Column(name = "LOGISTICA_DEPRECIACAO")
    private Boolean logisticaDepreciacao;
    @Column(name = "cadastro_tipo_proposta")
    private Boolean cadastroTipoProposta;
    @Column(name = "CADASTRO_LOCAL_TRABALHO")
    private Boolean cadastroLocalTrabalho;
    @Column(name = "CADASTRO_COLABORADOR")
    private Boolean cadastroColaborador;
    @Column(name = "CADASTRO_PERFIL")
    private Boolean cadastroPerfil;
    @Column(name = "CADASTRO_SETOR")
    private Boolean cadastroSetor;
    @Column(name = "CADASTRO_CARGO")
    private Boolean cadastroCargo;
    @Column(name = "CADASTRO_GRUPO")
    private Boolean cadastroGrupo;
    @Column(name = "CADASTRO_SUBGRUPO")
    private Boolean cadastroSubgrupo;
    @Column(name = "CADASTRO_GRUPOATIVO")
    private Boolean cadastroGrupoativo;
    @Column(name = "CADASTRO_TIPOATIVO")
    private Boolean cadastroTipoativo;
    @Column(name = "CADASTRO_MODELOATIVO")
    private Boolean cadastroModeloativo;
    @Column(name = "CADASTRO_UNIDADE")
    private Boolean cadastroUnidade;
    @Column(name = "CADASTRO_NCM")
    private Boolean cadastroNcm;
    @Column(name = "CADASTRO_LOCALIZACAO")
    private Boolean cadastroLocalizacao;
    @Column(name = "CADASTRO_AGRUPAMENTO")
    private Boolean cadastroAgrupamento;
    @Column(name = "CADASTRO_CENTROCUSTO")
    private Boolean cadastroCentrocusto;
    @Column(name = "CADASTRO_CONTACORRENTE")
    private Boolean cadastroContacorrente;
    @Column(name = "CADASTRO_FORMAPAGAMENTO")
    private Boolean cadastroFormapagamento;
    @Column(name = "CADASTRO_ESTADO")
    private Boolean cadastroEstado;
    @Column(name = "CADASTRO_CIDADE")
    private Boolean cadastroCidade;
    @Column(name = "LOGISTICA_ANALISE_REQUISICAO")
    private Boolean logisticaAnaliseRequisicao;
    @Column(name = "LOGISTICA_SAIDA_REQUISICAO")
    private Boolean logisticaSaidaRequisicao;
    @Column(name = "LOGISTICA_COTACAO")
    private Boolean logisticaCotacao;
    @Column(name = "LOGISTICA_NOTA_ENTRADA")
    private Boolean logisticaNotaEntrada;
    @Column(name = "LOGISTICA_NOTA_SAIDA")
    private Boolean logisticaNotaSaida;
    @Column(name = "COMERCIAL_PROPOSTA")
    private Boolean comercialProposta;
    @Column(name = "COMERCIAL_CONTRATO")
    private Boolean comercialContrato;
    @Column(name = "CADASTRO_USUARIO")
    private Boolean cadastroUsuario;
    @OneToMany(mappedBy = "idAcesso")
    private List<Perfil> perfilList;

    public Acesso() {
    }

    public Acesso(Integer idAcesso) {
        this.idAcesso = idAcesso;
    }

    public Integer getIdAcesso() {
        return idAcesso;
    }

    public void setIdAcesso(Integer idAcesso) {
        this.idAcesso = idAcesso;
    }

    public Boolean getModuloFinanceiro() {
        return moduloFinanceiro;
    }

    public void setModuloFinanceiro(Boolean moduloFinanceiro) {
        this.moduloFinanceiro = moduloFinanceiro;
    }

    public Boolean getFinanceiroContasPagar() {
        return financeiroContasPagar;
    }

    public void setFinanceiroContasPagar(Boolean financeiroContasPagar) {
        this.financeiroContasPagar = financeiroContasPagar;
    }

    public Boolean getFinanceiroContasReceber() {
        return financeiroContasReceber;
    }

    public void setFinanceiroContasReceber(Boolean financeiroContasReceber) {
        this.financeiroContasReceber = financeiroContasReceber;
    }

    public Boolean getFinanceiroCaixa() {
        return financeiroCaixa;
    }

    public void setFinanceiroCaixa(Boolean financeiroCaixa) {
        this.financeiroCaixa = financeiroCaixa;
    }

    public Boolean getFinanceiroBanco() {
        return financeiroBanco;
    }

    public void setFinanceiroBanco(Boolean financeiroBanco) {
        this.financeiroBanco = financeiroBanco;
    }

    public Boolean getFinanceiroCheque() {
        return financeiroCheque;
    }

    public void setFinanceiroCheque(Boolean financeiroCheque) {
        this.financeiroCheque = financeiroCheque;
    }

    public Boolean getContasPagarPesquisar() {
        return contasPagarPesquisar;
    }

    public void setContasPagarPesquisar(Boolean contasPagarPesquisar) {
        this.contasPagarPesquisar = contasPagarPesquisar;
    }

    public Boolean getContasPagarInserir() {
        return contasPagarInserir;
    }

    public void setContasPagarInserir(Boolean contasPagarInserir) {
        this.contasPagarInserir = contasPagarInserir;
    }

    public Boolean getContasPagarPagar() {
        return contasPagarPagar;
    }

    public void setContasPagarPagar(Boolean contasPagarPagar) {
        this.contasPagarPagar = contasPagarPagar;
    }

    public Boolean getContasPagarExcluir() {
        return contasPagarExcluir;
    }

    public void setContasPagarExcluir(Boolean contasPagarExcluir) {
        this.contasPagarExcluir = contasPagarExcluir;
    }

    public Boolean getContasPagarEstornar() {
        return contasPagarEstornar;
    }

    public void setContasPagarEstornar(Boolean contasPagarEstornar) {
        this.contasPagarEstornar = contasPagarEstornar;
    }

    public Boolean getContasPagarVer() {
        return contasPagarVer;
    }

    public void setContasPagarVer(Boolean contasPagarVer) {
        this.contasPagarVer = contasPagarVer;
    }

    public Boolean getContasReceberPesquisar() {
        return contasReceberPesquisar;
    }

    public void setContasReceberPesquisar(Boolean contasReceberPesquisar) {
        this.contasReceberPesquisar = contasReceberPesquisar;
    }

    public Boolean getContasReceberInserir() {
        return contasReceberInserir;
    }

    public void setContasReceberInserir(Boolean contasReceberInserir) {
        this.contasReceberInserir = contasReceberInserir;
    }

    public Boolean getContasReceberReceber() {
        return contasReceberReceber;
    }

    public void setContasReceberReceber(Boolean contasReceberReceber) {
        this.contasReceberReceber = contasReceberReceber;
    }

    public Boolean getContasReceberExcluir() {
        return contasReceberExcluir;
    }

    public void setContasReceberExcluir(Boolean contasReceberExcluir) {
        this.contasReceberExcluir = contasReceberExcluir;
    }

    public Boolean getContasReceberEstornar() {
        return contasReceberEstornar;
    }

    public void setContasReceberEstornar(Boolean contasReceberEstornar) {
        this.contasReceberEstornar = contasReceberEstornar;
    }

    public Boolean getContasReceberVer() {
        return contasReceberVer;
    }

    public void setContasReceberVer(Boolean contasReceberVer) {
        this.contasReceberVer = contasReceberVer;
    }

    public Boolean getCaixaPesquisar() {
        return caixaPesquisar;
    }

    public void setCaixaPesquisar(Boolean caixaPesquisar) {
        this.caixaPesquisar = caixaPesquisar;
    }

    public Boolean getCaixaSangria() {
        return caixaSangria;
    }

    public void setCaixaSangria(Boolean caixaSangria) {
        this.caixaSangria = caixaSangria;
    }

    public Boolean getCaixaSuprimento() {
        return caixaSuprimento;
    }

    public void setCaixaSuprimento(Boolean caixaSuprimento) {
        this.caixaSuprimento = caixaSuprimento;
    }

    public Boolean getCaixaTransferencia() {
        return caixaTransferencia;
    }

    public void setCaixaTransferencia(Boolean caixaTransferencia) {
        this.caixaTransferencia = caixaTransferencia;
    }

    public Boolean getBancoPesquisar() {
        return bancoPesquisar;
    }

    public void setBancoPesquisar(Boolean bancoPesquisar) {
        this.bancoPesquisar = bancoPesquisar;
    }

    public Boolean getBancoSaque() {
        return bancoSaque;
    }

    public void setBancoSaque(Boolean bancoSaque) {
        this.bancoSaque = bancoSaque;
    }

    public Boolean getBancoDeposito() {
        return bancoDeposito;
    }

    public void setBancoDeposito(Boolean bancoDeposito) {
        this.bancoDeposito = bancoDeposito;
    }

    public Boolean getBancoTransferencia() {
        return bancoTransferencia;
    }

    public void setBancoTransferencia(Boolean bancoTransferencia) {
        this.bancoTransferencia = bancoTransferencia;
    }

    public Boolean getGestaoResultado() {
        return gestaoResultado;
    }

    public void setGestaoResultado(Boolean gestaoResultado) {
        this.gestaoResultado = gestaoResultado;
    }

    public Boolean getFinanceiroAcessoTotal() {
        return financeiroAcessoTotal;
    }

    public void setFinanceiroAcessoTotal(Boolean financeiroAcessoTotal) {
        this.financeiroAcessoTotal = financeiroAcessoTotal;
    }

    public Boolean getChequeCompensar() {
        return chequeCompensar;
    }

    public void setChequeCompensar(Boolean chequeCompensar) {
        this.chequeCompensar = chequeCompensar;
    }

    public Boolean getChequePesquisar() {
        return chequePesquisar;
    }

    public void setChequePesquisar(Boolean chequePesquisar) {
        this.chequePesquisar = chequePesquisar;
    }

    public Boolean getChequeInserir() {
        return chequeInserir;
    }

    public void setChequeInserir(Boolean chequeInserir) {
        this.chequeInserir = chequeInserir;
    }

    public Boolean getChequeAlterar() {
        return chequeAlterar;
    }

    public void setChequeAlterar(Boolean chequeAlterar) {
        this.chequeAlterar = chequeAlterar;
    }

    public Boolean getChequeExcluir() {
        return chequeExcluir;
    }

    public void setChequeExcluir(Boolean chequeExcluir) {
        this.chequeExcluir = chequeExcluir;
    }

    public Boolean getModuloLogistica() {
        return moduloLogistica;
    }

    public void setModuloLogistica(Boolean moduloLogistica) {
        this.moduloLogistica = moduloLogistica;
    }

    public Boolean getModuloComercial() {
        return moduloComercial;
    }

    public void setModuloComercial(Boolean moduloComercial) {
        this.moduloComercial = moduloComercial;
    }

    public Boolean getCadastroCliente() {
        return cadastroCliente;
    }

    public void setCadastroCliente(Boolean cadastroCliente) {
        this.cadastroCliente = cadastroCliente;
    }

    public Boolean getCadastroFornecedor() {
        return cadastroFornecedor;
    }

    public void setCadastroFornecedor(Boolean cadastroFornecedor) {
        this.cadastroFornecedor = cadastroFornecedor;
    }

    public Boolean getCadastroTransportadora() {
        return cadastroTransportadora;
    }

    public void setCadastroTransportadora(Boolean cadastroTransportadora) {
        this.cadastroTransportadora = cadastroTransportadora;
    }

    public Boolean getCadastroProduto() {
        return cadastroProduto;
    }

    public void setCadastroProduto(Boolean cadastroProduto) {
        this.cadastroProduto = cadastroProduto;
    }

    public Boolean getCadastroAtivo() {
        return cadastroAtivo;
    }

    public void setCadastroAtivo(Boolean cadastroAtivo) {
        this.cadastroAtivo = cadastroAtivo;
    }

    public Boolean getCadastroPlanilha() {
        return cadastroPlanilha;
    }

    public void setCadastroPlanilha(Boolean cadastroPlanilha) {
        this.cadastroPlanilha = cadastroPlanilha;
    }

    public Boolean getCadastroPlanoContas() {
        return cadastroPlanoContas;
    }

    public void setCadastroPlanoContas(Boolean cadastroPlanoContas) {
        this.cadastroPlanoContas = cadastroPlanoContas;
    }

    public Boolean getCadastroEmpresa() {
        return cadastroEmpresa;
    }

    public void setCadastroEmpresa(Boolean cadastroEmpresa) {
        this.cadastroEmpresa = cadastroEmpresa;
    }

    public Boolean getCadastroBanco() {
        return cadastroBanco;
    }

    public void setCadastroBanco(Boolean cadastroBanco) {
        this.cadastroBanco = cadastroBanco;
    }

    public Boolean getCadastroOutros() {
        return cadastroOutros;
    }

    public void setCadastroOutros(Boolean cadastroOutros) {
        this.cadastroOutros = cadastroOutros;
    }

    public Boolean getConfiguracao() {
        return configuracao;
    }

    public void setConfiguracao(Boolean configuracao) {
        this.configuracao = configuracao;
    }

    public Boolean getLogisticaAcessoTotal() {
        return logisticaAcessoTotal;
    }

    public void setLogisticaAcessoTotal(Boolean logisticaAcessoTotal) {
        this.logisticaAcessoTotal = logisticaAcessoTotal;
    }

    public Boolean getLogisticaRequisicao() {
        return logisticaRequisicao;
    }

    public void setLogisticaRequisicao(Boolean logisticaRequisicao) {
        this.logisticaRequisicao = logisticaRequisicao;
    }

    public Boolean getLogisticaCompras() {
        return logisticaCompras;
    }

    public void setLogisticaCompras(Boolean logisticaCompras) {
        this.logisticaCompras = logisticaCompras;
    }

    public Boolean getLogisticaLancamentos() {
        return logisticaLancamentos;
    }

    public void setLogisticaLancamentos(Boolean logisticaLancamentos) {
        this.logisticaLancamentos = logisticaLancamentos;
    }

    public Boolean getLogisticaEstoque() {
        return logisticaEstoque;
    }

    public void setLogisticaEstoque(Boolean logisticaEstoque) {
        this.logisticaEstoque = logisticaEstoque;
    }

    public Boolean getLogisticaDepreciacao() {
        return logisticaDepreciacao;
    }

    public void setLogisticaDepreciacao(Boolean logisticaDepreciacao) {
        this.logisticaDepreciacao = logisticaDepreciacao;
    }

    public Boolean getCadastroTipoProposta() {
        return cadastroTipoProposta;
    }

    public void setCadastroTipoProposta(Boolean cadastroTipoProposta) {
        this.cadastroTipoProposta = cadastroTipoProposta;
    }

    public Boolean getCadastroLocalTrabalho() {
        return cadastroLocalTrabalho;
    }

    public void setCadastroLocalTrabalho(Boolean cadastroLocalTrabalho) {
        this.cadastroLocalTrabalho = cadastroLocalTrabalho;
    }

    public Boolean getCadastroColaborador() {
        return cadastroColaborador;
    }

    public void setCadastroColaborador(Boolean cadastroColaborador) {
        this.cadastroColaborador = cadastroColaborador;
    }

    public Boolean getCadastroPerfil() {
        return cadastroPerfil;
    }

    public void setCadastroPerfil(Boolean cadastroPerfil) {
        this.cadastroPerfil = cadastroPerfil;
    }

    public Boolean getCadastroSetor() {
        return cadastroSetor;
    }

    public void setCadastroSetor(Boolean cadastroSetor) {
        this.cadastroSetor = cadastroSetor;
    }

    public Boolean getCadastroCargo() {
        return cadastroCargo;
    }

    public void setCadastroCargo(Boolean cadastroCargo) {
        this.cadastroCargo = cadastroCargo;
    }

    public Boolean getCadastroGrupo() {
        return cadastroGrupo;
    }

    public void setCadastroGrupo(Boolean cadastroGrupo) {
        this.cadastroGrupo = cadastroGrupo;
    }

    public Boolean getCadastroSubgrupo() {
        return cadastroSubgrupo;
    }

    public void setCadastroSubgrupo(Boolean cadastroSubgrupo) {
        this.cadastroSubgrupo = cadastroSubgrupo;
    }

    public Boolean getCadastroGrupoativo() {
        return cadastroGrupoativo;
    }

    public void setCadastroGrupoativo(Boolean cadastroGrupoativo) {
        this.cadastroGrupoativo = cadastroGrupoativo;
    }

    public Boolean getCadastroTipoativo() {
        return cadastroTipoativo;
    }

    public void setCadastroTipoativo(Boolean cadastroTipoativo) {
        this.cadastroTipoativo = cadastroTipoativo;
    }

    public Boolean getCadastroModeloativo() {
        return cadastroModeloativo;
    }

    public void setCadastroModeloativo(Boolean cadastroModeloativo) {
        this.cadastroModeloativo = cadastroModeloativo;
    }

    public Boolean getCadastroUnidade() {
        return cadastroUnidade;
    }

    public void setCadastroUnidade(Boolean cadastroUnidade) {
        this.cadastroUnidade = cadastroUnidade;
    }

    public Boolean getCadastroNcm() {
        return cadastroNcm;
    }

    public void setCadastroNcm(Boolean cadastroNcm) {
        this.cadastroNcm = cadastroNcm;
    }

    public Boolean getCadastroLocalizacao() {
        return cadastroLocalizacao;
    }

    public void setCadastroLocalizacao(Boolean cadastroLocalizacao) {
        this.cadastroLocalizacao = cadastroLocalizacao;
    }

    public Boolean getCadastroAgrupamento() {
        return cadastroAgrupamento;
    }

    public void setCadastroAgrupamento(Boolean cadastroAgrupamento) {
        this.cadastroAgrupamento = cadastroAgrupamento;
    }

    public Boolean getCadastroCentrocusto() {
        return cadastroCentrocusto;
    }

    public void setCadastroCentrocusto(Boolean cadastroCentrocusto) {
        this.cadastroCentrocusto = cadastroCentrocusto;
    }

    public Boolean getCadastroContacorrente() {
        return cadastroContacorrente;
    }

    public void setCadastroContacorrente(Boolean cadastroContacorrente) {
        this.cadastroContacorrente = cadastroContacorrente;
    }

    public Boolean getCadastroFormapagamento() {
        return cadastroFormapagamento;
    }

    public void setCadastroFormapagamento(Boolean cadastroFormapagamento) {
        this.cadastroFormapagamento = cadastroFormapagamento;
    }

    public Boolean getCadastroEstado() {
        return cadastroEstado;
    }

    public void setCadastroEstado(Boolean cadastroEstado) {
        this.cadastroEstado = cadastroEstado;
    }

    public Boolean getCadastroCidade() {
        return cadastroCidade;
    }

    public void setCadastroCidade(Boolean cadastroCidade) {
        this.cadastroCidade = cadastroCidade;
    }

    public Boolean getLogisticaAnaliseRequisicao() {
        return logisticaAnaliseRequisicao;
    }

    public void setLogisticaAnaliseRequisicao(Boolean logisticaAnaliseRequisicao) {
        this.logisticaAnaliseRequisicao = logisticaAnaliseRequisicao;
    }

    public Boolean getLogisticaSaidaRequisicao() {
        return logisticaSaidaRequisicao;
    }

    public void setLogisticaSaidaRequisicao(Boolean logisticaSaidaRequisicao) {
        this.logisticaSaidaRequisicao = logisticaSaidaRequisicao;
    }

    public Boolean getLogisticaCotacao() {
        return logisticaCotacao;
    }

    public void setLogisticaCotacao(Boolean logisticaCotacao) {
        this.logisticaCotacao = logisticaCotacao;
    }

    public Boolean getLogisticaNotaEntrada() {
        return logisticaNotaEntrada;
    }

    public void setLogisticaNotaEntrada(Boolean logisticaNotaEntrada) {
        this.logisticaNotaEntrada = logisticaNotaEntrada;
    }

    public Boolean getLogisticaNotaSaida() {
        return logisticaNotaSaida;
    }

    public void setLogisticaNotaSaida(Boolean logisticaNotaSaida) {
        this.logisticaNotaSaida = logisticaNotaSaida;
    }

    public Boolean getComercialProposta() {
        return comercialProposta;
    }

    public void setComercialProposta(Boolean comercialProposta) {
        this.comercialProposta = comercialProposta;
    }

    public Boolean getComercialContrato() {
        return comercialContrato;
    }

    public void setComercialContrato(Boolean comercialContrato) {
        this.comercialContrato = comercialContrato;
    }

    public Boolean getCadastroUsuario() {
        return cadastroUsuario;
    }

    public void setCadastroUsuario(Boolean cadastroUsuario) {
        this.cadastroUsuario = cadastroUsuario;
    }

    @XmlTransient
    public List<Perfil> getPerfilList() {
        return perfilList;
    }

    public void setPerfilList(List<Perfil> perfilList) {
        this.perfilList = perfilList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAcesso != null ? idAcesso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acesso)) {
            return false;
        }
        Acesso other = (Acesso) object;
        if ((this.idAcesso == null && other.idAcesso != null) || (this.idAcesso != null && !this.idAcesso.equals(other.idAcesso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.Acesso[ idAcesso=" + idAcesso + " ]";
    }
    
}
