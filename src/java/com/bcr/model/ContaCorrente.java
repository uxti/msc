/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "conta_corrente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ContaCorrente.findAll", query = "SELECT c FROM ContaCorrente c"),
    @NamedQuery(name = "ContaCorrente.findByIdContaCorrente", query = "SELECT c FROM ContaCorrente c WHERE c.idContaCorrente = :idContaCorrente"),
    @NamedQuery(name = "ContaCorrente.findByNumAgencia", query = "SELECT c FROM ContaCorrente c WHERE c.numAgencia = :numAgencia"),
    @NamedQuery(name = "ContaCorrente.findByGestor", query = "SELECT c FROM ContaCorrente c WHERE c.gestor = :gestor"),
    @NamedQuery(name = "ContaCorrente.findByNumConta", query = "SELECT c FROM ContaCorrente c WHERE c.numConta = :numConta")})
public class ContaCorrente implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_conta_corrente")
    private Integer idContaCorrente;
    @Size(max = 10)
    @Column(name = "num_agencia")
    private String numAgencia;
    @Size(max = 100)
    @Column(name = "gestor")
    private String gestor;
    @Size(max = 10)
    @Column(name = "num_conta")
    private String numConta;
    @OneToMany(mappedBy = "idContaCorrente")
    private List<ContasPagar> contasPagarList;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_banco", referencedColumnName = "id_banco")
    @ManyToOne
    private Banco idBanco;
    @OneToMany(mappedBy = "idContaCorrente")
    private List<MovimentacaoBancaria> movimentacaoBancariaList;
    @OneToMany(mappedBy = "idContaCorrente")
    private List<Cheque> chequeList;
    @OneToMany(mappedBy = "idContaCorrente")
    private List<ContasReceber> contasReceberList;

    public ContaCorrente() {
    }

    public ContaCorrente(Integer idContaCorrente) {
        this.idContaCorrente = idContaCorrente;
    }

    public Integer getIdContaCorrente() {
        return idContaCorrente;
    }

    public void setIdContaCorrente(Integer idContaCorrente) {
        this.idContaCorrente = idContaCorrente;
    }

    public String getNumAgencia() {
        return numAgencia;
    }

    public void setNumAgencia(String numAgencia) {
        this.numAgencia = numAgencia;
    }

    public String getGestor() {
        return gestor;
    }

    public void setGestor(String gestor) {
        this.gestor = gestor;
    }

    public String getNumConta() {
        return numConta;
    }

    public void setNumConta(String numConta) {
        this.numConta = numConta;
    }

    @XmlTransient
    public List<ContasPagar> getContasPagarList() {
        return contasPagarList;
    }

    public void setContasPagarList(List<ContasPagar> contasPagarList) {
        this.contasPagarList = contasPagarList;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Banco getIdBanco() {
        return idBanco;
    }

    public void setIdBanco(Banco idBanco) {
        this.idBanco = idBanco;
    }

    @XmlTransient
    public List<MovimentacaoBancaria> getMovimentacaoBancariaList() {
        return movimentacaoBancariaList;
    }

    public void setMovimentacaoBancariaList(List<MovimentacaoBancaria> movimentacaoBancariaList) {
        this.movimentacaoBancariaList = movimentacaoBancariaList;
    }

    @XmlTransient
    public List<Cheque> getChequeList() {
        return chequeList;
    }

    public void setChequeList(List<Cheque> chequeList) {
        this.chequeList = chequeList;
    }

    @XmlTransient
    public List<ContasReceber> getContasReceberList() {
        return contasReceberList;
    }

    public void setContasReceberList(List<ContasReceber> contasReceberList) {
        this.contasReceberList = contasReceberList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContaCorrente != null ? idContaCorrente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContaCorrente)) {
            return false;
        }
        ContaCorrente other = (ContaCorrente) object;
        if ((this.idContaCorrente == null && other.idContaCorrente != null) || (this.idContaCorrente != null && !this.idContaCorrente.equals(other.idContaCorrente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ContaCorrente[ idContaCorrente=" + idContaCorrente + " ]";
    }
    
}
