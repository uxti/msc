/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "proposta_pessoal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PropostaPessoal.findAll", query = "SELECT p FROM PropostaPessoal p"),
    @NamedQuery(name = "PropostaPessoal.findByIdPropostaPessoal", query = "SELECT p FROM PropostaPessoal p WHERE p.idPropostaPessoal = :idPropostaPessoal"),
    @NamedQuery(name = "PropostaPessoal.findByQuantidade", query = "SELECT p FROM PropostaPessoal p WHERE p.quantidade = :quantidade")})
public class PropostaPessoal implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proposta_pessoal")
    private Integer idPropostaPessoal;
    @Column(name = "quantidade")
    private Integer quantidade;
    @JoinColumn(name = "id_cargo", referencedColumnName = "id_cargo")
    @ManyToOne
    private Cargo idCargo;
    @JoinColumn(name = "id_setor", referencedColumnName = "id_setor")
    @ManyToOne
    private Setor idSetor;
    @JoinColumn(name = "id_proposta", referencedColumnName = "id_proposta")
    @ManyToOne
    private Proposta idProposta;
    @JoinColumn(name = "id_planilha", referencedColumnName = "id_planilha")
    @ManyToOne
    private Planilha idPlanilha;

    public PropostaPessoal() {
    }

    public PropostaPessoal(Integer idPropostaPessoal) {
        this.idPropostaPessoal = idPropostaPessoal;
    }

    public Integer getIdPropostaPessoal() {
        return idPropostaPessoal;
    }

    public void setIdPropostaPessoal(Integer idPropostaPessoal) {
        this.idPropostaPessoal = idPropostaPessoal;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Cargo getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Cargo idCargo) {
        this.idCargo = idCargo;
    }

    public Setor getIdSetor() {
        return idSetor;
    }

    public void setIdSetor(Setor idSetor) {
        this.idSetor = idSetor;
    }

    public Proposta getIdProposta() {
        return idProposta;
    }

    public void setIdProposta(Proposta idProposta) {
        this.idProposta = idProposta;
    }

    public Planilha getIdPlanilha() {
        return idPlanilha;
    }

    public void setIdPlanilha(Planilha idPlanilha) {
        this.idPlanilha = idPlanilha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPropostaPessoal != null ? idPropostaPessoal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropostaPessoal)) {
            return false;
        }
        PropostaPessoal other = (PropostaPessoal) object;
        if ((this.idPropostaPessoal == null && other.idPropostaPessoal != null) || (this.idPropostaPessoal != null && !this.idPropostaPessoal.equals(other.idPropostaPessoal))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PropostaPessoal[ idPropostaPessoal=" + idPropostaPessoal + " ]";
    }
    
}
