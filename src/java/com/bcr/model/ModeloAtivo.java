/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "modelo_ativo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ModeloAtivo.findAll", query = "SELECT m FROM ModeloAtivo m"),
    @NamedQuery(name = "ModeloAtivo.findByIdModeloVeiculo", query = "SELECT m FROM ModeloAtivo m WHERE m.idModeloVeiculo = :idModeloVeiculo"),
    @NamedQuery(name = "ModeloAtivo.findByDescricao", query = "SELECT m FROM ModeloAtivo m WHERE m.descricao = :descricao")})
public class ModeloAtivo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_modelo_veiculo")
    private Integer idModeloVeiculo;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @JoinColumn(name = "id_grupo_ativo", referencedColumnName = "id_grupo_ativo")
    @ManyToOne
    private GrupoAtivo idGrupoAtivo;
    @OneToMany(mappedBy = "idModeloVeiculo")
    private List<Ativo> ativoList;

    public ModeloAtivo() {
    }

    public ModeloAtivo(Integer idModeloVeiculo) {
        this.idModeloVeiculo = idModeloVeiculo;
    }

    public Integer getIdModeloVeiculo() {
        return idModeloVeiculo;
    }

    public void setIdModeloVeiculo(Integer idModeloVeiculo) {
        this.idModeloVeiculo = idModeloVeiculo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public GrupoAtivo getIdGrupoAtivo() {
        return idGrupoAtivo;
    }

    public void setIdGrupoAtivo(GrupoAtivo idGrupoAtivo) {
        this.idGrupoAtivo = idGrupoAtivo;
    }

    @XmlTransient
    public List<Ativo> getAtivoList() {
        return ativoList;
    }

    public void setAtivoList(List<Ativo> ativoList) {
        this.ativoList = ativoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModeloVeiculo != null ? idModeloVeiculo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModeloAtivo)) {
            return false;
        }
        ModeloAtivo other = (ModeloAtivo) object;
        if ((this.idModeloVeiculo == null && other.idModeloVeiculo != null) || (this.idModeloVeiculo != null && !this.idModeloVeiculo.equals(other.idModeloVeiculo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.ModeloAtivo[ idModeloVeiculo=" + idModeloVeiculo + " ]";
    }
    
}
