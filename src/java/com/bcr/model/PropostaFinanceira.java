/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "proposta_financeira")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PropostaFinanceira.findAll", query = "SELECT p FROM PropostaFinanceira p"),
    @NamedQuery(name = "PropostaFinanceira.findByIdPropostaFinanceira", query = "SELECT p FROM PropostaFinanceira p WHERE p.idPropostaFinanceira = :idPropostaFinanceira"),
    @NamedQuery(name = "PropostaFinanceira.findByDtCadastro", query = "SELECT p FROM PropostaFinanceira p WHERE p.dtCadastro = :dtCadastro"),
    @NamedQuery(name = "PropostaFinanceira.findByDtFinalizacao", query = "SELECT p FROM PropostaFinanceira p WHERE p.dtFinalizacao = :dtFinalizacao"),
    @NamedQuery(name = "PropostaFinanceira.findByDescricao", query = "SELECT p FROM PropostaFinanceira p WHERE p.descricao = :descricao"),
    @NamedQuery(name = "PropostaFinanceira.findByDataBase", query = "SELECT p FROM PropostaFinanceira p WHERE p.dataBase = :dataBase"),
    @NamedQuery(name = "PropostaFinanceira.findByValorTotal", query = "SELECT p FROM PropostaFinanceira p WHERE p.valorTotal = :valorTotal"),
    @NamedQuery(name = "PropostaFinanceira.findByObservacao", query = "SELECT p FROM PropostaFinanceira p WHERE p.observacao = :observacao")})
public class PropostaFinanceira implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_proposta_financeira")
    private Integer idPropostaFinanceira;
    @Column(name = "dt_cadastro")
    @Temporal(TemporalType.DATE)
    private Date dtCadastro;
    @Column(name = "dt_finalizacao")
    @Temporal(TemporalType.DATE)
    private Date dtFinalizacao;
    @Size(max = 255)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 100)
    @Column(name = "data_base")
    private String dataBase;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_total")
    private BigDecimal valorTotal;
    @Size(max = 255)
    @Column(name = "observacao")
    private String observacao;
    @OneToMany(mappedBy = "idPropostaFinanceira")
    private List<Contrato> contratoList;
    @OneToMany(mappedBy = "idPropostaFinanceira")
    private List<Proposta> propostaList;
    @JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
    @ManyToOne
    private Cliente idCliente;

    public PropostaFinanceira() {
    }

    public PropostaFinanceira(Integer idPropostaFinanceira) {
        this.idPropostaFinanceira = idPropostaFinanceira;
    }

    public Integer getIdPropostaFinanceira() {
        return idPropostaFinanceira;
    }

    public void setIdPropostaFinanceira(Integer idPropostaFinanceira) {
        this.idPropostaFinanceira = idPropostaFinanceira;
    }

    public Date getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Date dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public Date getDtFinalizacao() {
        return dtFinalizacao;
    }

    public void setDtFinalizacao(Date dtFinalizacao) {
        this.dtFinalizacao = dtFinalizacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDataBase() {
        return dataBase;
    }

    public void setDataBase(String dataBase) {
        this.dataBase = dataBase;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    @XmlTransient
    public List<Contrato> getContratoList() {
        return contratoList;
    }

    public void setContratoList(List<Contrato> contratoList) {
        this.contratoList = contratoList;
    }

    @XmlTransient
    public List<Proposta> getPropostaList() {
        return propostaList;
    }

    public void setPropostaList(List<Proposta> propostaList) {
        this.propostaList = propostaList;
    }

    public Cliente getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Cliente idCliente) {
        this.idCliente = idCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPropostaFinanceira != null ? idPropostaFinanceira.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PropostaFinanceira)) {
            return false;
        }
        PropostaFinanceira other = (PropostaFinanceira) object;
        if ((this.idPropostaFinanceira == null && other.idPropostaFinanceira != null) || (this.idPropostaFinanceira != null && !this.idPropostaFinanceira.equals(other.idPropostaFinanceira))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.bcr.model.PropostaFinanceira[ idPropostaFinanceira=" + idPropostaFinanceira + " ]";
    }
    
}
