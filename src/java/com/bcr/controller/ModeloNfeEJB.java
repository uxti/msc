
package com.bcr.controller;

import com.bcr.model.ModeloNfe;
import javax.ejb.Stateless;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class ModeloNfeEJB extends FacadeEJB<ModeloNfe>{

    public ModeloNfeEJB() {
        super(ModeloNfe.class);
    }
}
