/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Unidade;
import javax.ejb.Stateless;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class UnidadeEJB extends FacadeEJB<Unidade> {

    public UnidadeEJB() {
        super(Unidade.class);
    }
    
    
}
