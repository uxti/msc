/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Cheque;
import com.bcr.model.ContaCorrente;
import com.bcr.util.Bcrutils;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Charles
 */
@Stateless
public class ChequeEJB extends FacadeEJB<Cheque> {

    public ChequeEJB() {
        super(Cheque.class);
    }

    public String verificarCompensado(Integer id) {
        Query query = em.createQuery("Select c.compensado From Cheque c where c.idCheque = :id");
        query.setParameter("id", id);
        return (String) query.getSingleResult();
    }

    public void compensar(Cheque cheque, String usuario) {
        if (cheque.getIdCheque() != null) {
            cheque.setCompensado("S");
            cheque.setDtCompensacaoVencimento(new Date());
            em.merge(cheque);
        }
    }

    public List<Cheque> pesquisar(Cheque chq, String por, String status, Date dtIni, Date dtFim) {
        String colaborador;
        String numCheque, contaCorrente;
        StringBuilder sb = new StringBuilder();

        if (chq.getIdColaborador() != null) {
            colaborador = chq.getIdColaborador().getNome().toString();
            sb.append(" and c.idColaborador.idColaborador LIKE " + colaborador);
        }

        if (chq.getNumeroCheque() == null) {
            numCheque = "%";
        } else {
            numCheque = chq.getNumeroCheque().toString();
        }
        if (chq.getIdContaCorrente() != null) {
            contaCorrente = chq.getIdContaCorrente().getIdContaCorrente().toString();
            sb.append(" and c.idContaCorrente.idContaCorrente LIKE " + contaCorrente);
        }

        if (!status.equals("T")) {
            sb.append(" and c.compensado LIKE '" + status.toUpperCase() + "'");
        }

        if (por != null) {
            if (por.equals("E")) {
                sb.append(" and c.dtRecebimentoEmissao BETWEEN :dtIni and :dtFim ");
            } else if (por.equals("C")) {
                sb.append(" and c.dtCompensacaoVencimento BETWEEN :dtIni and :dtFim  ");
            }
        }


        String sql = "SELECT c FROM Cheque c WHERE c.numeroCheque Like :nCheque " + sb;
        Query query = em.createQuery(sql);
        query.setParameter("nCheque", numCheque + "%");
        query.setParameter("dtIni", dtIni, TemporalType.DATE);
        query.setParameter("dtFim", dtFim, TemporalType.DATE);
        System.out.println(sql);
        System.out.println(numCheque);
        System.out.println(dtIni);
        System.out.println(dtFim);
        return query.getResultList();
    }

    public List<Cheque> listaChequesCompensarMensal() {
        Date dtInicial = Bcrutils.primeiroDiaDoMes();
        Date dtFinal = Bcrutils.ultimoDiaDoMes();
        System.out.println(dtInicial);
        System.out.println(dtFinal);
        Query query = em.createQuery("SELECT c FROM Cheque c WHERE c.dtRecebimentoEmissao BETWEEN :dtIni AND :dtFim");
        query.setParameter("dtIni", dtInicial);
        query.setParameter("dtFim", dtFinal);;
        return query.getResultList();
    }
    
    public List<Cheque> pesquisarChequesNaoCompensadosParaExtratoBancario(ContaCorrente cc, Date ini, Date fim){
        Query query = em.createQuery("Select c From Cheque c where c.idContaCorrente.idContaCorrente = :idc and c.compensado = 'N' and c.dtCompensacaoVencimento BETWEEN :ini and :fim");
        query.setParameter("ini", ini, TemporalType.DATE);
        query.setParameter("fim", fim);
        return query.getResultList();
    }   
    
    public List<Cheque> listarChequesACompensarPorPeriodo(String tipo){
        Date dini = new Date();
        Date dfim = new Date();
        if(tipo.equals("s")){
            dini = Bcrutils.primeiroDiaDaSemana();
            dfim = Bcrutils.ultimoDiaDaSemana();
        }else if(tipo.equals("m")){
            dini = Bcrutils.primeiroDiaDoMes();
            dfim = Bcrutils.ultimoDiaDoMes();
        }else{
            dini = Bcrutils.primeiroDiaDoAno();
            dfim = Bcrutils.ultimoDiaDoAno();
        }
        Query query = em.createQuery("Select c From Cheque c where c.dtCompensacaoVencimento BETWEEN :ini and :fim and c.compensado = 'N' and c.tipo != 'Cancelado'");
        query.setParameter("ini", dini, TemporalType.DATE);
        query.setParameter("fim", dfim, TemporalType.DATE);
        return query.getResultList();
    }
}
