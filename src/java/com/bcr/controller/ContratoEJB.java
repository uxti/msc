/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.ComunicadoImplantacao;
import com.bcr.model.Contrato;
import com.bcr.util.Bcrutils;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class ContratoEJB extends FacadeEJB<Contrato> {

    public ContratoEJB() {
        super(Contrato.class);
    }
    
    public void salvarComunicado(ComunicadoImplantacao ci){
        em.merge(ci);
    }
    
    public List<Contrato> listarContratos(){
        Query query = em.createQuery("Select c From Contrato c");
        return query.getResultList();
    }

    public Integer ultimoContrato() {

        Query query = em.createQuery("Select MAX(c.idContrato)  From Contrato c");
        Object obj = query.getSingleResult();
        if (obj == null) {
            return 0;
        }
        return (Integer) obj;
    }
    
    public List<ComunicadoImplantacao> listarComunicadosNaoGeradosDoMes(){
        Query query = em.createQuery("Select c From ComunicadoImplantacao c where (c.mesGerado is null OR c.mesGerado between :ini and :fim )");
        query.setParameter("ini", Bcrutils.primeiroDiaDoMes());
        query.setParameter("fim", Bcrutils.ultimoDiaDoMes());
        return query.getResultList();
    }
}
