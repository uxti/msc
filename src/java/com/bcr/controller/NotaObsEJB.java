/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.NotaObservacao;
import javax.ejb.Stateless;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class NotaObsEJB extends FacadeEJB<NotaObservacao>{

    public NotaObsEJB(){
        super(NotaObservacao.class);
    }
}
