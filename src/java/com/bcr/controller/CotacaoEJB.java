/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Cotacao;
import com.bcr.model.CotacaoItem;
import com.bcr.model.CotacaoItemFornecedor;
import com.bcr.model.RequisicaoMaterialItem;
import com.bcr.model.Usuario;
import com.bcr.util.Bcrutils;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class CotacaoEJB extends FacadeEJB<Cotacao> {

    public CotacaoEJB() {
        super(Cotacao.class);
    }

    public void finalizarCotacao(Cotacao cotacao) {
        BigDecimal vlrTotal = new BigDecimal(BigInteger.ZERO);
        try {
            for (CotacaoItem cotacaoItem : cotacao.getCotacaoItemList()) {
                vlrTotal = vlrTotal.add(cotacaoItem.getValorTotal());
            }
        } catch (NullPointerException e) {
            vlrTotal = new BigDecimal(BigInteger.ZERO);
        }

        try {
            cotacao.setValortotal(vlrTotal);
            em.merge(cotacao);
            List<String> itensMudarStatus = new ArrayList<String>();
            Pattern pattern = Pattern.compile("([^,])+");
            for (CotacaoItem cotacaoItem : cotacao.getCotacaoItemList()) {
                cotacaoItem.setIdCotacao(cotacao);
                cotacaoItem.setIdEmpresa(cotacao.getIdUsuario().getIdEmpresa());
                Matcher matcher = pattern.matcher(cotacaoItem.getRequisicoesReferentes());
                while (matcher.find()) {
                    itensMudarStatus.add(cotacaoItem.getRequisicoesReferentes().substring(matcher.start(), matcher.end()));
                }
                em.merge(cotacaoItem);
                for (CotacaoItemFornecedor cif : cotacaoItem.getCotacaoItemFornecedorList()) {
                    if (cif.getIdFornecedor() != null) {
                        cif.setIdCotacao(cotacao);
                        cif.setIdCotacaoItem(cotacaoItem);
                        em.merge(cif);
                    }
                }
            }
            for (String s : itensMudarStatus) {
                try {
                    String id = s.trim();
                    Query query = em.createQuery("UPDATE RequisicaoMaterialItem rmi set rmi.status = 'Cotado' where rmi.idRequisicaoMaterialItem = " + id);
                    query.executeUpdate();
                } catch (Exception e) {
                    System.out.println("Subiu uma exceção no atualizar os statu das requisicoesMateriaisItens");
                }
            }
        } catch (EJBException e) {
            Bcrutils.descobreErroDeEJBException(e);
        }
    }

    public void finalizarCotacaoNormal(Cotacao cotacao) {
        BigDecimal vlrTotal = new BigDecimal(BigInteger.ZERO);
        try {
            for (CotacaoItem cotacaoItem : cotacao.getCotacaoItemList()) {
                vlrTotal = vlrTotal.add(cotacaoItem.getValorTotal());
            }
        } catch (NullPointerException e) {
            vlrTotal = new BigDecimal(BigInteger.ZERO);
        }
        int situacao = 0;
        for (CotacaoItem cotacaoItem : cotacao.getCotacaoItemList()) {
            if (cotacaoItem.getIdFornecedorSelecionado() == null) {
                situacao = 1;
                System.out.println("Ta falando que este aqui esta nulo!" + cotacaoItem.getIdProduto().getDescricao());
            }
        }
        if (situacao == 1) {
            cotacao.setStatus("Cotando");
        } else {
            cotacao.setStatus("Aguardando Pedido");
        }

        try {
            cotacao.setValortotal(vlrTotal);
            em.merge(cotacao);
            for (CotacaoItem cotacaoItem : cotacao.getCotacaoItemList()) {
                cotacaoItem.setIdCotacao(cotacao);
                cotacaoItem.setIdEmpresa(cotacao.getIdUsuario().getIdEmpresa());
                em.merge(cotacaoItem);
                for (CotacaoItemFornecedor cif : cotacaoItem.getCotacaoItemFornecedorList()) {
                    cif.setIdCotacao(cotacao);
                    cif.setIdCotacaoItem(cotacaoItem);
                    em.merge(cif);
                }
            }
        } catch (EJBException e) {
            Bcrutils.descobreErroDeEJBException(e);
        }
    }

    public List<Cotacao> listaCotacoesNaoDeLote() {
        Query query = em.createQuery("Select c From Cotacao c ");
        return query.getResultList();
    }

    public List<Cotacao> listaCotacoesDeLote() {
        Query query = em.createQuery("Select  c.lote, c.idUsuario, c.dtCadastro , c.status From Cotacao c where c.lote IS NOT NULL  GROUP BY c.lote , c.idUsuario, c.dtCadastro , c.status");
        List<Object[]> objs = query.getResultList();
        List<Cotacao> cotacoes = new ArrayList<Cotacao>();
        for (Object[] obj : objs) {
            Cotacao c = new Cotacao();
            c.setLote((String) obj[0]);
            c.setIdUsuario((Usuario) obj[1]);
            c.setDtCadastro((Date) obj[2]);
            c.setStatus((String) obj[3]);
            cotacoes.add(c);
        }
        return cotacoes;
    }

    public List<Cotacao> listarCotacoesPorLote(String lote, String status) {
        Query query = em.createQuery("Select c From Cotacao c where c.lote = :lote and c.status = :status");
        query.setParameter("lote", lote);
        query.setParameter("status", status);
        return query.getResultList();
    }

    public String retornaUltimoLote(String param) {
        System.out.println("veio esse parametro " + param);
        Query query = em.createQuery("Select c.lote From Cotacao c where c.lote LIKE :param ORDER BY c.lote DESC");
        query.setMaxResults(1);
        query.setParameter("param", param + "%");
        String retorno = "";
        try {
            retorno = (String) query.getSingleResult();
            if (retorno.equals("") || retorno == null) {
                return "01";
            } else {
                Integer i = Integer.parseInt(retorno.substring(retorno.length() - 2)) + 1;
                String fim = "0" + i.toString();
                return fim;
            }
        } catch (NoResultException e) {
            return "01";
        }



    }

    public Long TotalProdutoPendentesCotacao() {
        Query query = em.createQuery("Select COUNT(ri.idRequisicaoMaterialItem) From RequisicaoMaterialItem ri where ri.status = 'Cotação'");
        return (Long) query.getSingleResult();
    }

    public List<RequisicaoMaterialItem> itensPendentes() {
        Query query = em.createQuery("Select ri From RequisicaoMaterialItem ri where ri.status = 'Cotação'");
        return query.getResultList();
    }

    public void excluirCotacaoItem(CotacaoItem c) {
        if (!c.getCotacaoItemFornecedorList().isEmpty()) {
            for (CotacaoItemFornecedor cif : c.getCotacaoItemFornecedorList()) {
                cif = em.getReference(CotacaoItemFornecedor.class, cif.getIDCotacaoItemFornecedor());
                em.remove(cif);
            }
        }
        c = em.getReference(CotacaoItem.class, c.getIdCotacaoItem());
        em.remove(c);
    }

    public void excluirCotacaoFornecedor(CotacaoItemFornecedor cif) {
        cif = em.getReference(CotacaoItemFornecedor.class, cif.getIDCotacaoItemFornecedor());
        em.remove(cif);
    }
}
