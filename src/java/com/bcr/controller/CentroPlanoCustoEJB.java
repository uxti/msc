/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Centrocustoplanocontas;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class CentroPlanoCustoEJB extends FacadeEJB<Centrocustoplanocontas> {

    public CentroPlanoCustoEJB(){
        super(Centrocustoplanocontas.class);
    }

}
