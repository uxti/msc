/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Cliente;
import com.bcr.model.RequisicaoMaterial;
import com.bcr.model.RequisicaoMaterialItem;
import com.bcr.util.Bcrutils;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Charles
 */
@Stateless
public class RequisicaoMaterialEJB extends FacadeEJB<RequisicaoMaterial> {

    public RequisicaoMaterialEJB() {
        super(RequisicaoMaterial.class);
    }

    public void atualizarItem(RequisicaoMaterialItem ri) {
        em.merge(ri);
    }

    public void SalvarRequisicao(RequisicaoMaterial rm) {
        em.merge(rm);
        Collections.reverse(rm.getRequisicaoMaterialItemList());
        for (RequisicaoMaterialItem rmi : rm.getRequisicaoMaterialItemList()) {
            rmi.setIdRequisicaoMaterial(rm);
            em.merge(rmi);
        }
    }

    public RequisicaoMaterialItem selecionarRequisicaoPorItem(Integer id) {
        Query query = em.createQuery("SELECT c From RequisicaoMaterialItem c where c.idRequisicaoMaterialItem = :req");
        query.setParameter("req", id);
        return (RequisicaoMaterialItem) query.getSingleResult();
    }

    public RequisicaoMaterial pesquisarRequisicaoPorID(Integer id) {
        Query query = em.createQuery("Select r From RequisicaoMaterialItem r where r.idRequisicaoMaterialItem = :id");
        query.setParameter("id", id);
        RequisicaoMaterialItem rmi = (RequisicaoMaterialItem) query.getSingleResult();
        return (RequisicaoMaterial) em.createQuery("Select r From RequisicaoMaterial r where r.idRequisicaoMaterial = :id").setParameter("id", rmi.getIdRequisicaoMaterial().getIdRequisicaoMaterial()).getSingleResult();
    }

    public List<RequisicaoMaterial> listarRequisaoMateriais() {
        Query query = em.createQuery("Select c From RequisicaoMaterial c ORDER BY c.dtCadastro");
        return query.getResultList();
    }

    public List<RequisicaoMaterial> listarRequisaoMateriais(Cliente c, Date ini, Date fim, String status) {
        StringBuilder sb = new StringBuilder();
        if (ini == null) {
            ini = Bcrutils.primeiroDiaDoMes();
        }
        if (fim == null) {
            fim = Bcrutils.ultimoDiaDoMes();
        }
        if (c != null) {
            sb.append(" and c.idCliente.idCliente = ").append(c.getIdCliente());
        }
        if (!"t".equals(status) && status != null) {
            sb.append(" and c.status = ").append(status);
        }
        Query query = em.createQuery("Select c From RequisicaoMaterial  c  where c.dtCadastro BETWEEN :ini and :fim " + sb + "  ORDER BY c.dtCadastro");
        query.setParameter("ini", ini);
        query.setParameter("fim", fim);
        return query.getResultList();
    }

    public List<RequisicaoMaterial> listarRequisicoesParaAnalise() {
        Query query = em.createQuery("Select c From RequisicaoMaterial c WHERE c.status= 'Aguardando Análise'");
        return query.getResultList();
    }

    public List<RequisicaoMaterial> listarRequisicoesParaAlmoxarifado() {
        Query query = em.createQuery("Select c From RequisicaoMaterial c WHERE c.status= 'Análise de Almoxarifado' or c.status = 'Em Cotação' or c.status = 'Aguardando Saida' or c.status = 'Aguardando Pedido' ORDER BY c.idRequisicaoMaterial desc");
        return query.getResultList();
    }

    public List<RequisicaoMaterial> listarRequisicaoMaterialLazyModeWhere(int primeiro, int qtd, String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From RequisicaoMaterial p " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<RequisicaoMaterial> carregaRequisicaoDoMesAtual(Date pd, Date ud) {
        Query query = em.createQuery("Select c From RequisicaoMaterial c WHERE c.dtCadastro BETWEEN :pd and :ud ");
        query.setParameter("pd", pd, TemporalType.DATE);
        query.setParameter("ud", ud, TemporalType.DATE);

        return query.getResultList();
    }

    public Number contarRegistro(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(p.idRequisicaoMaterial) From RequisicaoMaterial p " + clausula);
        return (Number) query.getSingleResult();
    }
}
