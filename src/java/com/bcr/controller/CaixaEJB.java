/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Caixa;
import com.bcr.model.CaixaItem;
import com.bcr.model.ContasPagar;
import com.bcr.model.ContasReceber;
import com.bcr.model.Empresa;
import com.bcr.util.Bcrutils;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Renato
 */
@Stateless
public class CaixaEJB extends FacadeEJB<Caixa> {

    public CaixaEJB() {
        super(Caixa.class);
    }

    public List<CaixaItem> pesquisaMovimentoCaixaPorCaixa(Caixa caixa) {
        Query query = em.createQuery("Select c From CaixaItem c where c.idCaixa = :idc");
        query.setParameter("idc", caixa.getIdCaixa());
        return query.getResultList();
    }

    public List<CaixaItem> listaTodosMovimento() {
        Query query = em.createQuery("Select c From CaixaItem c ");
        return query.getResultList();
    }

    public int deletarMovimentoPorPagamento(ContasPagar p) {
        Query query = em.createQuery("Delete from CaixaItem c where c.idContasPagar.idContasPagar = :idp");
        query.setParameter("idp", p.getIdContasPagar());
        int res = query.executeUpdate();
        return res;
    }
    public int deletarMovimentoPorRecebimento(ContasReceber r) {
        Query query = em.createQuery("Delete from CaixaItem c where c.idContasReceber.idContasReceber = :idr");
        query.setParameter("idr", r.getIdContasReceber());
        int res = query.executeUpdate();
        return res;
    }

    public Long verificarExisteMovimentoCaixa(Date dt, Empresa emp) {
        Query query = em.createQuery("Select count(c) From Caixa c where c.dtCaixa = :dt and c.idEmpresa.idEmpresa = :ide");
        query.setParameter("ide", emp.getIdEmpresa());
        query.setParameter("dt", dt, TemporalType.DATE);
        return (Long) query.getSingleResult();
    }

    public Caixa retornaCaixaPorData(Date dt, Empresa emp) {
        Query query = em.createQuery("Select c From Caixa c where c.dtCaixa = :dt and c.idEmpresa.idEmpresa = :ide ORDER BY c.dtCaixa DESC");
        query.setParameter("ide", emp.getIdEmpresa());
        query.setParameter("dt", dt, TemporalType.DATE);
        try {
            return (Caixa) query.getSingleResult();
        } catch (NoResultException e) {
            Caixa c = new Caixa();
            c.setDtCaixa(dt);
            c.setIdEmpresa(emp);
            em.persist(c);
            return c;
        }
    }
    
    
    public List<Caixa> listaCaixasPorData( Empresa emp){
        return em.createQuery("Select c From Caixa c  where c.idEmpresa.idEmpresa = :ide order BY c.dtCaixa asc").setParameter("ide", emp.getIdEmpresa()).getResultList();
    }
    
    public List<Caixa> listaCaixasParaProcessar( Empresa emp){
        return em.createQuery("Select c From Caixa c  where  c.idEmpresa.idEmpresa = :ide  order BY c.dtCaixa asc")
                .setParameter("ide", emp.getIdEmpresa())
                .getResultList();
    }
    public List<Caixa> listaCaixasParaProcessar20DiasAntes( Empresa emp){
        Date dataParaProcessar = Bcrutils.addDia(new Date(), -32);
        return em.createQuery("Select c From Caixa c  where  c.idEmpresa.idEmpresa = :ide and c.dtCaixa > :dt order BY c.dtCaixa asc")
                .setParameter("ide", emp.getIdEmpresa())
                .setParameter("dt", dataParaProcessar, TemporalType.DATE)
                .getResultList();
    }

    public void criaNovoCaixa(Date d, Empresa e) {
        Caixa c = new Caixa();
        c.setIdEmpresa(e);
        c.setDtCaixa(d);
        em.merge(c);
    }

    public void inserirMovimentacao(CaixaItem ci) {
        em.merge(ci);
    }
    
    public List<Caixa> listaCaixas(Empresa emp){
        return em.createQuery("Select c From Caixa c  where c.idEmpresa.idEmpresa = :ide order BY c.dtCaixa desc").setParameter("ide", emp.getIdEmpresa()).getResultList();
    }
}
