/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Parametrizacao;
import com.bcr.util.Conexao;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class ConfiguracaoEJB extends Conexao{

   public boolean verificaExistenciaDeEmpresa(){
       Query query = em.createQuery("Select COUNT(c.idEmpresa) From Empresa c");
       Long total = (Long) query.getSingleResult();
       if(total == 0){
           return false;
       }else{
           return true;
       }
   }
   
   public void inserirEmpresaDemo(){
       Query query = em.createNativeQuery("insert into empresa (nome_fantasia)values('Empresa Demonstrativa')");
       query.executeUpdate();
   }
   public void inserirAcesso(){
       Query query = em.createNativeQuery("INSERT INTO acesso (ID_ACESSO, MODULO_FINANCEIRO, FINANCEIRO_CONTAS_PAGAR, FINANCEIRO_CONTAS_RECEBER, FINANCEIRO_CAIXA, FINANCEIRO_BANCO, CONTAS_PAGAR_PESQUISAR, CONTAS_PAGAR_INSERIR, CONTAS_PAGAR_PAGAR, CONTAS_PAGAR_EXCLUIR, CONTAS_PAGAR_ESTORNAR, CONTAS_PAGAR_VER, CONTAS_RECEBER_PESQUISAR, CONTAS_RECEBER_INSERIR, CONTAS_RECEBER_RECEBER, CONTAS_RECEBER_EXCLUIR, CONTAS_RECEBER_ESTORNAR, CONTAS_RECEBER_VER, CAIXA_PESQUISAR, CAIXA_SANGRIA, CAIXA_SUPRIMENTO, CAIXA_TRANSFERENCIA, BANCO_PESQUISAR, BANCO_SAQUE, BANCO_DEPOSITO, BANCO_TRANSFERENCIA, GESTAO_RESULTADO, FINANCEIRO_ACESSO_TOTAL) VALUES (1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);");
       query.executeUpdate();
   }
   public void inserirPerfil(){
       Query query = em.createNativeQuery("INSERT INTO perfil (role, descricao, ID_ACESSO) VALUES ('ROLE_ROOT', 'ADMINISTRADOR', 1)");
       query.executeUpdate();
   }
   public void inserirUsuario(){
       Query query = em.createNativeQuery("INSERT INTO usuario (id_perfil,id_empresa, nome, login, senha, dt_cadastro, dt_ultima_atualizacao, dt_ultimo_acesso, inativo) VALUES (1, 1 ,'ADMINISTRADOR', 'admin', '123', '2015-02-18 11:43:15', '2015-02-18 11:43:16', '2015-02-18 11:43:16', '1')");
       query.executeUpdate();
   }   
   
   public void executarConfiguracaoBasica(){
       inserirEmpresaDemo();
       inserirAcesso();
       inserirPerfil();
       inserirUsuario();
   }
   
   public void salvarParametrizacao(Parametrizacao p) {
        em.persist(p);
        em.flush();
        em.refresh(p);
   }
   
   public void atualizarParametrizacao(Parametrizacao p) {
        em.merge(p);
   }
   
   public Parametrizacao selecionarParametrizacao(Integer id){
       Query query = em.createQuery("SELECT p FROM Parametrizacao p where p.idParametrizacao = :id");
       query.setParameter("id", id);
       return (Parametrizacao) query.getSingleResult();
       
   }
   
}
