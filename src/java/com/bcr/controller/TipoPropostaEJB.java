/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.TipoProposta;
import javax.ejb.Stateless;

/**
 *
 * @author Charles
 */
@Stateless
public class TipoPropostaEJB extends FacadeEJB<TipoProposta> {
    
    public TipoPropostaEJB(){
        super(TipoProposta.class);
    }
}
