/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.GrupoAtivo;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class AtivoGrupoEJB extends FacadeEJB<GrupoAtivo> {

    public AtivoGrupoEJB() {
        super(GrupoAtivo.class);
    }
}
