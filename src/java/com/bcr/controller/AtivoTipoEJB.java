/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Ativo;
import com.bcr.model.TipoAtivo;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class AtivoTipoEJB extends FacadeEJB<TipoAtivo> {

    public AtivoTipoEJB() {
        super(TipoAtivo.class);
    }
}
