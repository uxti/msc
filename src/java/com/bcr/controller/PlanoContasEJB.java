/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.PlanoContas;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class PlanoContasEJB extends FacadeEJB<PlanoContas> {

    public PlanoContasEJB() {
        super(PlanoContas.class);
    }

    public List<PlanoContas> listaPlanosContasSintetica() {
        Query query = em.createQuery("Select p From PlanoContas p WHERE p.tipo= 'S' ORDER BY p.codigo asc");
        return query.getResultList();
    }
    public List<PlanoContas> listaPlanosContasAnalitica() {
        Query query = em.createQuery("Select p From PlanoContas p WHERE p.tipo= 'A' ORDER BY p.codigo asc");
        return query.getResultList();
    }
    public List<PlanoContas> listaPlanosContas() {
        Query query = em.createQuery("Select p From PlanoContas p ORDER BY p.codigo asc");
        return query.getResultList();
    }
    public List<PlanoContas> listaPlanosContasCentroCustos() {
        Query query = em.createQuery("Select p From PlanoContas p where p.codigo LIKE '5%' ORDER BY p.codigo asc");
        return query.getResultList();
    }
    public List<PlanoContas> listaPlanosContasCredito() {
        Query query = em.createQuery("Select p From PlanoContas p where p.creditodebito='C' ORDER BY p.codigo asc");
        return query.getResultList();
    }
    public List<PlanoContas> listaPlanosContasDebito() {
        Query query = em.createQuery("Select p From PlanoContas p where p.creditodebito='D' ORDER BY p.codigo asc");
        return query.getResultList();
    }
}
