/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Cotacao;
import com.bcr.model.PedidoCompra;
import com.bcr.model.PedidoCompraItem;
import com.bcr.model.RequisicaoMaterial;
import com.bcr.model.RequisicaoMaterialItem;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author Renato
 */
@Stateless
public class PedidoCompraEJB extends FacadeEJB<PedidoCompra> {

    @EJB
    RequisicaoMaterialEJB rmEJB;
    @EJB
    ProdutoEJB pEJB;

    public PedidoCompraEJB() {
        super(PedidoCompra.class);
    }

    public void SalvarPedidoAvulso(PedidoCompra p) {
        em.merge(p);
        List<PedidoCompraItem> listaItens = new ArrayList<PedidoCompraItem>();
        for (PedidoCompraItem pci : p.getPedidoCompraItemList()) {
            pci.setIdPedidoCompra(p);
            if (pci.getQtdChegada() == null) {
                pci.setQtdChegada(new BigDecimal(BigInteger.ZERO));
            }
            pci.setQtdRestante(pci.getQtdSolicitada().subtract(pci.getQtdChegada()));
            if (pci.getStatus() == null) {
                pci.setStatus("S");
            }
            em.merge(pci);
            if (p.getMovimentaEstoque().equals("S")) {
                pEJB.EntradaEstoque(pci.getIdProduto(), pci.getQtdChegada());
                if (pci.getQtdChegada().longValue() >= pci.getQtdSolicitada().longValue() ) {
                    List<String> requisicoes = new ArrayList<String>();
                    Pattern pattern = Pattern.compile("([^,])+");
                    Matcher matcher = pattern.matcher(pci.getDescricao());
                    while (matcher.find()) {
                        requisicoes.add(pci.getIdCotacaoItem().getRequisicoesReferentes().substring(matcher.start(), matcher.end()));
                        System.out.println(pci.getIdCotacaoItem().getRequisicoesReferentes().substring(matcher.start(), matcher.end()));
                    }
                    for (String s : requisicoes) {
                        String id = s.trim();
                        Query query2 = em.createQuery("UPDATE RequisicaoMaterialItem rmi SET rmi.status = 'Aguardando Saida' where rmi.idRequisicaoMaterialItem = " + id);
                        query2.executeUpdate();
                        RequisicaoMaterial rm = rmEJB.selecionarRequisicaoPorItem(Integer.parseInt(id)).getIdRequisicaoMaterial();
                        verificaStatusDaRequisicao(rm);
                    }
                } else {
                    System.out.println("adicionando item com qntidade chegada menor!");
                    listaItens.add(pci);
                }
            }
        }
        if (listaItens.size() > 0) {
            PedidoCompra pc = new PedidoCompra();
            PedidoCompraItem old = listaItens.get(0);
            pc.setDtEmissao(new Date());
            pc.setStatus("Aguardando Chegada");
            pc.setMovimentaEstoque("N");
            pc.setIdEmpresa(old.getIdPedidoCompra().getIdEmpresa());
            pc.setIdFornecedor(old.getIdPedidoCompra().getIdFornecedor());
//            pc.setLoteReferente(old.getIdPedidoCompra().getLoteReferente());
            for (PedidoCompraItem pci : listaItens) {
                PedidoCompraItem itemNew = new PedidoCompraItem();
                itemNew.setDescricao(pci.getDescricao());
                itemNew.setStatus("S");
                itemNew.setQtdSolicitada(pci.getQtdRestante());
                itemNew.setIdProduto(pci.getIdProduto());
                itemNew.setValorUnitario(pci.getValorUnitario());
                itemNew.setValorTotal(pci.getQtdSolicitada().multiply(itemNew.getValorUnitario()));
                itemNew.setIdCotacaoItem(pci.getIdCotacaoItem());
                try {
                    pc.getPedidoCompraItemList().add(itemNew);
                } catch (Exception e) {
                    pc.setPedidoCompraItemList(new ArrayList<PedidoCompraItem>());
                    pc.getPedidoCompraItemList().add(itemNew);
                }
            }
            SalvarPedido(pc);
        }
    }

    public void SalvarPedido(PedidoCompra p) {
        em.merge(p);
        for (PedidoCompraItem pci : p.getPedidoCompraItemList()) {
            pci.setIdPedidoCompra(p);
            Cotacao c = pci.getIdCotacaoItem().getIdCotacao();
            c.setStatus("Aguardando Chegada");
            em.merge(c);
            em.merge(pci);
        }

        for (PedidoCompraItem pci : p.getPedidoCompraItemList()) {
            List<String> requisicoes = new ArrayList<String>();
            Pattern pattern = Pattern.compile("([^,])+");
            Matcher matcher = pattern.matcher(pci.getDescricao());
            while (matcher.find()) {
                requisicoes.add(pci.getIdCotacaoItem().getRequisicoesReferentes().substring(matcher.start(), matcher.end()));
                System.out.println(pci.getIdCotacaoItem().getRequisicoesReferentes().substring(matcher.start(), matcher.end()));
            }
            for (String s : requisicoes) {
                String id = s.trim();
                Query query2 = em.createQuery("UPDATE RequisicaoMaterialItem rmi set rmi.status = 'Aguardando Chegada' where rmi.idRequisicaoMaterialItem = " + id);
                query2.executeUpdate();
                RequisicaoMaterial rm = rmEJB.selecionarRequisicaoPorItem(Integer.parseInt(id)).getIdRequisicaoMaterial();
                System.out.println("A Requisição: " + rm.getIdRequisicaoMaterial());
                verificaStatusDaRequisicao(rm);
            }
        }
    }

    public void verificaStatusDaRequisicao(RequisicaoMaterial r) {
        boolean bol = false;
        for (RequisicaoMaterialItem rmi : r.getRequisicaoMaterialItemList()) {
            if (rmi.getStatus().equals("Aguardando")) {
                bol = true;
                break;
            }
        }
        if (bol) {
            if (!r.getStatus().equals("Análise de Almoxarifado")) {
                r.setStatus("Análise de Almoxarifado");
                rmEJB.Atualizar(r);
            }
        } else {
            boolean bol2 = false;
            for (RequisicaoMaterialItem rmi : r.getRequisicaoMaterialItemList()) {
                if (rmi.getStatus().equals("Aceito")) {
                    bol2 = true;
                } else {
                    bol2 = false;
                    break;
                }
            }
            if (bol2) {
                r.setStatus("Aguardando Saída");
                rmEJB.Atualizar(r);
            } else {
                r.setStatus("Em Cotação");
                rmEJB.Atualizar(r);
            }
        }

        boolean bol3 = false;
        for (RequisicaoMaterialItem rmi : r.getRequisicaoMaterialItemList()) {
            if (!rmi.getStatus().equals("Aguardando Chegada")) {
                bol = true;
                break;
            }
        }
        if (bol3 == false) {
            r.setStatus("Aguardando Pedido");
            rmEJB.Atualizar(r);
        }
    }

    public void AtualizarPedidoAposNota(PedidoCompra p) {
        em.merge(p);
        for (PedidoCompraItem pci : p.getPedidoCompraItemList()) {
            pci.setIdPedidoCompra(p);
            Cotacao c = pci.getIdCotacaoItem().getIdCotacao();
            c.setStatus("Finalizado");
            em.merge(c);
            em.merge(pci);
        }

        for (PedidoCompraItem pci : p.getPedidoCompraItemList()) {
            List<String> requisicoes = new ArrayList<String>();
            Pattern pattern = Pattern.compile("([^,])+");
            Matcher matcher = pattern.matcher(pci.getDescricao());
            while (matcher.find()) {
                requisicoes.add(pci.getIdCotacaoItem().getRequisicoesReferentes().substring(matcher.start(), matcher.end()));
                System.out.println(pci.getIdCotacaoItem().getRequisicoesReferentes().substring(matcher.start(), matcher.end()));
            }
            for (String s : requisicoes) {
                String id = s;
                System.out.println("Esta atualizando o item da requisicao " + id);
                Query query2 = em.createQuery("UPDATE RequisicaoMaterialItem rmi set rmi.status = 'Aguardando Saida' where rmi.idRequisicaoMaterialItem = " + id);
                query2.executeUpdate();
            }
        }
    }

    public void excluirPedido(PedidoCompra pc) {
        pc = em.getReference(PedidoCompra.class, pc.getIdPedidoCompra());
        for (PedidoCompraItem pci : pc.getPedidoCompraItemList()) {
            pci = em.getReference(PedidoCompraItem.class, pci.getIdPedidoCompraItem());
            em.remove(pci);
        }
        em.remove(pc);
    }

    public void atualizarStatusCotacao(String lote) {
        Query query = em.createNativeQuery("UPDATE Cotacao  set status='Aguardando Chegada' where lote = '" + lote + "'");
        query.executeUpdate();
    }

    public List<PedidoCompra> listarPedidosNaoImportados() {
        Query query = em.createQuery("Select c From PedidoCompra c where c.status = 'Analisado'");
        return query.getResultList();
    }

    public List<PedidoCompra> completeMethod(String id, Integer idFornecedor) {
        Integer idd = Integer.parseInt(id);
        Query query = em.createQuery("Select p From PedidoCompra p where p.idPedidoCompra = :id and p.status = 'Analisado' and p.idFornecedor.idFornecedor = :idf");
        query.setParameter("id", idd);
        query.setParameter("idf", idFornecedor);
        return query.getResultList();
    }
}
