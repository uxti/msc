/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Cfop;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class CfopEJB extends FacadeEJB<Cfop> {

    public CfopEJB() {
        super(Cfop.class);
    }

    public List<Cfop> autoCompleteCfop(String param) {
        Query query = em.createQuery("Select c From Cfop c where c.cfopExterno LIKE :par or c.cfopInterno LIKE :par or c.descricao like :par");
        query.setParameter("par", param+"%");
        return query.getResultList();
    }
}
