/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Banco;
import com.bcr.model.Cheque;
import com.bcr.model.ContaCorrente;
import com.bcr.model.ContasPagar;
import com.bcr.model.ContasReceber;
import com.bcr.model.MovimentacaoBancaria;
import com.bcr.pojo.ExtratoBancario;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import org.apache.commons.beanutils.BeanComparator;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class BancoEJB extends FacadeEJB<Banco> {

    public BancoEJB() {
        super(Banco.class);
    }

    public void inserirMovimentoBancario(MovimentacaoBancaria movimentacaoBancaria) {
        em.merge(movimentacaoBancaria);
    }

    public int deletarMovimentoBancarioPorPagamento(ContasPagar cp) {
        Query query = em.createQuery("Delete from MovimentacaoBancaria m where m.idContasPagar.idContasPagar = :idp");
        query.setParameter("idp", cp.getIdContasPagar());
        int res = query.executeUpdate();
        return res;
    }

    public int deletarMovimentoBancarioPorRecebimento(ContasReceber cr) {
        Query query = em.createQuery("Delete from MovimentacaoBancaria m where m.idContasReceber.idContasReceber = :idp");
        query.setParameter("idp", cr.getIdContasReceber());
        int res = query.executeUpdate();
        return res;
    }

    public List<MovimentacaoBancaria> pesquisarMovimentacaoPorContaCorrente(ContaCorrente cc, Date dIni, Date dFim) {
        Query query = em.createQuery("SELECT m FROM MovimentacaoBancaria m where m.idBanco.idBanco = :idb and m.idContaCorrente.idContaCorrente = :idc and m.dtMovimentacaoBancariaItem BETWEEN :ini and :fim");
        query.setParameter("idb", cc.getIdBanco().getIdBanco());
        query.setParameter("idc", cc.getIdContaCorrente());
        query.setParameter("ini", dIni, TemporalType.DATE);
        query.setParameter("fim", dFim, TemporalType.DATE);
        return query.getResultList();
    }

    public List<ExtratoBancario> imprimirExtratoBancario(ContaCorrente cc, Date ini, Date fim) {
        List<MovimentacaoBancaria> movb = new ArrayList<MovimentacaoBancaria>();
        List<Cheque> cheqs = new ArrayList<Cheque>();
        List<ExtratoBancario> extrato = new ArrayList<ExtratoBancario>();
        BigDecimal saldoAnteriorDebito = new BigDecimal(BigInteger.ZERO);
        try {
            saldoAnteriorDebito = (BigDecimal) em.createQuery("Select SUM(m.valor) From MovimentacaoBancaria m where m.dtMovimentacaoBancariaItem < :ini and m.idBanco.idBanco = :idb and m.idContaCorrente.idContaCorrente = :idc and m.tipo = 'D'").setParameter("ini", ini).setParameter("idb", cc.getIdBanco().getIdBanco()).setParameter("idc", cc.getIdContaCorrente()).getSingleResult();
            System.out.println(saldoAnteriorDebito);
        } catch (NoResultException e) {
            saldoAnteriorDebito = new BigDecimal(BigInteger.ZERO);
        }
        BigDecimal saldoAnteriorCredito = new BigDecimal(BigInteger.ZERO);
        try {
            saldoAnteriorCredito = (BigDecimal) em.createQuery("Select SUM(m.valor) From MovimentacaoBancaria m where m.dtMovimentacaoBancariaItem < :ini and m.idBanco.idBanco = :idb and m.idContaCorrente.idContaCorrente = :idc and m.tipo = 'C'").setParameter("ini", ini).setParameter("idb", cc.getIdBanco().getIdBanco()).setParameter("idc", cc.getIdContaCorrente()).getSingleResult();
        } catch (NoResultException e) {
            saldoAnteriorCredito = new BigDecimal(BigInteger.ZERO);
        }
        BigDecimal saldoAnterior = saldoAnteriorCredito.subtract(saldoAnteriorDebito);
        movb = em.createQuery("Select m From MovimentacaoBancaria m where m.dtMovimentacaoBancariaItem BETWEEN :ini and :fim and m.idBanco.idBanco = :idb and m.idContaCorrente.idContaCorrente = :idc and m.tipo = 'D'").setParameter("ini", ini, TemporalType.DATE).setParameter("fim", fim, TemporalType.DATE).setParameter("idb", cc.getIdBanco().getIdBanco()).setParameter("idc", cc.getIdContaCorrente()).getResultList();
        cheqs = em.createQuery("Select c From Cheque c where c.tipo != 'Cancelado' and c.compensado = 'N' and c.idContaCorrente.idContaCorrente = :idc and c.dtCompensacaoVencimento BETWEEN :ini and :fim ").setParameter("idc", cc.getIdContaCorrente()).setParameter("ini", ini).setParameter("fim", fim).getResultList();

        ExtratoBancario eb1 = new ExtratoBancario();
        eb1.setData(ini);
        eb1.setHistorico("Saldo Anterior");
//        eb1.setValor(saldoAnterior);
        eb1.setSaldo(saldoAnterior);
        eb1.setTipoMovimentacao("C");
        extrato.add(eb1);

        for (MovimentacaoBancaria mb : movb) {
            ExtratoBancario eb = new ExtratoBancario();
            eb.setBanco(mb.getIdBanco().getNome());
            eb.setConta(mb.getIdContaCorrente().getNumConta());
            eb.setData(mb.getDtMovimentacaoBancariaItem());
            eb.setHistorico(mb.getHistorico());
            eb.setMes_referente(ini);
            eb.setNome(mb.getIdContaCorrente().getGestor());
            eb.setValor(mb.getValor());
            eb.setTipoMovimentacao(mb.getTipo());
            eb.setSaldo(new BigDecimal(BigInteger.ZERO));
//            eb.setSaldo_anterior(saldoAnterior);
            extrato.add(eb);
        }
        for (Cheque c : cheqs) {
            ExtratoBancario eb = new ExtratoBancario();
            eb.setBanco(c.getIdContaCorrente().getIdBanco().getNome());
            eb.setConta(c.getIdContaCorrente().getNumConta());
            eb.setData(c.getDtCompensacaoVencimento());
            eb.setHistorico(c.getNominal());
            eb.setMes_referente(ini);
//            eb.setNome(mb.getIdContaCorrente().getGestor());
            eb.setValor(c.getValor());
            eb.setTipoMovimentacao("D");
            eb.setSaldo_anterior(new BigDecimal(BigInteger.ZERO));
            eb.setSaldo(new BigDecimal(BigInteger.ZERO));
            extrato.add(eb);
        }


        Comparator<ExtratoBancario> comp = new BeanComparator("data");
        Collections.sort(extrato, comp);

        int i = 0;
        for (ExtratoBancario eb : extrato) {
            System.out.println("Saldo: " + eb.getSaldo());
            System.out.println("Valor: " + eb.getValor());
            if (i <= 0) {
                i++;
            } else {
                if (eb.getTipoMovimentacao().equals("D")) {
                    eb.setSaldo(extrato.get(i - 1).getSaldo().subtract(eb.getValor()));
                } else {
                    eb.setSaldo(extrato.get(i - 1).getSaldo().add(eb.getValor()));
                }
                i++;
            }

        }
      
        ExtratoBancario eb2 = new ExtratoBancario();
        eb2.setData(fim);
        eb2.setHistorico("SALDO");
        eb2.setValor(null);
        eb2.setSaldo(extrato.get(extrato.size()-1).getSaldo());
        extrato.add(eb2);
        
        return extrato;
    }
}
