/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.CentroCusto;
import com.bcr.model.Cliente;
import com.bcr.model.ClienteResponsavel;
import com.bcr.model.Telefone;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class ClienteEJB extends FacadeEJB<Cliente> {

    public ClienteEJB() {
        super(Cliente.class);
    }

    public void Salvar(Cliente c, List<Telefone> telefones, List<ClienteResponsavel> responsaveis, List<CentroCusto> centros) {
        em.merge(c);
        for (Telefone t : telefones) {
            t.setIdCliente(c);
            em.merge(t);
        }
        for (ClienteResponsavel cr : responsaveis) {
            cr.setIdCliente(c);
            em.merge(cr);
        }

        for (CentroCusto cc : centros) {
            cc.setIdCliente(c);
            em.merge(cc);
        }
    }
    
    public Number contarRegistros(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(c.idCliente) From Cliente c " + clausula);
        return (Number) query.getSingleResult();
    }
    
     public List<Cliente> listarClientesLazy(int primeiro, int qtd, String clausula) {
        Query query = em.createQuery("Select c From Cliente c " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }
     
     public List<Cliente> buscarPorNome(String nome) {
        Query query = em.createQuery("Select c From Cliente c Where c.nome like :nome");
        query.setParameter("nome", "%" + nome + "%");
        return query.getResultList();
    }
     
     public List<CentroCusto> listaCentroCustoPorCliente(Cliente c){
         Query query = em.createQuery("Select c From CentroCusto c where c.idCliente.idCliente = :idc");
         query.setParameter("idc", c.getIdCliente());
         return query.getResultList();
     }
     
     public List<Cliente> completeListaCliente(String nome) {
        Query query = em.createQuery("Select c From Cliente c where c.inativo = 'N' and c.nome LIKE :nome or c.idCliente LIKE :id");
        query.setParameter("nome", nome + "%");
        query.setParameter("id", nome + "%");
        return query.getResultList();
    }
}
