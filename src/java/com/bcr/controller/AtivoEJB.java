/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Ativo;
import com.bcr.model.GrupoAtivo;
import com.bcr.pojo.Depreciacao;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class AtivoEJB extends FacadeEJB<Ativo> {

    public AtivoEJB() {
        super(Ativo.class);
    }

    public List<Ativo> autoCompleteAtivo(String par) {
        Query query = em.createQuery("Select a From Ativo a where a.descricao LIKE :desc or a.idAtivo LIKE :desc");
        query.setParameter("desc", par+"%");
        return query.getResultList();
    }

    public Number contarRegistros(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(c.idAtivo) From Ativo c " + clausula);
        return (Number) query.getSingleResult();
    }

    public List<Ativo> listarLazy(int primeiro, int qtd, String clausula) {
        System.out.println(clausula);
        Query query = em.createQuery("Select c From Ativo c " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }
    
    public List<Ativo> listarAtivosPorGrupo(GrupoAtivo ga){
        Query query = em.createQuery("Select c From Ativo c where c.idGrupoAtivo.idGrupoAtivo = :idg");
        query.setParameter("idg", ga.getIdGrupoAtivo());
        return query.getResultList();
    }
    
    
    
}
