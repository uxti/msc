/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Transportadora;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class TransportadoraEJB extends FacadeEJB<Transportadora> {

    public TransportadoraEJB() {
        super(Transportadora.class);
    }

    public List<Transportadora> autoCompleteTransportadora(String param) {
        Query query = em.createQuery("Select t From Transportadora t where t.nomeFantasia LIKE :par or t.razaoSocial LIKE :par");
        query.setParameter("par", param + "%");
        return query.getResultList();
    }
}
