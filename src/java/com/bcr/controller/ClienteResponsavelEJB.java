/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.ClienteResponsavel;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class ClienteResponsavelEJB extends FacadeEJB<ClienteResponsavel> {

    public ClienteResponsavelEJB() {
        super(ClienteResponsavel.class);
    }
}
