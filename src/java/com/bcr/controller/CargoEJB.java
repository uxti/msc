/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Cargo;
import javax.ejb.Stateless;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class CargoEJB extends FacadeEJB<Cargo> {

    public CargoEJB() {
        super(Cargo.class);
    }

}
