/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Cliente;
import com.bcr.model.LocalTrabalho;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class LocalTrabalhoEJB extends FacadeEJB<LocalTrabalho> {

    public LocalTrabalhoEJB() {
        super(LocalTrabalho.class);
    }
    
    public List<LocalTrabalho> ListarLocaisdeTrabalhoPorCentroCusto(Integer id) {
        System.out.println(id);
        Query query = em.createQuery("Select l FROM LocalTrabalho l WHERE l.idCentroCusto.idCliente.idCliente = :id");
        query.setParameter("id", id);
        return query.getResultList();
    }
    
    public List<LocalTrabalho> autoCompleteLocalTrabalho(String nome){
        Query query = em.createQuery("Select c From LocalTrabalho c where c.descricao LIKE :nome");
        query.setParameter("nome", nome+"%");
        return query.getResultList();
    }

}
