/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Grupo;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class GrupoEJB extends FacadeEJB<Grupo> {

    public GrupoEJB() {
        super(Grupo.class);
    }
}
