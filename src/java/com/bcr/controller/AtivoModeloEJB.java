/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.ModeloAtivo;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class AtivoModeloEJB extends FacadeEJB<ModeloAtivo> {

    public AtivoModeloEJB() {
        super(ModeloAtivo.class);
    }
}
