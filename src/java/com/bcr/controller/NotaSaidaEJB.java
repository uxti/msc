/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Centrocustoplanocontas;
import com.bcr.model.ContasReceber;
import com.bcr.model.NotaEntrada;
import com.bcr.model.NotaSaida;
import com.bcr.model.NotaSaidaItem;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Renato
 */
@Stateless
public class NotaSaidaEJB extends FacadeEJB<NotaSaida> {

    @EJB
    ProdutoEJB pEJB;

    public NotaSaidaEJB() {
        super(NotaSaida.class);
    }

    public void SalvarNovo(NotaSaida nfs) {
        em.merge(nfs);
        for (NotaSaidaItem nsi : nfs.getNotaSaidaItemList()) {
            nsi.setIdNotaSaida(nfs);
            if (nsi.getIdNotaSaidaItem() == null) {
                pEJB.SaidaEstoque(nsi.getIdProduto(), nsi.getQuantidade());
            } else {
                NotaSaidaItem itemAntes = selecionaItemNota(nsi);
                pEJB.EntradaEstoque(nsi.getIdProduto(), itemAntes.getQuantidade());
                pEJB.SaidaEstoque(nsi.getIdProduto(), nsi.getQuantidade());
            }
            em.merge(nsi);
        }
        for (ContasReceber cr : nfs.getContasReceberList()) {
            cr.setIdNotaSaida(nfs);
            em.merge(cr);
            if (cr.getCentrocustoplanocontasList().size() > 0) {
                for (Centrocustoplanocontas cpcp : cr.getCentrocustoplanocontasList()) {
                    cpcp.setDataMovimento(cr.getDtVencimento());
                    cpcp.setIdContasReceber(cr);
                    cpcp.setTipo("C");
                    em.merge(cpcp);
                }
            }
        }
    }

    public void excluirNovo(NotaSaida nfs) {
        for (NotaSaidaItem nsi : nfs.getNotaSaidaItemList()) {
            nsi = em.getReference(NotaSaidaItem.class, nsi.getIdNotaSaidaItem());
            pEJB.EntradaEstoque(nsi.getIdProduto(), nsi.getQuantidade());
            em.remove(nsi);
        }
        for (ContasReceber cr : nfs.getContasReceberList()) {
            cr = em.getReference(ContasReceber.class, cr.getIdContasReceber());
            em.remove(cr);
        }
        nfs = em.getReference(NotaSaida.class, nfs.getIdNotaSaida());
        em.remove(nfs);
    }

    public void excluirItem(NotaSaidaItem nsi) {
        nsi = em.getReference(NotaSaidaItem.class, nsi.getIdNotaSaidaItem());
        pEJB.EntradaEstoque(nsi.getIdProduto(), nsi.getQuantidade());
        em.remove(nsi);
    }

    public NotaSaidaItem selecionaItemNota(NotaSaidaItem nsi) {
        Query query = em.createQuery("Select n From NotaSaidaItem n where n.idNotaSaidaItem = :nei");
        query.setParameter("nei", nsi.getIdNotaSaidaItem());
        return (NotaSaidaItem) query.getSingleResult();
    }

    public List<NotaSaida> listarNotaSaidaLazyModeWhere(int primeiro, int qtd, String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select n From NotaSaida n " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public Number contarRegistro(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(n.idNotaSaida) From NotaSaida n " + clausula);
        return (Number) query.getSingleResult();
    }
}
