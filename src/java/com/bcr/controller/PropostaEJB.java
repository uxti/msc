/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Proposta;
import com.bcr.model.PropostaFinanceira;
import com.bcr.model.PropostaItem;
import com.bcr.model.PropostaItemProduto;
import com.bcr.model.PropostaPessoal;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class PropostaEJB extends FacadeEJB<PropostaFinanceira> {

    public PropostaEJB() {
        super(PropostaFinanceira.class);
    }

    public void SalvarPropostaCompleta(PropostaFinanceira propostaFinanceira) {
        em.merge(propostaFinanceira);
        for (Proposta proposta : propostaFinanceira.getPropostaList()) {
            proposta.setIdPropostaFinanceira(propostaFinanceira);
            em.merge(proposta);
            for (PropostaItem pi : proposta.getPropostaItemList()) {
                pi.setIdProposta(proposta);
                em.merge(pi);
            }
            for (PropostaItemProduto pip : proposta.getPropostaItemProdutoList()) {
                pip.setIdProposta(proposta);
                em.merge(pip);
            }
            for (PropostaPessoal pp : proposta.getPropostaPessoalList()) {
                pp.setIdProposta(proposta);
                em.merge(pp);
            }
        }
    }

    public void excluir(Proposta proposta) {
        for (PropostaItem item : proposta.getPropostaItemList()) {
            item = em.getReference(PropostaItem.class, item.getIdPropostaItem());
            em.remove(item);
        }
        for (PropostaItemProduto itemProd : proposta.getPropostaItemProdutoList()) {
            itemProd = em.getReference(PropostaItemProduto.class, itemProd.getIDPropostaItemProduto());
            em.remove(itemProd);
        }
        for (PropostaPessoal pp : proposta.getPropostaPessoalList()) {
            pp = em.getReference(PropostaPessoal.class, pp.getIdProposta());
            em.remove(pp);
        }
        proposta = em.getReference(Proposta.class, proposta.getIdProposta());
        em.remove(proposta);
    }

    public void excluirPropostaItem(PropostaItem pi) {
        pi = em.getReference(PropostaItem.class, pi.getIdPropostaItem());
        em.remove(pi);
    }

    public void excluirPropostaItemProduto(PropostaItemProduto pip) {
        pip = em.getReference(PropostaItemProduto.class, pip.getIDPropostaItemProduto());
        em.remove(pip);
    }

    public void excluirPropostaPessoa(PropostaPessoal pp) {
        pp = em.getReference(PropostaPessoal.class, pp.getIdPropostaPessoal());
        em.remove(pp);
    }
}
