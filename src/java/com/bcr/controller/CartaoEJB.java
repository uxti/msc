/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Cartao;
import javax.ejb.Stateless;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class CartaoEJB extends FacadeEJB<Cartao> {

    public CartaoEJB() {
        super(Cartao.class);
    }

}
