/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Planilha;
import com.bcr.model.PlanilhaItem;
import com.bcr.model.PlanilhaMateriais;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class PlanilhaProdutoEJB extends FacadeEJB<Planilha> {

    public PlanilhaProdutoEJB() {
        super(Planilha.class);
    }

    public void salvarCompleto(Planilha planilha) {
        em.merge(planilha);
        for (PlanilhaItem pi : planilha.getPlanilhaItemList()) {
            if (pi.getAcao() != null) {
                if (pi.getAcao().equals("%")) {
                    if (!pi.getPosicaoFinal().contains("P")) {
                        pi.setPosicaoFinal(pi.getPosicaoFinal() + "P");
                    }
                }
            }
            pi.setIdPlanilha(planilha);
            em.merge(pi);
        }
        for(PlanilhaMateriais pm : planilha.getPlanilhaMateriaisList()){
            pm.setIdPlanilha(planilha);
            em.merge(pm);
        }
    }

    public void excluirCompleto(Planilha planilha) {
        for (PlanilhaItem pi : planilha.getPlanilhaItemList()) {
            pi = em.getReference(PlanilhaItem.class, pi.getIdPlanilhaItem());
            em.remove(pi);
        }
        for (PlanilhaMateriais pi : planilha.getPlanilhaMateriaisList()) {
            pi = em.getReference(PlanilhaMateriais.class, pi.getIdPlanilhaMateriais());
            em.remove(pi);
        }
        planilha = em.getReference(Planilha.class, planilha.getIdPlanilha());
        em.remove(planilha);
    }

    public void excluirItemPlanilha(PlanilhaItem pi) {
        pi = em.getReference(PlanilhaItem.class, pi.getIdPlanilhaItem());
        em.remove(pi);
    }

    public void excluirPlanilhaMaterial(PlanilhaMateriais pi) {
        pi = em.getReference(PlanilhaMateriais.class, pi.getIdPlanilhaMateriais());
        em.remove(pi);
    }

    public List<Planilha> listaPlanilhasProduto() {
        Query query = em.createQuery("Select c From Planilha c where c.tipo = 'P'");
        return query.getResultList();
    }

    public List<Planilha> listaPlanilhasMaoObra() {
        Query query = em.createQuery("Select c From Planilha c where c.tipo = 'M'");
        return query.getResultList();
    }
}
