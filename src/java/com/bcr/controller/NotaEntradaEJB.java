/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.ContasPagar;
import com.bcr.model.NotaEntrada;
import com.bcr.model.NotaEntradaItem;
import com.bcr.model.PedidoCompra;
import com.bcr.model.PedidoCompraItem;
import com.bcr.model.RequisicaoMaterial;
import com.bcr.model.RequisicaoMaterialItem;
import com.bcr.util.Bcrutils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author Charles
 */
@Stateless
public class NotaEntradaEJB extends FacadeEJB<NotaEntrada> {

    public NotaEntradaEJB() {
        super(NotaEntrada.class);
    }
    @EJB
    UsuarioEJB uEJB;
    @EJB
    ProdutoEJB pEJB;
    @EJB
    PagamentoEJB pagamentoEJB;
    @EJB
    RequisicaoMaterialEJB rmEJB;
    @EJB
    PedidoCompraEJB pcEJB;

    public void atualizarItem(NotaEntradaItem notaEntradaItem) {
        em.merge(notaEntradaItem);
    }

    public void SalvarNotaEntrada(NotaEntrada notaEntrada, List<ContasPagar> listaPagar, ContasPagar contasPagar) {
        try {
            if (notaEntrada.getPedidoCompraList().size() > 0) {
                notaEntrada.setTipoProcesso("N");
            }
        } catch (NullPointerException e) {
            notaEntrada.setTipoProcesso("N");
        }

        try {
            em.merge(notaEntrada);
            for (NotaEntradaItem notaEntradaItem : notaEntrada.getNotaEntradaItemList()) {
                notaEntradaItem.setIdNotaEntrada(notaEntrada);
                if (notaEntradaItem.getIdProduto().getIdFornecedorUltimo() == null) {
                    notaEntradaItem.getIdProduto().setIdFornecedorUltimo(notaEntrada.getIdFornecedor());
                    notaEntradaItem.getIdProduto().setPrecoUltimaCompra(notaEntradaItem.getValorUnitario());
                    notaEntradaItem.getIdProduto().setDtUltimaCompra(notaEntrada.getDtEmissao());
                    pEJB.Atualizar(notaEntradaItem.getIdProduto());
                } else {
                    if (notaEntradaItem.getIdProduto().getDtUltimaCompra().after(notaEntrada.getDtEmissao())) {
                        notaEntradaItem.getIdProduto().setIdFornecedorUltimo(notaEntrada.getIdFornecedor());
                        notaEntradaItem.getIdProduto().setPrecoUltimaCompra(notaEntradaItem.getValorUnitario());
                        notaEntradaItem.getIdProduto().setDtUltimaCompra(notaEntrada.getDtEmissao());
                        pEJB.Atualizar(notaEntradaItem.getIdProduto());
                    }
                }
                em.merge(notaEntradaItem);

                if (notaEntrada.getTipoProcesso().equals("S")) {
                    if (notaEntradaItem.getIdNotaEntradaItem() == null) {
                        pEJB.EntradaEstoque(notaEntradaItem.getIdProduto(), notaEntradaItem.getQuantidade());
                    } else {
                        NotaEntradaItem itemAntes = selecionarNotaItem(notaEntradaItem);
                        pEJB.SaidaEstoque(notaEntradaItem.getIdProduto(), itemAntes.getQuantidade());
                        pEJB.EntradaEstoque(notaEntradaItem.getIdProduto(), notaEntradaItem.getQuantidade());
                    }
                }
            }
            if (notaEntrada.getIdFormaPagamento() != null) {
                System.out.println("selecionou a forma de pagamento");
                if (listaPagar.size() > 0) {
                    for (ContasPagar cp : listaPagar) {
                        cp.setIdNotaEntrada(notaEntrada);
                        if (cp.getIdEmpresa() == null) {
                            cp.setIdEmpresa(uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa());
                        }
                        //cp.setIdEmpresa(uEJB.pegaUsuarioLogadoNaSessao().getIdEmpresa());
                        pagamentoEJB.salvarAtualizarNormal(cp);
                    }
                } else {
                    contasPagar.setIdNotaEntrada(notaEntrada);
                    if (contasPagar.getIdEmpresa() == null) {
                        contasPagar.setIdEmpresa(uEJB.pegaEmpresaLogadoNaSessao());
                    }
                    pagamentoEJB.salvarAtualizarNormal(contasPagar);
                }
            } else {
                System.out.println("falou q nao selecionou a forma de pagamento");
            }
            if (notaEntrada.getPedidoCompraList().size() > 0) {
                List<PedidoCompraItem> listaItens = new ArrayList<PedidoCompraItem>();
                for (PedidoCompra pc : notaEntrada.getPedidoCompraList()) {
                    pc.setIdNotaEntrada(notaEntrada);
                    pc.setStatus("Finalizado");
                    em.merge(pc);
                    if (notaEntrada.getTipoProcesso().equals("S")) {
                        for (PedidoCompraItem pci : pc.getPedidoCompraItemList()) {
                            if (pci.getQtdChegada().longValue() >= pci.getQtdSolicitada().longValue()) {
                                List<String> requisicoes = new ArrayList<String>();
                                Pattern pattern = Pattern.compile("([^,])+");
                                Matcher matcher = pattern.matcher(pci.getDescricao());
                                while (matcher.find()) {
                                    requisicoes.add(pci.getIdCotacaoItem().getRequisicoesReferentes().substring(matcher.start(), matcher.end()));
                                    System.out.println(pci.getIdCotacaoItem().getRequisicoesReferentes().substring(matcher.start(), matcher.end()));
                                }
                                for (String s : requisicoes) {
                                    String id = s;
                                    Query query2 = em.createQuery("UPDATE RequisicaoMaterialItem rmi SET rmi.status = 'Aguardando Saida' where rmi.idRequisicaoMaterialItem = " + id);
                                    query2.executeUpdate();
                                    RequisicaoMaterial rm = rmEJB.selecionarRequisicaoPorItem(Integer.parseInt(id)).getIdRequisicaoMaterial();
                                    verificaStatusDaRequisicao(rm);
                                }
                            } else {
                                listaItens.add(pci);
                            }
                        }
                    }

                }
                if (listaItens.size() > 0) {
                    PedidoCompra pc = new PedidoCompra();
                    PedidoCompraItem old = listaItens.get(0);
                    pc.setDtEmissao(new Date());
                    pc.setStatus("Aguardando Chegada");
                    pc.setIdEmpresa(old.getIdPedidoCompra().getIdEmpresa());
                    pc.setIdFornecedor(old.getIdPedidoCompra().getIdFornecedor());
                    pc.setLoteReferente(old.getIdPedidoCompra().getLoteReferente());
                    for (PedidoCompraItem pci : listaItens) {
                        PedidoCompraItem itemNew = new PedidoCompraItem();
                        itemNew.setDescricao(pci.getDescricao());
                        itemNew.setStatus("S");
                        itemNew.setQtdSolicitada(pci.getQtdRestante());
                        itemNew.setIdProduto(pci.getIdProduto());
                        itemNew.setValorUnitario(pci.getValorUnitario());
                        itemNew.setValorTotal(pci.getQtdSolicitada().multiply(itemNew.getValorUnitario()));
                        itemNew.setIdCotacaoItem(pci.getIdCotacaoItem());
                        try {
                            pc.getPedidoCompraItemList().add(itemNew);
                        } catch (Exception e) {
                            pc.setPedidoCompraItemList(new ArrayList<PedidoCompraItem>());
                            pc.getPedidoCompraItemList().add(itemNew);
                        }
                    }
                    pcEJB.SalvarPedido(pc);
                }
            }
        } catch (Exception e) {
            Bcrutils.escreveLogErro(e);
        }
    }

    public void verificaStatusDaRequisicao(RequisicaoMaterial r) {
        boolean bol = false;
        for (RequisicaoMaterialItem rmi : r.getRequisicaoMaterialItemList()) {
            if (rmi.getStatus().equals("Aguardando")) {
                bol = true;
                break;
            }
        }
        if (bol) {
            if (!r.getStatus().equals("Análise de Almoxarifado")) {
                r.setStatus("Análise de Almoxarifado");
                rmEJB.Atualizar(r);
            }
        } else {
            boolean bol2 = false;
            for (RequisicaoMaterialItem rmi : r.getRequisicaoMaterialItemList()) {
                if (rmi.getStatus().equals("Aceito")) {
                    bol2 = true;
                } else {
                    bol2 = false;
                    break;
                }
            }
            if (bol2) {
                r.setStatus("Aguardando Saída");
                rmEJB.Atualizar(r);
            } else {
                r.setStatus("Em Cotação");
                rmEJB.Atualizar(r);
            }
        }
    }

    public void excluirTudo(NotaEntrada ne) {
        ne = em.getReference(NotaEntrada.class, ne.getIdNotaEntrada());
        for (NotaEntradaItem ni : ne.getNotaEntradaItemList()) {
            ni = em.getReference(NotaEntradaItem.class, ni.getIdNotaEntradaItem());
            pEJB.SaidaEstoque(ni.getIdProduto(), ni.getQuantidade());
            em.remove(ni);
        }
        for (ContasPagar cp : ne.getContasPagarList()) {
            cp = em.getReference(ContasPagar.class, cp.getIdContasPagar());
            em.remove(cp);
        }
        em.remove(ne);
    }

    public Number contarRegistro(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(n.idNotaEntrada) From NotaEntrada n " + clausula);
        return (Number) query.getSingleResult();
    }

    public List<NotaEntrada> listarRequisicaoMaterialLazyModeWhere(int primeiro, int qtd, String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select n From NotaEntrada n " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public NotaEntradaItem selecionarNotaItem(NotaEntradaItem nei) {
        Query query = em.createQuery("Select n From NotaEntradaItem n where n.idNotaEntradaItem = :nei");
        query.setParameter("nei", nei.getIdNotaEntradaItem());
        return (NotaEntradaItem) query.getSingleResult();
    }

    public List<NotaEntrada> pesquisar(NotaEntrada notaEntrada, String por, String status, Date dtIni, Date dtFim) {
        String fornecedor;
        String planContas = "";
        String numDocto;
        StringBuilder sb = new StringBuilder();
        if (notaEntrada.getIdFornecedor() != null) {
            fornecedor = notaEntrada.getIdFornecedor().getIdFornecedor().toString();
            sb.append(" and n.idFornecedor.idFornecedor  =  " + fornecedor);
        }
        if (!notaEntrada.getNumero().isEmpty()) {
            numDocto = notaEntrada.getNumero();
            sb.append(" and n.numDocto =  " + numDocto);
        }
        if (status.equals("Lançada")) {
            sb.append(" and n.status = 'Lançada'");
        } else {
            sb.append(" and n.status = 'Faturada'");
        }

        if (por.equals("E")) {
            sb.append(" and n.dtEmissao BETWEEN :dtIni and :dtFim ");
        } else if (por.equals("N")) {
            sb.append(" and n.dtEntrada BETWEEN :dtIni and :dtFim  ");
        }

        String sql = "Select n From NotaEntrada n where n.TipoEntrada like :tipo " + sb;
        System.out.println(sql);
        Query query = em.createQuery(sql);
        query.setParameter("tipo", planContas + "%");
        if (sb.toString().contains("and n.dtEmissao BETWEEN :dtIni and :dtFim")
                || sb.toString().contains("and c.dtVencimento BETWEEN :dtIni and :dtFim")) {
            query.setParameter("dtIni", dtIni, TemporalType.DATE);
            query.setParameter("dtFim", dtFim, TemporalType.DATE);
        }
        return query.getResultList();
    }
}
