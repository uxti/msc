/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.ProdutoFornecedor;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class ProdutoFornecedorEJB extends FacadeEJB<ProdutoFornecedor> {

    public ProdutoFornecedorEJB() {
        super(ProdutoFornecedor.class);
    }
    
    public List<ProdutoFornecedor> listaproforPorProduto(Integer idProduto){
        Query query = em.createQuery("select pf from ProdutoFornecedor pf where pf.idProduto.idProduto = :idpro");
        query.setParameter("idpro", idProduto);
        return query.getResultList();
    }
}
