/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Colaborador;
import javax.ejb.Stateless;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class ColaboradorEJB extends FacadeEJB<Colaborador> {

    public ColaboradorEJB() {
        super(Colaborador.class);
    }

}
