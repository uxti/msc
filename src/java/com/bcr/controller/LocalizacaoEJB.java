/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Localizacao;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class LocalizacaoEJB extends FacadeEJB<Localizacao> {

    
    public LocalizacaoEJB(){
        super(Localizacao.class);
    }

}
