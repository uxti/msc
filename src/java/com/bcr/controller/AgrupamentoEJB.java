/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Agrupamento;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class AgrupamentoEJB extends FacadeEJB<Agrupamento> {

    public AgrupamentoEJB(){
        super(Agrupamento.class);
    }
    
    public void salvar(Agrupamento agrupamento){
        em.merge(agrupamento);
    }
    
   

}
