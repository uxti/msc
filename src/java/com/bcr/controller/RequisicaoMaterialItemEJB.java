/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.RequisicaoMaterialItem;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class RequisicaoMaterialItemEJB extends FacadeEJB<RequisicaoMaterialItem> {

    public RequisicaoMaterialItemEJB() {
        super(RequisicaoMaterialItem.class);
    }
    @EJB
    ProdutoEJB pEJB;

    public List<RequisicaoMaterialItem> listaProdutosPendentesParaComprar() {
        Query query = em.createNativeQuery("Select m.id_Produto, m.descricao descricao, SUM(m.quantidade_Cotada)quantidadeCotada From Requisicao_Material_Item m where m.status = 'Cotação' GROUP BY   m.id_Produto, m.descricao");
        List<Object[]> objectReturn = new ArrayList<Object[]>();
        objectReturn = query.getResultList();
        List<RequisicaoMaterialItem> itens = new ArrayList<RequisicaoMaterialItem>();
        for (Object[] o : objectReturn) {
            RequisicaoMaterialItem rmi = new RequisicaoMaterialItem();
            rmi.setIdProduto(pEJB.SelecionarPorID((Integer) o[0]));
            rmi.setDescricao((String) o[1]);
            BigDecimal qnt = (BigDecimal) o[2];
            rmi.setQuantidade(qnt);
            itens.add(rmi);
        }
        return itens;
    }

    public List<RequisicaoMaterialItem> listaProdutosPendentesCotacao() {
        return em.createQuery("Select c From RequisicaoMaterialItem c where c.status = 'Cotação'").getResultList();
    }
}
