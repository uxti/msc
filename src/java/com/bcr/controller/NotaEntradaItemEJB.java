/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.NotaEntradaItem;
import javax.ejb.Stateless;

/**
 *
 * @author Charles
 */
@Stateless
public class NotaEntradaItemEJB extends FacadeEJB<NotaEntradaItem> {

    public NotaEntradaItemEJB() {
        super(NotaEntradaItem.class);
    }

}
