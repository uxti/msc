/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Estado;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class EstadoEJB extends FacadeEJB<Estado> {

    public EstadoEJB() {
        super(Estado.class);
    }

    public Long verificarEstadoExiste(String sigla) {
        Query query = em.createQuery("Select COUNT(e.idEstado) From Estado e where e.sigla = :sig");
        query.setParameter("sig", sigla);
        return (Long) query.getSingleResult();
    }

    public Estado pesquisarPorSigla(String sigla) {

        Query query = em.createQuery("Select e From Estado e where e.sigla = :uf");
        query.setParameter("uf", sigla);
        try {
            return (Estado) query.getSingleResult();
        } catch (Exception e) {
            for (Estado est : carregarEstados()) {
                try {
                    if (verificarEstadoExiste(est.getSigla()) == 0) {
                        em.merge(est);
                    }
                } catch (Exception ee) {
                    System.out.println(ee);
                }
            }
            return (Estado) query.getSingleResult();
        }
    }

    public List<Estado> carregarEstados() {
        List<Estado> estados = new ArrayList<Estado>();

        Estado ac = new Estado();
        ac.setNome("Acre");
        ac.setSigla("AC");
        estados.add(ac);

        Estado al = new Estado();
        al.setNome("Alagoas");
        al.setSigla("AL");
        estados.add(al);

        Estado AP = new Estado();
        AP.setNome("Amapá");
        AP.setSigla("AP");
        estados.add(AP);

        Estado AM = new Estado();
        AM.setNome("Amazonas");
        AM.setSigla("AM");
        estados.add(AM);

        Estado BA = new Estado();
        BA.setNome("Bahia");
        BA.setSigla("BA");
        estados.add(BA);

        Estado CE = new Estado();
        CE.setNome("Ceará");
        CE.setSigla("CE");
        estados.add(CE);

        Estado DF = new Estado();
        DF.setNome("Distrito Federal");
        DF.setSigla("DF");
        estados.add(DF);

        Estado ES = new Estado();
        ES.setNome("Espírito Santo");
        ES.setSigla("ES");
        estados.add(ES);

        Estado GO = new Estado();
        GO.setNome("Goiás");
        GO.setSigla("GO");
        estados.add(GO);

        Estado MA = new Estado();
        MA.setNome("Maranhão");
        MA.setSigla("MA");
        estados.add(MA);

        Estado MT = new Estado();
        MT.setNome("Mato Grosso");
        MT.setSigla("MT");
        estados.add(MT);

        Estado MS = new Estado();
        MS.setNome("Mato Grosso do Sul");
        MS.setSigla("MS");
        estados.add(MS);

        Estado MG = new Estado();
        MG.setNome("Minas Gerais");
        MG.setSigla("MG");
        estados.add(MG);

        Estado PA = new Estado();
        PA.setNome("Pará");
        PA.setSigla("PA");
        estados.add(PA);

        Estado PB = new Estado();
        PB.setNome("Paraíba");
        PB.setSigla("PB");
        estados.add(PB);

        Estado PR = new Estado();
        PR.setNome("Paraná");
        PR.setSigla("PR");
        estados.add(PR);

        Estado PE = new Estado();
        PE.setNome("Pernambuco");
        PE.setSigla("PE");
        estados.add(PE);

        Estado PI = new Estado();
        PI.setNome("Piauí");
        PI.setSigla("PI");
        estados.add(PI);

        Estado RJ = new Estado();
        RJ.setNome("Rio de Janeiro");
        RJ.setSigla("RJ");
        estados.add(RJ);

        Estado RN = new Estado();
        RN.setNome("Rio Grande do Norte");
        RN.setSigla("RN");
        estados.add(RN);

        Estado RS = new Estado();
        RS.setNome("Rio Grande do Sul");
        RS.setSigla("RS");
        estados.add(RS);

        Estado RO = new Estado();
        RO.setNome("Rondônia");
        RO.setSigla("RO");
        estados.add(RO);

        Estado RR = new Estado();
        RR.setNome("Roraima");
        RR.setSigla("RR");
        estados.add(RR);

        Estado SC = new Estado();
        SC.setNome("Santa Catarina");
        SC.setSigla("SC");
        estados.add(SC);

        Estado SP = new Estado();
        SP.setNome("São Paulo");
        SP.setSigla("SP");
        estados.add(SP);

        Estado SE = new Estado();
        SE.setNome("Sergipe");
        SE.setSigla("SE");
        estados.add(SE);

        Estado TO = new Estado();
        TO.setNome("Tocantins");
        TO.setSigla("TO");
        estados.add(TO);
        return estados;
    }
}
