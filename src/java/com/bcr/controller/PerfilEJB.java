/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Acesso;
import com.bcr.model.Perfil;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class PerfilEJB extends FacadeEJB<Perfil> {

    public PerfilEJB() {
        super(Perfil.class);
    }

    public void salvar(Perfil p, Acesso a) {
        em.merge(a);
        p.setIdAcesso(a);
        em.merge(p);

    }
}
