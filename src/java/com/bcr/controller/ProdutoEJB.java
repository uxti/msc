/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.controller;

import com.bcr.model.Grupo;
import com.bcr.model.Produto;
import com.bcr.model.ProdutoFornecedor;
import com.bcr.model.SubGrupo;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class ProdutoEJB extends FacadeEJB<Produto> {

    public ProdutoEJB() {
        super(Produto.class);
    }

    public void salvar(Produto p, List<ProdutoFornecedor> lista) {
        em.getEntityManagerFactory().getCache().evictAll();
        em.merge(p);
        for (ProdutoFornecedor pf : lista) {
            pf.setIdProduto(p);
            em.merge(pf);
        }
    }

    public List<SubGrupo> carregaListadeSubgrupoPorGrupo(Integer idGrupo) {
        Query query = em.createQuery("select sg from SubGrupo sg where sg.idGrupo.idGrupo =:idgru");
        query.setParameter("idgru", idGrupo);
        em.getEntityManagerFactory().getCache().evictAll();

        return query.getResultList();
    }

    public Number contarRegistros(String clausula) {
        Query query = em.createQuery("Select COUNT(c.idProduto) From Produto c " + clausula);
        em.getEntityManagerFactory().getCache().evictAll();
        return (Number) query.getSingleResult();
    }

    public List<Produto> listarProdutosLazy(int primeiro, int qtd, String clausula) {
        Query query = em.createQuery("Select c From Produto c " + clausula);
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        em.getEntityManagerFactory().getCache().evictAll();
        return query.getResultList();
    }

    public void EntradaEstoque(Produto produto, BigDecimal quantidade) {
        produto = SelecionarPorID(produto.getIdProduto());
        produto.setQuantidade(produto.getQuantidade().add(quantidade));
//        System.out.println("Nova entrada no estoque do produto " + produto.getDescricao() + " na quantidade de " + quantidade);
//        System.out.println("Produto " + produto.getQuantidade());
        em.merge(produto);
    }

    public void SaidaEstoque(Produto produto, BigDecimal quantidade) {
        produto.setQuantidade(produto.getQuantidade().subtract(quantidade));
//        System.out.println("Nova saida no estoque do produto " + produto.getDescricao() + " na quantidade de " + quantidade);
//        System.out.println("Produto " + produto.getQuantidade());
        em.merge(produto);
    }

    public void AtualizarQuantidadeReserva(Produto p, BigDecimal quantidade) {
        p.setQuantidadeReservada(p.getQuantidadeReservada().add(quantidade));
        em.merge(p);
    }

    public List<Produto> autoCompleteProduto(String var) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From Produto p where  p.inativo = 0 and p.descricao LIKE :desc OR p.idProduto like :id ");
        query.setParameter("desc", var + "%");
        query.setParameter("id", var + "%");
        em.getEntityManagerFactory().getCache().evictAll();
        return query.getResultList();
    }

    public List<Produto> listarProdutosEstoqueMinimoAtingido() {
        Query query = em.createQuery("Select p From Produto p where p.quantidade < p.estoqueMin");
        return query.getResultList();
    }

    public List<Produto> pesquisarProdutosPorGrupo(Grupo g) {
        Query q = em.createQuery("Select p From Produto p where p.idGrupo.idGrupo = :ig");
        q.setParameter("ig", g.getIdGrupo());
        return q.getResultList();
    }
}
