/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

import com.bcr.model.Grupo;
import java.math.BigDecimal;

/**
 *
 * @author Renato
 */
public class GrupoValores {
    private Grupo grupo;
    private BigDecimal valor;

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
    
    
    
}
