/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

import com.bcr.model.Ativo;
import com.bcr.util.Bcrutils;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Renato
 */
public class Depreciacao {

    private Ativo ativo;
    private Date periodo;
    private BigDecimal valorRateio;
    private BigDecimal custoMensal;
    private BigDecimal valorAtivo;
    private BigDecimal totalDepreciado;

    public Ativo getAtivo() {
        return ativo;
    }

    public void setAtivo(Ativo ativo) {
        this.ativo = ativo;
    }

    public Date getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Date periodo) {
        this.periodo = periodo;
    }

    public BigDecimal getValorRateio() {
        return valorRateio;
    }

    public void setValorRateio(BigDecimal valorRateio) {
        this.valorRateio = valorRateio;
    }

    public BigDecimal getCustoMensal() {
        return custoMensal;
    }

    public void setCustoMensal(BigDecimal custoMensal) {
        this.custoMensal = custoMensal;
    }

    public BigDecimal getTotalDepreciado() {
        return totalDepreciado;
    }

    public void setTotalDepreciado(BigDecimal totalDepreciado) {
        this.totalDepreciado = totalDepreciado;
    }

    public BigDecimal getValorAtivo() {
        return valorAtivo;
    }

    public void setValorAtivo(BigDecimal valorAtivo) {
        this.valorAtivo = valorAtivo;
    }
    
    

}
