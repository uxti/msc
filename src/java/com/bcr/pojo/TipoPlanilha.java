/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

import java.util.List;

/**
 *
 * @author Renato
 */
public class TipoPlanilha {
    private String descricao;
    private List<GrupoPlanilha> grupos;

    public TipoPlanilha() {
    }
    
    public TipoPlanilha(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<GrupoPlanilha> getGrupos() {
        return grupos;
    }

    public void setGrupos(List<GrupoPlanilha> grupos) {
        this.grupos = grupos;
    }
    
}
