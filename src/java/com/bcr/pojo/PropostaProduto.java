/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

import com.bcr.model.Ativo;
import com.bcr.model.Produto;
import java.math.BigDecimal;

/**
 *
 * @author Renato
 */
public class PropostaProduto {
    private BigDecimal quantidade;
    private Produto produto;
    private Ativo ativo;
    private BigDecimal mesesDepreciacao;
    private BigDecimal valorTotal;
    private BigDecimal valorTotalDepreciado;
    private String tipo;

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public BigDecimal getMesesDepreciacao() {
        return mesesDepreciacao;
    }

    public void setMesesDepreciacao(BigDecimal mesesDepreciacao) {
        this.mesesDepreciacao = mesesDepreciacao;
    }

    public Ativo getAtivo() {
        return ativo;
    }

    public void setAtivo(Ativo ativo) {
        this.ativo = ativo;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public BigDecimal getValorTotalDepreciado() {
        return valorTotalDepreciado;
    }

    public void setValorTotalDepreciado(BigDecimal valorTotalDepreciado) {
        this.valorTotalDepreciado = valorTotalDepreciado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
}
