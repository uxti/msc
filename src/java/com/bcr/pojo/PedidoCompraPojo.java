/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

import com.bcr.model.CotacaoItem;
import com.bcr.model.Fornecedor;
import com.bcr.model.Produto;
import java.math.BigDecimal;

/**
 *
 * @author Renato
 */
public class PedidoCompraPojo {

    private Fornecedor fornecedor;
    private Produto produto;
    private BigDecimal quantidade;
    private BigDecimal vlrUnitario;
    private BigDecimal vlrTotal;
    private String cotacoesReferentes;
    private CotacaoItem cotacaoItem;
    private String loteReferente;

    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    
    public BigDecimal getVlrUnitario() {
        return vlrUnitario;
    }

    public void setVlrUnitario(BigDecimal vlrUnitario) {
        this.vlrUnitario = vlrUnitario;
    }

    public BigDecimal getVlrTotal() {
        return vlrTotal;
    }

    public void setVlrTotal(BigDecimal vlrTotal) {
        this.vlrTotal = vlrTotal;
    }

    public String getCotacoesReferentes() {
        return cotacoesReferentes;
    }

    public void setCotacoesReferentes(String cotacoesReferentes) {
        this.cotacoesReferentes = cotacoesReferentes;
    }

    public CotacaoItem getCotacaoItem() {
        return cotacaoItem;
    }

    public void setCotacaoItem(CotacaoItem cotacaoItem) {
        this.cotacaoItem = cotacaoItem;
    }

    public String getLoteReferente() {
        return loteReferente;
    }

    public void setLoteReferente(String loteReferente) {
        this.loteReferente = loteReferente;
    }
    

    
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PedidoCompraPojo other = (PedidoCompraPojo) obj;
        return true;
    }
}
