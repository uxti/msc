/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

/**
 *
 * @author Renato
 */
public class GrupoPlanilha {
    private String descricao;
    private TipoPlanilha tipoPlanilha;

    public GrupoPlanilha(String descricao, TipoPlanilha tipoPlanilha) {
        this.descricao = descricao;
        this.tipoPlanilha = tipoPlanilha;
    }
    
    

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoPlanilha getTipoPlanilha() {
        return tipoPlanilha;
    }

    public void setTipoPlanilha(TipoPlanilha tipoPlanilha) {
        this.tipoPlanilha = tipoPlanilha;
    }
}
