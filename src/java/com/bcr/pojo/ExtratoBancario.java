/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Renato
 */
public class ExtratoBancario {

    private String nome;
    private Date mes_referente;
    private String conta;
    private String banco;
    private BigDecimal saldo_anterior;
    private Date data;
    private String historico;
    private String cheque;
    private BigDecimal valor;
    private BigDecimal saldo;
    private String tipoMovimentacao;
    private BigDecimal saldo_final;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getMes_referente() {
        return mes_referente;
    }

    public void setMes_referente(Date mes_referente) {
        this.mes_referente = mes_referente;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public BigDecimal getSaldo_anterior() {
        return saldo_anterior;
    }

    public void setSaldo_anterior(BigDecimal saldo_anterior) {
        this.saldo_anterior = saldo_anterior;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public String getCheque() {
        return cheque;
    }

    public void setCheque(String cheque) {
        this.cheque = cheque;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public String getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(String tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

    public BigDecimal getSaldo_final() {
        return saldo_final;
    }

    public void setSaldo_final(BigDecimal saldo_final) {
        this.saldo_final = saldo_final;
    }
    
    
    
}
