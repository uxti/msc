/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

import com.bcr.model.Grupo;
import java.math.BigDecimal;

/**
 *
 * @author Renato
 */
public class PropostaProdutoResumo {
    private Grupo grupo;
    private BigDecimal custo;
    private BigDecimal custoPorFuncionario;

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public BigDecimal getCusto() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo = custo;
    }

    public BigDecimal getCustoPorFuncionario() {
        return custoPorFuncionario;
    }

    public void setCustoPorFuncionario(BigDecimal custoPorFuncionario) {
        this.custoPorFuncionario = custoPorFuncionario;
    }
    
    
}
