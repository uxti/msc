/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

/**
 *
 * @author Renato
 */
public class Menu {
    private String label;
    private String href;

    public Menu() {
    }

    public Menu(String label, String href) {
        this.label = label;
        this.href = href;
    }
    
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
    
    
}
