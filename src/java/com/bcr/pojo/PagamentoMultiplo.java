/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

import com.bcr.model.Banco;
import com.bcr.model.Cheque;
import com.bcr.model.Colaborador;
import com.bcr.model.ContaCorrente;
import com.bcr.model.ContasPagar;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Renato
 */
public class PagamentoMultiplo {

    private ContasPagar contasPagar;
    private String destino;
    private Banco banco;
    private ContaCorrente contaCorrente;
    private Cheque cheque;
    private BigDecimal valor;
    private String tipoDocumento;
    private String numCheque;
    private Date dtCompensar;
    private Colaborador colaborador;
    private String nominal;
    private Date dtPagamento;

    public PagamentoMultiplo() {
    }

    
    
    public PagamentoMultiplo(ContasPagar contasPagar, String destino, Banco banco, ContaCorrente contaCorrente, Cheque cheque, BigDecimal valor, String tipoDocumento,Date dtPagamento) {
        this.contasPagar = contasPagar;
        this.destino = destino;
        this.banco = banco;
        this.contaCorrente = contaCorrente;
        this.cheque = cheque;
        this.valor = valor;
        this.tipoDocumento = tipoDocumento;
        this.dtPagamento = dtPagamento;
    }

    
    

    public ContasPagar getContasPagar() {
        return contasPagar;
    }

    public void setContasPagar(ContasPagar contasPagar) {
        this.contasPagar = contasPagar;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumCheque() {
        return numCheque;
    }

    public void setNumCheque(String numCheque) {
        this.numCheque = numCheque;
    }

    public Date getDtCompensar() {
        return dtCompensar;
    }

    public void setDtCompensar(Date dtCompensar) {
        this.dtCompensar = dtCompensar;
    }

    public Colaborador getColaborador() {
        return colaborador;
    }

    public void setColaborador(Colaborador colaborador) {
        this.colaborador = colaborador;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public Date getDtPagamento() {
        return dtPagamento;
    }

    public void setDtPagamento(Date dtPagamento) {
        this.dtPagamento = dtPagamento;
    }
    
}
