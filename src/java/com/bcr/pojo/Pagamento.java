/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bcr.pojo;

import com.bcr.model.Banco;
import com.bcr.model.ContaCorrente;
import com.bcr.model.ContasPagar;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Renato
 */
public class Pagamento implements Serializable{
    private Integer idMovimentoBancario;
    private Banco banco;
    private ContaCorrente contaCorrente;
    private BigDecimal valorPago;
    private Date dtPagament;
    private ContasPagar contasPagar;
    private String historico;

    public Integer getIdMovimentoBancario() {
        return idMovimentoBancario;
    }

    public void setIdMovimentoBancario(Integer idMovimentoBancario) {
        this.idMovimentoBancario = idMovimentoBancario;
    }
    
    
    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public ContaCorrente getContaCorrente() {
        return contaCorrente;
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        this.contaCorrente = contaCorrente;
    }

    public BigDecimal getValorPago() {
        return valorPago;
    }

    public void setValorPago(BigDecimal valorPago) {
        this.valorPago = valorPago;
    }


    public Date getDtPagament() {
        return dtPagament;
    }

    public void setDtPagament(Date dtPagament) {
        this.dtPagament = dtPagament;
    }

    public ContasPagar getContasPagar() {
        return contasPagar;
    }

    public void setContasPagar(ContasPagar contasPagar) {
        this.contasPagar = contasPagar;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }
    
    
    
 
    
}
