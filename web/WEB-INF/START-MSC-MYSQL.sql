DROP DATABASE MSC;
CREATE DATABASE MSC;
USE MSC;
CREATE TABLE Acesso (
  ID_ACESSO                 int(10) NOT NULL AUTO_INCREMENT, 
  id_perfil                 int(10), 
  MODULO_FINANCEIRO         tinyint(1), 
  FINANCEIRO_CONTAS_PAGAR   tinyint(1), 
  FINANCEIRO_CONTAS_RECEBER tinyint(1), 
  FINANCEIRO_CAIXA          tinyint(1), 
  FINANCEIRO_BANCO          tinyint(1), 
  CONTAS_PAGAR_PESQUISAR    tinyint(1), 
  CONTAS_PAGAR_INSERIR      tinyint(1), 
  CONTAS_PAGAR_PAGAR        tinyint(1), 
  CONTAS_PAGAR_EXCLUIR      tinyint(1), 
  CONTAS_PAGAR_ESTORNAR     tinyint(1), 
  CONTAS_PAGAR_VER          tinyint(1), 
  CONTAS_RECEBER_PESQUISAR  tinyint(1), 
  CONTAS_RECEBER_INSERIR    tinyint(1), 
  CONTAS_RECEBER_RECEBER    tinyint(1), 
  CONTAS_RECEBER_EXCLUIR    tinyint(1), 
  CONTAS_RECEBER_ESTORNAR   tinyint(1), 
  CONTAS_RECEBER_VER        tinyint(1), 
  CAIXA_PESQUISAR           tinyint(1), 
  CAIXA_SANGRIA             tinyint(1), 
  CAIXA_SUPRIMENTO          tinyint(1), 
  CAIXA_TRANSFERENCIA       tinyint(1), 
  BANCO_PESQUISAR           tinyint(1), 
  BANCO_SAQUE               tinyint(1), 
  BANCO_DEPOSITO            tinyint(1), 
  BANCO_TRANSFERENCIA       tinyint(1), 
  PRIMARY KEY (ID_ACESSO));
CREATE TABLE Localizacao (
  ID_LOCALIZACAO int(10) NOT NULL AUTO_INCREMENT, 
  DESCRICAO      varchar(200), 
  PRIMARY KEY (ID_LOCALIZACAO));
CREATE TABLE Centro_Custo_Variavel (
  id_centro_custo_variavel int(10) NOT NULL AUTO_INCREMENT, 
  tipo                     varchar(1), 
  data_movimento           datetime, 
  valor                    decimal(12, 2), 
  percentual               smallint(5), 
  id_centro_custo          int(10), 
  id_contas_pagar          int(10), 
  id_plano_contas          int(10), 
  id_contas_receber        int(10), 
  id_nota_saida            int(10), 
  id_nota_entrada          int(10), 
  id_caixa_item            int(10), 
  id_cliente               int(10), 
  PRIMARY KEY (id_centro_custo_variavel));
CREATE TABLE contas_DRE (
  id_contaDRE int(10) NOT NULL AUTO_INCREMENT, 
  descricao   varchar(255), 
  PRIMARY KEY (id_contaDRE));
CREATE TABLE Agrupamento (
  id_agrupamento int(10) NOT NULL AUTO_INCREMENT, 
  descricao      varchar(255), 
  PRIMARY KEY (id_agrupamento));
CREATE TABLE Mensagem (
  ID_MENSAGEM         int(10) NOT NULL AUTO_INCREMENT, 
  MENSAGEM            varchar(255), 
  VISTA               char(1), 
  id_usuario_sender   int(10), 
  id_usuario_receiver int(10), 
  PRIMARY KEY (ID_MENSAGEM));
CREATE TABLE Contrato_Negocio (
  id_contrato_negocio int(10) NOT NULL AUTO_INCREMENT, 
  id_contrato         int(10) NOT NULL, 
  id_negocio          int(10) NOT NULL, 
  PRIMARY KEY (id_contrato_negocio), 
  CONSTRAINT uniq_contrato_negocio 
    UNIQUE (id_contrato, id_negocio));
CREATE TABLE Setor (
  id_setor   int(10) NOT NULL AUTO_INCREMENT, 
  descricao  varchar(100), 
  id_empresa int(10) NOT NULL, 
  PRIMARY KEY (id_setor));
CREATE TABLE Cotacao_Item (
  id_cotacao_item int(10) NOT NULL AUTO_INCREMENT, 
  id_cotacao      int(10), 
  valor_cotado    decimal(12, 2), 
  id_fornecedor   int(10), 
  id_produto      int(10) NOT NULL, 
  qde             int(10), 
  id_empresa      int(10) NOT NULL, 
  PRIMARY KEY (id_cotacao_item));
CREATE TABLE Produto_Fornecedor (
  id_produto_fornecedor int(10) NOT NULL AUTO_INCREMENT, 
  id_produto            int(10), 
  id_fornecedor         int(10) NOT NULL, 
  PRIMARY KEY (id_produto_fornecedor), 
  CONSTRAINT uniq_produto_fornecedor 
    UNIQUE (id_produto, id_fornecedor));
CREATE TABLE Cotacao_item_historico (
  id_cotacao_item_historico int(10) NOT NULL AUTO_INCREMENT, 
  id_fornecedor             int(10) NOT NULL, 
  id_fornecedor_selecionado int(10), 
  id_cotacao                int(10) NOT NULL, 
  id_produto                int(10) NOT NULL, 
  valor_anterior            decimal(12, 2), 
  valor_cotado              decimal(12, 2), 
  PRIMARY KEY (id_cotacao_item_historico));
CREATE TABLE Ordem_Compra_Item (
  id_ordem_compra_item        int(10) NOT NULL AUTO_INCREMENT, 
  id_ordem_compra             int(10) NOT NULL, 
  id_requisicao_material_item int(10), 
  id_produto                  int(10) NOT NULL, 
  descricao                   varchar(100), 
  quantidade                  int(11), 
  vlr_unitario                decimal(12, 2), 
  PRIMARY KEY (id_ordem_compra_item));
CREATE TABLE Ordem_Compra (
  id_ordem_compra        int(10) NOT NULL AUTO_INCREMENT, 
  id_requisicao_material int(10), 
  id_usuario             int(10), 
  status                 varchar(100), 
  dt_cadastro            datetime, 
  dt_autorizacao         datetime, 
  dt_atualizacao         datetime, 
  id_empresa             int(10) NOT NULL, 
  id_fornecedor          int(10), 
  PRIMARY KEY (id_ordem_compra));
CREATE TABLE Cotacao (
  id_cotacao      int(10) NOT NULL AUTO_INCREMENT, 
  dt_cadastro     datetime, 
  dt_autorizacao  datetime, 
  status          varchar(100), 
  id_usuario      int(10), 
  id_ordem_compra int(10) NOT NULL, 
  id_fornecedor   int(10), 
  PRIMARY KEY (id_cotacao));
CREATE TABLE Requisicao_Material_item (
  id_requisicao_material_item int(10) NOT NULL AUTO_INCREMENT, 
  id_requisicao_material      int(10) NOT NULL, 
  id_produto                  int(10) NOT NULL, 
  descricao                   varchar(100), 
  quantidade                  int(10), 
  vlr_unitario                decimal(12, 2), 
  PRIMARY KEY (id_requisicao_material_item));
CREATE TABLE Local_trabalho (
  id_local_trabalho int(10) NOT NULL AUTO_INCREMENT, 
  descricao         varchar(100), 
  id_empresa        int(10) NOT NULL, 
  PRIMARY KEY (id_local_trabalho));
CREATE TABLE Requisicao_Material (
  id_requisicao_material int(10) NOT NULL AUTO_INCREMENT, 
  id_local_trabalho      int(10) NOT NULL, 
  id_usuario             int(10), 
  id_cliente             int(10), 
  id_custo               int(10), 
  status                 varchar(100), 
  dt_cadastro            datetime, 
  valor                  decimal(12, 2), 
  aplicacao              varchar(255), 
  dt_atualizacao         datetime, 
  id_empresa             int(10) NOT NULL, 
  PRIMARY KEY (id_requisicao_material));
CREATE TABLE Colaborador (
  id_colaborador    int(10) NOT NULL AUTO_INCREMENT, 
  id_usuario        int(10), 
  id_cargo          int(10), 
  id_local_trabalho int(10) NOT NULL, 
  nome              varchar(255), 
  CPF               varchar(14), 
  RG                varchar(20), 
  email             varchar(255), 
  dt_nascimento     datetime, 
  dt_cadastro       datetime, 
  logradouro        varchar(255), 
  numero            varchar(5), 
  bloqueado         tinyint(1), 
  bairro            varchar(255), 
  complemento       varchar(255), 
  sexo              varchar(1), 
  CEP               varchar(9), 
  tipo_logradouro   varchar(20), 
  contrato_atual    int(10), 
  id_setor          int(10) NOT NULL, 
  id_empresa        int(10) NOT NULL, 
  PRIMARY KEY (id_colaborador));
CREATE TABLE Servico_Resumido (
  id_servico_resumido int(10) NOT NULL AUTO_INCREMENT, 
  descricao           varchar(255), 
  valor               decimal(12, 2), 
  PRIMARY KEY (id_servico_resumido));
CREATE TABLE Arquivo (
  id_arquivo  int(10) NOT NULL AUTO_INCREMENT, 
  id_contrato int(10), 
  arquivo     blob, 
  PRIMARY KEY (id_arquivo));
CREATE TABLE Categoria (
  id_categoria int(10) NOT NULL AUTO_INCREMENT, 
  descricao    varchar(255), 
  id_empresa   int(10) NOT NULL, 
  PRIMARY KEY (id_categoria));
CREATE TABLE Comunicado_Enc_Financeiro (
  id_comunicado_enc_Financeiro int(10) NOT NULL AUTO_INCREMENT, 
  id_comunicado_encerramento   int(10) NOT NULL, 
  id_cliente                   int(10), 
  dt_envio_nf                  datetime, 
  valor_restante               decimal(12, 2), 
  observacao                   varchar(255), 
  PRIMARY KEY (id_comunicado_enc_Financeiro));
CREATE TABLE Comunicado_Enc_Item (
  id_comunicado_enc_item     int(10) NOT NULL AUTO_INCREMENT, 
  id_comunicado_encerramento int(10) NOT NULL, 
  id_cargo                   int(10) NOT NULL, 
  id_servico_resumido        int(10), 
  id_setor                   int(10) NOT NULL, 
  id_local_trabalho          int(10), 
  qtd_cargos                 tinyint, 
  dt_dispensa                datetime, 
  observacao                 varchar(255), 
  PRIMARY KEY (id_comunicado_enc_item));
CREATE TABLE Comunicado_Encerramento (
  id_comunicado_encerramento int(10) NOT NULL AUTO_INCREMENT, 
  id_usuario                 int(10), 
  id_contrato                int(10), 
  id_cliente                 int(10), 
  id_local_trabalho          int(10) NOT NULL, 
  dt_cancelamento            datetime, 
  descricao                  varchar(255), 
  status                     varchar(100), 
  area_aproximada            varchar(100), 
  informacoes_diversas       varchar(255), 
  recomendacoes              varchar(255), 
  observacao                 varchar(255), 
  id_empresa                 int(10) NOT NULL, 
  PRIMARY KEY (id_comunicado_encerramento));
CREATE TABLE Comunicado_Impl_Financeiro (
  id_comunicado_imp_financeiro int(10) NOT NULL AUTO_INCREMENT, 
  id_contrato                  int(10) NOT NULL, 
  id_comunicado_implantacao    int(10), 
  id_cliente                   int(10), 
  modalidade                   varchar(100), 
  responsavel_nfe              varchar(99), 
  contato                      varchar(99), 
  email_envio                  varchar(100), 
  documento_nf                 varchar(100), 
  dt_primeira_nf               datetime, 
  valor_primeira_nf            decimal(12, 2), 
  valor_mensal                 decimal(12, 2), 
  dia_preferencial_nf          datetime, 
  ISS                          tinyint, 
  observacao                   varchar(255), 
  id_usuario                   int(10), 
  PRIMARY KEY (id_comunicado_imp_financeiro));
CREATE TABLE Comunicado_Impl_Item (
  id_comunicado_implantacao_item int(10) NOT NULL AUTO_INCREMENT, 
  id_comunicado_implantacao      int(10) NOT NULL, 
  id_cargo                       int(10), 
  id_servico_resumido            int(10) NOT NULL, 
  id_setor                       int(10) NOT NULL, 
  qtd_cargos                     int(10), 
  carga_horaria_semanal          varchar(255), 
  salario                        decimal(12, 2), 
  PCMSO                          decimal(12, 2), 
  adicional_insalubridade        varchar(1), 
  adicional_periculosidade       varchar(1), 
  horas_extras                   varchar(1), 
  adicional_noturno              varchar(1), 
  gratificacao                   varchar(1), 
  alimentacao                    decimal(12, 2), 
  acumulo_funcao                 varchar(1), 
  vale_transporte                varchar(1), 
  seguro_vida                    decimal(12, 2), 
  PRIMARY KEY (id_comunicado_implantacao_item));
CREATE TABLE Comunicado_Implantacao (
  id_comunicado_implantacao int(10) NOT NULL AUTO_INCREMENT, 
  id_cliente                int(10), 
  id_contrato               int(10), 
  id_usuario                int(10), 
  id_local_trabalho         int(10) NOT NULL, 
  dt_solicitacao            datetime, 
  dt_aprovacao              datetime, 
  dt_previsao_implantacao   datetime, 
  descricao                 varchar(255), 
  status                    varchar(100), 
  area_aproximada           varchar(100), 
  informacoes_diversas      varchar(255), 
  exigencias                varchar(255), 
  recomendacoes             varchar(255), 
  observacao                varchar(255), 
  PRIMARY KEY (id_comunicado_implantacao));
CREATE TABLE Negocio (
  id_negocio             int(10) NOT NULL AUTO_INCREMENT, 
  id_contrato            int(10), 
  id_cliente             int(10), 
  id_usuario             int(10), 
  dt_abertura            datetime, 
  dt_fechamento          datetime, 
  titulo                 varchar(255), 
  descricao              varchar(255), 
  valor                  decimal(12, 2), 
  prioridade             varchar(255), 
  fase                   varchar(255), 
  situacao               varchar(255), 
  horario_funcionamento1 varchar(255), 
  horario_funcionamento2 varchar(255), 
  horario_funcionamento3 varchar(255), 
  id_empresa             int(10) NOT NULL, 
  PRIMARY KEY (id_negocio));
CREATE TABLE Contrato (
  id_contrato         int(10) NOT NULL AUTO_INCREMENT, 
  id_proposta         int(10), 
  id_categoria        int(10), 
  id_arquivo          int(10), 
  id_cliente          int(10), 
  id_usuario          int(10), 
  id_forma_pagamento  int(10), 
  descricao           varchar(255), 
  dt_inicio           datetime, 
  dt_fim              datetime, 
  status              varchar(100), 
  valor               decimal(12, 2), 
  observação          varchar(255), 
  id_empresa          int(10) NOT NULL, 
  ID_Contrato_Aditivo int(10) NOT NULL, 
  PRIMARY KEY (id_contrato));
CREATE TABLE aliquota_servico (
  id_aliquota_servico int(10) NOT NULL AUTO_INCREMENT, 
  id_cidade           int(10), 
  id_servico          int(10), 
  descricao           varchar(100), 
  aliquota            decimal(12, 2), 
  PRIMARY KEY (id_aliquota_servico));
CREATE TABLE Nota_observacao (
  id_nota_observacao int(10) NOT NULL AUTO_INCREMENT, 
  descricao          varchar(100), 
  mensagem           varchar(255), 
  id_empresa         int(10) NOT NULL, 
  PRIMARY KEY (id_nota_observacao));
CREATE TABLE Centro_Custo (
  id_centro_custo int(10) NOT NULL AUTO_INCREMENT, 
  id_cliente      int(10), 
  descricao       varchar(255), 
  implantacao     datetime, 
  tipo            varchar(30), 
  complemento     varchar(255), 
  ativo           varchar(1), 
  id_fornecedor   int(10), 
  PRIMARY KEY (id_centro_custo));
CREATE TABLE Nota_Saida (
  id_nota_saida              int(10) NOT NULL AUTO_INCREMENT, 
  id_modelo                  int(10), 
  id_CFOP                    int(10), 
  id_forma_pagamento         int(10), 
  id_nota_observacao         int(10), 
  id_servico                 int(10), 
  id_plano_contas            int(10), 
  id_fornecedor              int(10), 
  numero                     varchar(20), 
  serie                      varchar(3), 
  status                     int(10), 
  tipo_saida                 varchar(100), 
  chave_acesso               int(10), 
  protocolo_autorizacao      varchar(99), 
  protocolo_recebimento      varchar(99), 
  dt_emissao                 timestamp, 
  dt_saida                   timestamp, 
  hora_saida                 timestamp, 
  base_calculo_ICMS          decimal(12, 2), 
  valor_ICMS                 decimal(12, 2), 
  base_calculo_ICMS_ST       decimal(12, 2), 
  valor_ICMS_ST              decimal(12, 2), 
  valor_frete                decimal(12, 2), 
  valor_seguro               decimal(12, 2), 
  valor_desconto             decimal(12, 2), 
  outras_despesas            decimal(12, 2), 
  valor_IPI                  decimal(12, 2), 
  valor_total_NF             decimal(12, 2), 
  valor_total_produtos       decimal(12, 2), 
  valor_total_servicos       decimal(12, 2), 
  base_calculo_ISSQN         decimal(12, 2), 
  valor_ISSQN                decimal(12, 2), 
  tipo_frete                 varchar(100), 
  placa_frete                varchar(8), 
  placa_UF_frete             varchar(2), 
  quantidade_frete           tinyint, 
  modalidade_frete           varchar(1), 
  marca_frete                varchar(60), 
  numero_frete               int(10), 
  peso_bruto                 decimal(15, 3), 
  peso_liquido               decimal(15, 3), 
  informacoes_complementares varchar(255), 
  observacoes                varchar(255), 
  codigo_numerico            int(8), 
  codigo_municipio_gerador   varchar(7), 
  impressao_danf             varchar(1), 
  id_empresa                 int(10) NOT NULL, 
  PRIMARY KEY (id_nota_saida));
CREATE TABLE Modelo_NFE (
  id_modelo int(10) NOT NULL AUTO_INCREMENT, 
  descricao varchar(255), 
  codigo    varchar(2), 
  PRIMARY KEY (id_modelo));
CREATE TABLE Parametrizacao (
  id_parametrizacao int(10) NOT NULL AUTO_INCREMENT, 
  id_empresa        int(10) NOT NULL, 
  enquadramento     varchar(1), 
  msg_introducao    varchar(255), 
  tabela_preco      varchar(1), 
  versao_NFE        varchar(4), 
  ambiente_NFE      varchar(1), 
  assinatura_XML    blob, 
  CPFCNPJ_Duplicado tinyint(1), 
  PRIMARY KEY (id_parametrizacao));
CREATE TABLE Proposta_Planilha (
  id_proposta_planilha int(10) NOT NULL AUTO_INCREMENT, 
  id_proposta_item     int(10) NOT NULL, 
  id_Planilha_custo    int(10), 
  PRIMARY KEY (id_proposta_planilha));
CREATE TABLE Plano_Contas (
  id_plano_contas int(10) NOT NULL AUTO_INCREMENT, 
  id_empresa      int(10) NOT NULL, 
  id_contaDRE     int(10), 
  id_agrupamento  int(10), 
  codigo          varchar(10) UNIQUE, 
  tipo            varchar(1) UNIQUE, 
  descricao       varchar(255), 
  PRIMARY KEY (id_plano_contas));
CREATE TABLE Conta_Corrente (
  id_conta_corrente int(10) NOT NULL AUTO_INCREMENT, 
  num_agencia       varchar(10), 
  gestor            varchar(100), 
  num_conta         varchar(10), 
  id_banco          int(10), 
  id_empresa        int(10) NOT NULL, 
  PRIMARY KEY (id_conta_corrente));
CREATE TABLE Cheque (
  id_cheque                 int(10) NOT NULL AUTO_INCREMENT, 
  tipo                      varchar(100), 
  conta                     varchar(99), 
  numero_cheque             int(10), 
  valor                     decimal(12, 2), 
  dt_recebimento_emissao    datetime, 
  dt_compensacao_vencimento datetime, 
  observacao                varchar(255), 
  compensado                varchar(1), 
  id_conta_corrente         int(10), 
  id_empresa                int(10) NOT NULL, 
  PRIMARY KEY (id_cheque));
CREATE TABLE Cartao (
  id_cartao    int(10) NOT NULL AUTO_INCREMENT, 
  descricao    varchar(255), 
  tipo         varchar(100), 
  inativo      varchar(1), 
  taxa_desagio decimal(12, 2), 
  id_banco     int(10), 
  id_empresa   int(10) NOT NULL, 
  PRIMARY KEY (id_cartao));
CREATE TABLE Forma_Pagamento (
  id_forma_pagamento int(10) NOT NULL AUTO_INCREMENT, 
  tipo               varchar(1), 
  descricao          varchar(255), 
  padrao             varchar(1), 
  PRIMARY KEY (id_forma_pagamento));
CREATE TABLE Contas_Receber (
  id_contas_receber        int(10) NOT NULL AUTO_INCREMENT, 
  id_nota_saida            int(10), 
  id_cheque                int(10), 
  dt_lancamento            datetime, 
  dt_pagamento             datetime, 
  dt_vencimento            datetime, 
  dt_estorno               datetime, 
  num_parcela              int(11), 
  num_docto                varchar(60), 
  valor_docto              decimal(12, 2), 
  valor_descto             decimal(12, 2), 
  valor_acrescimo          decimal(12, 2), 
  valor_juros              decimal(12, 2), 
  status                   varchar(1), 
  observacao               varchar(255), 
  num_duplicata            varchar(60), 
  valor_duplicata          decimal(12, 2), 
  dt_duplicata             datetime, 
  id_empresa               int(10) NOT NULL, 
  id_plano_contas          int(10), 
  id_forma_pagamento       int(10), 
  id_cliente               int(10), 
  id_centro_custo_variavel int(10), 
  recebido_com             varchar(10), 
  PRIMARY KEY (id_contas_receber));
CREATE TABLE Contas_Pagar (
  id_contas_pagar    int(10) NOT NULL AUTO_INCREMENT, 
  id_nota_entrada    int(10), 
  id_cheque          int(10), 
  dt_lancamento      datetime, 
  dt_pagamento       datetime, 
  dt_vencimento      datetime, 
  dt_estorno         datetime, 
  num_parcela        int(11), 
  num_docto          varchar(60), 
  valor              decimal(12, 2), 
  valor_descto       decimal(12, 2), 
  valor_acrescimo    decimal(12, 2), 
  valor_juros        decimal(12, 2), 
  status             varchar(1), 
  observacao         varchar(255), 
  num_duplicata      varchar(60), 
  valor_duplicata    decimal(12, 2), 
  dt_duplicata       datetime, 
  id_empresa         int(10) NOT NULL, 
  id_plano_contas    int(10), 
  id_forma_pagamento int(10), 
  id_fornecedor      int(10), 
  pago_com           varchar(10), 
  PRIMARY KEY (id_contas_pagar));
CREATE TABLE Caixa_Item (
  id_caixa_item     int(10) NOT NULL AUTO_INCREMENT, 
  tipo              varchar(100), 
  Tipo_movimento    varchar(20), 
  historico         varchar(255), 
  valor             decimal(12, 2), 
  observacao        varchar(255), 
  id_contas_pagar   int(10), 
  id_contas_receber int(10) NOT NULL, 
  id_cheque         int(10), 
  id_caixa          int(10), 
  PRIMARY KEY (id_caixa_item));
CREATE TABLE Caixa (
  id_caixa       int(10) NOT NULL AUTO_INCREMENT, 
  dt_caixa       datetime, 
  saldo_anterior decimal(12, 2), 
  total_entradas decimal(12, 2), 
  total_saidas   decimal(12, 2), 
  saldo_atual    decimal(12, 2), 
  id_empresa     int(10) NOT NULL, 
  PRIMARY KEY (id_caixa), 
  CONSTRAINT UQ_Caixa 
    UNIQUE (dt_caixa, id_empresa));
CREATE TABLE Banco (
  id_banco     int(10) NOT NULL AUTO_INCREMENT, 
  nome         varchar(255), 
  numero_banco varchar(10), 
  id_empresa   int(10) NOT NULL, 
  PRIMARY KEY (id_banco));
CREATE TABLE Movimentacao_Bancaria (
  id_movimentacao_bancaria_Item int(10) NOT NULL AUTO_INCREMENT, 
  id_contas_receber             int(10), 
  id_contas_pagar               int(10), 
  dt_movimentacao_bancaria_item datetime, 
  historico                     varchar(255), 
  tipo                          varchar(100), 
  tipo_movimento                varchar(100), 
  valor                         decimal(12, 2), 
  observacao                    varchar(255), 
  id_cheque                     int(10), 
  id_banco                      int(10), 
  id_empresa                    int(10) NOT NULL, 
  id_conta_corrente             int(10), 
  PRIMARY KEY (id_movimentacao_bancaria_Item));
CREATE TABLE Pedido_Compra_Item (
  id_pedido_compra_item int(10) NOT NULL AUTO_INCREMENT, 
  id_pedido_compra      int(10) NOT NULL, 
  descricao             varchar(255), 
  status                varchar(100), 
  qtd_solicitada        smallint(5), 
  qtd_chegada           smallint(5), 
  qtd_restante          smallint(5), 
  valor_unitario        decimal(12, 2), 
  valor_total           decimal(12, 2), 
  PRIMARY KEY (id_pedido_compra_item));
CREATE TABLE Pedido_Compra (
  id_pedido_compra int(10) NOT NULL AUTO_INCREMENT, 
  numero           varchar(20), 
  status           varchar(100), 
  dt_emissao       datetime, 
  dt_previsao      datetime, 
  dt_recebimento   datetime, 
  valor_total      decimal(12, 2), 
  observacao       varchar(255), 
  id_cotacao_item  int(10), 
  id_empresa       int(10) NOT NULL, 
  id_fornecedor    int(10), 
  PRIMARY KEY (id_pedido_compra));
CREATE TABLE Transportadora (
  id_transportadora int(10) NOT NULL AUTO_INCREMENT, 
  id_cidade         int(10), 
  id_estado         int(10), 
  nome_fantasia     varchar(255), 
  razao_social      varchar(255), 
  codigo_ANTT       varchar(20), 
  endereco          varchar(255), 
  IE                varchar(20), 
  id_empresa        int(10) NOT NULL, 
  PRIMARY KEY (id_transportadora));
CREATE TABLE Nota_Saida_Item (
  id_nota_saida_item int(10) NOT NULL AUTO_INCREMENT, 
  id_nota_saida      int(10) NOT NULL, 
  id_CFOP            int(10), 
  id_produto         int(10) NOT NULL, 
  CST                varchar(3), 
  quantidade         int(10), 
  valor_unitario     decimal(12, 2), 
  valor_total        decimal(12, 2), 
  base_calculo_ICMS  decimal(12, 2), 
  valor_ICMS         decimal(12, 2), 
  valor_IPI          decimal(12, 2), 
  aliquota_ICMS      decimal(12, 2), 
  aliquota_IPI       decimal(12, 2), 
  numeracao          tinyint, 
  valor_frete        decimal(12, 2), 
  valor_seguro       decimal(12, 2), 
  valor_desconto     decimal(12, 2), 
  PRIMARY KEY (id_nota_saida_item));
CREATE TABLE Nota_entrada_Item (
  id_nota_entrada_item int(10) NOT NULL AUTO_INCREMENT, 
  id_nota_entrada      int(10) NOT NULL, 
  id_produto           int(10) NOT NULL, 
  id_CFOP              int(10), 
  CST                  varchar(3), 
  quantidade           int(10), 
  valor_unitario       decimal(12, 2), 
  base_calculo_ICMS    decimal(12, 2), 
  valor_ICMS           decimal(12, 2), 
  valor_IPI            decimal(12, 2), 
  aliquota_ICMS        decimal(12, 2), 
  aliquota_IPI         decimal(12, 2), 
  numeracao            tinyint, 
  valor_frete          decimal(12, 2), 
  valor_seguro         decimal(12, 2), 
  valor_desconto       decimal(12, 2), 
  PRIMARY KEY (id_nota_entrada_item));
CREATE TABLE Nota_entrada (
  id_nota_entrada            int(10) NOT NULL AUTO_INCREMENT, 
  id_CFOP                    int(10), 
  id_forma_pagamento         int(10), 
  id_modelo                  int(10), 
  id_nota_observacao         int(10), 
  id_plano_contas            int(10), 
  id_transportadora          int(10) NOT NULL, 
  numero                     varchar(20), 
  serie                      varchar(3), 
  status                     int(10), 
  tipo_entrada               varchar(100), 
  tipo_emissao               varchar(100), 
  chave_acesso               int(10), 
  protocolo_autorizacao      varchar(99), 
  dt_emissao                 timestamp, 
  dt_entrada                 timestamp, 
  hora_saida                 timestamp, 
  base_calculo_ICMS          decimal(12, 2), 
  valor_ICMS                 decimal(12, 2), 
  base_calculo_ICMS_ST       decimal(12, 2), 
  valor_ICMS_ST              decimal(12, 2), 
  valor_frete                decimal(12, 2), 
  valor_seguro               decimal(12, 2), 
  valor_desconto             decimal(12, 2), 
  outras_despesas            decimal(12, 2), 
  valor_IPI                  decimal(12, 2), 
  valor_total_NF             decimal(12, 2), 
  valor_total_produtos       decimal(12, 2), 
  valor_total_servicos       decimal(12, 2), 
  base_calculo_ISSQN         decimal(12, 2), 
  valor_ISSQN                decimal(12, 2), 
  tipo_frete                 varchar(100), 
  placa_frete                varchar(8), 
  placa_UF_frete             varchar(2), 
  quantidade_frete           tinyint, 
  modalidade_frete           varchar(1), 
  marca_frete                varchar(60), 
  numero_frete               int(10), 
  peso_bruto                 decimal(15, 3), 
  peso_liquido               decimal(15, 3), 
  informacoes_complementares varchar(255), 
  observacoes                varchar(255), 
  codigo_numerico            int(8), 
  cod_municipio_gerador      varchar(7), 
  impressao_danf             varchar(1), 
  id_fornecedor              int(10), 
  id_pedido_compra           int(10), 
  id_empresa                 int(10) NOT NULL, 
  PRIMARY KEY (id_nota_entrada));
CREATE TABLE Planilha_Custo_Item (
  id_planilha_custos_item int(10) NOT NULL AUTO_INCREMENT, 
  id_Planilha_custo       int(10) NOT NULL, 
  tipo                    varchar(100), 
  grupo                   varchar(100), 
  desricao                varchar(255), 
  percentual              int(10), 
  valor                   int(10), 
  PRIMARY KEY (id_planilha_custos_item));
CREATE TABLE Planilha_Custos (
  id_Planilha_custo int(10) NOT NULL AUTO_INCREMENT, 
  descricao         varchar(255), 
  tipo              varchar(100), 
  PRIMARY KEY (id_Planilha_custo));
CREATE TABLE Proposta_Item (
  id_proposta_item int(10) NOT NULL AUTO_INCREMENT, 
  id_proposta      int(10), 
  tipo             varchar(100), 
  descricao        varchar(255), 
  quantidade       int(10), 
  sub_total        decimal(12, 2), 
  total            decimal(12, 2), 
  PRIMARY KEY (id_proposta_item));
CREATE TABLE Proposta (
  id_proposta    int(10) NOT NULL AUTO_INCREMENT, 
  nome           varchar(255), 
  status         varchar(1), 
  dt_emissao     datetime, 
  dt_validade    datetime, 
  valor_desconto decimal(12, 2), 
  valor_total    decimal(12, 2), 
  observacao     varchar(255), 
  introducao     varchar(255), 
  id_empresa     int(10) NOT NULL, 
  PRIMARY KEY (id_proposta));
CREATE TABLE Perfil (
  id_perfil int(10) NOT NULL AUTO_INCREMENT, 
  role      varchar(20), 
  descricao varchar(255), 
  PRIMARY KEY (id_perfil));
CREATE TABLE Usuario (
  id_usuario            int(10) NOT NULL AUTO_INCREMENT, 
  id_perfil             int(10), 
  id_empresa            int(10) NOT NULL, 
  nome                  varchar(255), 
  email                 varchar(100), 
  login                 varchar(20), 
  senha                 varchar(100), 
  dt_cadastro           datetime, 
  dt_ultima_atualizacao datetime, 
  dt_ultimo_acesso      timestamp, 
  inativo               varchar(1), 
  PRIMARY KEY (id_usuario));
CREATE TABLE CFOP (
  ID_CFOP   int(10) NOT NULL AUTO_INCREMENT, 
  DESCRICAO varchar(255), 
  ESTADO    varchar(2), 
  CONSTRAINT SYS_C008176 
    PRIMARY KEY (ID_CFOP));
CREATE TABLE NCM (
  ID_NCM          int(10) NOT NULL AUTO_INCREMENT, 
  DESCRICAO       varchar(255), 
  NOMECLATURA_NCM varchar(11), 
  CONSTRAINT SYS_C008178 
    PRIMARY KEY (ID_NCM));
CREATE TABLE Unidade (
  id_unidade         int(10) NOT NULL AUTO_INCREMENT, 
  descricao          varchar(255), 
  Descricao_Resumida varchar(5), 
  PRIMARY KEY (id_unidade));
CREATE TABLE Servico (
  id_servico int(10) NOT NULL AUTO_INCREMENT, 
  descricao  varchar(255), 
  codigo     varchar(10), 
  id_empresa int(10) NOT NULL, 
  PRIMARY KEY (id_servico));
CREATE TABLE Sub_Grupo (
  id_subgrupo int(10) NOT NULL AUTO_INCREMENT, 
  descricao   varchar(255), 
  id_grupo    int(10), 
  PRIMARY KEY (id_subgrupo));
CREATE TABLE Grupo (
  id_grupo  int(10) NOT NULL AUTO_INCREMENT, 
  descricao varchar(255), 
  PRIMARY KEY (id_grupo));
CREATE TABLE Produto (
  id_produto              int(10) NOT NULL AUTO_INCREMENT, 
  id_NCM                  int(10), 
  id_unidade_entrada      int(10), 
  id_grupo                int(10), 
  descricao               varchar(255), 
  dt_cadastro             datetime, 
  dt_ultima_atualizacao   timestamp, 
  inativo                 varchar(1), 
  referencia_fabricante   varchar(18), 
  descricao_reduzida      varchar(100), 
  quantidade              int(10), 
  estoque_min             int(10), 
  estoque_max             int(10), 
  preco_custo             decimal(12, 2), 
  preco_custo_anterior    decimal(12, 2), 
  preco_custo_inicial     decimal(12, 2), 
  preco_custo_promocional decimal(12, 2), 
  base_conversao          varchar(1), 
  fator_conversao_entrada decimal(9, 2), 
  fator_conversao_saida   decimal(5, 2), 
  fracionado              varchar(1), 
  tipo_tributacao         varchar(1), 
  CST                     varchar(3), 
  aliq_icms_entrada       decimal(4, 2), 
  aliq_icms_saida         decimal(4, 2), 
  base_calculo_icms       decimal(12, 2), 
  aliquota_ipi            decimal(4, 2), 
  EAN                     varchar(14), 
  id_subgrupo             int(10), 
  id_empresa              int(10) NOT NULL, 
  id_unidade_saida        int(10), 
  aliquota_cofins         decimal(4, 2), 
  aliquota_pis            decimal(4, 2), 
  CST_PIS_ENTRADA         int(10), 
  CST_PIS_SAIDA           int(10), 
  CST_COFINS_ENTRADA      int(10), 
  CST_COFINS_SAIDA        int(10), 
  aliquota_pis_saida      decimal(4, 2), 
  aliquota_cofins_saida   decimal(4, 2), 
  CSOSN                   varchar(3), 
  ID_LOCALIZACAO          int(10), 
  LOCALIZACAO_DESCRITA    varchar(200), 
  PRIMARY KEY (id_produto));
CREATE TABLE Cargo (
  id_cargo                 int(10) NOT NULL AUTO_INCREMENT, 
  descricao                varchar(255), 
  CBO                      varchar(20), 
  salario_base             decimal(12, 2), 
  adicional_insalubridade  decimal(12, 2), 
  adicional_periculosidade decimal(12, 2), 
  alimentacao              decimal(12, 2), 
  acumulo_funcao           decimal(12, 2), 
  vale_transporte          decimal(12, 2), 
  seguro_vida              decimal(12, 2), 
  id_empresa               int(10) NOT NULL, 
  PRIMARY KEY (id_cargo));
CREATE TABLE Cliente_Responsavel (
  id_cliente_responsavel int(10) NOT NULL AUTO_INCREMENT, 
  id_cliente             int(10), 
  tipo                   varchar(20), 
  nome                   varchar(255), 
  CPF                    varchar(14), 
  email                  varchar(255), 
  CEP                    varchar(9), 
  tipo_logradouro        varchar(20), 
  logradouro             varchar(255), 
  numero                 varchar(5), 
  complemento            varchar(255), 
  estado_civil           varchar(15), 
  observacoes            varchar(255), 
  PRIMARY KEY (id_cliente_responsavel));
CREATE TABLE Cliente (
  id_cliente               int(10) NOT NULL AUTO_INCREMENT, 
  id_cidade                int(10), 
  nome                     varchar(255), 
  CGCCPF                   varchar(20), 
  RG                       varchar(20), 
  IE                       varchar(20), 
  IM                       varchar(20), 
  email                    varchar(255), 
  sexo                     varchar(1), 
  dt_nascimento            datetime, 
  CEP                      varchar(9), 
  tipo_logradouro          varchar(20), 
  logradouro               varchar(255), 
  numero                   varchar(5), 
  bairro                   varchar(255), 
  complemento              varchar(255), 
  CEP_cobranca             varchar(9), 
  tipo_logradouro_cobranca varchar(20), 
  logradouro_cobranca      varchar(255), 
  numero_cobranca          varchar(5), 
  bairro_cobranca          varchar(255), 
  complemento_cobranca     varchar(255), 
  tipo                     varchar(1), 
  home_page                varchar(255), 
  dt_cadastro              datetime, 
  dt_ultima_atualizacao    datetime, 
  dt_inicio_contrato       datetime, 
  inativo                  varchar(1), 
  contato                  varchar(255), 
  observacoes              varchar(255), 
  bloqueado                varchar(1), 
  codigo_gps               varchar(20), 
  tributacao               varchar(200), 
  id_cidade_cobranca       int(10), 
  id_empresa               int(10) NOT NULL, 
  PRIMARY KEY (id_cliente));
CREATE TABLE Fornecedor (
  id_fornecedor   int(10) NOT NULL AUTO_INCREMENT, 
  nome            varchar(255), 
  CGCCPF          varchar(20), 
  tipo_fornecedor varchar(1), 
  CEP             varchar(9), 
  tipo_logradouro varchar(20), 
  logradouro      varchar(200), 
  numero          varchar(5), 
  bairro          varchar(255), 
  complemento     varchar(255), 
  email           varchar(255), 
  home_page       varchar(255), 
  dt_cadastro     datetime, 
  observacao      varchar(255), 
  inativo         varchar(1), 
  codigo_gps      varchar(20), 
  num_juridico    varchar(20), 
  id_empresa      int(10) NOT NULL, 
  id_cidade       int(10), 
  PRIMARY KEY (id_fornecedor));
CREATE TABLE Estado (
  id_estado  int(10) NOT NULL AUTO_INCREMENT, 
  nome       varchar(255), 
  sigla      varchar(2) UNIQUE, 
  regiao     varchar(20), 
  sub_regiao varchar(100), 
  PRIMARY KEY (id_estado));
CREATE TABLE Cidade (
  id_cidade   int(10) NOT NULL AUTO_INCREMENT, 
  id_estado   int(10), 
  nome        varchar(255), 
  codigo_IBGE varchar(8), 
  CEP         varchar(20), 
  PRIMARY KEY (id_cidade));
CREATE TABLE Telefone (
  id_telefone   int(10) NOT NULL AUTO_INCREMENT, 
  numero        varchar(15), 
  operadora     varchar(100), 
  id_cliente    int(10), 
  id_fornecedor int(10), 
  PRIMARY KEY (id_telefone));
CREATE TABLE Empresa (
  id_empresa          int(10) NOT NULL AUTO_INCREMENT, 
  id_cidade           int(10), 
  id_estado           int(10), 
  CNPJ                varchar(17), 
  nome_fantasia       varchar(255), 
  razao_social        varchar(255), 
  CEP                 varchar(9), 
  tipo_logradouro     varchar(20), 
  logradouro          varchar(255), 
  numero              varchar(5), 
  bairro              varchar(255), 
  complemento         varchar(255), 
  email               varchar(255), 
  contato             varchar(255), 
  inativo             varchar(1), 
  IE                  varchar(20), 
  CNAE                varchar(7), 
  nome_contador       varchar(255), 
  CRC_contador        varchar(20), 
  junta_comercial     varchar(20), 
  IM                  varchar(20), 
  NIRE                varchar(20), 
  observacao          varchar(255), 
  inscricao_suframa   int(10), 
  num_juridico        varchar(20), 
  Caixa_Inicial_Valor decimal(10, 2), 
  PRIMARY KEY (id_empresa));
ALTER TABLE Movimentacao_Bancaria ADD INDEX FKMovimentac743588 (id_conta_corrente), ADD CONSTRAINT FKMovimentac743588 FOREIGN KEY (id_conta_corrente) REFERENCES Conta_Corrente (id_conta_corrente);
ALTER TABLE Acesso ADD INDEX FKAcesso500032 (id_perfil), ADD CONSTRAINT FKAcesso500032 FOREIGN KEY (id_perfil) REFERENCES Perfil (id_perfil);
ALTER TABLE Contrato ADD INDEX FKContrato112395 (ID_Contrato_Aditivo), ADD CONSTRAINT FKContrato112395 FOREIGN KEY (ID_Contrato_Aditivo) REFERENCES Contrato (id_contrato);
ALTER TABLE Centro_Custo_Variavel ADD INDEX FKCentro_Cus572819 (id_cliente), ADD CONSTRAINT FKCentro_Cus572819 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Centro_Custo ADD INDEX FKCentro_Cus664334 (id_fornecedor), ADD CONSTRAINT FKCentro_Cus664334 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Produto ADD INDEX FKProduto730682 (ID_LOCALIZACAO), ADD CONSTRAINT FKProduto730682 FOREIGN KEY (ID_LOCALIZACAO) REFERENCES Localizacao (ID_LOCALIZACAO);
ALTER TABLE Movimentacao_Bancaria ADD INDEX FKMovimentac743589 (id_conta_corrente), ADD CONSTRAINT FKMovimentac743589 FOREIGN KEY (id_conta_corrente) REFERENCES Conta_Corrente (id_conta_corrente);
ALTER TABLE Ordem_Compra_Item ADD INDEX FKOrdem_Comp811482 (id_produto), ADD CONSTRAINT FKOrdem_Comp811482 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Ordem_Compra_Item ADD INDEX FKOrdem_Comp783266 (id_requisicao_material_item), ADD CONSTRAINT FKOrdem_Comp783266 FOREIGN KEY (id_requisicao_material_item) REFERENCES Requisicao_Material_item (id_requisicao_material_item);
ALTER TABLE Movimentacao_Bancaria ADD INDEX FKMovimentac394382 (id_empresa), ADD CONSTRAINT FKMovimentac394382 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Movimentacao_Bancaria ADD INDEX FKMovimentac564322 (id_banco), ADD CONSTRAINT FKMovimentac564322 FOREIGN KEY (id_banco) REFERENCES Banco (id_banco);
ALTER TABLE Centro_Custo_Variavel ADD INDEX FKCentro_Cus851461 (id_plano_contas), ADD CONSTRAINT FKCentro_Cus851461 FOREIGN KEY (id_plano_contas) REFERENCES Plano_Contas (id_plano_contas);
ALTER TABLE Centro_Custo_Variavel ADD INDEX FKCentro_Cus676942 (id_caixa_item), ADD CONSTRAINT FKCentro_Cus676942 FOREIGN KEY (id_caixa_item) REFERENCES Caixa_Item (id_caixa_item);
ALTER TABLE Centro_Custo_Variavel ADD INDEX FKCentro_Cus697570 (id_nota_entrada), ADD CONSTRAINT FKCentro_Cus697570 FOREIGN KEY (id_nota_entrada) REFERENCES Nota_entrada (id_nota_entrada);
ALTER TABLE Centro_Custo_Variavel ADD INDEX FKCentro_Cus87269 (id_nota_saida), ADD CONSTRAINT FKCentro_Cus87269 FOREIGN KEY (id_nota_saida) REFERENCES Nota_Saida (id_nota_saida);
ALTER TABLE Centro_Custo_Variavel ADD INDEX FKCentro_Cus284872 (id_contas_receber), ADD CONSTRAINT FKCentro_Cus284872 FOREIGN KEY (id_contas_receber) REFERENCES Contas_Receber (id_contas_receber);
ALTER TABLE Centro_Custo_Variavel ADD INDEX FKCentro_Cus947639 (id_contas_pagar), ADD CONSTRAINT FKCentro_Cus947639 FOREIGN KEY (id_contas_pagar) REFERENCES Contas_Pagar (id_contas_pagar);
ALTER TABLE Contas_Receber ADD INDEX FKContas_Rec200801 (id_centro_custo_variavel), ADD CONSTRAINT FKContas_Rec200801 FOREIGN KEY (id_centro_custo_variavel) REFERENCES Centro_Custo_Variavel (id_centro_custo_variavel);
ALTER TABLE Centro_Custo_Variavel ADD INDEX FKCentro_Cus666916 (id_centro_custo), ADD CONSTRAINT FKCentro_Cus666916 FOREIGN KEY (id_centro_custo) REFERENCES Centro_Custo (id_centro_custo);
ALTER TABLE Plano_Contas ADD INDEX FKPlano_Cont591988 (id_agrupamento), ADD CONSTRAINT FKPlano_Cont591988 FOREIGN KEY (id_agrupamento) REFERENCES Agrupamento (id_agrupamento);
ALTER TABLE Plano_Contas ADD INDEX FKPlano_Cont388635 (id_contaDRE), ADD CONSTRAINT FKPlano_Cont388635 FOREIGN KEY (id_contaDRE) REFERENCES contas_DRE (id_contaDRE);
ALTER TABLE Mensagem ADD INDEX FKMensagem443202 (id_usuario_receiver), ADD CONSTRAINT FKMensagem443202 FOREIGN KEY (id_usuario_receiver) REFERENCES Usuario (id_usuario);
ALTER TABLE Mensagem ADD INDEX FKMensagem127800 (id_usuario_sender), ADD CONSTRAINT FKMensagem127800 FOREIGN KEY (id_usuario_sender) REFERENCES Usuario (id_usuario);
ALTER TABLE Contas_Receber ADD INDEX FKContas_Rec867318 (id_cliente), ADD CONSTRAINT FKContas_Rec867318 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Contas_Pagar ADD INDEX FKContas_Pag459482 (id_fornecedor), ADD CONSTRAINT FKContas_Pag459482 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Contas_Receber ADD INDEX FKContas_Rec735197 (id_forma_pagamento), ADD CONSTRAINT FKContas_Rec735197 FOREIGN KEY (id_forma_pagamento) REFERENCES Forma_Pagamento (id_forma_pagamento);
ALTER TABLE Contas_Pagar ADD INDEX FKContas_Pag475941 (id_forma_pagamento), ADD CONSTRAINT FKContas_Pag475941 FOREIGN KEY (id_forma_pagamento) REFERENCES Forma_Pagamento (id_forma_pagamento);
ALTER TABLE Pedido_Compra ADD INDEX FKPedido_Com721962 (id_fornecedor), ADD CONSTRAINT FKPedido_Com721962 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Cotacao ADD INDEX FKCotacao854533 (id_fornecedor), ADD CONSTRAINT FKCotacao854533 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Ordem_Compra ADD INDEX FKOrdem_Comp778082 (id_fornecedor), ADD CONSTRAINT FKOrdem_Comp778082 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Contas_Receber ADD INDEX FKContas_Rec411323 (id_plano_contas), ADD CONSTRAINT FKContas_Rec411323 FOREIGN KEY (id_plano_contas) REFERENCES Plano_Contas (id_plano_contas);
ALTER TABLE Contas_Pagar ADD INDEX FKContas_Pag799815 (id_plano_contas), ADD CONSTRAINT FKContas_Pag799815 FOREIGN KEY (id_plano_contas) REFERENCES Plano_Contas (id_plano_contas);
ALTER TABLE Fornecedor ADD INDEX FKFornecedor571917 (id_cidade), ADD CONSTRAINT FKFornecedor571917 FOREIGN KEY (id_cidade) REFERENCES Cidade (id_cidade);
ALTER TABLE Produto ADD INDEX FKProduto948521 (id_unidade_saida), ADD CONSTRAINT FKProduto948521 FOREIGN KEY (id_unidade_saida) REFERENCES Unidade (id_unidade);
ALTER TABLE Pedido_Compra ADD INDEX FKPedido_Com786785 (id_empresa), ADD CONSTRAINT FKPedido_Com786785 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cotacao_Item ADD INDEX FKCotacao_It117113 (id_empresa), ADD CONSTRAINT FKCotacao_It117113 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Ordem_Compra ADD INDEX FKOrdem_Comp258331 (id_empresa), ADD CONSTRAINT FKOrdem_Comp258331 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Transportadora ADD INDEX FKTransporta234325 (id_empresa), ADD CONSTRAINT FKTransporta234325 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Requisicao_Material ADD INDEX FKRequisicao176983 (id_empresa), ADD CONSTRAINT FKRequisicao176983 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Contas_Receber ADD INDEX FKContas_Rec605798 (id_empresa), ADD CONSTRAINT FKContas_Rec605798 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Contas_Pagar ADD INDEX FKContas_Pag394659 (id_empresa), ADD CONSTRAINT FKContas_Pag394659 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cartao ADD INDEX FKCartao877858 (id_empresa), ADD CONSTRAINT FKCartao877858 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Conta_Corrente ADD INDEX FKConta_Corr754890 (id_empresa), ADD CONSTRAINT FKConta_Corr754890 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cheque ADD INDEX FKCheque952955 (id_empresa), ADD CONSTRAINT FKCheque952955 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Banco ADD INDEX FKBanco418507 (id_empresa), ADD CONSTRAINT FKBanco418507 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Caixa ADD INDEX FKCaixa499153 (id_empresa), ADD CONSTRAINT FKCaixa499153 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cargo ADD INDEX FKCargo491017 (id_empresa), ADD CONSTRAINT FKCargo491017 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Local_trabalho ADD INDEX FKLocal_trab250657 (id_empresa), ADD CONSTRAINT FKLocal_trab250657 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Proposta ADD INDEX FKProposta402772 (id_empresa), ADD CONSTRAINT FKProposta402772 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Negocio ADD INDEX FKNegocio516523 (id_empresa), ADD CONSTRAINT FKNegocio516523 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Nota_entrada ADD INDEX FKNota_entra467910 (id_empresa), ADD CONSTRAINT FKNota_entra467910 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Servico ADD INDEX FKServico440633 (id_empresa), ADD CONSTRAINT FKServico440633 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Nota_Saida ADD INDEX FKNota_Saida961027 (id_empresa), ADD CONSTRAINT FKNota_Saida961027 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Nota_observacao ADD INDEX FKNota_obser478591 (id_empresa), ADD CONSTRAINT FKNota_obser478591 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Plano_Contas ADD INDEX FKPlano_Cont606995 (id_empresa), ADD CONSTRAINT FKPlano_Cont606995 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Produto ADD INDEX FKProduto188482 (id_empresa), ADD CONSTRAINT FKProduto188482 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Comunicado_Encerramento ADD INDEX FKComunicado256461 (id_empresa), ADD CONSTRAINT FKComunicado256461 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Categoria ADD INDEX FKCategoria445726 (id_empresa), ADD CONSTRAINT FKCategoria445726 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Contrato ADD INDEX FKContrato298598 (id_empresa), ADD CONSTRAINT FKContrato298598 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Colaborador ADD INDEX FKColaborado260212 (id_empresa), ADD CONSTRAINT FKColaborado260212 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Setor ADD INDEX FKSetor593329 (id_empresa), ADD CONSTRAINT FKSetor593329 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Usuario ADD INDEX FKUsuario559516 (id_empresa), ADD CONSTRAINT FKUsuario559516 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cliente ADD INDEX FKCliente873744 (id_empresa), ADD CONSTRAINT FKCliente873744 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Fornecedor ADD INDEX FKFornecedor728838 (id_empresa), ADD CONSTRAINT FKFornecedor728838 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Cliente ADD INDEX FKCliente448042 (id_cidade_cobranca), ADD CONSTRAINT FKCliente448042 FOREIGN KEY (id_cidade_cobranca) REFERENCES Cidade (id_cidade);
ALTER TABLE Telefone ADD INDEX FKTelefone271741 (id_cliente), ADD CONSTRAINT FKTelefone271741 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente) ON DELETE Cascade;
ALTER TABLE Telefone ADD INDEX FKTelefone809681 (id_fornecedor), ADD CONSTRAINT FKTelefone809681 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor) ON DELETE Cascade;
ALTER TABLE Colaborador ADD INDEX FKColaborado277140 (id_setor), ADD CONSTRAINT FKColaborado277140 FOREIGN KEY (id_setor) REFERENCES Setor (id_setor);
ALTER TABLE Usuario ADD INDEX FKUsuario833184 (id_perfil), ADD CONSTRAINT FKUsuario833184 FOREIGN KEY (id_perfil) REFERENCES Perfil (id_perfil);
ALTER TABLE Produto ADD INDEX FKProduto532613 (id_subgrupo), ADD CONSTRAINT FKProduto532613 FOREIGN KEY (id_subgrupo) REFERENCES Sub_Grupo (id_subgrupo);
ALTER TABLE Produto ADD INDEX FKProduto163784 (id_grupo), ADD CONSTRAINT FKProduto163784 FOREIGN KEY (id_grupo) REFERENCES Grupo (id_grupo);
ALTER TABLE Sub_Grupo ADD INDEX FKSub_Grupo364878 (id_grupo), ADD CONSTRAINT FKSub_Grupo364878 FOREIGN KEY (id_grupo) REFERENCES Grupo (id_grupo);
ALTER TABLE Caixa_Item ADD INDEX FKCaixa_Item223632 (id_caixa), ADD CONSTRAINT FKCaixa_Item223632 FOREIGN KEY (id_caixa) REFERENCES Caixa (id_caixa);
ALTER TABLE Cotacao_Item ADD INDEX FKCotacao_It805320 (id_produto), ADD CONSTRAINT FKCotacao_It805320 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Nota_entrada ADD INDEX FKNota_entra209806 (id_pedido_compra), ADD CONSTRAINT FKNota_entra209806 FOREIGN KEY (id_pedido_compra) REFERENCES Pedido_Compra (id_pedido_compra);
ALTER TABLE Pedido_Compra ADD INDEX FKPedido_Com428724 (id_cotacao_item), ADD CONSTRAINT FKPedido_Com428724 FOREIGN KEY (id_cotacao_item) REFERENCES Cotacao_Item (id_cotacao_item);
ALTER TABLE Cotacao ADD INDEX FKCotacao57563 (id_ordem_compra), ADD CONSTRAINT FKCotacao57563 FOREIGN KEY (id_ordem_compra) REFERENCES Ordem_Compra (id_ordem_compra);
ALTER TABLE Contrato_Negocio ADD INDEX FKContrato_N42795 (id_negocio), ADD CONSTRAINT FKContrato_N42795 FOREIGN KEY (id_negocio) REFERENCES Negocio (id_negocio);
ALTER TABLE Contrato_Negocio ADD INDEX FKContrato_N864326 (id_contrato), ADD CONSTRAINT FKContrato_N864326 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Comunicado_Enc_Item ADD INDEX FKComunicado932900 (id_local_trabalho), ADD CONSTRAINT FKComunicado932900 FOREIGN KEY (id_local_trabalho) REFERENCES Local_trabalho (id_local_trabalho);
ALTER TABLE Colaborador ADD INDEX FKColaborado641515 (id_local_trabalho), ADD CONSTRAINT FKColaborado641515 FOREIGN KEY (id_local_trabalho) REFERENCES Local_trabalho (id_local_trabalho);
ALTER TABLE Comunicado_Enc_Item ADD INDEX FKComunicado120034 (id_setor), ADD CONSTRAINT FKComunicado120034 FOREIGN KEY (id_setor) REFERENCES Setor (id_setor);
ALTER TABLE Comunicado_Enc_Item ADD INDEX FKComunicado7553 (id_servico_resumido), ADD CONSTRAINT FKComunicado7553 FOREIGN KEY (id_servico_resumido) REFERENCES Servico_Resumido (id_servico_resumido);
ALTER TABLE Comunicado_Enc_Item ADD INDEX FKComunicado426970 (id_cargo), ADD CONSTRAINT FKComunicado426970 FOREIGN KEY (id_cargo) REFERENCES Cargo (id_cargo);
ALTER TABLE Comunicado_Encerramento ADD INDEX FKComunicado362235 (id_local_trabalho), ADD CONSTRAINT FKComunicado362235 FOREIGN KEY (id_local_trabalho) REFERENCES Local_trabalho (id_local_trabalho);
ALTER TABLE Comunicado_Impl_Item ADD INDEX FKComunicado302763 (id_setor), ADD CONSTRAINT FKComunicado302763 FOREIGN KEY (id_setor) REFERENCES Setor (id_setor);
ALTER TABLE Comunicado_Enc_Financeiro ADD INDEX FKComunicado316727 (id_cliente), ADD CONSTRAINT FKComunicado316727 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Comunicado_Encerramento ADD INDEX FKComunicado216656 (id_cliente), ADD CONSTRAINT FKComunicado216656 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Comunicado_Impl_Financeiro ADD INDEX FKComunicado599435 (id_usuario), ADD CONSTRAINT FKComunicado599435 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Comunicado_Impl_Financeiro ADD INDEX FKComunicado72371 (id_cliente), ADD CONSTRAINT FKComunicado72371 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Arquivo ADD INDEX FKArquivo952118 (id_contrato), ADD CONSTRAINT FKArquivo952118 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Cotacao_item_historico ADD INDEX FKCotacao_it432784 (id_produto), ADD CONSTRAINT FKCotacao_it432784 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Cotacao_item_historico ADD INDEX FKCotacao_it599875 (id_fornecedor_selecionado), ADD CONSTRAINT FKCotacao_it599875 FOREIGN KEY (id_fornecedor_selecionado) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Cotacao_Item ADD INDEX FKCotacao_It842332 (id_cotacao), ADD CONSTRAINT FKCotacao_It842332 FOREIGN KEY (id_cotacao) REFERENCES Cotacao (id_cotacao);
ALTER TABLE Produto_Fornecedor ADD INDEX FKProduto_Fo272158 (id_fornecedor), ADD CONSTRAINT FKProduto_Fo272158 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Produto_Fornecedor ADD INDEX FKProduto_Fo519127 (id_produto), ADD CONSTRAINT FKProduto_Fo519127 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto) ON DELETE Cascade;
ALTER TABLE Cotacao_item_historico ADD INDEX FKCotacao_it320246 (id_fornecedor), ADD CONSTRAINT FKCotacao_it320246 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Cotacao_item_historico ADD INDEX FKCotacao_it469796 (id_cotacao), ADD CONSTRAINT FKCotacao_it469796 FOREIGN KEY (id_cotacao) REFERENCES Cotacao (id_cotacao);
ALTER TABLE Nota_entrada ADD INDEX FKNota_entra403087 (id_fornecedor), ADD CONSTRAINT FKNota_entra403087 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Nota_Saida ADD INDEX FKNota_Saida25851 (id_fornecedor), ADD CONSTRAINT FKNota_Saida25851 FOREIGN KEY (id_fornecedor) REFERENCES Fornecedor (id_fornecedor);
ALTER TABLE Cotacao ADD INDEX FKCotacao988399 (id_usuario), ADD CONSTRAINT FKCotacao988399 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Ordem_Compra_Item ADD INDEX FKOrdem_Comp347583 (id_ordem_compra), ADD CONSTRAINT FKOrdem_Comp347583 FOREIGN KEY (id_ordem_compra) REFERENCES Ordem_Compra (id_ordem_compra);
ALTER TABLE Ordem_Compra ADD INDEX FKOrdem_Comp59642 (id_usuario), ADD CONSTRAINT FKOrdem_Comp59642 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Ordem_Compra ADD INDEX FKOrdem_Comp847763 (id_requisicao_material), ADD CONSTRAINT FKOrdem_Comp847763 FOREIGN KEY (id_requisicao_material) REFERENCES Requisicao_Material (id_requisicao_material);
ALTER TABLE Pedido_Compra_Item ADD INDEX FKPedido_Com510361 (id_pedido_compra), ADD CONSTRAINT FKPedido_Com510361 FOREIGN KEY (id_pedido_compra) REFERENCES Pedido_Compra (id_pedido_compra);
ALTER TABLE Nota_entrada ADD INDEX FKNota_entra933979 (id_transportadora), ADD CONSTRAINT FKNota_entra933979 FOREIGN KEY (id_transportadora) REFERENCES Transportadora (id_transportadora);
ALTER TABLE Requisicao_Material_item ADD INDEX FKRequisicao460104 (id_produto), ADD CONSTRAINT FKRequisicao460104 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Requisicao_Material ADD INDEX FKRequisicao703865 (id_cliente), ADD CONSTRAINT FKRequisicao703865 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Requisicao_Material_item ADD INDEX FKRequisicao558879 (id_requisicao_material), ADD CONSTRAINT FKRequisicao558879 FOREIGN KEY (id_requisicao_material) REFERENCES Requisicao_Material (id_requisicao_material);
ALTER TABLE Requisicao_Material ADD INDEX FKRequisicao375672 (id_usuario), ADD CONSTRAINT FKRequisicao375672 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Requisicao_Material ADD INDEX FKRequisicao441713 (id_local_trabalho), ADD CONSTRAINT FKRequisicao441713 FOREIGN KEY (id_local_trabalho) REFERENCES Local_trabalho (id_local_trabalho);
ALTER TABLE Comunicado_Implantacao ADD INDEX FKComunicado142486 (id_local_trabalho), ADD CONSTRAINT FKComunicado142486 FOREIGN KEY (id_local_trabalho) REFERENCES Local_trabalho (id_local_trabalho);
ALTER TABLE Comunicado_Implantacao ADD INDEX FKComunicado24307 (id_contrato), ADD CONSTRAINT FKComunicado24307 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Nota_entrada ADD INDEX FKNota_entra309205 (id_plano_contas), ADD CONSTRAINT FKNota_entra309205 FOREIGN KEY (id_plano_contas) REFERENCES Plano_Contas (id_plano_contas);
ALTER TABLE Nota_Saida ADD INDEX FKNota_Saida233447 (id_plano_contas), ADD CONSTRAINT FKNota_Saida233447 FOREIGN KEY (id_plano_contas) REFERENCES Plano_Contas (id_plano_contas);
ALTER TABLE Movimentacao_Bancaria ADD INDEX FKMovimentac642968 (id_cheque), ADD CONSTRAINT FKMovimentac642968 FOREIGN KEY (id_cheque) REFERENCES Cheque (id_cheque);
ALTER TABLE Caixa_Item ADD INDEX FKCaixa_Item544216 (id_cheque), ADD CONSTRAINT FKCaixa_Item544216 FOREIGN KEY (id_cheque) REFERENCES Cheque (id_cheque);
ALTER TABLE Caixa_Item ADD INDEX FKCaixa_Item852352 (id_contas_receber), ADD CONSTRAINT FKCaixa_Item852352 FOREIGN KEY (id_contas_receber) REFERENCES Contas_Receber (id_contas_receber);
ALTER TABLE Caixa_Item ADD INDEX FKCaixa_Item515120 (id_contas_pagar), ADD CONSTRAINT FKCaixa_Item515120 FOREIGN KEY (id_contas_pagar) REFERENCES Contas_Pagar (id_contas_pagar);
ALTER TABLE Movimentacao_Bancaria ADD INDEX FKMovimentac416368 (id_contas_pagar), ADD CONSTRAINT FKMovimentac416368 FOREIGN KEY (id_contas_pagar) REFERENCES Contas_Pagar (id_contas_pagar);
ALTER TABLE Movimentacao_Bancaria ADD INDEX FKMovimentac753600 (id_contas_receber), ADD CONSTRAINT FKMovimentac753600 FOREIGN KEY (id_contas_receber) REFERENCES Contas_Receber (id_contas_receber);
ALTER TABLE Conta_Corrente ADD INDEX FKConta_Corr257995 (id_banco), ADD CONSTRAINT FKConta_Corr257995 FOREIGN KEY (id_banco) REFERENCES Banco (id_banco);
ALTER TABLE Cheque ADD INDEX FKCheque90927 (id_conta_corrente), ADD CONSTRAINT FKCheque90927 FOREIGN KEY (id_conta_corrente) REFERENCES Conta_Corrente (id_conta_corrente);
ALTER TABLE Contas_Receber ADD INDEX FKContas_Rec328441 (id_cheque), ADD CONSTRAINT FKContas_Rec328441 FOREIGN KEY (id_cheque) REFERENCES Cheque (id_cheque);
ALTER TABLE Contas_Pagar ADD INDEX FKContas_Pag432010 (id_cheque), ADD CONSTRAINT FKContas_Pag432010 FOREIGN KEY (id_cheque) REFERENCES Cheque (id_cheque);
ALTER TABLE Contas_Receber ADD INDEX FKContas_Rec444183 (id_nota_saida), ADD CONSTRAINT FKContas_Rec444183 FOREIGN KEY (id_nota_saida) REFERENCES Nota_Saida (id_nota_saida);
ALTER TABLE Contas_Pagar ADD INDEX FKContas_Pag622742 (id_nota_entrada), ADD CONSTRAINT FKContas_Pag622742 FOREIGN KEY (id_nota_entrada) REFERENCES Nota_entrada (id_nota_entrada);
ALTER TABLE Cartao ADD INDEX FKCartao836563 (id_banco), ADD CONSTRAINT FKCartao836563 FOREIGN KEY (id_banco) REFERENCES Banco (id_banco);
ALTER TABLE Comunicado_Impl_Item ADD INDEX FKComunicado190282 (id_servico_resumido), ADD CONSTRAINT FKComunicado190282 FOREIGN KEY (id_servico_resumido) REFERENCES Servico_Resumido (id_servico_resumido);
ALTER TABLE Cliente ADD INDEX FKCliente544579 (id_cidade), ADD CONSTRAINT FKCliente544579 FOREIGN KEY (id_cidade) REFERENCES Cidade (id_cidade);
ALTER TABLE Cliente_Responsavel ADD INDEX FKCliente_Re195604 (id_cliente), ADD CONSTRAINT FKCliente_Re195604 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Parametrizacao ADD INDEX FKParametriz282702 (id_empresa), ADD CONSTRAINT FKParametriz282702 FOREIGN KEY (id_empresa) REFERENCES Empresa (id_empresa);
ALTER TABLE Comunicado_Implantacao ADD INDEX FKComunicado674899 (id_usuario), ADD CONSTRAINT FKComunicado674899 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Comunicado_Encerramento ADD INDEX FKComunicado455150 (id_usuario), ADD CONSTRAINT FKComunicado455150 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Negocio ADD INDEX FKNegocio715212 (id_usuario), ADD CONSTRAINT FKNegocio715212 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Contrato ADD INDEX FKContrato474303 (id_usuario), ADD CONSTRAINT FKContrato474303 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Colaborador ADD INDEX FKColaborado458901 (id_usuario), ADD CONSTRAINT FKColaborado458901 FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario);
ALTER TABLE Colaborador ADD INDEX FKColaborado970203 (id_cargo), ADD CONSTRAINT FKColaborado970203 FOREIGN KEY (id_cargo) REFERENCES Cargo (id_cargo);
ALTER TABLE Comunicado_Impl_Financeiro ADD INDEX FKComunicado948842 (id_contrato), ADD CONSTRAINT FKComunicado948842 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Comunicado_Encerramento ADD INDEX FKComunicado804557 (id_contrato), ADD CONSTRAINT FKComunicado804557 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Comunicado_Enc_Financeiro ADD INDEX FKComunicado168261 (id_comunicado_encerramento), ADD CONSTRAINT FKComunicado168261 FOREIGN KEY (id_comunicado_encerramento) REFERENCES Comunicado_Encerramento (id_comunicado_encerramento);
ALTER TABLE Comunicado_Impl_Financeiro ADD INDEX FKComunicado656126 (id_comunicado_implantacao), ADD CONSTRAINT FKComunicado656126 FOREIGN KEY (id_comunicado_implantacao) REFERENCES Comunicado_Implantacao (id_comunicado_implantacao);
ALTER TABLE Comunicado_Impl_Item ADD INDEX FKComunicado609699 (id_cargo), ADD CONSTRAINT FKComunicado609699 FOREIGN KEY (id_cargo) REFERENCES Cargo (id_cargo);
ALTER TABLE Contrato ADD INDEX FKContrato951391 (id_categoria), ADD CONSTRAINT FKContrato951391 FOREIGN KEY (id_categoria) REFERENCES Categoria (id_categoria);
ALTER TABLE Comunicado_Enc_Item ADD INDEX FKComunicado302333 (id_comunicado_encerramento), ADD CONSTRAINT FKComunicado302333 FOREIGN KEY (id_comunicado_encerramento) REFERENCES Comunicado_Encerramento (id_comunicado_encerramento);
ALTER TABLE Comunicado_Impl_Item ADD INDEX FKComunicado783242 (id_comunicado_implantacao), ADD CONSTRAINT FKComunicado783242 FOREIGN KEY (id_comunicado_implantacao) REFERENCES Comunicado_Implantacao (id_comunicado_implantacao);
ALTER TABLE Comunicado_Implantacao ADD INDEX FKComunicado996906 (id_cliente), ADD CONSTRAINT FKComunicado996906 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Negocio ADD INDEX FKNegocio43406 (id_cliente), ADD CONSTRAINT FKNegocio43406 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Negocio ADD INDEX FKNegocio64620 (id_contrato), ADD CONSTRAINT FKNegocio64620 FOREIGN KEY (id_contrato) REFERENCES Contrato (id_contrato);
ALTER TABLE Contrato ADD INDEX FKContrato427997 (id_forma_pagamento), ADD CONSTRAINT FKContrato427997 FOREIGN KEY (id_forma_pagamento) REFERENCES Forma_Pagamento (id_forma_pagamento);
ALTER TABLE Contrato ADD INDEX FKContrato825480 (id_cliente), ADD CONSTRAINT FKContrato825480 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Contrato ADD INDEX FKContrato159217 (id_proposta), ADD CONSTRAINT FKContrato159217 FOREIGN KEY (id_proposta) REFERENCES Proposta (id_proposta);
ALTER TABLE Nota_Saida ADD INDEX FKNota_Saida783014 (id_servico), ADD CONSTRAINT FKNota_Saida783014 FOREIGN KEY (id_servico) REFERENCES Servico (id_servico);
ALTER TABLE aliquota_servico ADD INDEX FKaliquota_s567779 (id_servico), ADD CONSTRAINT FKaliquota_s567779 FOREIGN KEY (id_servico) REFERENCES Servico (id_servico);
ALTER TABLE aliquota_servico ADD INDEX FKaliquota_s11066 (id_cidade), ADD CONSTRAINT FKaliquota_s11066 FOREIGN KEY (id_cidade) REFERENCES Cidade (id_cidade);
ALTER TABLE Nota_entrada ADD INDEX FKNota_entra471119 (id_nota_observacao), ADD CONSTRAINT FKNota_entra471119 FOREIGN KEY (id_nota_observacao) REFERENCES Nota_observacao (id_nota_observacao);
ALTER TABLE Nota_Saida ADD INDEX FKNota_Saida957818 (id_nota_observacao), ADD CONSTRAINT FKNota_Saida957818 FOREIGN KEY (id_nota_observacao) REFERENCES Nota_observacao (id_nota_observacao);
ALTER TABLE Centro_Custo ADD INDEX FKCentro_Cus126394 (id_cliente), ADD CONSTRAINT FKCentro_Cus126394 FOREIGN KEY (id_cliente) REFERENCES Cliente (id_cliente);
ALTER TABLE Nota_Saida_Item ADD INDEX FKNota_Saida284139 (id_produto), ADD CONSTRAINT FKNota_Saida284139 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Nota_Saida_Item ADD INDEX FKNota_Saida160859 (id_nota_saida), ADD CONSTRAINT FKNota_Saida160859 FOREIGN KEY (id_nota_saida) REFERENCES Nota_Saida (id_nota_saida);
ALTER TABLE Nota_Saida ADD INDEX FKNota_Saida909572 (id_forma_pagamento), ADD CONSTRAINT FKNota_Saida909572 FOREIGN KEY (id_forma_pagamento) REFERENCES Forma_Pagamento (id_forma_pagamento);
ALTER TABLE Nota_Saida ADD INDEX FKNota_Saida592076 (id_CFOP), ADD CONSTRAINT FKNota_Saida592076 FOREIGN KEY (id_CFOP) REFERENCES CFOP (ID_CFOP);
ALTER TABLE Nota_Saida ADD INDEX FKNota_Saida215686 (id_modelo), ADD CONSTRAINT FKNota_Saida215686 FOREIGN KEY (id_modelo) REFERENCES Modelo_NFE (id_modelo);
ALTER TABLE Nota_Saida_Item ADD INDEX FKNota_Saida368195 (id_CFOP), ADD CONSTRAINT FKNota_Saida368195 FOREIGN KEY (id_CFOP) REFERENCES CFOP (ID_CFOP);
ALTER TABLE Nota_entrada_Item ADD INDEX FKNota_entra20649 (id_CFOP), ADD CONSTRAINT FKNota_entra20649 FOREIGN KEY (id_CFOP) REFERENCES CFOP (ID_CFOP);
ALTER TABLE Produto ADD INDEX FKProduto198756 (id_unidade_entrada), ADD CONSTRAINT FKProduto198756 FOREIGN KEY (id_unidade_entrada) REFERENCES Unidade (id_unidade);
ALTER TABLE Produto ADD INDEX FKProduto724800 (id_NCM), ADD CONSTRAINT FKProduto724800 FOREIGN KEY (id_NCM) REFERENCES NCM (ID_NCM);
ALTER TABLE Nota_entrada_Item ADD INDEX FKNota_entra631685 (id_produto), ADD CONSTRAINT FKNota_entra631685 FOREIGN KEY (id_produto) REFERENCES Produto (id_produto);
ALTER TABLE Nota_entrada_Item ADD INDEX FKNota_entra423614 (id_nota_entrada), ADD CONSTRAINT FKNota_entra423614 FOREIGN KEY (id_nota_entrada) REFERENCES Nota_entrada (id_nota_entrada);
ALTER TABLE Cidade ADD INDEX FKCidade412353 (id_estado), ADD CONSTRAINT FKCidade412353 FOREIGN KEY (id_estado) REFERENCES Estado (id_estado);
ALTER TABLE Empresa ADD INDEX FKEmpresa34707 (id_estado), ADD CONSTRAINT FKEmpresa34707 FOREIGN KEY (id_estado) REFERENCES Estado (id_estado);
ALTER TABLE Empresa ADD INDEX FKEmpresa945441 (id_cidade), ADD CONSTRAINT FKEmpresa945441 FOREIGN KEY (id_cidade) REFERENCES Cidade (id_cidade);
ALTER TABLE Transportadora ADD INDEX FKTransporta347244 (id_estado), ADD CONSTRAINT FKTransporta347244 FOREIGN KEY (id_estado) REFERENCES Estado (id_estado);
ALTER TABLE Transportadora ADD INDEX FKTransporta436509 (id_cidade), ADD CONSTRAINT FKTransporta436509 FOREIGN KEY (id_cidade) REFERENCES Cidade (id_cidade);
ALTER TABLE Nota_entrada ADD INDEX FKNota_entra326966 (id_modelo), ADD CONSTRAINT FKNota_entra326966 FOREIGN KEY (id_modelo) REFERENCES Modelo_NFE (id_modelo);
ALTER TABLE Nota_entrada ADD INDEX FKNota_entra633079 (id_forma_pagamento), ADD CONSTRAINT FKNota_entra633079 FOREIGN KEY (id_forma_pagamento) REFERENCES Forma_Pagamento (id_forma_pagamento);
ALTER TABLE Nota_entrada ADD INDEX FKNota_entra836861 (id_CFOP), ADD CONSTRAINT FKNota_entra836861 FOREIGN KEY (id_CFOP) REFERENCES CFOP (ID_CFOP);
ALTER TABLE Planilha_Custo_Item ADD INDEX FKPlanilha_C570266 (id_Planilha_custo), ADD CONSTRAINT FKPlanilha_C570266 FOREIGN KEY (id_Planilha_custo) REFERENCES Planilha_Custos (id_Planilha_custo);
ALTER TABLE Proposta_Planilha ADD INDEX FKProposta_P343805 (id_Planilha_custo), ADD CONSTRAINT FKProposta_P343805 FOREIGN KEY (id_Planilha_custo) REFERENCES Planilha_Custos (id_Planilha_custo);
ALTER TABLE Proposta_Planilha ADD INDEX FKProposta_P743137 (id_proposta_item), ADD CONSTRAINT FKProposta_P743137 FOREIGN KEY (id_proposta_item) REFERENCES Proposta_Item (id_proposta_item);
ALTER TABLE Proposta_Item ADD INDEX FKProposta_I811884 (id_proposta), ADD CONSTRAINT FKProposta_I811884 FOREIGN KEY (id_proposta) REFERENCES Proposta (id_proposta);

INSERT INTO EMPRESA (ID_EMPRESA, NOME_FANTASIA, RAZAO_SOCIAL) VALUES ('1', 'Empresa Demonstrativa', 'ED LTDA');
INSERT INTO PERFIL (ID_PERFIL, ROLE, DESCRICAO) VALUES ('1', 'ROLE_ADM', 'ADMINISTRADOR');
INSERT INTO USUARIO(ID_USUARIO, ID_PERFIL, ID_EMPRESA, NOME, LOGIN, SENHA, INATIVO) VALUES ('1', '1', '1', 'Renato Gonçalves', 'renato', '123', '1');
